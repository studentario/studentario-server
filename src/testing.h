#ifndef TESTING_H
#define TESTING_H

#include "database_controller.h"

using namespace Cutelyst;

class Testing: public DatabaseController
{
    Q_OBJECT
public:
    explicit Testing(QObject *parent = nullptr);
    ~Testing();

    C_ATTR(wipeDb, :Local)
    void wipeDb(Context *c);

    C_ATTR(createMaster, :Local)
    void createMaster(Context *c);

    C_ATTR(quit, :Local)
    void quit(Context *c);
};

#endif // TESTING_H
