#ifndef STATUS_CHANGES_H
#define STATUS_CHANGES_H

#include "database_controller.h"

#include "Core/status_changes.h"

using namespace Cutelyst;

class StatusChanges: public DatabaseController
{
    Q_OBJECT
public:
    explicit StatusChanges(QObject *parent = nullptr);
    ~StatusChanges();

    C_ATTR(index, :Path :AutoArgs :ActionClass(REST))
    void index(Context *) {}

    C_ATTR(index_GET, :Private)
    void index_GET(Context *c);

    C_ATTR(statuschange, :Path :CaptureArgs(1) :Object("statuschange") :ActionClass(REST))
    void statuschange(Context *c, const QString &changeId);

    C_ATTR(statuschange_DELETE, :Private)
    void statuschange_DELETE(Context *c, const QString &changeId);

protected:
    bool postFork(Cutelyst::Application *app) override;

private:
    int changeIdFromString(Context *c, const QString &changeId);

private:
    Core::StatusChanges m_statusChanges;
};

#endif // STATUS_CHANGES_H
