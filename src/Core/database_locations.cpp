#include "database_p.h"

#include "utils.h"

using namespace Core;

const QString locationSelect = QStringLiteral(
    "SELECT DISTINCT l.id,"
    " l.name, l.description, l.creatorId, l.created, l.color, l.icon "
    "FROM `Locations` l "
);

bool DatabasePrivate::locationFromQuery(const QSqlQuery &q, Location *location)
{
    int i = 0;
    location->id = q.value(i++).toInt();
    location->name = q.value(i++).toString();
    location->description = q.value(i++).toString();
    location->creatorId = q.value(i++).toInt();
    location->created = q.value(i++).toDateTime();
    location->color = Utils::stringFromColor(q.value(i++));
    location->icon = q.value(i++).toString();
    return true;
}

bool DatabasePrivate::createTableLocations()
{
    QSqlDatabase &db = this->db();

    // Pro le calendario
    db.exec("CREATE TABLE Locations ("
            + autoIncrement("id") + ","
            " name VARCHAR(80) NOT NULL,"
            " description VARCHAR(2048),"
            " creatorId INTEGER DEFAULT NULL,"
            " created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,"
            " color INTEGER,"
            " icon VARCHAR(80),"
            " PRIMARY KEY(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE LocationTags ("
            " locationId INTEGER NOT NULL,"
            " tagId INTEGER NOT NULL,"
            " PRIMARY KEY(locationId, tagId),"
            " UNIQUE(tagId, locationId),"
            " FOREIGN KEY(locationId) REFERENCES Locations(id),"
            " FOREIGN KEY(tagId) REFERENCES Tags(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    return true;
}

int Database::addLocation(const Location &location)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare("INSERT INTO `Locations` ("
              " name, description, creatorId, created,"
              " color, icon) "
              "VALUES ("
              " ?, ?, ?, ?, ?, ?)");
    q.addBindValue(location.name);
    q.addBindValue(location.description);
    q.addBindValue(location.creatorId);
    q.addBindValue(location.created.isValid() ?
                   location.created : QDateTime::currentDateTime());
    q.addBindValue(Utils::stringToColor(location.color));
    q.addBindValue(location.icon);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "addLocation failed:" << q.lastError();
        return -1;
    }

    return q.lastInsertId().toInt();
}

Error Database::updateLocation(const Location &location, Location::Fields fields)
{
    Q_D(Database);

    QStringList assignments;
    if (fields & Location::Field::Name) {
        assignments.append("name = :name");
    }
    if (fields & Location::Field::Description) {
        assignments.append("description = :description");
    }
    if (fields & Location::Field::CreatorId) {
        assignments.append("creatorId = :creatorId");
    }
    if (fields & Location::Field::Color) {
        assignments.append("color = :color");
    }
    if (fields & Location::Field::Icon) {
        assignments.append("icon = :icon");
    }

    if (Q_UNLIKELY(assignments.isEmpty())) {
        qWarning() << "Nothing to update";
        return Error::NoError;
    }

    QString query = "UPDATE `Locations` SET " +
        assignments.join(", ") +
        " WHERE id = :id";
    QSqlQuery q(d->db());
    bool ok = q.prepare(query);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error preparing query" << q.lastError();
        return Error::DatabaseError;
    }

    q.bindValue(":id", location.id);

    q.bindValue(":name", location.name);
    q.bindValue(":description", location.description);
    q.bindValue(":creatorId", location.creatorId);
    q.bindValue(":color", Utils::stringToColor(location.color));
    q.bindValue(":icon", location.icon);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "updateLocation failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return Error::NoError;
}

Error Database::deleteLocation(int id)
{
    Q_D(Database);

    QSqlQuery q(d->db());
    q.prepare("DELETE FROM `Locations` WHERE id = :id");
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "deleteLocation failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return q.numRowsAffected() == 0 ? Error::LocationNotFound : Error::NoError;
}

bool Database::location(int id, Location *location) const
{
    Q_D(const Database);

    QSqlQuery q =
        d->prepareQuery(locationSelect + QStringLiteral("WHERE l.id = :id"));
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    if (Q_UNLIKELY(!q.next())) {
        qDebug() << "Location not found" << id;
        return false;
    }
    return d->locationFromQuery(q, location);
}

QVector<Location> Database::locations() const
{
    Q_D(const Database);

    QVector<Location> locations;

    QSqlQuery q(d->db());
    QString query = locationSelect;
    query += QStringLiteral(" ORDER BY l.name");
    bool ok = q.prepare(query);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error preparing query" << q.lastError();
        return locations;
    }

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError() << q.executedQuery();
        return locations;
    }

    while (q.next()) {
        locations.append(Location());
        d->locationFromQuery(q, &locations.last());
    }
    return locations;
}
