#ifndef STUDENTARIO_CORE_PERMISSIONS_H
#define STUDENTARIO_CORE_PERMISSIONS_H

#include "user.h"

#include <QScopedPointer>

namespace Core {

class PermissionsPrivate;
class Permissions {
public:
    using Role = User::Role;
    using Roles = User::Roles;

    enum class Action {
        CreateUser = 0,
        EditUser,
        ViewUser,
        ResetUserPassword,
        DeleteUser,

        // actiones super gruppos
        CreateGroup,
        EditGroup,
        DeleteGroup,
        ViewGroupMembers,

        // actiones super lectiones
        CreateLesson,
        EditLesson,
        ViewLesson,
        DeleteLesson,
        ViewLessonMembers,
        EditAttendance,

        // Actiones super locationes
        EditLocation, // create e delete es equal

        // actiones super etiquettas
        ViewTag,
        EditTag, // create e delete es equal
        AttachTag,
    };

    Permissions();
    ~Permissions();

    bool can(Roles roles, Action action) const;
    bool canManageUser(Roles roles, Action action, Roles user) const;

private:
    Q_DECLARE_PRIVATE(Permissions)
    QScopedPointer<PermissionsPrivate> d_ptr;
};

} // namespace

#endif // STUDENTARIO_CORE_PERMISSIONS_H
