#include "lessons.h"

#include "database.h"
#include "groups.h"
#include "users.h"

#include <QDateTime>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonValue>
#include <algorithm>
#include <functional>

using namespace Core;

namespace Core {

using F = Lessons::Fields;
const QString F::LessonId = QStringLiteral("lessonId");
const QString F::CreatorId = QStringLiteral("creatorId");
const QString F::Created = QStringLiteral("created");
const QString F::StartTime = QStringLiteral("startTime");
const QString F::EndTime = QStringLiteral("endTime");
const QString F::LocationId = QStringLiteral("locationId");

const QString F::Groups = QStringLiteral("groups");
const QString F::Students = QStringLiteral("students");
const QString F::Teachers = QStringLiteral("teachers");
const QString F::Modified = QStringLiteral("modified");
const QString F::Attended = QStringLiteral("attended");
const QString F::Invited = QStringLiteral("invited");
const QString F::Note = QStringLiteral("note");

const QString F::Since = QStringLiteral("since");
const QString F::To = QStringLiteral("to");

class LessonsPrivate {
public:
    LessonsPrivate();

private:
    friend class Lessons;
    Database *m_db;
};

} // namespace

static QDateTime timeValue(const QJsonValue &v)
{
    return QDateTime::fromString(v.toString(), Qt::ISODate);
}

static QDateTime timeField(const QJsonObject &o, const QString &field)
{
    return QDateTime::fromString(o.value(field).toString(), Qt::ISODate);
}

static Error inviteesFromJson(const QJsonArray &invitees, QVector<int> *ids)
{
    for (const QJsonValue &v: invitees) {
        int id = v.toInt();
        if (Q_LIKELY(id > 0)) {
            ids->append(id);
        } else {
            return Error(Error::InvalidParameters,
                         "Not an integer: " + QJsonDocument(invitees).toJson());
        }
    }
    return Error::NoError;
}

using OnInviteeListCb = std::function<Error(EntityType, const QVector<int>)>;
static Error onInviteeList(const QJsonObject &invitees,
                           OnInviteeListCb callback)
{
    using F = Lessons::Fields;
    for (auto i = invitees.begin(); i != invitees.end(); i++) {
        EntityType entityType;
        if (i.key() == F::Groups) {
            entityType = EntityType::Group;
        } else if (i.key() == F::Students) {
            entityType = EntityType::Student;
        } else if (i.key() == F::Teachers) {
            entityType = EntityType::Teacher;
        } else {
            return Error(Error::InvalidParameters,
                         "Unexpected field " + i.key());
        }
        QVector<int> entityIds;
        Error error = inviteesFromJson(i.value().toArray(), &entityIds);
        if (Q_UNLIKELY(error)) return error;
        error = callback(entityType, entityIds);
        if (Q_UNLIKELY(error)) return error;

    }
    return Error::NoError;
}

static bool appendInvitees(Database *db, int groupId, User::Role role,
                           int lessonId,
                           QVector<DetailedParticipation> *participations,
                           QVector<int> *visitedUsers)
{
    Database::Users users;
    bool ok = db->usersForGroup(groupId, &users, role);
    if (Q_UNLIKELY(!ok)) return false;

    EntityType entityType = (role == User::Role::Student) ?
        EntityType::Student : EntityType::Teacher;
    for (const User &u: users) {
        if (visitedUsers->contains(u.id)) continue;
        participations->append({
            Participation{
                -1, // id
                -1, // creatorId
                QDateTime(),
                lessonId,
                { u.id, entityType },
                false, // invited
                false, // attended
                QString(), // note
            }, u
        });
        visitedUsers->append(u.id);
    }
    return true;
}

static Error participationsFromJson(const QJsonArray &jsonParticipations,
                                    EntityType type,
                                    QVector<ParticipationUpdate> *participations)
{
    using F = Lessons::Fields;
    for (const QJsonValue &v: jsonParticipations) {
        const QJsonObject o = v.toObject();
        ParticipationUpdate p;
        for (auto i = o.begin(); i != o.end(); i++) {
            p.p.participant.type = type;
            if ((type == EntityType::Student || type == EntityType::Teacher) &&
                i.key() == Users::Fields::UserId) {
                p.p.participant.id = i.value().toInt();
                p.fields |= Participation::Field::ParticipantId;
            } else if (i.key() == Groups::Fields::GroupId) {
                p.p.participant.id = i.value().toInt();
                p.fields |= Participation::Field::ParticipantId;
            } else if (i.key() == F::Attended) {
                p.p.attended = i.value().toBool();
                p.fields |= Participation::Field::Attended;
            } else if (i.key() == F::Invited) {
                p.p.invited = i.value().toBool();
                p.fields |= Participation::Field::Invited;
            } else if (i.key() == F::Note) {
                p.p.note = i.value().toString();
                p.fields |= Participation::Field::Note;
            } else {
                return Error(Error::InvalidParameters,
                             "Cannot update attendance field " + i.key());
            }
        }
        if (Q_UNLIKELY(!(p.fields & Participation::Field::ParticipantId))) {
            return Error(Error::InvalidParameters, "Missing participant ID");
        }
        Participation::Fields dataFields =
            p.fields ^ Participation::Field::ParticipantId;
        if (Q_UNLIKELY(!dataFields)) {
            return Error(Error::InvalidParameters,
                         "Nothing to update in attendance");
        }
        participations->append(p);
    }
    return Error::NoError;
}

LessonsPrivate::LessonsPrivate():
    m_db(nullptr)
{
}

Lessons::Lessons():
    d_ptr(new LessonsPrivate())
{
}

Lessons::~Lessons() = default;

void Lessons::setDatabase(Database *db)
{
    Q_D(Lessons);
    d->m_db = db;
}

Error Lessons::addLesson(const QJsonObject &lesson, int *lessonId)
{
    Q_D(Lessons);

    QDateTime startTime = timeField(lesson, Fields::StartTime);
    QDateTime endTime = timeField(lesson, Fields::EndTime);
    if (Q_UNLIKELY(!(startTime.isValid() && endTime.isValid()))) {
        return Error(Error::InvalidParameters, "Lesson times are invalid");
    }

    Lesson l = {
        0, // id
        lesson.value(Fields::CreatorId).toInt(-1),
        QDateTime::currentDateTime(),
        startTime,
        endTime,
        lesson.value(Fields::LocationId).toInt(-1),
    };

    *lessonId = d->m_db->addLesson(l);
    return *lessonId >= 0 ? Error::NoError : Error::DatabaseError;
}

Error Lessons::updateLesson(const Lesson &lesson, const QJsonObject &changes)
{
    Q_D(Lessons);

    using F = Lessons::Fields;
    Lesson l = lesson;
    Lesson::Fields fields;

    for (auto i = changes.begin(); i != changes.end(); i++) {
        if (i.key() == F::StartTime) {
            l.startTime = timeValue(i.value());
            if (Q_UNLIKELY(!l.startTime.isValid())) {
                return Error(Error::InvalidParameters,
                             "Lesson start time is invalid");
            }
            fields |= Lesson::Field::StartTime;
        } else if (i.key() == F::EndTime) {
            l.endTime = timeValue(i.value());
            if (Q_UNLIKELY(!l.endTime.isValid())) {
                return Error(Error::InvalidParameters,
                             "Lesson end time is invalid");
            }
            fields |= Lesson::Field::EndTime;
        } else if (i.key() == F::LocationId) {
            l.locationId = i.value().toInt();
            fields |= Lesson::Field::LocationId;
        } else {
            return Error(Error::InvalidParameters,
                         "Cannot update field " + i.key());
        }
    }

    return d->m_db->updateLesson(l, fields);
}

Error Lessons::deleteLesson(int lessonId)
{
    Q_D(Lessons);
    return d->m_db->deleteLesson(lessonId);
}

Error Lessons::lesson(int id, Lesson *g) const
{
    Q_D(const Lessons);
    return d->m_db->lesson(id, g) ? Error::NoError : Error::LessonNotFound;
}

QVector<Lesson> Lessons::lessons(const QJsonObject &filters) const
{
    Q_D(const Lessons);
    /* Si le filtros es absente, le QDateTime essera invalide, e alora le
     * Database los ignorara. */
    QDateTime since = timeField(filters, Fields::Since);
    QDateTime to = timeField(filters, Fields::To);
    int locationId = filters.value(Fields::LocationId).toInt(-1);
    return d->m_db->lessons(since, to, locationId);
}

Error Lessons::inviteToLesson(int creatorId, int lessonId,
                              const QJsonObject &invitees)
{
    Q_D(Lessons);
    Error error = onInviteeList(invitees, [d,creatorId,lessonId]
                         (EntityType entityType, QVector<int> entityIds) {
        bool ok = d->m_db->inviteToLesson(creatorId, lessonId,
                                          entityIds, entityType);
        return ok ? Error::NoError : Error::DatabaseError;
    });
    return error;
}

Error Lessons::uninviteFromLesson(int lessonId, const QJsonObject &invitees)
{
    Q_D(Lessons);
    return onInviteeList(invitees, [d,lessonId]
                         (EntityType entityType, QVector<int> entityIds) {
        bool ok = d->m_db->uninviteFromLesson(lessonId, entityIds, entityType);
        return ok ? Error::NoError : Error::DatabaseError;
    });
}

Error Lessons::lessonsByParticipants(const Participants &participants,
                                     QVector<Lesson> *lessons,
                                     const QJsonObject &filters) const
{
    Q_D(const Lessons);

    QDateTime since = timeField(filters, Fields::Since);
    QDateTime to = timeField(filters, Fields::To);

    QVector<Lesson> tmpLessons;
    bool ok = d->m_db->lessonsByParticipants(participants, &tmpLessons,
                                             since, to);
    if (Q_UNLIKELY(!ok)) return Error::DatabaseError;

    *lessons = tmpLessons;
    return Error::NoError;
}

Error Lessons::updateAttendance(int lessonId, const QJsonObject &participations)
{
    Q_D(Lessons);

    QVector<ParticipationUpdate> updates;
    for (auto i = participations.begin(); i != participations.end(); i++) {
        using F = Lessons::Fields;
        EntityType entityType;
        if (i.key() == F::Students) {
            entityType = EntityType::Student;
        } else if (i.key() == F::Teachers) {
            entityType = EntityType::Teacher;
        } else {
            return Error(Error::InvalidParameters,
                         "Unexpected field " + i.key());
        }
        Error error = participationsFromJson(i.value().toArray(), entityType,
                                             &updates);
        if (Q_UNLIKELY(error)) return error;
    }

    QVector<Participation> olds;
    bool ok = d->m_db->lessonAttendance(lessonId, &olds);
    if (Q_UNLIKELY(!ok)) return Error::DatabaseError;

    QVector<Participation> newParticipations;
    for (const ParticipationUpdate &update: updates) {
        // Controla si isto es un actualisation
        auto old = std::find_if(olds.begin(), olds.end(),
                                [&update](const Participation &p) {
            return p.participant == update.p.participant;
        });
        if (old != olds.end()) { // actualisation
            using F = Participation::Field;
            Participation p = *old;
            // adde le cambios
            if (update.fields & F::CreatorId) p.creatorId = update.p.creatorId;
            if (update.fields & F::Invited) p.invited = update.p.invited;
            if (update.fields & F::Attended) p.attended = update.p.attended;
            if (update.fields & F::Note) p.note = update.p.note;
            newParticipations.append(p);
        } else { // nove participation
            newParticipations.append(update.p);
        }
    }

    ok = d->m_db->updateAttendance(lessonId, newParticipations);
    return ok ? Error::NoError : Error::DatabaseError;
}

Error Lessons::lessonAttendance(int lessonId,
                                QVector<DetailedParticipation> *participations,
                                const QJsonObject &filters) const
{
    Q_D(const Lessons);
    if (!filters.isEmpty()) {
        return Error(Error::InvalidParameters,
                     "Unsupported filters: " + filters.keys().join(", "));
    }
    QVector<Participation> dbParticipations;
    bool ok = d->m_db->lessonAttendance(lessonId, &dbParticipations);
    if (Q_UNLIKELY(!ok)) return Error::DatabaseError;

    QVector<int> visitedUsers;
    QVector<int> visitedGroups;
    for (const Participation &dbParticipation: dbParticipations) {
        DetailedParticipation p(&dbParticipation);
        if (p.participant.type == EntityType::Group) {
            p.details.emplace<Group>();
            ok = d->m_db->group(p.participant.id, &std::get<Group>(p.details));
            visitedGroups.append(p.participant.id);
        } else {
            p.details.emplace<User>();
            ok = d->m_db->user(p.participant.id, &std::get<User>(p.details));
            visitedUsers.append(p.participant.id);
        }
        if (Q_UNLIKELY(!ok)) return Error::DatabaseError;
        participations->append(p);
    }

    /* Ora expande le gruppos, ma non adde usatores duplicate */
    for (int groupId: visitedGroups) {
        appendInvitees(d->m_db, groupId, User::Role::Student, lessonId,
                       participations, &visitedUsers);
        appendInvitees(d->m_db, groupId, User::Role::Teacher, lessonId,
                       participations, &visitedUsers);
    }

    return Error::NoError;
}

QJsonObject Lessons::jsonFromDb(const Lesson &l)
{
    using F = Lessons::Fields;

    QJsonObject ret = {
        { F::LessonId, l.id },
        { F::CreatorId, l.creatorId },
        { F::Created, l.created.toString(Qt::ISODate) },
        { F::StartTime, l.startTime.toString(Qt::ISODate) },
        { F::EndTime, l.endTime.toString(Qt::ISODate) },
        { F::LocationId, l.locationId },
    };
    return ret;
}

QJsonArray Lessons::jsonFromDb(const QVector<Lesson> &lessons)
{
    QJsonArray ret;
    for (const Lesson &g: lessons) {
        ret.append(jsonFromDb(g));
    }
    return ret;
}

QJsonObject
Lessons::jsonFromDb(const Participation &p)
{
    using F = Lessons::Fields;

    QJsonObject ret = {
        // Le id del lection e del participation non es necessari
        { F::CreatorId, p.creatorId },
        { F::Modified, p.modified.toString(Qt::ISODate) },
        // Nos non include le participante, perque nos sape que illo es addite
        // in DetailedParticipation
        { F::Invited, p.invited },
        { F::Attended, p.attended },
        { F::Note, p.note },
    };

    return ret;
}

QJsonObject
Lessons::jsonFromDb(const QVector<DetailedParticipation> &participations,
                    const QStringList &userFields)
{
    QJsonObject ret;
    QJsonArray groups, students, teachers;

    Users::FieldSet fieldSet = Users::fieldsFromNames(userFields);

    for (const Core::DetailedParticipation &p: participations) {
        QJsonObject o = jsonFromDb(p);
        if (p.participant.type == EntityType::Group) {
            o["group"] = Groups::jsonFromDb(std::get<Group>(p.details));
            groups.append(o);
        } else {
            o["user"] = Users::jsonFromDb(std::get<User>(p.details), fieldSet);
            if (p.participant.type == EntityType::Student) {
                students.append(o);
            } else {
                Q_ASSERT(p.participant.type == EntityType::Teacher);
                teachers.append(o);
            }
        }
    }
    ret.insert(F::Groups, groups);
    ret.insert(F::Students, students);
    ret.insert(F::Teachers, teachers);
    return ret;
}
