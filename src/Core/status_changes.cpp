#include "status_changes.h"

#include "database.h"
#include "groups.h"
#include "users.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonValue>

using namespace Core;

namespace Core {

using F = StatusChanges::Fields;
const QString F::ChangeId = QStringLiteral("changeId");
const QString F::CreatorId = QStringLiteral("creatorId");
const QString F::Modified = QStringLiteral("modified");
const QString F::UserId = QStringLiteral("userId");
const QString F::Status = QStringLiteral("status");
const QString F::Time = QStringLiteral("time");
const QString F::Reason = QStringLiteral("reason");
const QString F::Since = QStringLiteral("since");
const QString F::To = QStringLiteral("to");
const QString F::User = QStringLiteral("user");
const QString F::Groups = QStringLiteral("groups");
const QString F::Teachers = QStringLiteral("teachers");

class StatusChangesPrivate {
public:
    StatusChangesPrivate();

private:
    friend class StatusChanges;
    Database *m_db;
};

} // namespace

static QDateTime timeField(const QJsonObject &o, const QString &field)
{
    return QDateTime::fromString(o.value(field).toString(), Qt::ISODate);
}

StatusChangesPrivate::StatusChangesPrivate():
    m_db(nullptr)
{
    qDebug() << Q_FUNC_INFO;
}

StatusChanges::StatusChanges():
    d_ptr(new StatusChangesPrivate())
{
    qDebug() << Q_FUNC_INFO;
}

StatusChanges::~StatusChanges() = default;

void StatusChanges::setDatabase(Database *db)
{
    qDebug() << Q_FUNC_INFO;
    Q_D(StatusChanges);
    d->m_db = db;
}

Error StatusChanges::addUserStatusChange(const QJsonObject &statusChange,
                                         int *changeId)
{
    qDebug() << Q_FUNC_INFO;
    Q_D(StatusChanges);

    StatusChange s;
    s.creatorId = statusChange.value(Fields::CreatorId).toInt();
    s.userId = statusChange.value(Fields::UserId).toInt();
    QString status = statusChange.value(Fields::Status).toString();
    s.status = Users::statusFromString(status);
    s.time = QDateTime::fromString(statusChange.value(Fields::Time).toString(),
                                   Qt::ISODate);
    s.reason = statusChange.value(Fields::Reason).toString();

    return d->m_db->addUserStatusChange(s, changeId);
}

Error StatusChanges::deleteUserStatusChange(int changeId)
{
    Q_D(StatusChanges);
    return d->m_db->deleteUserStatusChange(changeId);
}

QVector<StatusChange>
StatusChanges::userStatusChanges(const QJsonObject &filters) const
{
    qDebug() << Q_FUNC_INFO;
    Q_D(const StatusChanges);
    /* Si le filtros es absente, le QDateTime essera invalide, e alora le
     * Database los ignorara. */
    QDateTime since = timeField(filters, Fields::Since);
    QDateTime to = timeField(filters, Fields::To);
    int userId = filters.value(Fields::UserId).toInt(-1);
    return d->m_db->userStatusChanges(userId, since, to);
}

QJsonObject StatusChanges::jsonFromDb(const StatusChange &sc,
                                      Users::FieldSet userFields,
                                      Users::FieldSet teacherFields) const
{
    qDebug() << Q_FUNC_INFO;
    Q_D(const StatusChanges);

    QJsonObject ret;
    if (Q_UNLIKELY(sc.userId <= 0)) return ret;

    // TODO lege le status change
    ret = {
        { Fields::ChangeId, sc.id },
        { Fields::CreatorId, sc.creatorId },
        { Fields::Modified, sc.modified.toString(Qt::ISODate) },
        { Fields::UserId, sc.userId },
        { Fields::Status, Users::statusToString(sc.status) },
        { Fields::Time, sc.time.toString(Qt::ISODate) },
        { Fields::Reason, sc.reason },
    };

    User u;
    bool ok = d->m_db->user(sc.userId, &u);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error reading user for status change";
    }
    ret.insert(Fields::User, Users::jsonFromDb(u, userFields));

    QVector<Group> groups;
    ok = d->m_db->groupsForUser(sc.userId, &groups, User::Role::Student);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error reading groups for status change";
    }
    ret.insert(Fields::Groups, Groups::jsonFromDb(groups));

    /* Adde le inseniantes del gruppos: forsan nos deberea adder les in le
     * gruppo correspondente? */
    QVector<User> teachers;
    for (const Group &g: groups) {
        ok = d->m_db->usersForGroup(g.id, &teachers, User::Role::Teacher);
        if (Q_UNLIKELY(!ok)) {
            qWarning() << "Error reading teachers for status change";
        }
    }
    ret.insert(Fields::Teachers, Users::jsonFromDb(teachers, teacherFields));
    return ret;
}

QJsonArray
StatusChanges::jsonFromDb(const QVector<StatusChange> &statusChanges,
                          Users::FieldSet userFields,
                          Users::FieldSet teacherFields) const
{
    qDebug() << Q_FUNC_INFO;
    QJsonArray ret;
    for (const StatusChange &s: statusChanges) {
        ret.append(jsonFromDb(s, userFields, teacherFields));
    }
    return ret;
}
