#include "database_p.h"

#include "utils.h"

#include <QDebug>
#include <QHash>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QStringView>

using namespace Core;

static const QString tagSelect = QStringLiteral(
    "SELECT id,"
    " parentId, name, description, creatorId, created,"
    " color, icon "
    "FROM `Tags` "
);

static const QHash<Tag::Entity, TableInfo> s_tableInfoByType = {
    { Tag::Entity::Users, {
        QStringLiteral("UserTags"),
        QStringLiteral("userId"),
        userSelect,
        QStringLiteral(" ORDER BY name"),
    }},
    { Tag::Entity::Groups, {
        QStringLiteral("GroupTags"),
        QStringLiteral("groupId"),
        groupSelect,
        QStringLiteral(" ORDER BY name"),
    }},
    { Tag::Entity::Lessons, {
        QStringLiteral("LessonTags"),
        QStringLiteral("lessonId"),
        lessonSelect,
        QStringLiteral(" ORDER BY l.startTime"),
    }},
    { Tag::Entity::LessonNotes, {
        QStringLiteral("LessonNoteTags"),
        QStringLiteral("noteId"),
        lessonNoteSelect,
        QStringLiteral(" ORDER BY n.modified DESC"),
    }},
    { Tag::Entity::Locations, {
        QStringLiteral("LocationTags"),
        QStringLiteral("locationId"),
        locationSelect,
        QStringLiteral(" ORDER BY l.name"),
    }},
};

static const TableInfo &tableInfoForType(Tag::Entity type)
{
    const auto i = s_tableInfoByType.find(type);
    if (Q_UNLIKELY(i == s_tableInfoByType.end())) {
        static TableInfo nullTable;
        qCritical() << "Unknown entity type" << int(type);
        return nullTable;
    }
    return i.value();
}

static bool tagFromQuery(const QSqlQuery &q, Tag *tag)
{
    int i = 0;
    tag->id = q.value(i++).toInt();
    tag->parentId = q.value(i++).toInt();
    tag->name = q.value(i++).toString();
    tag->description = q.value(i++).toString();
    tag->creatorId = q.value(i++).toInt();
    tag->created = q.value(i++).toDateTime();
    tag->color = Utils::stringFromColor(q.value(i++));
    tag->icon = q.value(i++).toString();
    return true;
}

bool DatabasePrivate::tagExists(int tagId) const
{
    QString query = QStringLiteral("SELECT id FROM `Tags` WHERE id = %1").
        arg(tagId);

    QSqlQuery q(db());
    if (Q_UNLIKELY(!q.exec(query))) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    return q.next();
}

bool DatabasePrivate::tagHasChildren(int tagId) const
{
    QString query = QStringLiteral("SELECT id FROM `Tags` WHERE parentId = %1"
                                   " LIMIT 1").arg(tagId);
    QSqlQuery q(db());
    if (Q_UNLIKELY(!q.exec(query))) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    return q.next();
}

bool DatabasePrivate::createTableTags()
{
    QSqlDatabase &db = this->db();

    db.exec("CREATE TABLE Tags ("
            + autoIncrement("id") + ","
            " parentId INTEGER NOT NULL DEFAULT -1,"
            " name VARCHAR(128),"
            " description TEXT,"
            " color INTEGER,"
            " icon VARCHAR(128),"
            " created DATETIME NOT NULL,"
            " creatorId INTEGER DEFAULT NULL,"
            " PRIMARY KEY(id),"
            " UNIQUE(parentId, name)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    return true;
}

bool DatabasePrivate::createTableUserGroupTags()
{
    QSqlDatabase &db = this->db();

    db.exec("CREATE TABLE UserTags ("
            " userId INTEGER NOT NULL,"
            " tagId INTEGER NOT NULL,"
            " PRIMARY KEY(userId, tagId),"
            " UNIQUE(tagId, userId),"
            " FOREIGN KEY(userId) REFERENCES Users(id),"
            " FOREIGN KEY(tagId) REFERENCES Tags(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE GroupTags ("
            " groupId INTEGER NOT NULL,"
            " tagId INTEGER NOT NULL,"
            " PRIMARY KEY(groupId, tagId),"
            " UNIQUE(tagId, groupId),"
            " FOREIGN KEY(groupId) REFERENCES `Groups`(id),"
            " FOREIGN KEY(tagId) REFERENCES Tags(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    return true;
}

bool DatabasePrivate::addTagToIds(int tagId, const QVector<int> &entityIds,
                                  const TableInfo &table)
{
    QSqlQuery q(db());
    q.prepare(QString("REPLACE INTO %1 (tagId, %2) "
                      "VALUES (?, ?)").arg(table.table).arg(table.field));
    for (int entityId: entityIds) {
        q.addBindValue(tagId);
        q.addBindValue(entityId);
        if (Q_UNLIKELY(!q.exec())) {
            qWarning() << "Failed to run query" << q.lastError();
            return false;
        }
    }

    return true;
}

bool DatabasePrivate::removeTagFromIds(int tagId,
                                       const QVector<int> &entityIds,
                                       const TableInfo &table)
{
    QSqlQuery q(db());
    q.prepare(QString("DELETE FROM %1 "
                      "WHERE tagId = ? AND %2 = ?").
              arg(table.table).arg(table.field));
    for (int entityId: entityIds) {
        q.addBindValue(tagId);
        q.addBindValue(entityId);
        if (Q_UNLIKELY(!q.exec())) {
            qWarning() << "Failed to run query" << q.lastError();
            return false;
        }
    }

    return true;
}

bool DatabasePrivate::tagsForId(int entityId, Database::Tags *tags,
                                const TableInfo &table) const
{
    QString query = tagSelect + QStringLiteral(
        "INNER JOIN %1 u ON u.tagId = id "
        "WHERE u.%2 = :id "
        "ORDER BY name").arg(table.table).arg(table.field);

    QSqlQuery q(db());
    q.prepare(query);
    q.bindValue(":id", entityId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    while (q.next()) {
        tags->append(Tag());
        tagFromQuery(q, &tags->last());
    }
    return true;
}

QSqlQuery DatabasePrivate::entitiesForTag(int tagId,
                                          const TableInfo &table) const
{
    QString query = table.select + QStringLiteral(
        "INNER JOIN %1 u ON u.%2 = id "
        "WHERE u.tagId = :id"
        "%3").arg(table.table).arg(table.field).arg(table.orderBy);

    QSqlQuery q(db());
    q.prepare(query);
    q.bindValue(":id", tagId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError() << q.executedQuery();
    }

    return q;
}

Error Database::addTag(const Tag &tag, int *tagId)
{
    Q_D(Database);

    if (tag.parentId != -1) {
        if (Q_UNLIKELY(!d->tagExists(tag.parentId))) {
            return { Error::InvalidParameters, "Invalid parent tag" };
        }
    }

    QSqlQuery q(d->db());
    q.prepare("INSERT INTO `Tags` ("
              " parentId, name, description, creatorId, created,"
              " color, icon"
              ") VALUES ("
              " ?, ?, ?, ?, ?,"
              " ?, ?)");
    q.addBindValue(tag.parentId);
    q.addBindValue(tag.name);
    q.addBindValue(tag.description);
    q.addBindValue(tag.creatorId);
    q.addBindValue(tag.created);
    q.addBindValue(Utils::stringToColor(tag.color));
    q.addBindValue(tag.icon);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "addTag failed:" << q.lastError();
        return Error::DatabaseError;
    }

    if (tagId) {
        *tagId = q.lastInsertId().toInt();
    }
    return Error::NoError;
}

Error Database::updateTag(const Tag &tag, Tag::Fields fields)
{
    Q_D(Database);

    QStringList assignments;
    if (fields & Tag::Field::ParentId) {
        if (tag.parentId != -1) {
            if (Q_UNLIKELY(!d->tagExists(tag.parentId) ||
                           tag.parentId == tag.id)) {
                return { Error::InvalidParameters, "Invalid parent tag" };
            }
        }

        assignments.append("parentId = :parentId");
    }
    if (fields & Tag::Field::Name) {
        assignments.append("name = :name");
    }
    if (fields & Tag::Field::Description) {
        assignments.append("description = :description");
    }
    if (fields & Tag::Field::CreatorId) {
        assignments.append("creatorId = :creatorId");
    }
    if (fields & Tag::Field::Color) {
        assignments.append("color = :color");
    }
    if (fields & Tag::Field::Icon) {
        assignments.append("icon = :icon");
    }

    if (Q_UNLIKELY(assignments.isEmpty())) {
        qWarning() << "Nothing to update";
        return Error::NoError;
    }

    QString query = "UPDATE `Tags` SET " +
        assignments.join(", ") +
        " WHERE id = :id";
    QSqlQuery q(d->db());
    bool ok = q.prepare(query);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error preparing query" << q.lastError();
        return Error::DatabaseError;
    }

    q.bindValue(":id", tag.id);

    q.bindValue(":parentId", tag.parentId);
    q.bindValue(":name", tag.name);
    q.bindValue(":description", tag.description);
    q.bindValue(":creatorId", tag.creatorId);
    q.bindValue(":color", Utils::stringToColor(tag.color));
    q.bindValue(":icon", tag.icon);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "updateTag failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return Error::NoError;
}

Error Database::deleteTag(int id)
{
    Q_D(Database);

    if (Q_UNLIKELY(d->tagHasChildren(id))) {
        return { Error::InvalidParameters, "Cannot remove tag with children" };
    }

    QSqlQuery q(d->db());
    q.prepare("DELETE FROM `Tags` WHERE id = :id");
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "deleteTag failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return q.numRowsAffected() == 0 ? Error::TagNotFound : Error::NoError;
}

bool Database::tag(int id, Tag *tag) const
{
    Q_D(const Database);

    QSqlQuery q =
        d->prepareQuery(tagSelect + QStringLiteral("WHERE id = :id"));
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    if (Q_UNLIKELY(!q.next())) {
        qDebug() << "Tag not found" << id;
        return false;
    }
    return tagFromQuery(q, tag);
}

QVector<Tag> Database::tags(int parentId, const QString &pattern) const
{
    Q_D(const Database);

    QVector<Tag> tags;

    QSqlQuery q(d->db());
    QStringList conditions;
    if (parentId != 0) {
        conditions.append(QStringLiteral("parentId = :parentId"));
    }
    if (!pattern.isEmpty()) {
        QString safePattern = Utils::cleanPattern(pattern);
        conditions.append(QStringLiteral("name LIKE '%") + safePattern + "%'");
    }
    QString query = tagSelect;
    if (!conditions.isEmpty()) {
        query += QStringLiteral("WHERE ") + conditions.join(" AND ");
    }
    query += QStringLiteral(" ORDER BY name");

    q.prepare(query);
    q.bindValue(":parentId", parentId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return tags;
    }

    while (q.next()) {
        tags.append(Tag());
        tagFromQuery(q, &tags.last());
    }
    return tags;
}

bool Database::addTagToEntities(int tagId, Tag::Entity type, const QVector<int> &userIds)
{
    Q_D(Database);
    return d->addTagToIds(tagId, userIds, tableInfoForType(type));
}

bool Database::removeTagFromEntities(int tagId, Tag::Entity type, const QVector<int> &userIds)
{
    Q_D(Database);
    return d->removeTagFromIds(tagId, userIds, tableInfoForType(type));
}

bool Database::tagsForEntity(Tag::Entity type, int userId, Tags *tags) const
{
    Q_D(const Database);
    return d->tagsForId(userId, tags, tableInfoForType(type));
}

bool Database::addTagToUsers(int tagId, const QVector<int> &userIds)
{
    return addTagToEntities(tagId, Tag::Entity::Users, userIds);
}

bool Database::removeTagFromUsers(int tagId, const QVector<int> &userIds)
{
    return removeTagFromEntities(tagId, Tag::Entity::Users, userIds);
}

bool Database::tagsForUser(int userId, Tags *tags) const
{
    return tagsForEntity(Tag::Entity::Users, userId, tags);
}

bool Database::usersForTag(int tagId, Users *users) const
{
    Q_D(const Database);
    QSqlQuery q =
        d->entitiesForTag(tagId, tableInfoForType(Tag::Entity::Users));
    if (Q_UNLIKELY(!q.isActive())) return false;

    while (q.next()) {
        users->append(User());
        d->userFromQuery(q, &users->last());
    }
    return true;
}

bool Database::addTagToGroups(int tagId, const QVector<int> &groupIds)
{
    return addTagToEntities(tagId, Tag::Entity::Groups, groupIds);
}

bool Database::removeTagFromGroups(int tagId, const QVector<int> &groupIds)
{
    return removeTagFromEntities(tagId, Tag::Entity::Groups, groupIds);
}

bool Database::tagsForGroup(int groupId, Tags *tags) const
{
    return tagsForEntity(Tag::Entity::Groups, groupId, tags);
}

bool Database::groupsForTag(int tagId, Groups *groups) const
{
    Q_D(const Database);
    QSqlQuery q =
        d->entitiesForTag(tagId, tableInfoForType(Tag::Entity::Groups));
    if (Q_UNLIKELY(!q.isActive())) return false;

    while (q.next()) {
        groups->append(Group());
        d->groupFromQuery(q, &groups->last());
    }
    return true;
}

bool Database::addTagToLessons(int tagId, const QVector<int> &lessonIds)
{
    return addTagToEntities(tagId, Tag::Entity::Lessons, lessonIds);
}

bool Database::removeTagFromLessons(int tagId, const QVector<int> &lessonIds)
{
    return removeTagFromEntities(tagId, Tag::Entity::Lessons, lessonIds);
}

bool Database::tagsForLesson(int lessonId, Tags *tags) const
{
    return tagsForEntity(Tag::Entity::Lessons, lessonId, tags);
}

bool Database::lessonsForTag(int tagId, Lessons *lessons) const
{
    Q_D(const Database);
    QSqlQuery q =
        d->entitiesForTag(tagId, tableInfoForType(Tag::Entity::Lessons));
    if (Q_UNLIKELY(!q.isActive())) return false;

    while (q.next()) {
        lessons->append(Lesson());
        d->lessonFromQuery(q, &lessons->last());
    }
    return true;
}

bool Database::addTagToLessonNotes(int tagId, const QVector<int> &noteIds)
{
    return addTagToEntities(tagId, Tag::Entity::LessonNotes, noteIds);
}

bool Database::removeTagFromLessonNotes(int tagId, const QVector<int> &noteIds)
{
    return removeTagFromEntities(tagId, Tag::Entity::LessonNotes, noteIds);
}

bool Database::tagsForLessonNote(int lessonNoteId, Tags *tags) const
{
    return tagsForEntity(Tag::Entity::LessonNotes, lessonNoteId, tags);
}

bool Database::lessonNotesForTag(int tagId, LessonNotes *lessonNotes) const
{
    Q_D(const Database);
    QSqlQuery q =
        d->entitiesForTag(tagId, tableInfoForType(Tag::Entity::LessonNotes));
    if (Q_UNLIKELY(!q.isActive())) return false;

    while (q.next()) {
        lessonNotes->append(LessonNote());
        d->lessonNoteFromQuery(q, &lessonNotes->last());
    }
    return true;
}

bool Database::addTagToLocations(int tagId, const QVector<int> &locationIds)
{
    return addTagToEntities(tagId, Tag::Entity::Locations, locationIds);
}

bool Database::removeTagFromLocations(int tagId,
                                      const QVector<int> &locationIds)
{
    return removeTagFromEntities(tagId, Tag::Entity::Locations, locationIds);
}

bool Database::tagsForLocation(int locationId, Tags *tags) const
{
    return tagsForEntity(Tag::Entity::Locations, locationId, tags);
}

bool Database::locationsForTag(int tagId, Locations *locations) const
{
    Q_D(const Database);
    QSqlQuery q =
        d->entitiesForTag(tagId, tableInfoForType(Tag::Entity::Locations));
    if (Q_UNLIKELY(!q.isActive())) return false;

    while (q.next()) {
        locations->append(Location());
        d->locationFromQuery(q, &locations->last());
    }
    return true;
}
