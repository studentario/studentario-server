#ifndef STUDENTARIO_CORE_LOCATIONS_H
#define STUDENTARIO_CORE_LOCATIONS_H

#include "error.h"
#include "location.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QScopedPointer>
#include <QString>

namespace Core {

class Database;

class LocationsPrivate;
class Locations {
public:
    Locations();
    virtual ~Locations();

    void setDatabase(Database *db);

    Error addLocation(const QJsonObject &location, int *locationId);
    Error updateLocation(const Location &location, const QJsonObject &changes);
    Error deleteLocation(int locationId);
    Error location(int id, Location *location) const;
    QVector<Location> locations() const;

    static QJsonObject jsonFromDb(const Location &location);
    static QJsonArray jsonFromDb(const QVector<Location> &locations);

    struct Fields {
    static const QString LocationId;
    static const QString Name;
    static const QString Description;
    static const QString CreatorId;
    static const QString Created;
    static const QString Color;
    static const QString Icon;

    static const QString Location;
    };

private:
    Q_DECLARE_PRIVATE(Locations)
    QScopedPointer<LocationsPrivate> d_ptr;
};

} // namespace

#endif // STUDENTARIO_CORE_LOCATIONS_H
