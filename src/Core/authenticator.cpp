#include "authenticator.h"

#include "database.h"
#include "users.h"

#include <QCryptographicHash>
#include <QDebug>
#include <QJsonValue>
#include <QRandomGenerator>

using namespace Core;

namespace Core {

/* Le claves "login", "password", "pinCode" es jam definite in le classe Users;
 * alora, simplemente importa los hic. */
using Fields = Users::Fields;

class AuthenticatorPrivate {
public:
    AuthenticatorPrivate();

    bool checkPassword(const QByteArray &password) const;
    bool checkPinCode(const QByteArray &pinCode) const;

    Error login(const QJsonObject &loginData);

private:
    friend class Authenticator;
    Database *m_db;
    User m_user;
    Authenticator::Status m_status;
    Error m_error;
};

} // namespace

AuthenticatorPrivate::AuthenticatorPrivate():
    m_db(nullptr),
    m_status(Authenticator::Status::NotAuthenticated)
{
}

bool AuthenticatorPrivate::checkPassword(const QByteArray &password) const
{
    QByteArray hash = Authenticator::passwordHash(password, m_user.salt);
    return hash == m_user.password;
}

bool AuthenticatorPrivate::checkPinCode(const QByteArray &pinCode) const
{
    return pinCode.size() >= 4 && pinCode == m_user.pinCode;
}

Error AuthenticatorPrivate::login(const QJsonObject &loginData)
{
    m_status = Authenticator::Status::NotAuthenticated;

    QString login = loginData.value(Fields::Login).toString();
    bool ok = m_db->user(login, &m_user);
    if (Q_UNLIKELY(!ok)) {
        return Error(Error::LoginNotFound, "Login not found");
    }

    QString password = loginData.value(Fields::Password).toString();
    if (!password.isEmpty()) {
        bool ok = checkPassword(password.toUtf8());
        if (ok) {
            m_status = Authenticator::Status::AuthenticatedWithPassword;
            return Error::NoError;
        } else {
            return Error::InvalidPassword;
        }
    }

    QString pinCode = loginData.value(Fields::PinCode).toString();
    if (!pinCode.isEmpty()) {
        bool ok = checkPinCode(pinCode.toUtf8());
        if (ok) {
            m_status = Authenticator::Status::AuthenticatedWithPin;
            return Error::NoError;
        } else {
            return Error::InvalidPassword;
        }
    }

    return Error(Error::MissingRequiredField,
                 "No password or PIN given");
}

Authenticator::Authenticator():
    d_ptr(new AuthenticatorPrivate())
{
}

Authenticator::~Authenticator() = default;

void Authenticator::setDatabase(Database *db)
{
    Q_D(Authenticator);
    d->m_db = db;
}

QByteArray Authenticator::generateSalt()
{
    union {
        char array[16];
        uint64_t parts[2];
    } u;

    QRandomGenerator::global()->fillRange(u.parts);
    return QByteArray(u.array, sizeof(u.array));
}

QByteArray Authenticator::passwordHash(const QByteArray &password,
                                       const QByteArray &salt)
{
    QByteArray salted = password + salt;
    return QCryptographicHash::hash(salted, QCryptographicHash::Sha3_512);
}

Error Authenticator::login(const QJsonObject &loginData)
{
    Q_D(Authenticator);
    return d->login(loginData);
}

Authenticator::Status Authenticator::status() const
{
    Q_D(const Authenticator);
    return d->m_status;
}

const User &Authenticator::authenticatedUser() const
{
    Q_D(const Authenticator);
    return d->m_user;
}
