#include "database_p.h"

#include "utils.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>

using namespace Core;

#define FIELD_DB_VERSION "DB version"
#define LATEST_DB_VERSION 5

#ifndef BUILDING_TESTS
#define STATIC_THREAD_LOCAL static thread_local
#else
#define STATIC_THREAD_LOCAL
#endif

const QString userSelect = QStringLiteral(
    "SELECT id,"
    " isStudent, isParent, isTeacher, isAdmin, isDirector, isMaster,"
    " login, password, salt, pinCode, pinCodeCreation,"
    " name, birthDate, keywords, contactInfo, created,"
    " status, statusChangeTime "
    "FROM `Users` "
);

const QString groupSelect = QStringLiteral(
    "SELECT id,"
    " name, description, created, locationId "
    "FROM `Groups` "
);

static const QString statusChangeSelect = QStringLiteral(
    "SELECT id,"
    " creatorId, modified, userId, status, time, reason "
    "FROM `StatusChanges` "
);

DatabasePrivate::DatabasePrivate(const QVariantMap &configuration,
                                 const QString &connectionName):
    m_conf(configuration),
    m_connectionName(connectionName)
{
}

DatabasePrivate::~DatabasePrivate()
{
    m_db.close();
    m_db = QSqlDatabase();
    QSqlDatabase::removeDatabase(m_connectionName);
}

bool DatabasePrivate::init()
{
    QString driver = m_conf.value(QStringLiteral("DatabaseDriver"),
                                  QStringLiteral("QSQLITE")).toString();
    auto db = QSqlDatabase::addDatabase(driver, m_connectionName);

    QString dbName = m_conf.value(QStringLiteral("DatabaseName")).toString();
    db.setDatabaseName(dbName);
    qDebug() << "Driver is" << driver << "DB name:" << dbName;

    db.setHostName(m_conf.value(QStringLiteral("DatabaseHost")).toString());
    int port = m_conf.value(QStringLiteral("DatabasePort"), -1).toInt();
    if (port >= 0) {
        db.setPort(port);
    }

    db.setUserName(m_conf.value(QStringLiteral("DatabaseUser")).toString());
    db.setPassword(m_conf.value(QStringLiteral("DatabasePassword")).
                   toString());
    db.setConnectOptions(m_conf.value(QStringLiteral("DatabaseOptions")).
                         toString());

    if (Q_UNLIKELY(!db.open())) {
        qWarning() << "Failed to open database";
        return false;
    }

    qDebug() << "Database opened:" << db.connectionName();
    m_db = db;

    if (driver == "QSQLITE") {
        /* Abilita le supporto pro claves estranier */
        db.exec(QStringLiteral("PRAGMA foreign_keys = ON"));
        if (Q_UNLIKELY(db.lastError().isValid())) {
            qWarning() << "Missing support for foreign keys" << db.lastError();
            return false;
        }
    }

    return true;
}

QString DatabasePrivate::autoIncrement(const QString &name) const
{
    QString definition = name +
        QStringLiteral(" INTEGER NOT NULL");
    if (m_db.driverName() == "QMYSQL") {
        definition += QStringLiteral(" AUTO_INCREMENT");
    }
    return definition;
}

bool DatabasePrivate::addForeignColumn(const QString &table,
                                       const QString &column,
                                       const QString &reference)
{
    m_db.exec(QStringLiteral("ALTER TABLE `%1` ADD COLUMN"
                             " %2 INTEGER REFERENCES %3").
              arg(table).arg(column).arg(reference));
    // Si il ha un error, assume que le columna existeva jam
    bool columnExisted = m_db.lastError().isValid();
    // MySQL non supporta "inline references"; nos debe facer isto in omne caso
    if (m_db.driverName() == "QMYSQL") {
        m_db.exec(QStringLiteral("ALTER TABLE `%1` ADD FOREIGN KEY(%2)"
                                 " REFERENCES %3").
                  arg(table).arg(column).arg(reference));
        if (Q_UNLIKELY(m_db.lastError().isValid())) return false;
    } else if (columnExisted) {
        // TODO: recrear le columna in SQLite
    }
    return true;
}

bool DatabasePrivate::addForeignColumns(const QVector<ForeignKeyDesc> &columns)
{
    for (const ForeignKeyDesc &c: columns) {
        if (Q_UNLIKELY(!addForeignColumn(c.table, c.column, c.reference))) {
            return false;
        }
    }
    return true;
}

bool DatabasePrivate::ensureConnected()
{
    if (Q_LIKELY(m_db.isOpen())) return true;

    return init();
}

bool DatabasePrivate::setDbVersion(int version)
{
    QSqlQuery q(db());
    q.prepare("UPDATE meta SET data = :version WHERE name = '"
              FIELD_DB_VERSION "'");
    q.bindValue(":version", version);
    q.exec();
    return !q.lastError().isValid();
}

int DatabasePrivate::dbVersion() const
{
    QSqlQuery q = m_db.exec("SELECT data FROM meta WHERE name = '"
                            FIELD_DB_VERSION "'");
    if (q.next()) {
        return q.value(0).toInt();
    } else {
        qDebug() << "Reading DB version failed, assuming new";
        return 0;
    }
}

bool DatabasePrivate::createDb(int latestDbVersion)
{
    QSqlDatabase &db = this->db();
    db.exec("CREATE TABLE meta ("
            + autoIncrement("id") + ","
            " name VARCHAR(32) UNIQUE NOT NULL,"
            " data VARCHAR(256),"
            "PRIMARY KEY(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    QSqlQuery q(db);
    q.prepare("INSERT INTO meta (name, data) VALUES ('"
              FIELD_DB_VERSION "', :version)");
    q.bindValue(":version", latestDbVersion);
    q.exec();
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    if (Q_UNLIKELY(!createTableUsers("Users"))) return false;
    if (Q_UNLIKELY(!createTableStatusChanges())) return false;
    if (Q_UNLIKELY(!createTableRelations())) return false;
    if (Q_UNLIKELY(!createIndexesForUsers())) return false;

    if (Q_UNLIKELY(!createTableTags())) return false;
    if (Q_UNLIKELY(!createTableLocations())) return false;

    /*
     * Gruppos e lor membrato
     */
    db.exec("CREATE TABLE `Groups` ("
            + autoIncrement("id") + ","
            " name VARCHAR(80) NOT NULL,"
            " description VARCHAR(2048),"
            " created DATETIME NOT NULL,"
            " locationId INTEGER,"
            " PRIMARY KEY(id),"
            " FOREIGN KEY(locationId) REFERENCES Locations(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE INDEX GroupNameIdx ON `Groups` (name)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE Students ("
            " groupId INTEGER NOT NULL,"
            " userId INTEGER NOT NULL,"
            " PRIMARY KEY(groupId,userId)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE Teachers ("
            " userId INTEGER NOT NULL,"
            " groupId INTEGER NOT NULL,"
            " PRIMARY KEY(userId,groupId)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE Admins ("
            " userId INTEGER NOT NULL,"
            " groupId INTEGER NOT NULL,"
            " PRIMARY KEY(userId,groupId)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    if (Q_UNLIKELY(!createTableUserGroupTags())) return false;

    if (Q_UNLIKELY(!createTableLessons())) return false;

    if (Q_UNLIKELY(!createTriggers())) return false;

    return true;
}

bool DatabasePrivate::createTableUsers(const QString &tableName)
{
    QSqlDatabase &db = this->db();
    db.exec("CREATE TABLE `" + tableName + "` ("
            + autoIncrement("id") + ","
            " isStudent BOOLEAN NOT NULL DEFAULT FALSE,"
            " isParent BOOLEAN NOT NULL DEFAULT FALSE,"
            " isTeacher BOOLEAN NOT NULL DEFAULT FALSE,"
            " isAdmin BOOLEAN NOT NULL DEFAULT FALSE,"
            " isDirector BOOLEAN NOT NULL DEFAULT FALSE,"
            " isMaster BOOLEAN NOT NULL DEFAULT FALSE,"
            " login VARCHAR(64) NOT NULL UNIQUE,"
            " password VARBINARY(128) DEFAULT NULL,"
            " salt VARBINARY(16) DEFAULT NULL,"
            " pinCode CHAR(8) DEFAULT NULL,"
            " pinCodeCreation DATETIME DEFAULT NULL,"
            " name VARCHAR(80),"
            " birthDate DATETIME DEFAULT NULL,"
            " keywords VARCHAR(128),"
            // JSON array de URLs
            " contactInfo VARCHAR(2048),"
            " created DATETIME NOT NULL,"
            // Pote esser vacue; manualmente actualisate con le plus
            // recente rango del tabula StatusChanges TODO
            " status TINYINT NOT NULL DEFAULT 0,"
            " statusChangeTime DATETIME DEFAULT NULL,"
            " PRIMARY KEY(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    return true;
}

bool DatabasePrivate::createIndexesForUsers()
{
    QSqlDatabase &db = this->db();
    db.exec("CREATE INDEX UserNameIdx ON `Users` (name)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    db.exec("CREATE INDEX LoginIdx ON `Users` (login)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    return true;
}

bool DatabasePrivate::createTableStatusChanges()
{
    QSqlDatabase &db = this->db();
    db.exec("CREATE TABLE `StatusChanges` ("
            + autoIncrement("id") + ","
            " creatorId INTEGER NOT NULL DEFAULT -1,"
            " modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,"
            " userId INTEGER NOT NULL,"
            " status TINYINT NOT NULL,"
            " time DATETIME NOT NULL,"
            " reason TEXT DEFAULT NULL,"
            " PRIMARY KEY(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE INDEX StatusChangesIdx ON `StatusChanges` (userId, time)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    db.exec("CREATE INDEX StatusChanges2Idx ON `StatusChanges` (time)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    return true;
}

bool DatabasePrivate::createTriggers()
{
    QSqlDatabase &db = this->db();

    db.exec("DROP TRIGGER IF EXISTS tg_delete_users");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    db.exec("DROP TRIGGER IF EXISTS tg_delete_groups");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    db.exec("DROP TRIGGER IF EXISTS tg_delete_tags");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    db.exec("DROP TRIGGER IF EXISTS tg_delete_lessons");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    db.exec("DROP TRIGGER IF EXISTS tg_delete_lesson_notes");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TRIGGER tg_delete_users "
         "BEFORE DELETE ON `Users` "
         "FOR EACH ROW BEGIN "
         "  DELETE FROM Students WHERE userId = OLD.id;"
         "  DELETE FROM Teachers WHERE userId = OLD.id;"
         "  DELETE FROM Admins WHERE userId = OLD.id;"
         "  DELETE FROM UserTags WHERE userId = OLD.id;"
         "  DELETE FROM Attendance WHERE userId = OLD.id;"
         "  DELETE FROM StatusChanges WHERE userId = OLD.id;"
         "  DELETE FROM Relations WHERE subjectId = OLD.id;"
         "  DELETE FROM Relations WHERE objectId = OLD.id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TRIGGER tg_delete_groups "
         "BEFORE DELETE ON `Groups` "
         "FOR EACH ROW BEGIN "
         "  DELETE FROM Students WHERE groupId = OLD.id;"
         "  DELETE FROM Teachers WHERE groupId = OLD.id;"
         "  DELETE FROM Admins WHERE groupId = OLD.id;"
         "  DELETE FROM GroupTags WHERE groupId = OLD.id;"
         "  DELETE FROM Attendance WHERE groupId = OLD.id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TRIGGER tg_delete_tags "
         "BEFORE DELETE ON `Tags` "
         "FOR EACH ROW BEGIN "
         "  DELETE FROM UserTags WHERE tagId = OLD.id;"
         "  DELETE FROM GroupTags WHERE tagId = OLD.id;"
         "  DELETE FROM LessonTags WHERE tagId = OLD.id;"
         "  DELETE FROM LessonNoteTags WHERE tagId = OLD.id;"
         "  DELETE FROM LocationTags WHERE tagId = OLD.id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TRIGGER tg_delete_lessons "
         "BEFORE DELETE ON `Lessons` "
         "FOR EACH ROW BEGIN "
         "  DELETE FROM Attendance WHERE lessonId = OLD.id;"
         "  DELETE FROM LessonTags WHERE lessonId = OLD.id;"
         "  DELETE FROM LessonNotesGlue WHERE lessonId = OLD.id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TRIGGER tg_delete_lesson_notes "
         "BEFORE DELETE ON `LessonNotes` "
         "FOR EACH ROW BEGIN "
         "  DELETE FROM LessonNoteTags WHERE noteId = OLD.id;"
         "  DELETE FROM LessonNotesGlue WHERE noteId = OLD.id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TRIGGER tg_delete_locations "
         "BEFORE DELETE ON `Locations` "
         "FOR EACH ROW BEGIN "
         "  DELETE FROM LocationTags WHERE locationId = OLD.id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    return true;
}

bool DatabasePrivate::updateFrom(int version)
{
    QSqlDatabase &db = this->db();

    bool mustRecreateTriggers = false;
    bool tableUsersIsUpdated = false;
    bool tableTagsIsUpdated = false;
    bool tableLessonsIsUpdated = false;
    bool tableLocationsIsUpdated = false;

    if (version == 1) {
        /* SQLite non supporta modificar un columna (nos debe augmentar le
         * dimension de 'contactInfo'). Alora, crea un copia del tabula.
         */
        if (Q_UNLIKELY(!createTableUsers("new_Users"))) return false;

        /* Copia tote le contento */
        QSqlQuery rq(db);
        rq.setForwardOnly(true);
        rq.exec("SELECT id,"
                " isStudent, isParent, isTeacher, isAdmin, isDirector, isMaster,"
                " login, password, salt, pinCode, pinCodeCreation,"
                " name, keywords, contactInfo, created "
                "FROM `Users`");

        QSqlQuery wq = prepareQuery(
            "INSERT INTO `new_Users` (id,"
            " isStudent, isParent, isTeacher, isAdmin, isDirector, isMaster,"
            " login, password, salt, pinCode, pinCodeCreation,"
            " name, keywords, contactInfo, created"
            ") VALUES (?,"
            " ?, ?, ?, ?, ?, ?,"
            " ?, ?, ?, ?, ?,"
            " ?, ?, ?, ?"
            ")");
        while (rq.next()) {
            for (int i = 0; i < (1 + 6 + 5 + 4); i++) {
                wq.addBindValue(rq.value(i));
            }
            if (Q_UNLIKELY(!wq.exec())) {
                qWarning() << "migrating User rows failed:" << wq.lastError();
                return false;
            }
        }

        db.exec("DROP TABLE `Users`");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;

        db.exec("ALTER TABLE `new_Users` RENAME TO `Users`");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;

        // recrea le indices e le triggers que esseva automaticamente delite
        if (Q_UNLIKELY(!createIndexesForUsers())) return false;

        tableUsersIsUpdated = true;
        mustRecreateTriggers = true;
        version++;
    }

    if (version == 2) {
        db.exec("CREATE TABLE Admins ("
                " userId INTEGER NOT NULL,"
                " groupId INTEGER NOT NULL,"
                " PRIMARY KEY(userId,groupId)"
                ")");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;

        mustRecreateTriggers = true;
        version++;
    }

    if (version == 3) {
        if (!tableUsersIsUpdated) {
            db.exec("ALTER TABLE `Users` ADD COLUMN"
                    " status TINYINT NOT NULL DEFAULT 0");
            if (Q_UNLIKELY(db.lastError().isValid())) return false;
            db.exec("ALTER TABLE `Users` ADD COLUMN"
                    " statusChangeTime DATETIME DEFAULT NULL");
            if (Q_UNLIKELY(db.lastError().isValid())) return false;
        }

        if (Q_UNLIKELY(!createTableStatusChanges())) return false;
        if (Q_UNLIKELY(!createTableRelations())) return false;
        if (Q_UNLIKELY(!createTableTags())) return false;
        if (Q_UNLIKELY(!createTableUserGroupTags())) return false;
        /* Iste tabula esseva addite in le version 5, ma illos es necessari pro
         * le nove tabula Lessons: */
        if (Q_UNLIKELY(!createTableLocations())) return false;
        if (Q_UNLIKELY(!createTableLessons())) return false;
        mustRecreateTriggers = true;
        tableTagsIsUpdated = true;
        tableLessonsIsUpdated = true;
        tableLocationsIsUpdated = true;
        version ++;
    }

    if (version == 4) {
        if (!tableTagsIsUpdated) {
            db.exec("ALTER TABLE `Tags` ADD COLUMN"
                    " parentId INTEGER NOT NULL DEFAULT -1");
            if (Q_UNLIKELY(db.lastError().isValid())) return false;
            db.exec("ALTER TABLE `Tags` ADD COLUMN color INTEGER");
            if (Q_UNLIKELY(db.lastError().isValid())) return false;
            db.exec("ALTER TABLE `Tags` ADD COLUMN icon VARCHAR(128)");
            if (Q_UNLIKELY(db.lastError().isValid())) return false;
            db.exec("CREATE UNIQUE INDEX TagsParentIdx"
                    " ON `Tags` (parentId, name)");
            if (Q_UNLIKELY(db.lastError().isValid())) return false;

            const static QVector<ForeignKeyDesc> foreignKeys {
                { "GroupTags", "groupId", "`Groups`(id)" },
                { "GroupTags", "tagId", "`Tags`(id)" },
                { "UserTags", "userId", "`Users`(id)" },
                { "UserTags", "tagId", "`Tags`(id)" },
            };
            if (Q_UNLIKELY(!addForeignColumns(foreignKeys))) {
                return false;
            }
        }

        if (!tableLocationsIsUpdated) {
            if (Q_UNLIKELY(!createTableLocations())) return false;
        }

        if (!tableLessonsIsUpdated) {
            /* Nos non usava locationId, alora nos pote remover le columna sin
             * problemas. Alteremente nos deberea copiar tote le tabula. */
            db.exec("ALTER TABLE `Lessons` DROP COLUMN locationId");
            if (Q_UNLIKELY(db.lastError().isValid())) return false;

            const static QVector<ForeignKeyDesc> foreignKeys {
                { "Lessons", "locationId", "`Locations`(id)" },
                { "LessonTags", "lessonId", "`Lessons`(id)" },
                { "LessonTags", "tagId", "`Tags`(id)" },
                { "LessonNoteTags", "noteId", "`LessonNotes`(id)" },
                { "LessonNoteTags", "tagId", "`Tags`(id)" },
            };
            if (Q_UNLIKELY(!addForeignColumns(foreignKeys))) {
                return false;
            }
        }

        if (Q_UNLIKELY(!addForeignColumn("Groups", "locationId",
                                         "Locations(id)"))) {
            return false;
        }
        version ++;
    }

    if (mustRecreateTriggers) {
        if (Q_UNLIKELY(!createTriggers())) return false;
    }

    if (!setDbVersion(LATEST_DB_VERSION)) return false;

    return true;
}

bool DatabasePrivate::createOrUpdateDb()
{
    if (Q_UNLIKELY(!ensureConnected())) return false;

    const int latestDbVersion = LATEST_DB_VERSION;
    int version = dbVersion();
    bool ok = true;

    qDebug() << "DB version is" << version;
    QSqlDatabase &db = this->db();
    if (Q_UNLIKELY(version == 0)) {
        db.transaction();
        ok = createDb(latestDbVersion);
        if (Q_LIKELY(ok)) {
            ok = db.commit();
        } else {
            qWarning() << "DB creation failed:" << db.lastError();
            db.rollback();
        }
    } else if (version != latestDbVersion) {
        db.transaction();
        ok = updateFrom(version);
        if (Q_LIKELY(ok)) {
            ok = db.commit();
        } else {
            qWarning() << "DB update failed:" << db.lastError();
            db.rollback();
        }
    }
    return ok;
}

QSqlQuery DatabasePrivate::prepareQuery(const QString &query) const
{
    QSqlQuery q(db());
    q.setForwardOnly(true);
    bool ok = q.prepare(query);
    if (Q_UNLIKELY(!ok)) {
        const QSqlError error = q.lastError();
        /* If we have been disconnected, attempt to reconnect. The error codes
         * can be different: when killing the connections (see the
         * tst_database::testReconnect() test) we get a 2006 code, whereas in
         * real life, when we get disconnected because of inactivity, we get a
         * 4031.
         * Reference:
         * https://dev.mysql.com/doc/mysql-errors/8.0/en/
         *     server-error-reference.html#error_er_client_interaction_timeout
         * https://dev.mysql.com/doc/mysql-errors/8.0/en/
         *     client-error-reference.html#error_cr_server_gone_error
         */
        if (error.nativeErrorCode() == "2006" ||
            error.nativeErrorCode() == "2013" ||
            error.nativeErrorCode() == "4031") {
            qDebug() << "Reconnecting on query" << query;
            QSqlDatabase &db = const_cast<DatabasePrivate*>(this)->db();
            db.close();
            if (db.open() && q.prepare(query)) {
                return q;
            }
        }
        qWarning() << "Preparing query failed:" << error;
        q = QSqlQuery();
    }
    return q;
}

bool DatabasePrivate::userFromQuery(const QSqlQuery &q, User *user)
{
    int i = 0;
    user->id = q.value(i++).toInt();
    user->roles.setFlag(Database::Role::Student, q.value(i++).toBool());
    user->roles.setFlag(Database::Role::Parent, q.value(i++).toBool());
    user->roles.setFlag(Database::Role::Teacher, q.value(i++).toBool());
    user->roles.setFlag(Database::Role::Admin, q.value(i++).toBool());
    user->roles.setFlag(Database::Role::Director, q.value(i++).toBool());
    user->roles.setFlag(Database::Role::Master, q.value(i++).toBool());
    user->login = q.value(i++).toString();
    user->password = q.value(i++).toByteArray();
    user->salt = q.value(i++).toByteArray();
    user->pinCode = q.value(i++).toByteArray();
    user->pinCodeCreation = q.value(i++).toDateTime();
    user->name = q.value(i++).toString();
    user->birthDate = q.value(i++).toDate();
    user->keywords = q.value(i++).toString();
    QJsonDocument jsonDoc =
        QJsonDocument::fromJson(q.value(i++).toByteArray());
    QStringList contactInfo;
    const QJsonArray contactDetails = jsonDoc.array();
    for (const QJsonValue &v: contactDetails) {
        contactInfo.append(v.toString());
    }
    user->contactInfo = contactInfo;
    user->created = q.value(i++).toDateTime();
    user->status = User::Status(q.value(i++).toInt());
    user->statusChangeTime = q.value(i++).toDateTime();
    return true;
}

bool DatabasePrivate::statusChangeFromQuery(const QSqlQuery &q,
                                            StatusChange *statusChange)
{
    int i = 0;
    statusChange->id = q.value(i++).toInt();
    statusChange->creatorId = q.value(i++).toInt();
    statusChange->modified = q.value(i++).toDateTime();
    statusChange->userId = q.value(i++).toInt();
    statusChange->status = User::Status(q.value(i++).toInt());
    statusChange->time = q.value(i++).toDateTime();
    statusChange->reason = q.value(i++).toString();
    return true;
}

bool DatabasePrivate::groupFromQuery(const QSqlQuery &q, Group *group)
{
    int i = 0;
    group->id = q.value(i++).toInt();
    group->name = q.value(i++).toString();
    group->description = q.value(i++).toString();
    group->created = q.value(i++).toDateTime();
    group->locationId = idFromDb(q.value(i++));
    return true;
}

bool DatabasePrivate::findLastStatusChange(int userId, const QDateTime &before,
                                           StatusChange *statusChange)
{
    QString query = statusChangeSelect + QStringLiteral("WHERE userId = ?");
    if (before.isValid()) {
        query += QStringLiteral(" AND time <= ?");
    }
    query += QStringLiteral(" ORDER BY time DESC LIMIT 1");

    QSqlQuery q(db());
    q.prepare(query);
    q.addBindValue(userId);
    if (before.isValid()) {
        q.addBindValue(before);
    }
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    if (!q.next()) return false;

    statusChangeFromQuery(q, statusChange);
    return true;
}

bool DatabasePrivate::updateUserStatus(int userId)
{
    StatusChange sc;
    if (!findLastStatusChange(userId, QDateTime(), &sc)) return true;

    QSqlQuery q(db());
    q.prepare("UPDATE `Users` SET status = ?, statusChangeTime = ? "
              "WHERE id = ?");
    q.addBindValue(int(sc.status));
    q.addBindValue(sc.time);
    q.addBindValue(userId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "updateUserStatus failed:" << q.lastError();
        return false;
    }
    return true;
}

QStringView DatabasePrivate::tableNameForGroupRole(User::Role role)
{
    switch (role) {
    case User::Role::Student: return u"Students";
    case User::Role::Teacher: return u"Teachers";
    case User::Role::Admin: return u"Admins";
    default:
        Q_UNREACHABLE();
        return u"";
    }
}

bool DatabasePrivate::addUsersToGroup(int groupId, const QVector<int> &userIds,
                                      QStringView table)
{
    QSqlQuery q(db());
    q.prepare(QString("REPLACE INTO %1 (groupId, userId) "
                      "VALUES (?, ?)").arg(table));
    for (int userId: userIds) {
        q.addBindValue(groupId);
        q.addBindValue(userId);
        if (Q_UNLIKELY(!q.exec())) {
            qWarning() << "Failed to run query" << q.lastError();
            return false;
        }
    }

    return true;
}

bool DatabasePrivate::removeUsersFromGroup(int groupId,
                                           const QVector<int> &userIds,
                                           QStringView table)
{
    QSqlQuery q(db());
    q.prepare(QString("DELETE FROM %1 "
                      "WHERE groupId = ? AND userId = ?").arg(table));
    for (int userId: userIds) {
        q.addBindValue(groupId);
        q.addBindValue(userId);
        if (Q_UNLIKELY(!q.exec())) {
            qWarning() << "Failed to run query" << q.lastError();
            return false;
        }
    }

    return true;
}

bool DatabasePrivate::groupsForUser(int userId, Database::Groups *groups,
                                    QStringView table) const
{
    QString query = groupSelect + QStringLiteral(
        "INNER JOIN %1 u ON u.groupId = id "
        "WHERE u.userId = :id "
        "ORDER BY name").arg(table);

    QSqlQuery q(db());
    q.prepare(query);
    q.bindValue(":id", userId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    while (q.next()) {
        groups->append(Group());
        groupFromQuery(q, &groups->last());
    }
    return true;
}

bool DatabasePrivate::usersForGroup(int groupId, Database::Users *users,
                                    QStringView table) const
{
    QString query = userSelect + QStringLiteral(
        "INNER JOIN %1 u ON u.userId = id "
        "WHERE u.groupId = :id "
        "ORDER BY name").arg(table);

    QSqlQuery q(db());
    q.prepare(query);
    q.bindValue(":id", groupId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    while (q.next()) {
        users->append(User());
        userFromQuery(q, &users->last());
    }
    return true;
}

Database::Database(const QVariantMap &configuration,
                   const QString &connectionName):
    d_ptr(new DatabasePrivate(configuration, connectionName))
{
}

Database::~Database() = default;

bool Database::init()
{
    Q_D(Database);
    return d->init();
}

bool Database::createOrUpdateDb()
{
    Q_D(Database);
    return d->createOrUpdateDb();
}

int Database::addUser(const User &user)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare("INSERT INTO `Users` ("
              " isStudent, isParent, isTeacher, isAdmin, isDirector, isMaster,"
              " login, password, salt, pinCode, pinCodeCreation,"
              " name, birthDate, keywords, contactInfo, created) "
              "VALUES ("
              " ?, ?, ?, ?, ?, ?,"
              " ?, ?, ?, ?, ?,"
              " ?, ?, ?, ?, ?)");
    q.addBindValue(user.roles.testFlag(Role::Student) ? 1 : 0);
    q.addBindValue(user.roles.testFlag(Role::Parent) ? 1 : 0);
    q.addBindValue(user.roles.testFlag(Role::Teacher) ? 1 : 0);
    q.addBindValue(user.roles.testFlag(Role::Admin) ? 1 : 0);
    q.addBindValue(user.roles.testFlag(Role::Director) ? 1 : 0);
    q.addBindValue(user.roles.testFlag(Role::Master) ? 1 : 0);

    q.addBindValue(user.login);
    q.addBindValue(user.password);
    q.addBindValue(user.salt);
    q.addBindValue(user.pinCode);
    q.addBindValue(user.pinCodeCreation);

    q.addBindValue(user.name);
    q.addBindValue(user.birthDate);
    q.addBindValue(user.keywords);
    QJsonDocument jsonDoc(QJsonArray::fromStringList(user.contactInfo));
    q.addBindValue(jsonDoc.toJson(QJsonDocument::Compact));
    q.addBindValue(user.created);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "addUser failed:" << q.lastError();
        return -1;
    }

    return q.lastInsertId().toInt();
}

bool Database::updateUser(const User &user, User::Fields fields)
{
    Q_D(Database);

    QStringList assignments;
    if (fields & User::Field::Roles) {
        assignments += QStringList {
            "isStudent = :isStudent",
            "isParent = :isParent",
            "isTeacher = :isTeacher",
            "isAdmin = :isAdmin",
            "isDirector = :isDirector",
            "isMaster = :isMaster",
        };
    }
    if (fields & User::Field::Login) {
        assignments.append("login = :login");
    }
    if (fields & User::Field::Password) {
        assignments += QStringList {
            "password = :password",
            "salt = :salt",
        };
    }
    if (fields & User::Field::PinCode) {
        assignments += QStringList {
            "pinCode = :pinCode",
            "pinCodeCreation = :pinCodeCreation",
        };
    }
    if (fields & User::Field::Name) {
        assignments.append("name = :name");
    }
    if (fields & User::Field::BirthDate) {
        assignments.append("birthDate = :birthDate");
    }
    if (fields & User::Field::Keywords) {
        assignments.append("keywords = :keywords");
    }
    if (fields & User::Field::ContactInfo) {
        assignments.append("contactInfo = :contactInfo");
    }

    if (Q_UNLIKELY(assignments.isEmpty())) {
        qWarning() << "Nothing to update";
        return true;
    }

    QString query = "UPDATE `Users` SET " +
        assignments.join(", ") +
        " WHERE id = :id";
    QSqlQuery q(d->db());
    bool ok = q.prepare(query);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error preparing query" << q.lastError();
        return false;
    }

    q.bindValue(":id", user.id);

    if (fields & User::Field::Roles) {
        auto r = user.roles;
        q.bindValue(":isStudent", r.testFlag(Role::Student) ? 1 : 0);
        q.bindValue(":isParent", r.testFlag(Role::Parent) ? 1 : 0);
        q.bindValue(":isTeacher", r.testFlag(Role::Teacher) ? 1 : 0);
        q.bindValue(":isAdmin", r.testFlag(Role::Admin) ? 1 : 0);
        q.bindValue(":isDirector", r.testFlag(Role::Director) ? 1 : 0);
        q.bindValue(":isMaster", r.testFlag(Role::Master) ? 1 : 0);
    }
    q.bindValue(":login", user.login);
    q.bindValue(":password", user.password);
    q.bindValue(":salt", user.salt);
    q.bindValue(":pinCode", user.pinCode);
    q.bindValue(":pinCodeCreation", user.pinCodeCreation);

    q.bindValue(":name", user.name);
    q.bindValue(":birthDate", user.birthDate);
    q.bindValue(":keywords", user.keywords);
    if (fields & User::Field::ContactInfo) {
        QJsonDocument jsonDoc(QJsonArray::fromStringList(user.contactInfo));
        q.bindValue(":contactInfo", jsonDoc.toJson(QJsonDocument::Compact));
    }

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "updateUser failed:" << q.lastError();
        return false;
    }

    return true;
}

Error Database::deleteUser(int id)
{
    Q_D(Database);

    QSqlQuery q(d->db());
    q.prepare("DELETE FROM `Users` WHERE id = :id");
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "deleteUser failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return q.numRowsAffected() == 0 ? Error::UserNotFound : Error::NoError;
}

bool Database::user(int id, User *user) const
{
    Q_D(const Database);

    QSqlQuery q =
        d->prepareQuery(userSelect + QStringLiteral("WHERE id = :id"));
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    if (Q_UNLIKELY(!q.next())) {
        qDebug() << "User not found" << id;
        return false;
    }
    return d->userFromQuery(q, user);
}

bool Database::user(const QString &login, User *user) const
{
    Q_D(const Database);

    QSqlQuery q =
        d->prepareQuery(userSelect + QStringLiteral("WHERE login = :login"));
    q.bindValue(":login", login);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastQuery() << q.lastError();
        return false;
    }

    if (Q_UNLIKELY(!q.next())) {
        qDebug() << "User not found" << login;
        return false;
    }
    return d->userFromQuery(q, user);
}

QVector<User> Database::users(const User::Filters &filters,
                              User::Sort sort) const
{
    Q_D(const Database);

    QVector<User> users;

    QSqlQuery q(d->db());
    QString query = userSelect;

    const auto requestedRoles = filters.requestedRoles;
    QStringList conditions;
    if (requestedRoles.testFlag(Role::Student))
        conditions.append(QStringLiteral("isStudent = 1"));
    if (requestedRoles.testFlag(Role::Parent))
        conditions.append(QStringLiteral("isParent = 1"));
    if (requestedRoles.testFlag(Role::Teacher))
        conditions.append(QStringLiteral("isTeacher = 1"));
    if (requestedRoles.testFlag(Role::Admin))
        conditions.append(QStringLiteral("isAdmin = 1"));
    if (requestedRoles.testFlag(Role::Director))
        conditions.append(QStringLiteral("isDirector = 1"));
    if (requestedRoles.testFlag(Role::Master))
        conditions.append(QStringLiteral("isMaster = 1"));

    {
        using S = User::StatusFlag;
        const auto possibleStatuses = filters.possibleStatuses;
        QStringList statusConds;
        const QVector<S> flags { S::Unknown, S::Active, S::Inactive };
        for (S flag: flags) {
            if (possibleStatuses.testFlag(flag)) {
                statusConds.append(QStringLiteral("status = %1").
                                   arg(int(flag)));
            }
        }
        if (!statusConds.isEmpty()) {
            QString c = '(' + statusConds.join(QStringLiteral(" OR ")) + ')';
            conditions.append(c);
        }
    }

    if (!filters.pattern.isEmpty()) {
        QString safePattern = Utils::cleanPattern(filters.pattern);
        conditions.append(QStringLiteral("name LIKE '%") + safePattern + "%'");
    }

    if (!conditions.isEmpty()) {
        query += QStringLiteral("WHERE ") + conditions.join(" AND ");
    }

    if (sort != User::Sort::None) {
        QString sortField;
        bool desc = false;
        switch (sort) {
        case User::Sort::IdDesc:
            desc = true; // fallthrough
        case User::Sort::Id:
            sortField = QStringLiteral("id");
            break;
        case User::Sort::NameDesc:
            desc = true; // fallthrough
        case User::Sort::Name:
            sortField = QStringLiteral("name");
            break;
        case User::Sort::Login: sortField = QStringLiteral("login"); break;
        default:
            qWarning() << "Unknown sort ID:" << int(sort);
        }
        query += QStringLiteral(" ORDER BY ") + sortField;
        if (desc) query += QStringLiteral(" DESC");
    }

    if (Q_UNLIKELY(!q.exec(query))) {
        qWarning() << "Failed to run query" << q.lastError();
        return users;
    }

    while (q.next()) {
        users.append(User());
        d->userFromQuery(q, &users.last());
    }
    return users;

}

int Database::maxUserId() const
{
    Q_D(const Database);

    QSqlQuery q(d->db());
    q.exec("SELECT MAX(id) FROM `Users`");
    return q.next() ? q.value(0).toInt() : -1;
}

Error Database::addUserStatusChange(const StatusChange &sc, int *changeId)
{
    Q_D(Database);

    StatusChange previous;
    bool ok = d->findLastStatusChange(sc.userId, sc.time, &previous);
    if (ok && previous.status == sc.status) {
        return Error(Error::UserStatusAlreadySet, QString::number(previous.id));
    }

    if (sc.userId <= 0) {
        return Error(Error::InvalidParameters,
                     QStringLiteral("Invalid userId %1").arg(sc.userId));
    }
    if (sc.status != User::Status::Active &&
        sc.status != User::Status::Inactive) {
        return Error(Error::InvalidParameters,
                     QStringLiteral("Invalid status %1").arg(int(sc.status)));
    }

    QSqlQuery q(d->db());
    q.prepare("REPLACE INTO `StatusChanges` ("
              " userId, creatorId, modified, time, status, reason) "
              "VALUES (?, ?, ?, ?, ?, ?)");
    q.addBindValue(sc.userId);
    q.addBindValue(sc.creatorId);
    q.addBindValue(QDateTime::currentDateTime());
    q.addBindValue(sc.time);
    q.addBindValue(int(sc.status));
    q.addBindValue(sc.reason);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "activateUser failed:" << q.lastError();
        return Error::DatabaseError;
    }

    d->updateUserStatus(sc.userId);
    if (changeId) *changeId = q.lastInsertId().toInt();
    return Error::NoError;
}

Error Database::deleteUserStatusChange(int changeId)
{
    Q_D(Database);

    QSqlQuery q(d->db());
    q.prepare("SELECT userId FROM `StatusChanges` WHERE id = :id");
    q.bindValue(":id", changeId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "deleteUserStatusChange select error:" << q.lastError();
        return Error::DatabaseError;
    }

    if (Q_UNLIKELY(!q.next())) return Error::UserStatusNotFound;
    int userId = q.value(0).toInt();

    q = QSqlQuery(d->db());
    q.prepare("DELETE FROM `StatusChanges` WHERE id = :id");
    q.bindValue(":id", changeId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "deleteUserStatusChange failed:" << q.lastError();
        return Error::DatabaseError;
    }

    Error ret = q.numRowsAffected() == 0 ?
        Error::UserStatusNotFound : Error::NoError;
    d->updateUserStatus(userId);
    return ret;
}

QVector<StatusChange>
Database::userStatusChanges(int id,
                            const QDateTime &since, const QDateTime &to) const
{
    Q_D(const Database);

    QVector<StatusChange> changes;

    QSqlQuery q(d->db());
    QString query = statusChangeSelect;
    QStringList conditions;
    if (id >= 0) {
        conditions.append(QStringLiteral("userId = :id"));
    }
    if (since.isValid()) {
        conditions.append(QStringLiteral("time >= :since"));
    }
    if (to.isValid()) {
        conditions.append(QStringLiteral("time <= :to"));
    }
    if (!conditions.isEmpty()) {
        query += QStringLiteral("WHERE ") + conditions.join(" AND ");
    }
    query += QStringLiteral(" ORDER BY time");
    q.prepare(query);
    if (id >= 0) {
        q.bindValue(":id", id);
    }
    if (since.isValid()) {
        q.bindValue(":since", since);
    }
    if (to.isValid()) {
        q.bindValue(":to", to);
    }
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return changes;
    }

    while (q.next()) {
        changes.append(StatusChange());
        d->statusChangeFromQuery(q, &changes.last());
    }
    return changes;
}

int Database::addGroup(const Group &group)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare("INSERT INTO `Groups` ("
              " name, description, created, locationId) "
              "VALUES ("
              " ?, ?, ?, ?)");
    q.addBindValue(group.name);
    q.addBindValue(group.description);
    q.addBindValue(group.created);
    q.addBindValue(d->idToDb(group.locationId));

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "addGroup failed:" << q.lastError();
        return -1;
    }

    return q.lastInsertId().toInt();
}

Error Database::updateGroup(const Group &group, Group::Fields fields)
{
    Q_D(Database);

    QStringList assignments;
    if (fields & Group::Field::Name) {
        assignments.append("name = :name");
    }
    if (fields & Group::Field::Description) {
        assignments.append("description = :description");
    }
    if (fields & Group::Field::LocationId) {
        assignments.append("locationId = :locationId");
    }

    if (Q_UNLIKELY(assignments.isEmpty())) {
        qWarning() << "Nothing to update";
        return Error::NoError;
    }

    QString query = "UPDATE `Groups` SET " +
        assignments.join(", ") +
        " WHERE id = :id";
    QSqlQuery q(d->db());
    bool ok = q.prepare(query);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error preparing query" << q.lastError();
        return Error::DatabaseError;
    }

    q.bindValue(":id", group.id);

    q.bindValue(":name", group.name);
    q.bindValue(":description", group.description);
    q.bindValue(":locationId", d->idToDb(group.locationId));

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "updateGroup failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return Error::NoError;
}

Error Database::deleteGroup(int id)
{
    Q_D(Database);

    QSqlQuery q(d->db());
    q.prepare("DELETE FROM `Groups` WHERE id = :id");
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "deleteGroup failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return q.numRowsAffected() == 0 ? Error::GroupNotFound : Error::NoError;
}

bool Database::group(int id, Group *group) const
{
    Q_D(const Database);

    QSqlQuery q =
        d->prepareQuery(groupSelect + QStringLiteral("WHERE id = :id"));
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    if (Q_UNLIKELY(!q.next())) {
        qDebug() << "Group not found" << id;
        return false;
    }
    return d->groupFromQuery(q, group);
}

QVector<Group> Database::groups(const QString &pattern) const
{
    Q_D(const Database);

    QVector<Group> groups;

    QSqlQuery q(d->db());
    QStringList conditions;
    if (!pattern.isEmpty()) {
        QString safePattern = Utils::cleanPattern(pattern);
        conditions.append(QStringLiteral("name LIKE '%") + safePattern + "%'");
    }
    QString query = groupSelect;
    if (!conditions.isEmpty()) {
        query += QStringLiteral("WHERE ") + conditions.join(" AND ");
    }
    query += QStringLiteral(" ORDER BY name");

    if (Q_UNLIKELY(!q.exec(query))) {
        qWarning() << "Failed to run query" << q.lastError();
        return groups;
    }

    while (q.next()) {
        groups.append(Group());
        d->groupFromQuery(q, &groups.last());
    }
    return groups;
}

bool Database::addUsersToGroup(int groupId, const QVector<int> &userIds,
                               User::Role role)
{
    Q_D(Database);
    return d->addUsersToGroup(groupId, userIds,
                              d->tableNameForGroupRole(role));
}

bool Database::removeUsersFromGroup(int groupId, const QVector<int> &userIds,
                                    User::Role role)
{
    Q_D(Database);
    return d->removeUsersFromGroup(groupId, userIds,
                                   d->tableNameForGroupRole(role));
}

bool Database::groupsForUser(int userId, Groups *groups, User::Role role) const
{
    Q_D(const Database);
    return d->groupsForUser(userId, groups, d->tableNameForGroupRole(role));
}

bool Database::usersForGroup(int groupId, Users *users, User::Role role) const
{
    Q_D(const Database);
    return d->usersForGroup(groupId, users, d->tableNameForGroupRole(role));
}

QSqlDatabase &Database::dbForTesting()
{
    Q_D(Database);
    return d->db();
}
