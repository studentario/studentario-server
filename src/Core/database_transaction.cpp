#include "database_p.h"

using namespace Core;

Database::Transaction::Transaction(Database *db, Error *watchedError):
    m_db(db->d_ptr->db()),
    m_watchedError(watchedError),
    m_isOpen(false)
{
    if (m_db.transaction()) {
        m_isOpen = true;
    }
}

Database::Transaction::~Transaction()
{
    if (!m_isOpen) return;

    if (m_watchedError && !m_watchedError->isError()) {
        if (Q_UNLIKELY(!m_db.commit())) {
            qWarning() << "Transaction commit failed:" << m_db.lastError();
        }
    } else {
        if (Q_UNLIKELY(!m_db.rollback())) {
            qWarning() << "Transaction rollback failed:" << m_db.lastError();
        }
    }
}

bool Database::Transaction::commit()
{
    if (Q_UNLIKELY(!m_isOpen)) {
        qWarning() << "Transaction not open!";
        return false;
    }

    bool ok = m_db.commit();
    if (Q_LIKELY(ok)) {
        m_isOpen = false;
    } else {
        qWarning() << "Transaction::commit failed:" << m_db.lastError();
    }
    return ok;
}

void Database::Transaction::watchError(Error *error)
{
    m_watchedError = error;
}
