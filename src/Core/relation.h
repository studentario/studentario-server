#ifndef STUDENTARIO_CORE_RELATION_H
#define STUDENTARIO_CORE_RELATION_H

#include <QString>

namespace Core {

struct Relation {
    enum class Type {
        Invalid = 0,
        IsParent,
    };

    int id;
    int subjectId;
    Type type;
    int objectId;

    bool operator==(const Relation &o) const {
        return id == o.id &&
            subjectId == o.subjectId &&
            type == o.type &&
            objectId == o.objectId;
    }
};

} // namespace

#endif // STUDENTARIO_CORE_RELATION_H
