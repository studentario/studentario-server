#ifndef STUDENTARIO_CORE_TAGS_H
#define STUDENTARIO_CORE_TAGS_H

#include "error.h"
#include "tag.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QScopedPointer>
#include <QString>

namespace Core {

class Database;

class TagsPrivate;
class Tags {
public:
    using Entity = Tag::Entity;
    using Entities = Tag::Entities;

    Tags();
    virtual ~Tags();

    void setDatabase(Database *db);

    Error addTag(const QJsonObject &tag, int *tagId);
    Error updateTag(const Tag &tag, const QJsonObject &changes);
    Error deleteTag(int tagId);
    Error tag(int id, Tag *tag) const;
    // parent 0: qualcunque parente, -1 = sin parente
    QVector<Tag> tags(int parentId = 0, const QString &pattern = {}) const;

    // mappa: { "users": [...ids...], "groups": [...] }
    Error assignTag(int tagId, const QJsonObject &entities);
    Error removeTag(int tagId, const QJsonObject &entities);
    QJsonObject taggedObjects(int tagId, Entities entities) const;
    Error loadTags(QJsonObject *object, Entity entity) const;
    Error loadTags(QJsonArray *objects, Entity entity) const;

    static QJsonObject jsonFromDb(const Tag &tag);
    static QJsonArray jsonFromDb(const QVector<Tag> &tags);

    struct Fields {
    static const QString TagId;
    static const QString ParentTagId;
    static const QString Name;
    static const QString Description;
    static const QString CreatorId;
    static const QString Created;
    static const QString Color;
    static const QString Icon;

    static const QString Children;
    };

private:
    Q_DECLARE_PRIVATE(Tags)
    QScopedPointer<TagsPrivate> d_ptr;
};

} // namespace

#endif // STUDENTARIO_CORE_TAGS_H
