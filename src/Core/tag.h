#ifndef STUDENTARIO_CORE_TAG_H
#define STUDENTARIO_CORE_TAG_H

#include <QDateTime>
#include <QFlags>
#include <QHash>
#include <QString>

namespace Core {

struct Tag {
    int id;
    int parentId = -1;
    QString name;
    QString description;
    int creatorId = -1;
    QDateTime created;
    QString color;
    QString icon;

    bool operator==(const Tag &o) const {
        return id == o.id &&
            parentId == o.parentId &&
            name == o.name &&
            description == o.description &&
            creatorId == o.creatorId &&
            created == o.created &&
            color == o.color &&
            icon == o.icon;
    }

    enum class Field {
        Name = 1 << 0,
        Description = 1 << 1,
        CreatorId = 1 << 2,
        Created = 1 << 3,
        ParentId = 1 << 4,
        Color = 1 << 5,
        Icon = 1 << 6,
    };
    Q_DECLARE_FLAGS(Fields, Field)

    enum class Entity {
        Empty = 0,
        Users = 1 << 0,
        Groups = 1 << 1,
        Lessons = 1 << 2,
        LessonNotes = 1 << 3,
        Locations = 1 << 4,
        All = Users | Groups,
    };
    Q_DECLARE_FLAGS(Entities, Entity)
};

inline uint qHash(Core::Tag::Entity entity, uint seed) noexcept {
    return ::qHash(static_cast<uint>(entity), seed);
}

} // namespace

Q_DECLARE_OPERATORS_FOR_FLAGS(Core::Tag::Fields)

#endif // STUDENTARIO_CORE_TAG_H
