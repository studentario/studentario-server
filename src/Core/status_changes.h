#ifndef STUDENTARIO_CORE_STATUS_CHANGES_H
#define STUDENTARIO_CORE_STATUS_CHANGES_H

#include "error.h"
#include "users.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QScopedPointer>
#include <QString>

namespace Core {

class Database;

class StatusChangesPrivate;
class StatusChanges {
public:
    StatusChanges();
    virtual ~StatusChanges();

    void setDatabase(Database *db);

    Error addUserStatusChange(const QJsonObject &statusChange,
                              int *changeId = nullptr);
    Error deleteUserStatusChange(int changeId);
    QVector<StatusChange> userStatusChanges(const QJsonObject &filters) const;

    /* Retorna non solo le campos de StatusChange, ma anque los del usator (le
     * campos specificate), le gruppos e le inseniantes (campos specificate) */
    QJsonObject jsonFromDb(const StatusChange &statusChange,
                           Users::FieldSet userFields = {},
                           Users::FieldSet teacherFields = {}) const;
    QJsonArray jsonFromDb(const QVector<StatusChange> &statusChanges,
                          Users::FieldSet userFields = {},
                          Users::FieldSet teacherFields = {}) const;

    struct Fields {
    static const QString ChangeId;
    static const QString CreatorId;
    static const QString Modified;
    static const QString UserId;
    static const QString Status;
    static const QString Time;
    static const QString Reason;
    static const QString Since;
    static const QString To;
    static const QString User;
    static const QString Groups;
    static const QString Teachers;
    };

private:
    Q_DECLARE_PRIVATE(StatusChanges)
    QScopedPointer<StatusChangesPrivate> d_ptr;
};

} // namespace

#endif // STUDENTARIO_CORE_STATUS_CHANGES_H
