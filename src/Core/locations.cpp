#include "locations.h"

#include "database.h"

#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>

using namespace Core;

namespace Core {

using F = Locations::Fields;
const QString F::LocationId = QStringLiteral("locationId");
const QString F::Name = QStringLiteral("name");
const QString F::Description = QStringLiteral("description");
const QString F::Created = QStringLiteral("created");
const QString F::CreatorId = QStringLiteral("creatorId");
const QString F::Color = QStringLiteral("color");
const QString F::Icon = QStringLiteral("icon");

const QString F::Location = QStringLiteral("location");

class LocationsPrivate {
public:
    LocationsPrivate();

private:
    friend class Locations;
    Database *m_db;
};

} // namespace

LocationsPrivate::LocationsPrivate():
    m_db(nullptr)
{
}

Locations::Locations():
    d_ptr(new LocationsPrivate())
{
}

Locations::~Locations() = default;

void Locations::setDatabase(Database *db)
{
    Q_D(Locations);
    d->m_db = db;
}

Error Locations::addLocation(const QJsonObject &location, int *locationId)
{
    Q_D(Locations);

    QString name = location.value(Fields::Name).toString();
    if (Q_UNLIKELY(name.isEmpty())) {
        return Error(Error::InvalidParameters, "Name cannot be empty");
    }

    Location g = {
        0, // id
        name,
        location.value(Fields::Description).toString(),
        location.value(Fields::CreatorId).toInt(-1),
        QDateTime::currentDateTime(),
        location.value(Fields::Color).toString(),
        location.value(Fields::Icon).toString(),
    };

    *locationId = d->m_db->addLocation(g);
    return *locationId >= 0 ? Error::NoError : Error::DatabaseError;
}

Error Locations::updateLocation(const Location &location, const QJsonObject &changes)
{
    Q_D(Locations);

    using F = Locations::Fields;
    Location l = location;
    Location::Fields fields;

    for (auto i = changes.begin(); i != changes.end(); i++) {
        if (i.key() == F::Name) {
            l.name = i.value().toString();
            if (Q_UNLIKELY(l.name.isEmpty())) {
                return Error(Error::InvalidParameters,
                             "Location name cannot be empty");
            }
            fields |= Location::Field::Name;
        } else if (i.key() == F::Description) {
            l.description = i.value().toString();
            fields |= Location::Field::Description;
        } else if (i.key() == F::Color) {
            l.color = i.value().toString();
            fields |= Location::Field::Color;
        } else if (i.key() == F::Icon) {
            l.icon = i.value().toString();
            fields |= Location::Field::Icon;
        } else {
            return Error(Error::InvalidParameters,
                         "Cannot update field " + i.key());
        }
    }

    return d->m_db->updateLocation(l, fields);
}

Error Locations::deleteLocation(int locationId)
{
    Q_D(Locations);
    return d->m_db->deleteLocation(locationId);
}

Error Locations::location(int id, Location *l) const
{
    Q_D(const Locations);
    return d->m_db->location(id, l) ? Error::NoError : Error::LocationNotFound;
}

QVector<Location> Locations::locations() const
{
    Q_D(const Locations);
    return d->m_db->locations();
}

QJsonObject Locations::jsonFromDb(const Location &l)
{
    using F = Locations::Fields;

    QJsonObject ret = {
        { F::LocationId, l.id },
        { F::Name, l.name },
        { F::Description, l.description },
        { F::CreatorId, l.creatorId },
        { F::Color, l.color },
        { F::Icon, l.icon },
    };
    if (l.created.isValid()) {
        ret.insert(F::Created, l.created.toString(Qt::ISODate));
    }
    return ret;
}

QJsonArray Locations::jsonFromDb(const QVector<Location> &locations)
{
    QJsonArray ret;
    for (const Location &l: locations) {
        ret.append(jsonFromDb(l));
    }
    return ret;
}
