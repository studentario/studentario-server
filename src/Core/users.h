#ifndef STUDENTARIO_CORE_USERS_H
#define STUDENTARIO_CORE_USERS_H

#include "error.h"
#include "group.h"
#include "user.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QScopedPointer>
#include <QString>

namespace Core {

class Database;

class UsersPrivate;
class Users {
public:
    enum class FieldId {
        Empty = 0,
        Password = 1 << 0,
        Roles = 1 << 1,
        Name = 1 << 2,
        Keywords = 1 << 3,
        ContactInfo = 1 << 4,
        Login = 1 << 5,
        Created = 1 << 6,
        BirthDate = 1 << 7,
        Status = 1 << 8,
        PublicInfo = Roles | Name | BirthDate | Keywords | ContactInfo |
            Login | Created | Status,
        All = PublicInfo | Password,
    };
    Q_DECLARE_FLAGS(FieldSet, FieldId)

    Users();
    virtual ~Users();

    using Role = User::Role;
    using Roles = User::Roles;

    void setDatabase(Database *db);

    Error addUser(const QJsonObject &user, int *userId);
    Error addLoginlessUser(const QJsonObject &user, int *userId);
    Error updateUser(const User &user, const QJsonObject &changes);
    Error updatePin(int userId, const QByteArray &pin);
    Error deleteUser(int userId);
    Error user(int id, User *user) const;
    QVector<User> users(const User::Filters &filters,
                        User::Sort sort = User::Sort::None) const;

    static User::Roles rolesFromJson(const QJsonObject &json,
        const User::Roles startingRoles = User::Role::NoRole);
    static void filtersFromJson(const QJsonObject &json,
                                User::Filters *filters);
    static User::Sort sortingFromJson(const QJsonObject &json);
    static FieldSet fieldsFromNames(const QStringList &fieldNames);
    static User::Status statusFromString(const QString &s);
    static QString statusToString(const User::Status s);
    static QJsonObject jsonFromDb(const User &u,
                                  FieldSet fields = FieldId::PublicInfo);
    static QJsonArray jsonFromDb(const QVector<User> &users,
                                 FieldSet fields = FieldId::PublicInfo);

    struct Fields {
    static const QString UserId;
    static const QString IsStudent;
    static const QString IsParent;
    static const QString IsTeacher;
    static const QString IsAdmin;
    static const QString IsDirector;
    static const QString IsMaster;
    static const QString Login;
    static const QString Password;
    static const QString Salt;
    static const QString PinCode;
    static const QString Name;
    static const QString BirthDate;
    static const QString Keywords;
    static const QString ContactInfo;
    static const QString Created;
    static const QString Status;
    static const QString StatusChangeTime;
    };

private:
    Q_DECLARE_PRIVATE(Users)
    QScopedPointer<UsersPrivate> d_ptr;
};

struct Values {
static const QString StatusActive;
static const QString StatusInactive;
};

} // namespace

#endif // STUDENTARIO_CORE_USERS_H
