#include "tags.h"

#include "database.h"
#include "groups.h"
#include "locations.h"
#include "users.h"
#include "utils.h"

#include <QDateTime>
#include <QDebug>
#include <QHash>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>

using namespace Core;

namespace Core {

using F = Tags::Fields;
const QString F::TagId = QStringLiteral("tagId");
const QString F::ParentTagId = QStringLiteral("parentTagId");
const QString F::Name = QStringLiteral("name");
const QString F::Description = QStringLiteral("description");
const QString F::Created = QStringLiteral("created");
const QString F::CreatorId = QStringLiteral("creatorId");
const QString F::Color = QStringLiteral("color");
const QString F::Icon = QStringLiteral("icon");

const QString F::Children = QStringLiteral("children");

struct EntityInfo {
    QString jsonField;
};

/* Nos non pote usar un simple variabile static, perque le elementos del mappa
 * poterea non esser preste.
 */
using EntityMap = QHash<Tags::Entity, EntityInfo>;
Q_GLOBAL_STATIC_WITH_ARGS(EntityMap, s_entityInfo, ({
    { Tag::Entity::Users, { Users::Fields::UserId }},
    { Tag::Entity::Groups, { Groups::Fields::GroupId }},
    { Tag::Entity::Locations, { Locations::Fields::LocationId }},
}))

class TagsPrivate {
public:
    TagsPrivate();

private:
    friend class Tags;
    Database *m_db;
};

} // namespace

TagsPrivate::TagsPrivate():
    m_db(nullptr)
{
}

Tags::Tags():
    d_ptr(new TagsPrivate())
{
}

Tags::~Tags() = default;

void Tags::setDatabase(Database *db)
{
    Q_D(Tags);
    d->m_db = db;
}

Error Tags::addTag(const QJsonObject &tag, int *tagId)
{
    Q_D(Tags);

    QString name = tag.value(Fields::Name).toString();
    if (Q_UNLIKELY(name.isEmpty())) {
        return Error(Error::InvalidParameters, "Name cannot be empty");
    }

    Tag t = {
        0, // id
        tag.value(Fields::ParentTagId).toInt(-1),
        name,
        tag.value(Fields::Description).toString(),
        tag.value(Fields::CreatorId).toInt(-1),
        QDateTime::currentDateTime(),
        tag.value(Fields::Color).toString(),
        tag.value(Fields::Icon).toString(),
    };

    return d->m_db->addTag(t, tagId);
}

Error Tags::updateTag(const Tag &tag, const QJsonObject &changes)
{
    Q_D(Tags);

    using F = Tags::Fields;
    Tag t = tag;
    Tag::Fields fields;

    for (auto i = changes.begin(); i != changes.end(); i++) {
        if (i.key() == F::ParentTagId) {
            t.parentId = i.value().toInt();
            fields |= Tag::Field::ParentId;
        } else if (i.key() == F::Name) {
            t.name = i.value().toString();
            if (Q_UNLIKELY(t.name.isEmpty())) {
                return Error(Error::InvalidParameters,
                             "Tag name cannot be empty");
            }
            fields |= Tag::Field::Name;
        } else if (i.key() == F::Description) {
            t.description = i.value().toString();
            fields |= Tag::Field::Description;
        } else if (i.key() == F::Color) {
            t.color = i.value().toString();
            fields |= Tag::Field::Color;
        } else if (i.key() == F::Icon) {
            t.icon = i.value().toString();
            fields |= Tag::Field::Icon;
        } else {
            return Error(Error::InvalidParameters,
                         "Cannot update field " + i.key());
        }
    }

    return d->m_db->updateTag(t, fields);
}

Error Tags::deleteTag(int tagId)
{
    Q_D(Tags);
    return d->m_db->deleteTag(tagId);
}

Error Tags::tag(int id, Tag *t) const
{
    Q_D(const Tags);
    return d->m_db->tag(id, t) ? Error::NoError : Error::TagNotFound;
}

QVector<Tag> Tags::tags(int parentId, const QString &pattern) const
{
    Q_D(const Tags);
    return d->m_db->tags(parentId, pattern);
}

Error Tags::assignTag(int tagId, const QJsonObject &entities)
{
    Q_D(Tags);

    for (auto i = entities.begin(); i != entities.end(); i++) {
        bool ok = false;
        QVector<int> ids = Utils::idsFromJson(i.value().toArray(), &ok);
        if (Q_UNLIKELY(!ok)) {
            return { Error::InvalidParameters, "Error parsing ID list" };
        }

        if (i.key() == "userIds") {
            ok = d->m_db->addTagToUsers(tagId, ids);
        } else if (i.key() == "groupIds") {
            ok = d->m_db->addTagToGroups(tagId, ids);
        } else if (i.key() == "locationIds") {
            ok = d->m_db->addTagToLocations(tagId, ids);
        } else {
            return {
                Error::InvalidParameters,
                QStringLiteral("Unknown tag target '%1'").arg(i.key())
            };
        }

        if (Q_UNLIKELY(!ok)) {
            return Error::DatabaseError;
        }
    }
    return Error::NoError;
}

Error Tags::removeTag(int tagId, const QJsonObject &entities)
{
    Q_D(Tags);

    for (auto i = entities.begin(); i != entities.end(); i++) {
        bool ok = false;
        QVector<int> ids = Utils::idsFromJson(i.value().toArray(), &ok);
        if (Q_UNLIKELY(!ok)) {
            return { Error::InvalidParameters, "Error parsing ID list" };
        }

        if (i.key() == "userIds") {
            ok = d->m_db->removeTagFromUsers(tagId, ids);
        } else if (i.key() == "groupIds") {
            ok = d->m_db->removeTagFromGroups(tagId, ids);
        } else if (i.key() == "locationIds") {
            ok = d->m_db->removeTagFromLocations(tagId, ids);
        } else {
            return {
                Error::InvalidParameters,
                QStringLiteral("Unknown tag target '%1'").arg(i.key())
            };
        }

        if (Q_UNLIKELY(!ok)) {
            return Error::DatabaseError;
        }
    }
    return Error::NoError;
}

QJsonObject Tags::taggedObjects(int tagId, Entities entities) const
{
    Q_D(const Tags);

    QJsonObject ret;

    if (entities & Entity::Users) {
        Database::Users users;
        bool ok = d->m_db->usersForTag(tagId, &users);
        if (Q_LIKELY(ok)) {
            ret.insert("users", Users::jsonFromDb(users));
        } else {
            qWarning() << "Error reading users for tag";
        }
    }

    if (entities & Entity::Groups) {
        Database::Groups groups;
        bool ok = d->m_db->groupsForTag(tagId, &groups);
        if (Q_LIKELY(ok)) {
            ret.insert("groups", Groups::jsonFromDb(groups));
        } else {
            qWarning() << "Error reading groups for tag";
        }
    }

    if (entities & Entity::Locations) {
        Database::Locations locations;
        bool ok = d->m_db->locationsForTag(tagId, &locations);
        if (Q_LIKELY(ok)) {
            ret.insert("locations", Locations::jsonFromDb(locations));
        } else {
            qWarning() << "Error reading locations for tag";
        }
    }

    return ret;
}

Error Tags::loadTags(QJsonObject *object, Entity entity) const
{
    Q_D(const Tags);

    const EntityInfo &info = s_entityInfo->value(entity);
    int objectId = object->value(info.jsonField).toInt();
    Database::Tags tags;
    bool ok = d->m_db->tagsForEntity(entity, objectId, &tags);
    object->insert("tags", jsonFromDb(tags));
    return ok ? Error::NoError : Error::DatabaseError;
}

Error Tags::loadTags(QJsonArray *objects, Entity entity) const
{
    for (QJsonValueRef v: *objects) {
        QJsonObject o = v.toObject();
        Error error = loadTags(&o, entity);
        if (Q_UNLIKELY(error)) {
            return error;
        }
        v = o;
    }
    return Error::NoError;
}

QJsonObject Tags::jsonFromDb(const Tag &t)
{
    using F = Tags::Fields;

    QJsonObject ret = {
        { F::TagId, t.id },
        { F::ParentTagId, t.parentId },
        { F::Name, t.name },
        { F::Description, t.description },
        { F::CreatorId, t.creatorId },
        { F::Color, t.color },
        { F::Icon, t.icon },
    };
    if (t.created.isValid()) {
        ret.insert(F::Created, t.created.toString(Qt::ISODate));
    }
    return ret;
}

QJsonArray Tags::jsonFromDb(const QVector<Tag> &tags)
{
    QJsonArray ret;
    for (const Tag &t: tags) {
        ret.append(jsonFromDb(t));
    }
    return ret;
}
