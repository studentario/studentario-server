#ifndef STUDENTARIO_CORE_DATABASE_H
#define STUDENTARIO_CORE_DATABASE_H

#include "attendance.h"
#include "error.h"
#include "group.h"
#include "lesson.h"
#include "location.h"
#include "relation.h"
#include "tag.h"
#include "user.h"

#include <QScopedPointer>
#include <QString>
#include <QVariantMap>
#include <QVector>

class DatabaseTest;
class Testing;

class QSqlDatabase;

namespace Core {

class DatabasePrivate;
class Database {
public:
    Database(const QVariantMap &configuration, const QString &connectionName);
    virtual ~Database();

    bool init();
    bool ensureConnected();
    bool createOrUpdateDb();

    class Transaction {
    public:
        Transaction(Database *db, Error *watchedError = nullptr);
        virtual ~Transaction(); // automatic rollback

        bool commit();
        void watchError(Error *error);

    private:
        QSqlDatabase &m_db;
        Error *m_watchedError;
        bool m_isOpen;
    };

    using Role = User::Role;
    using Roles = User::Roles;
    using Users = QVector<User>;

    int addUser(const User &user);
    bool updateUser(const User &user, User::Fields fields);
    Error deleteUser(int id);
    bool user(int id, User *user) const;
    bool user(const QString &login, User *user) const;
    QVector<User> users(const User::Filters &filters = {},
                        User::Sort sort = User::Sort::None) const;
    int maxUserId() const;
    using StatusChanges = QVector<StatusChange>;
    Error addUserStatusChange(const StatusChange &statusChange, int *changeId);
    Error deleteUserStatusChange(int changeId);
    StatusChanges userStatusChanges(int id,
                                    const QDateTime &since = {},
                                    const QDateTime &to = {}) const;

    using Relations = QVector<Relation>;
    int addRelation(const Relation &relation);
    Error deleteRelation(int relationId);
    Relations relationsBySubject(int subjectId,
        Relation::Type type = Relation::Type::Invalid) const;
    Relations relationsByObject(int objectId,
        Relation::Type type = Relation::Type::Invalid) const;
    bool relationExists(int subjectId, Relation::Type type, int objectId) const;

    using Groups = QVector<Group>;
    int addGroup(const Group &group);
    Error updateGroup(const Group &group, Group::Fields fields);
    Error deleteGroup(int id);
    bool group(int id, Group *group) const;
    Groups groups(const QString &pattern = {}) const;

    bool addUsersToGroup(int groupId, const QVector<int> &userIds,
                         User::Role role);
    bool removeUsersFromGroup(int groupId, const QVector<int> &userIds,
                              User::Role role);
    bool groupsForUser(int userId, Groups *groups, User::Role role) const;
    bool usersForGroup(int groupId, Users *users, User::Role role) const;

    using Tags = QVector<Tag>;
    Error addTag(const Tag &tag, int *tagId);
    Error updateTag(const Tag &tag, Tag::Fields fields);
    Error deleteTag(int id);
    bool tag(int id, Tag *tag) const;
    // parent 0: qualcunque parente, -1 = sin parente
    Tags tags(int parentId = 0, const QString &pattern = {}) const;

    bool addTagToEntities(int tagId, Tag::Entity type, const QVector<int> &ids);
    bool removeTagFromEntities(int tagId, Tag::Entity type, const QVector<int> &ids);
    bool tagsForEntity(Tag::Entity type, int id, Tags *tags) const;

    bool addTagToUsers(int tagId, const QVector<int> &userIds);
    bool removeTagFromUsers(int tagId, const QVector<int> &userIds);
    bool tagsForUser(int userId, Tags *tags) const;
    bool usersForTag(int tagId, Users *users) const;

    bool addTagToGroups(int tagId, const QVector<int> &groupIds);
    bool removeTagFromGroups(int tagId, const QVector<int> &groupIds);
    bool tagsForGroup(int groupId, Tags *tags) const;
    bool groupsForTag(int tagId, Groups *groups) const;

    using Lessons = QVector<Lesson>;
    int addLesson(const Lesson &lesson);
    Error updateLesson(const Lesson &lesson, Lesson::Fields fields);
    Error deleteLesson(int id);
    bool lesson(int id, Lesson *lesson) const;
    Lessons lessons(const QDateTime &since, const QDateTime &to,
                    int locationId = -1) const;

    using Participations = QVector<Participation>;
    bool inviteToLesson(int creatorId, int lessonId,
                        const QVector<int> &entityIds,
                        EntityType type);
    bool uninviteFromLesson(int lessonId, const QVector<int> &entityIds,
                            EntityType type);
    using Participants = QVector<Participant>;
    bool lessonsByParticipants(const Participants &participants, // OR
                               Lessons *lessons,
                               const QDateTime &since = QDateTime(),
                               const QDateTime &to = QDateTime()) const;
    // Solmente pro studentes e inseniantes, non gruppos!
    bool updateAttendance(int lessonId, const Participations &participations);
    bool lessonAttendance(int lessonId, Participations *participations) const;

    // Notas del lectiones
    using LessonNotes = QVector<LessonNote>;
    int addLessonNote(const LessonNote &lessonNote);
    Error updateLessonNote(const LessonNote &lessonNote);
    Error deleteLessonNote(int id);
    bool lessonNote(int id, LessonNote *lessonNote) const;
    // -1 es acceptate
    LessonNotes lessonNotes(int lessonId, int creatorId) const;
    bool attachLessonNote(int lessonId, int noteId, int priority = 0);
    bool detachLessonNote(int lessonId, int noteId);

    bool addTagToLessons(int tagId, const QVector<int> &lessonIds);
    bool removeTagFromLessons(int tagId, const QVector<int> &lessonIds);
    bool tagsForLesson(int lessonId, Tags *tags) const;
    bool lessonsForTag(int tagId, Lessons *lessons) const;

    bool addTagToLessonNotes(int tagId, const QVector<int> &lessonNoteIds);
    bool removeTagFromLessonNotes(int tagId, const QVector<int> &noteIds);
    bool tagsForLessonNote(int lessonNoteId, Tags *tags) const;
    bool lessonNotesForTag(int tagId, LessonNotes *lessonNotes) const;

    int addLocation(const Location &location);
    Error updateLocation(const Location &location, Location::Fields fields);
    Error deleteLocation(int id);
    bool location(int id, Location *location) const;
    using Locations = QVector<Location>;
    Locations locations() const;

    bool addTagToLocations(int tagId, const QVector<int> &locationIds);
    bool removeTagFromLocations(int tagId, const QVector<int> &locationIds);
    bool tagsForLocation(int locationId, Tags *tags) const;
    bool locationsForTag(int tagId, Locations *locations) const;

private:
    friend class Transaction;
    friend class ::DatabaseTest;
    friend class ::Testing;
    QSqlDatabase &dbForTesting();

private:
    Q_DECLARE_PRIVATE(Database)
    QScopedPointer<DatabasePrivate> d_ptr;
};

} // namespace

#endif // STUDENTARIO_CORE_DATABASE_H
