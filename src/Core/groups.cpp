#include "groups.h"

#include "database.h"

#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>

using namespace Core;

namespace Core {

using F = Groups::Fields;
const QString F::GroupId = QStringLiteral("groupId");
const QString F::Name = QStringLiteral("name");
const QString F::Description = QStringLiteral("description");
const QString F::Created = QStringLiteral("created");
const QString F::LocationId = QStringLiteral("locationId");

class GroupsPrivate {
public:
    GroupsPrivate();

private:
    friend class Groups;
    Database *m_db;
};

} // namespace

GroupsPrivate::GroupsPrivate():
    m_db(nullptr)
{
}

Groups::Groups():
    d_ptr(new GroupsPrivate())
{
}

Groups::~Groups() = default;

void Groups::setDatabase(Database *db)
{
    Q_D(Groups);
    d->m_db = db;
}

Error Groups::addGroup(const QJsonObject &group, int *groupId)
{
    Q_D(Groups);

    QString name = group.value(Fields::Name).toString();
    if (Q_UNLIKELY(name.isEmpty())) {
        return Error(Error::InvalidParameters, "Name cannot be empty");
    }

    Group g = {
        0, // id
        name,
        group.value(Fields::Description).toString(),
        QDateTime::currentDateTime(),
        group.value(Fields::LocationId).toInt(-1),
    };

    *groupId = d->m_db->addGroup(g);
    return *groupId >= 0 ? Error::NoError : Error::DatabaseError;
}

Error Groups::updateGroup(const Group &group, const QJsonObject &changes)
{
    Q_D(Groups);

    using F = Groups::Fields;
    Group g = group;
    Group::Fields fields;

    for (auto i = changes.begin(); i != changes.end(); i++) {
        if (i.key() == F::Name) {
            g.name = i.value().toString();
            if (Q_UNLIKELY(g.name.isEmpty())) {
                return Error(Error::InvalidParameters,
                             "Group name cannot be empty");
            }
            fields |= Group::Field::Name;
        } else if (i.key() == F::Description) {
            g.description = i.value().toString();
            fields |= Group::Field::Description;
        } else if (i.key() == F::LocationId) {
            g.locationId = i.value().toInt();
            fields |= Group::Field::LocationId;
        } else {
            return Error(Error::InvalidParameters,
                         "Cannot update field " + i.key());
        }
    }

    return d->m_db->updateGroup(g, fields);
}

Error Groups::deleteGroup(int groupId)
{
    Q_D(Groups);
    return d->m_db->deleteGroup(groupId);
}

Error Groups::group(int id, Group *g) const
{
    Q_D(const Groups);
    return d->m_db->group(id, g) ? Error::NoError : Error::GroupNotFound;
}

QVector<Group> Groups::groups(const QString &pattern) const
{
    Q_D(const Groups);
    return d->m_db->groups(pattern);
}

Error Groups::groupsForUser(int userId, QVector<Group> *groups,
                            User::Role role, const QString &pattern) const
{
    Q_D(const Groups);

    QVector<Group> tmpGroups;
    bool ok = d->m_db->groupsForUser(userId, &tmpGroups, role);
    if (Q_UNLIKELY(!ok)) return Error::DatabaseError;

    if (pattern.isEmpty()) {
        *groups = tmpGroups;
    } else {
        groups->clear();
        groups->reserve(tmpGroups.count());
        for (const auto &g: tmpGroups) {
            if (g.name.contains(pattern, Qt::CaseInsensitive)) {
                groups->append(g);
            }
        }
    }
    return Error::NoError;
}

Error Groups::addUsersToGroup(int groupId, QVector<int> &userIds,
                              User::Role role)
{
    Q_D(Groups);
    // TODO: Verifica que le usator existe e ha le rolo requirite
    bool ok = d->m_db->addUsersToGroup(groupId, userIds, role);
    return ok ? Error::NoError : Error::DatabaseError;
}

Error Groups::removeUsersFromGroup(int groupId, const QVector<int> &userIds,
                                   User::Role role)
{
    Q_D(Groups);
    bool ok = d->m_db->removeUsersFromGroup(groupId, userIds, role);
    return ok ? Error::NoError : Error::DatabaseError;
}

Error Groups::usersForGroup(int groupId, QVector<User> *users,
                            User::Role role) const
{
    Q_D(const Groups);
    bool ok = d->m_db->usersForGroup(groupId, users, role);
    return ok ? Error::NoError : Error::DatabaseError;
}

QJsonObject Groups::jsonFromDb(const Group &g)
{
    using F = Groups::Fields;

    QJsonObject ret = {
        { F::GroupId, g.id },
        { F::Name, g.name },
        { F::Description, g.description },
        { F::LocationId, g.locationId },
    };
    if (g.created.isValid()) {
        ret.insert(F::Created, g.created.toString(Qt::ISODate));
    }
    return ret;
}

QJsonArray Groups::jsonFromDb(const QVector<Group> &groups)
{
    QJsonArray ret;
    for (const Group &g: groups) {
        ret.append(jsonFromDb(g));
    }
    return ret;
}
