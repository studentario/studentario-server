#ifndef STUDENTARIO_CORE_UTILS_H
#define STUDENTARIO_CORE_UTILS_H

#include <QString>
#include <QVariant>
#include <QVector>

class QJsonArray;

namespace Core {
namespace Utils {

/*
 * Assecura que le pattern non pote esser usate pro inserer codice in SQL.
 */
QString cleanPattern(const QString &searchPattern);

/*
 * Converte un array JSON a numeros.
 */
QVector<int> idsFromJson(const QJsonArray &json, bool *ok = nullptr);

/*
 * Converte colores a QVariant pro SQL.
 */
QVariant stringToColor(const QString &color);
QString stringFromColor(const QVariant &color);

} // namespace
} // namespace

#endif // STUDENTARIO_CORE_UTILS_H
