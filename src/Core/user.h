#ifndef STUDENTARIO_CORE_USER_H
#define STUDENTARIO_CORE_USER_H

#include <QByteArray>
#include <QDate>
#include <QDateTime>
#include <QFlags>
#include <QString>
#include <QStringList>

namespace Core {

struct User {
    enum class Role {
        NoRole = 0,
        Student = 1 << 0,
        Parent = 1 << 1,
        Teacher = 1 << 2,
        Admin = 1 << 3,
        Director = 1 << 4,
        Master = 1 << 5,
    };
    Q_DECLARE_FLAGS(Roles, Role)

    enum class Status {
        Unknown = 0,
        Active,
        Inactive,
    };
    enum class StatusFlag {
        Unknown = 1 << 0,
        Active = 1 << 1,
        Inactive = 1 << 2,
        Any = Unknown | Active | Inactive,
    };
    Q_DECLARE_FLAGS(StatusQuery, StatusFlag)

    enum class Sort {
        None = 0,
        Id,
        IdDesc,
        Name,
        NameDesc,
        Login,
    };

    struct Filters {
        Roles requestedRoles;
        StatusQuery possibleStatuses = {};
        QString pattern;
    };

    int id;
    Roles roles;
    QString login;
    QByteArray password;
    QByteArray salt;
    QByteArray pinCode;
    QDateTime pinCodeCreation;
    QString name;
    QDate birthDate;
    QString keywords;
    QStringList contactInfo;
    QDateTime created;
    Status status = Status::Unknown;
    QDateTime statusChangeTime = {};

    bool operator==(const User &o) const {
        return id == o.id &&
            roles == o.roles &&
            login == o.login &&
            password == o.password &&
            salt == o.salt &&
            pinCode == o.pinCode &&
            pinCodeCreation == o.pinCodeCreation &&
            name == o.name &&
            birthDate == o.birthDate &&
            keywords == o.keywords &&
            contactInfo == o.contactInfo &&
            status == o.status &&
            statusChangeTime == o.statusChangeTime &&
            created == o.created;
    }

    enum class Field {
        Roles = 1 << 0,
        Password = 1 << 1, // include le "salt"
        PinCode = 1 << 2,
        Name = 1 << 3,
        Keywords = 1 << 4,
        ContactInfo = 1 << 5,
        Login = 1 << 6,
        BirthDate = 1 << 7,
    };
    Q_DECLARE_FLAGS(Fields, Field)
};

struct StatusChange {
    int id = -1;
    int creatorId;
    QDateTime modified;
    int userId;
    User::Status status;
    QDateTime time;
    QString reason;

    bool operator==(const StatusChange &o) const {
        return id == o.id &&
            creatorId == o.creatorId &&
            modified == o.modified &&
            userId == o.userId &&
            status == o.status &&
            time == o.time &&
            reason == o.reason;
    }
};

} // namespace

Q_DECLARE_OPERATORS_FOR_FLAGS(Core::User::Fields)
Q_DECLARE_OPERATORS_FOR_FLAGS(Core::User::Roles)
Q_DECLARE_OPERATORS_FOR_FLAGS(Core::User::StatusQuery)


#endif // STUDENTARIO_CORE_USER_H
