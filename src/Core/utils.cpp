#include "utils.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonValue>
#include <QRegularExpression>

namespace Core::Utils {

QString cleanPattern(const QString &pattern)
{
    QString safePattern(pattern);
    safePattern.remove(QRegularExpression(R"(['":;%])"));
    return safePattern;
}

QVector<int> idsFromJson(const QJsonArray &json, bool *ok)
{
    QVector<int> ids;
    for (const QJsonValue &v: json) {
        if (Q_UNLIKELY(!v.isDouble())) {
            if (ok) *ok = false;
            return {};
        }
        ids.append(v.toInt());
    }
    if (ok) *ok = true;
    return ids;
}

QVariant stringToColor(const QString &color)
{
    return color.isEmpty() ?
        QVariant() : color.midRef(1).toInt(nullptr, 16);
}

QString stringFromColor(const QVariant &color)
{
    return color.isNull() ?
        QString() : ('#' + QString::number(color.toInt(), 16));
}

} // namespace
