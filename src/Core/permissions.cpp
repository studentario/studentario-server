#include "permissions.h"

#include <QDebug>
#include <QVector>

using namespace Core;

namespace Core {

class PermissionsPrivate {
public:
    PermissionsPrivate();

private:
    friend class Permissions;
};

} // namespace

PermissionsPrivate::PermissionsPrivate()
{
}

Permissions::Permissions():
    d_ptr(new PermissionsPrivate())
{
}

Permissions::~Permissions() = default;

bool Permissions::can(Roles roles, Action action) const
{
    if (roles & Role::Master) {
        return true;
    }

    // TODO: rende isto configurabile
    static bool perms[][5] = {
        // Stud, Paren, Teach, Admin, Director
        { false, false, false, true,  true }, // CreateUser
        { false, false, false, true,  true }, // EditUser
        { false, false, false, true,  true }, // ViewUser
        { false, false, false, true,  true }, // ResetUserPassword
        { false, false, false, true,  true }, // DeleteUser

        { false, false, false, true,  true }, // CreateGroup
        { false, false, false, true,  true }, // EditGroup
        { false, false, false, true,  true }, // DeleteGroup
        { false, false, true,  true,  true }, // ViewGroupMembers
                                              //
        { false, false, true,  true,  true }, // CreateLesson
        { false, false, true,  true,  true }, // EditLesson
        { false, false, false,  true,  true }, // ViewLesson
        { false, false, true,  true,  true }, // DeleteLesson
        { false, false, true,  true,  true }, // ViewLessonMembers
        { false, false, true,  true,  true }, // EditAttendance

        { false, false, false, true,  true }, // EditLocation

        { false, false, true, true,  true }, // ViewTag
        { false, false, false, true,  true }, // EditTag
        { false, false, true,  true,  true }, // AttachTag
    };

    static const QVector<Role> allRoles = {
        // in ordine decrescente de potentia
        Role::Director,
        Role::Admin,
        Role::Teacher,
        Role::Parent,
        Role::Student,
    };
    int column = 4;
    for (auto r: allRoles) {
        int row = static_cast<int>(action);
        if (roles & r && perms[row][column]) return true;
        column--;
    }
    return false;
}

bool Permissions::canManageUser(Roles roles, Action action, Roles user) const
{
    if (roles & Role::Master) {
        return true;
    }

    // In omne casos, le controlo del rolo es exequite
    bool ret = can(roles, action);
    if (!ret) return ret;

    /* Controla que le rolo del actor es plus grande que le rolo del objecto */
    return static_cast<int>(roles) > static_cast<int>(user);
}
