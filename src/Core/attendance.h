#ifndef STUDENTARIO_CORE_ATTENDANCE_H
#define STUDENTARIO_CORE_ATTENDANCE_H

#include "group.h"
#include "user.h"

#include <QDateTime>
#include <QFlags>
#include <QString>
#include <variant>

namespace Core {

enum class EntityType {
    Unset,
    Student,
    Teacher,
    Admin,
    Director,
    Group,
};

struct Participant {
    int id; // student, teacher or group ID
    EntityType type = EntityType::Unset;

    bool operator==(const Participant &o) const {
        return o.id == this->id && o.type == this->type;
    }
};

/* Studentes e inseniantes qui participa como parte de un
 * gruppo habera le id e creatorId = -1 e invited = false.
 */
struct Participation {
    int id = -1;
    int creatorId = -1;
    QDateTime modified;
    int lessonId = -1;
    Participant participant;
    bool invited = false;
    bool attended = false;
    QString note;

    enum class Field {
        CreatorId = 1 << 0,
        Modified = 1 << 1,
        ParticipantId = 1 << 2,
        Invited = 1 << 3,
        Attended = 1 << 4,
        Note = 1 << 5,
    };
    Q_DECLARE_FLAGS(Fields, Field)
};

struct ParticipationUpdate {
    Participation p;
    Participation::Fields fields;
};

struct DetailedParticipation: public Participation {
    std::variant<User,Group> details;

    DetailedParticipation(const Participation *p):
        Participation(*p) {}
    DetailedParticipation(const Participation &p, const User &u):
        Participation(p), details(u) {}
    ~DetailedParticipation() = default;
};

} // namespace

Q_DECLARE_OPERATORS_FOR_FLAGS(Core::Participation::Fields)

#endif // STUDENTARIO_CORE_ATTENDANCE_H
