#include "users.h"

#include "authenticator.h"
#include "database.h"

#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>

using namespace Core;

static const QByteArray s_loginPattern = "auto_user_";
static const QHash<QString, Users::FieldSet> s_fieldNamesMap = {
    { "roles", Users::FieldId::Roles },
    { "name", Users::FieldId::Name },
    { "keywords", Users::FieldId::Keywords },
    { "contactInfo", Users::FieldId::ContactInfo },
    { "login", Users::FieldId::Login },
    { "birthDate", Users::FieldId::BirthDate },
    { "status", Users::FieldId::Status },
};

namespace Core {

using F = Users::Fields;
const QString F::UserId = QStringLiteral("userId");
const QString F::IsStudent = QStringLiteral("isStudent");
const QString F::IsParent = QStringLiteral("isParent");
const QString F::IsTeacher = QStringLiteral("isTeacher");
const QString F::IsAdmin = QStringLiteral("isAdmin");
const QString F::IsDirector = QStringLiteral("isDirector");
const QString F::IsMaster = QStringLiteral("isMaster");
const QString F::Login = QStringLiteral("login");
const QString F::Password = QStringLiteral("password");
const QString F::Salt = QStringLiteral("salt");
const QString F::PinCode = QStringLiteral("pinCode");
const QString F::Name = QStringLiteral("name");
const QString F::BirthDate = QStringLiteral("birthDate");
const QString F::Keywords = QStringLiteral("keywords");
const QString F::ContactInfo = QStringLiteral("contactInfo");
const QString F::Created = QStringLiteral("created");
const QString F::Status = QStringLiteral("status");
const QString F::StatusChangeTime = QStringLiteral("statusChangeTime");

const QString Values::StatusActive = QStringLiteral("active");
const QString Values::StatusInactive = QStringLiteral("inactive");

class UsersPrivate {
public:
    UsersPrivate();

    static QStringList parseContactInfo(const QJsonValue &value);
    static Error parseUser(const QJsonObject &user, User *u);

private:
    friend class Users;
    Database *m_db;
};

} // namespace

static User::Roles updateRoles(const QJsonObject::const_iterator &i,
                               const User::Roles startingRoles)
{
    User::Roles roles = startingRoles;
    if (i.key() == F::IsStudent) {
        roles.setFlag(User::Role::Student, i.value().toBool());
    } else if (i.key() == F::IsParent) {
        roles.setFlag(User::Role::Parent, i.value().toBool());
    } else if (i.key() == F::IsTeacher) {
        roles.setFlag(User::Role::Teacher, i.value().toBool());
    } else if (i.key() == F::IsAdmin) {
        roles.setFlag(User::Role::Admin, i.value().toBool());
    } else if (i.key() == F::IsDirector) {
        roles.setFlag(User::Role::Director, i.value().toBool());
    } else if (i.key() == F::IsMaster) {
        roles.setFlag(User::Role::Master, i.value().toBool());
    }
    return roles;
}

UsersPrivate::UsersPrivate():
    m_db(nullptr)
{
}

QStringList UsersPrivate::parseContactInfo(const QJsonValue &value)
{
    QStringList contactInfo;
    const QJsonArray jsonContactInfo = value.toArray();
    for (const QJsonValue &v: jsonContactInfo) {
        contactInfo.append(v.toString());
    }
    return contactInfo;
}

Error UsersPrivate::parseUser(const QJsonObject &user, User *u)
{
    using Role = User::Role;
    using Fields = Users::Fields;
    User::Roles roles;
    if (user.value(Fields::IsStudent).toBool()) roles |= Role::Student;
    if (user.value(Fields::IsParent).toBool()) roles |= Role::Parent;
    if (user.value(Fields::IsTeacher).toBool()) roles |= Role::Teacher;
    if (user.value(Fields::IsAdmin).toBool()) roles |= Role::Admin;
    if (user.value(Fields::IsDirector).toBool()) roles |= Role::Director;
    if (user.value(Fields::IsMaster).toBool()) roles |= Role::Master;
    if (Q_UNLIKELY(!roles)) {
        return Error(Error::InvalidParameters, "No roles given");
    }

    QStringList contactInfo = parseContactInfo(user.value(Fields::ContactInfo));

    QByteArray login = user.value(Fields::Login).toString().toUtf8();
    if (Q_UNLIKELY(login.startsWith(s_loginPattern))) {
        return Error::LoginAlreadyTaken;
    }

    QByteArray pin = user.value(Fields::PinCode).toString().toUtf8();
    QDateTime pinCreation =
        pin.isEmpty() ? QDateTime() : QDateTime::currentDateTime();
    QDate birthDate =
        QDate::fromString(user.value(Fields::BirthDate).toString(),
                          Qt::ISODate);

    *u = User {
        0, // id
        roles,
        login,
        QByteArray(), // hashed password
        QByteArray(), // salt
        pin, pinCreation,
        user.value(Fields::Name).toString(),
        birthDate,
        user.value(Fields::Keywords).toString(),
        contactInfo,
        QDateTime::currentDateTime(),
    };

    return Error::NoError;
}

Users::Users():
    d_ptr(new UsersPrivate())
{
}

Users::~Users() = default;

void Users::setDatabase(Database *db)
{
    Q_D(Users);
    d->m_db = db;
}

Error Users::addUser(const QJsonObject &user, int *userId)
{
    Q_D(Users);

    User u;
    Error error = d->parseUser(user, &u);
    if (Q_UNLIKELY(error)) {
        return error;
    }

    QByteArray password = user.value(Fields::Password).toString().toUtf8();
    u.salt = Authenticator::generateSalt();
    QByteArray hash = Authenticator::passwordHash(password, u.salt);
    if (Q_UNLIKELY(password.isEmpty())) {
        return Error(Error::InvalidParameters, "Password cannot be empty");
    }
    u.password = hash;

    if (Q_UNLIKELY(u.login.isEmpty())) {
        return Error(Error::InvalidParameters, "Login cannot be empty");
    }

    *userId = d->m_db->addUser(u);
    return *userId >= 0 ? Error::NoError : Error::DatabaseError;
}

Error Users::addLoginlessUser(const QJsonObject &user, int *userId)
{
    Q_D(Users);

    User u;
    Error error = d->parseUser(user, &u);
    if (Q_UNLIKELY(error)) {
        return error;
    }

    bool assignLogin = u.login.isEmpty();
    int remainingAttempts = 10;
    do {
        if (assignLogin) {
            int maxUserId = d->m_db->maxUserId();
            u.login = s_loginPattern + QByteArray::number(maxUserId + 1);
        }
        *userId = d->m_db->addUser(u);
        remainingAttempts--;
    } while (*userId < 0 && remainingAttempts > 0);

    return *userId >= 0 ? Error::NoError : Error::DatabaseError;
}

Error Users::updateUser(const User &user, const QJsonObject &changes)
{
    Q_D(Users);

    using F = Users::Fields;
    User u = user;
    User::Fields fields;

    for (auto i = changes.begin(); i != changes.end(); i++) {
        if (i.key() == F::Name) {
            u.name = i.value().toString();
            fields |= User::Field::Name;
        } else if (i.key() == F::Login) {
            u.login = i.value().toString();
            if (Q_UNLIKELY(u.login.isEmpty())) {
                return Error(Error::InvalidParameters,
                             "Login cannot be empty");
            }
            fields |= User::Field::Login;
        } else if (i.key() == F::Password) {
            QByteArray password = i.value().toString().toUtf8();
            if (Q_UNLIKELY(password.isEmpty())) {
                return Error(Error::InvalidParameters,
                             "Password cannot be empty");
            }
            QByteArray hash = Authenticator::passwordHash(password, u.salt);
            u.password = hash;
            fields |= User::Field::Password;
        } else if (i.key() == F::IsStudent ||
                   i.key() == F::IsParent ||
                   i.key() == F::IsTeacher ||
                   i.key() == F::IsAdmin ||
                   i.key() == F::IsDirector) {
            u.roles = updateRoles(i, u.roles);
            fields |= User::Field::Roles;
        } else if (i.key() == F::Keywords) {
            u.keywords = i.value().toString();
            fields |= User::Field::Keywords;
        } else if (i.key() == F::ContactInfo) {
            u.contactInfo = d->parseContactInfo(i.value());
            fields |= User::Field::ContactInfo;
        } else if (i.key() == F::BirthDate) {
            u.birthDate = QDate::fromString(i.value().toString(),
                                            Qt::ISODate);
            fields |= User::Field::BirthDate;
        } else {
            return Error(Error::InvalidParameters,
                         "Cannot update field " + i.key());
        }
    }

    bool ok = d->m_db->updateUser(u, fields);
    return ok ? Error::NoError : Error::DatabaseError;
}

Error Users::updatePin(int userId, const QByteArray &pin)
{
    Q_D(Users);

    if (pin.length() < 4) {
        return Error(Error::InvalidParameters, "Given PIN is too short");
    }
    User u;
    u.id = userId;
    u.pinCode = pin;
    u.pinCodeCreation = QDateTime::currentDateTime();
    return d->m_db->updateUser(u, User::Field::PinCode) ?
        Error::NoError : Error::DatabaseError;
}

Error Users::deleteUser(int userId)
{
    Q_D(Users);
    return d->m_db->deleteUser(userId);
}

Error Users::user(int id, User *u) const
{
    Q_D(const Users);
    return d->m_db->user(id, u) ? Error::NoError : Error::UserNotFound;
}

QVector<User> Users::users(const User::Filters &filters, User::Sort sort) const
{
    Q_D(const Users);
    return d->m_db->users(filters, sort);
}

User::Roles Users::rolesFromJson(const QJsonObject &json,
                                 const User::Roles startingRoles)
{
    User::Roles roles = startingRoles;
    for (auto i = json.begin(); i != json.end(); i++) {
        roles = updateRoles(i, roles);
    }
    return roles;
}

void Users::filtersFromJson(const QJsonObject &json,
                            User::Filters *filters)
{
    for (auto i = json.begin(); i != json.end(); i++) {
        if (i.key() == F::Name) {
            filters->pattern = i.value().toString();
        } else {
            filters->requestedRoles = updateRoles(i, filters->requestedRoles);
        }
    }
}

User::Sort Users::sortingFromJson(const QJsonObject &json)
{
    User::Sort sort = User::Sort::None;
    const QString sortField = json["sort"].toString();
    if (sortField == "id") {
        sort = User::Sort::Id;
    } else if (sortField == "-id") {
        sort = User::Sort::IdDesc;
    } else if (sortField == "name") {
        sort = User::Sort::Name;
    } else if (sortField == "-name") {
        sort = User::Sort::NameDesc;
    } else if (sortField == "login") {
        sort = User::Sort::Login;
    } else if (!sortField.isEmpty()) {
        qWarning() << "Unknown sort field:" << sortField;
    }
    return sort;
}

Users::FieldSet Users::fieldsFromNames(const QStringList &fieldNames)
{
    FieldSet fieldSet = FieldId::Empty;
    for (const QString &fieldName: fieldNames) {
        const auto i = s_fieldNamesMap.find(fieldName);
        if (i != s_fieldNamesMap.end()) {
            fieldSet |= i.value();
        }
    }
    return fieldSet;
}

User::Status Users::statusFromString(const QString &s)
{
    if (s == Values::StatusActive) return User::Status::Active;
    if (s == Values::StatusInactive) return User::Status::Inactive;
    qWarning() << "Unknown user status" << s;
    return User::Status::Unknown;
}

QString Users::statusToString(const User::Status s)
{
    switch (s) {
    case User::Status::Active: return Values::StatusActive;
    case User::Status::Inactive: return Values::StatusInactive;
    default:
        qWarning() << "Unknown user status code" << int(s);
    }
    return QString();
}

QJsonObject Users::jsonFromDb(const User &u, FieldSet fields)
{
    using F = Users::Fields;
    using Role = Users::Role;

    QJsonObject ret = {
        { F::UserId, u.id },
    };

    if (fields & FieldId::Roles) {
        ret.insert(F::IsStudent, u.roles.testFlag(Role::Student));
        ret.insert(F::IsParent, u.roles.testFlag(Role::Parent));
        ret.insert(F::IsTeacher, u.roles.testFlag(Role::Teacher));
        ret.insert(F::IsAdmin, u.roles.testFlag(Role::Admin));
        ret.insert(F::IsDirector, u.roles.testFlag(Role::Director));
        ret.insert(F::IsMaster, u.roles.testFlag(Role::Master));
    }

    if (fields & FieldId::Login) ret.insert(F::Login, u.login);
    if (fields & FieldId::Name) ret.insert(F::Name, u.name);
    if (fields & FieldId::BirthDate) {
        ret.insert(F::BirthDate, u.birthDate.toString(Qt::ISODate));
    }
    if (fields & FieldId::Keywords) ret.insert(F::Keywords, u.keywords);
    if (fields & FieldId::ContactInfo) {
        ret.insert(F::ContactInfo, QJsonArray::fromStringList(u.contactInfo));
    }
    if (fields & FieldId::Created && u.created.isValid()) {
        ret.insert(F::Created, u.created.toString(Qt::ISODate));
    }
    if (fields & FieldId::Status && u.statusChangeTime.isValid()) {
        ret.insert(F::Status, statusToString(u.status));
        ret.insert(F::StatusChangeTime,
                   u.statusChangeTime.toString(Qt::ISODate));
    }
    return ret;
}

QJsonArray Users::jsonFromDb(const QVector<User> &users, FieldSet fields)
{
    QJsonArray ret;
    for (const User &u: users) {
        ret.append(jsonFromDb(u, fields));
    }
    return ret;
}
