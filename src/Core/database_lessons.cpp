#include "database_p.h"

using namespace Core;

const QString lessonSelect = QStringLiteral(
    "SELECT DISTINCT l.id,"
    " l.creatorId, l.created, l.startTime, l.endTime, l.locationId "
    "FROM `Lessons` l "
);

const QString lessonNoteSelect = QStringLiteral(
    "SELECT n.id,"
    " n.creatorId, n.modified, n.text "
    "FROM `LessonNotes` n "
);

static void participantToDb(const Participant &p,
                            int *userId, bool *isTeacher, int *groupId)
{
    *userId = (p.type == EntityType::Group) ? -1 : p.id;
    *isTeacher = p.type == EntityType::Teacher;
    *groupId = (p.type == EntityType::Group) ? p.id : -1;
}

bool DatabasePrivate::lessonFromQuery(const QSqlQuery &q, Lesson *lesson)
{
    int i = 0;
    lesson->id = q.value(i++).toInt();
    lesson->creatorId = q.value(i++).toInt();
    lesson->created = q.value(i++).toDateTime();
    lesson->startTime = q.value(i++).toDateTime();
    lesson->endTime = q.value(i++).toDateTime();
    lesson->locationId = idFromDb(q.value(i++));
    return true;
}

static bool participationFromQuery(const QSqlQuery &q, Participation *p)
{
    int i = 0;
    p->id = q.value(i++).toInt();
    p->creatorId = q.value(i++).toInt();
    p->modified = q.value(i++).toDateTime();
    p->lessonId = q.value(i++).toInt();
    int groupId, userId;
    userId = q.value(i++).toInt();
    bool isTeacher = q.value(i++).toBool();
    groupId = q.value(i++).toInt();
    if (groupId > 0) {
        p->participant.type = EntityType::Group;
        p->participant.id = groupId;
    } else {
        p->participant.type = isTeacher ?
            EntityType::Teacher : EntityType::Student;
        p->participant.id = userId;
    }
    int attended = q.value(i++).toInt();
    p->attended = attended == 1;
    p->invited = q.value(i++).toBool();
    p->note = q.value(i++).toString();
    return true;
}

bool DatabasePrivate::lessonNoteFromQuery(const QSqlQuery &q, LessonNote *ln)
{
    int i = 0;
    ln->id = q.value(i++).toInt();
    ln->creatorId = q.value(i++).toInt();
    ln->modified = q.value(i++).toDateTime();
    ln->text = q.value(i++).toString();
    ln->typeId = -1; // Pro le momento nos non ha definite le typos
    return true;
}

bool DatabasePrivate::createTableLessons()
{
    QSqlDatabase &db = this->db();

    // Pro le calendario
    db.exec("CREATE TABLE Lessons ("
            + autoIncrement("id") + ","
            " creatorId INTEGER DEFAULT NULL,"
            " created DATETIME NOT NULL,"
            " startTime DATETIME NOT NULL,"
            " endTime DATETIME NOT NULL,"
            " locationId INTEGER,"
            " PRIMARY KEY(id),"
            " FOREIGN KEY(locationId) REFERENCES Locations(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE INDEX LessonTimeIdx ON `Lessons` (startTime, endTime)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    db.exec("CREATE INDEX LessonTime2Idx ON `Lessons` (endTime, startTime)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE Attendance ("
            + autoIncrement("id") + ","
            " creatorId INTEGER DEFAULT NULL,"
            " modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,"
            " lessonId INTEGER NOT NULL,"
            /* userId pote esser:
             * - le inseniante, con isTeacher=True
             * - un studente, pro lectiones individual, o variationes respecto
             *   al gruppo usual
             * Le default es -1 e non NULL, perque SQLite e MySQL considera
             * valores NULL sempre differente, e nostre coercition UNIQUE non
             * va functionar como nos expecta.
             */
            " userId INTEGER DEFAULT -1,"
            " isTeacher TINYINT NOT NULL DEFAULT 0,"
            " groupId INTEGER DEFAULT -1,"
            /* Nove record es addite pro cata studente qui es parte del
             * gruppos. -1 significa que le presentia non ha essite registrate.
             */
            " attended TINYINT NOT NULL DEFAULT -1,"
            // True si le studente non esseva parte del gruppo
            " invited TINYINT NOT NULL DEFAULT 0,"
            " note VARCHAR(160) DEFAULT NULL,"
            " PRIMARY KEY(id),"
            " UNIQUE(userId, groupId, lessonId),"
            " UNIQUE(lessonId, userId, groupId)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE LessonNotes ("
            + autoIncrement("id") + ","
            " creatorId INTEGER DEFAULT NULL,"
            " modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,"
            /* typeId essera definite in un altere tabula, e essera pro exemplo
             * "compitos per casa", "plano del lection", etc.
             */
            " typeId INTEGER DEFAULT NULL,"
            " text TEXT,"
            " PRIMARY KEY(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    /* Crea un indice temporal del notas, pro facilitar le modification del
     * notas le plus recente. */
    db.exec("CREATE INDEX LessonNotesIdx ON `LessonNotes` (modified)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE LessonNotesGlue ("
            " lessonId INTEGER NOT NULL,"
            // Le ordine in le qual le notas essera monstrate
            " priority INTEGER DEFAULT 0,"
            " noteId INTEGER NOT NULL,"
            " PRIMARY KEY(lessonId, priority, noteId)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE LessonTags ("
            " lessonId INTEGER NOT NULL,"
            " tagId INTEGER NOT NULL,"
            " PRIMARY KEY(lessonId, tagId),"
            " UNIQUE(tagId, lessonId),"
            " FOREIGN KEY(lessonId) REFERENCES Lessons(id),"
            " FOREIGN KEY(tagId) REFERENCES Tags(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE TABLE LessonNoteTags ("
            " noteId INTEGER NOT NULL,"
            " tagId INTEGER NOT NULL,"
            " PRIMARY KEY(noteId, tagId),"
            " UNIQUE(tagId, noteId),"
            " FOREIGN KEY(noteId) REFERENCES LessonNotes(id),"
            " FOREIGN KEY(tagId) REFERENCES Tags(id)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    return true;
}

int Database::addLesson(const Lesson &lesson)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare("INSERT INTO `Lessons` ("
              " creatorId, created, startTime, endTime, locationId) "
              "VALUES ("
              " ?, ?, ?, ?, ?)");
    q.addBindValue(lesson.creatorId);
    q.addBindValue(lesson.created);
    q.addBindValue(lesson.startTime);
    q.addBindValue(lesson.endTime);
    q.addBindValue(d->idToDb(lesson.locationId));

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "addLesson failed:" << q.lastError();
        return -1;
    }

    return q.lastInsertId().toInt();
}

Error Database::updateLesson(const Lesson &lesson, Lesson::Fields fields)
{
    Q_D(Database);

    QStringList assignments;
    if (fields & Lesson::Field::CreatorId) {
        assignments.append("creatorId = :creatorId");
    }
    if (fields & Lesson::Field::StartTime) {
        assignments.append("startTime = :startTime");
    }
    if (fields & Lesson::Field::EndTime) {
        assignments.append("endTime = :endTime");
    }
    if (fields & Lesson::Field::LocationId) {
        assignments.append("locationId = :locationId");
    }

    if (Q_UNLIKELY(assignments.isEmpty())) {
        qWarning() << "Nothing to update";
        return Error::NoError;
    }

    QString query = "UPDATE `Lessons` SET " +
        assignments.join(", ") +
        " WHERE id = :id";
    QSqlQuery q(d->db());
    bool ok = q.prepare(query);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error preparing query" << q.lastError();
        return Error::DatabaseError;
    }

    q.bindValue(":id", lesson.id);

    q.bindValue(":creatorId", lesson.creatorId);
    q.bindValue(":startTime", lesson.startTime);
    q.bindValue(":endTime", lesson.endTime);
    q.bindValue(":locationId", d->idToDb(lesson.locationId));

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "updateLesson failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return Error::NoError;
}

Error Database::deleteLesson(int id)
{
    Q_D(Database);

    QSqlQuery q(d->db());
    q.prepare("DELETE FROM `Lessons` WHERE id = :id");
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "deleteLesson failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return q.numRowsAffected() == 0 ? Error::LessonNotFound : Error::NoError;
}

bool Database::lesson(int id, Lesson *lesson) const
{
    Q_D(const Database);

    QSqlQuery q =
        d->prepareQuery(lessonSelect + QStringLiteral("WHERE l.id = :id"));
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    if (Q_UNLIKELY(!q.next())) {
        qDebug() << "Lesson not found" << id;
        return false;
    }
    return d->lessonFromQuery(q, lesson);
}

QVector<Lesson> Database::lessons(const QDateTime &since, const QDateTime &to,
                                  int locationId) const
{
    Q_D(const Database);

    QVector<Lesson> lessons;

    QSqlQuery q(d->db());
    QStringList conditions;
    if (!since.isNull()) {
        conditions.append(QStringLiteral("l.endTime >= ?"));
    }
    if (!to.isNull()) {
        conditions.append(QStringLiteral("l.startTime <= ?"));
    }
    if (locationId != -1) {
        conditions.append(QStringLiteral("l.locationId = ?"));
    }
    QString query = lessonSelect;
    if (!conditions.isEmpty()) {
        query += QStringLiteral("WHERE ") + conditions.join(" AND ");
    }
    query += QStringLiteral(" ORDER BY l.startTime");
    bool ok = q.prepare(query);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error preparing query" << q.lastError();
        return lessons;
    }
    if (!since.isNull()) q.addBindValue(since);
    if (!to.isNull()) q.addBindValue(to);
    if (locationId != -1) q.addBindValue(locationId);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError() << q.executedQuery();
        return lessons;
    }

    while (q.next()) {
        lessons.append(Lesson());
        d->lessonFromQuery(q, &lessons.last());
    }
    return lessons;
}

bool Database::inviteToLesson(int creatorId, int lessonId,
                              const QVector<int> &entityIds,
                              EntityType type)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare(QStringLiteral("INSERT INTO `Attendance` ("
                             "creatorId, lessonId, "
                             "userId, isTeacher, groupId, invited) "
                             "VALUES (?, ?, ?, ?, ?, ?)"));
    for (int entityId: entityIds) {
        q.addBindValue(creatorId);
        q.addBindValue(lessonId);
        q.addBindValue(type == EntityType::Group ? -1 : entityId);
        q.addBindValue(type == EntityType::Teacher);
        q.addBindValue(type == EntityType::Group ? entityId : -1);
        q.addBindValue(type == EntityType::Student);
        if (Q_UNLIKELY(!q.exec())) {
            qWarning() << "Failed to run query" << q.lastError();
            return false;
        }
    }

    return true;
}

bool Database::uninviteFromLesson(int lessonId, const QVector<int> &entityIds,
                                  EntityType type)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    QString query = QStringLiteral(
        "DELETE FROM `Attendance` WHERE lessonId = ? AND attended = -1 AND ");
    if (type == EntityType::Group) {
        query += QStringLiteral("groupId = ?");
    } else {
        int isTeacher = (type == EntityType::Teacher) ? 1 : 0;
        query += QString("userId = ? AND isTeacher = %1").arg(isTeacher);
    }
    q.prepare(query);
    for (int entityId: entityIds) {
        q.addBindValue(lessonId);
        q.addBindValue(entityId);
        if (Q_UNLIKELY(!q.exec())) {
            qWarning() << "Failed to run query" << q.lastError();
            return false;
        }
    }

    return true;
}

bool Database::lessonsByParticipants(const Participants &participants,
                                     Lessons *lessons,
                                     const QDateTime &since,
                                     const QDateTime &to) const
{
    Q_D(const Database);

    QStringList conditions;
    if (!since.isNull()) conditions.append("l.endTime >= :since");
    if (!to.isNull()) conditions.append("l.startTime <= :to");

    QStringList entityWheres;
    int i = 0;
    for (const Participant p: participants) {
        QString field = (p.type == EntityType::Group) ? "groupId" : "userId";
        QString where =
            QString("(a.%1 = :id%2 AND a.isTeacher = :isTeacher%2)").
            arg(field).arg(i);
        if (p.type == EntityType::Student || p.type == EntityType::Teacher) {
            QString table = (p.type == EntityType::Student) ?
                "Students" : "Teachers";
            where = QString("(%1 OR "
                "a.groupId IN (SELECT groupId FROM %2 WHERE userId = :id%3)"
                ")").arg(where).arg(table).arg(i);
        }
        entityWheres.append(where);
        i++;
    }
    QString query = lessonSelect + QStringLiteral(
        "INNER JOIN `Attendance` a ON a.lessonId = l.id "
        "WHERE (") + entityWheres.join(" OR ") + ')';
    if (!conditions.isEmpty()) {
        query += QStringLiteral(" AND ") + conditions.join(" AND ");
    }
    query += QStringLiteral(" ORDER BY l.startTime");

    QSqlQuery q(d->db());
    q.prepare(query);
    i = 0;
    for (const Participant p: participants) {
        q.bindValue(QStringLiteral(":id%1").arg(i), p.id);
        q.bindValue(QStringLiteral(":isTeacher%1").arg(i),
                    p.type == EntityType::Teacher ? 1 : 0);
        i++;
    }
    if (!since.isNull()) q.bindValue(":since", since);
    if (!to.isNull()) q.bindValue(":to", to);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError() << q.executedQuery();
        return false;
    }

    while (q.next()) {
        lessons->append(Lesson());
        d->lessonFromQuery(q, &lessons->last());
    }
    return true;
}

bool Database::updateAttendance(int lessonId, const Participations &participations)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare(QStringLiteral(
        "REPLACE INTO `Attendance` ("
        " creatorId, lessonId,"
        " userId, isTeacher, groupId,"
        " invited, attended, note) "
        "VALUES ("
        " :creatorId, :lessonId,"
        " :userId, :isTeacher, :groupId,"
        " :invited, :attended, :note)"));
    for (const Participation &p: participations) {
        q.bindValue(":creatorId", p.creatorId);
        q.bindValue(":lessonId", lessonId);
        int userId, groupId;
        bool isTeacher;
        participantToDb(p.participant, &userId, &isTeacher, &groupId);
        q.bindValue(":userId", userId);
        q.bindValue(":isTeacher", isTeacher);
        q.bindValue(":groupId", groupId);
        q.bindValue(":invited", p.invited);
        q.bindValue(":attended", p.attended);
        q.bindValue(":note", p.note);
        if (Q_UNLIKELY(!q.exec())) {
            qWarning() << "Failed to run query" << q.lastError() << q.executedQuery();
            return false;
        }
    }

    return true;
}

bool Database::lessonAttendance(int lessonId,
                                Participations *participations) const
{
    Q_D(const Database);
    QString query = QStringLiteral(
        "SELECT id, creatorId, modified, lessonId,"
        " userId, isTeacher, groupId,"
        " attended, invited, note "
        "FROM `Attendance` "
        "WHERE lessonId = :id "
        "ORDER BY userId, groupId");

    QSqlQuery q(d->db());
    q.prepare(query);
    q.bindValue(":id", lessonId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    while (q.next()) {
        participations->append(Participation());
        participationFromQuery(q, &participations->last());
    }
    return true;
}

int Database::addLessonNote(const LessonNote &lessonNote)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare("INSERT INTO `LessonNotes` ("
              " creatorId, modified, text) "
              "VALUES ("
              " ?, ?, ?)");
    q.addBindValue(lessonNote.creatorId);
    q.addBindValue(lessonNote.modified);
    q.addBindValue(lessonNote.text);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "addLessonNote failed:" << q.lastError();
        return -1;
    }

    return q.lastInsertId().toInt();
}

Error Database::updateLessonNote(const LessonNote &lessonNote)
{
    Q_D(Database);

    QString query = QStringLiteral(
        "UPDATE `LessonNotes` SET text = :text"
        " WHERE id = :id");
    QSqlQuery q(d->db());
    q.prepare(query);
    q.bindValue(":id", lessonNote.id);

    q.bindValue(":text", lessonNote.text);
    q.bindValue(":modified", QDateTime::currentDateTime());

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "updateLessonNote failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return Error::NoError;
}

Error Database::deleteLessonNote(int id)
{
    Q_D(Database);

    QSqlQuery q(d->db());
    q.prepare("DELETE FROM `LessonNotes` WHERE id = :id");
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "deleteLessonNote failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return q.numRowsAffected() == 0 ? Error::LessonNoteNotFound : Error::NoError;
}

bool Database::lessonNote(int id, LessonNote *lessonNote) const
{
    Q_D(const Database);

    QSqlQuery q =
        d->prepareQuery(lessonNoteSelect + QStringLiteral("WHERE n.id = :id"));
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    if (Q_UNLIKELY(!q.next())) {
        qDebug() << "LessonNote not found" << id;
        return false;
    }
    return d->lessonNoteFromQuery(q, lessonNote);
}

QVector<LessonNote> Database::lessonNotes(int lessonId, int creatorId) const
{
    Q_D(const Database);

    QVector<LessonNote> lessonNotes;

    QSqlQuery q(d->db());
    QStringList conditions;
    QString query = lessonNoteSelect;
    QString orderBy = QStringLiteral("n.modified DESC");
    if (lessonId != -1) {
        query += " INNER JOIN `LessonNotesGlue` g ON g.noteId = n.id";
        orderBy = QStringLiteral("g.priority DESC, ") + orderBy;
        conditions.append(QStringLiteral("g.lessonId = ?"));
    }
    if (creatorId != -1) {
        conditions.append(QStringLiteral("n.creatorId = ?"));
    }
    if (!conditions.isEmpty()) {
        query += QStringLiteral(" WHERE ") + conditions.join(" AND ");
    }
    query += QStringLiteral(" ORDER BY ") + orderBy;
    bool ok = q.prepare(query);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error preparing query" << q.lastError();
        return lessonNotes;
    }
    if (lessonId != -1) q.addBindValue(lessonId);
    if (creatorId != -1) q.addBindValue(creatorId);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError() << q.executedQuery();
        return lessonNotes;
    }

    while (q.next()) {
        lessonNotes.append(LessonNote());
        d->lessonNoteFromQuery(q, &lessonNotes.last());
    }
    return lessonNotes;
}

bool Database::attachLessonNote(int lessonId, int noteId, int priority)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare(QStringLiteral("REPLACE INTO LessonNotesGlue ("
                             "lessonId, priority, noteId) "
                             "VALUES (?, ?, ?)"));
    q.addBindValue(lessonId);
    q.addBindValue(priority);
    q.addBindValue(noteId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    return true;
}

bool Database::detachLessonNote(int lessonId, int noteId)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare(QStringLiteral("DELETE FROM LessonNotesGlue "
                             "WHERE lessonId = ? AND noteId = ?"));
    q.addBindValue(lessonId);
    q.addBindValue(noteId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    return true;
}
