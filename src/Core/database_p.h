#ifndef STUDENTARIO_CORE_DATABASE_P_H
#define STUDENTARIO_CORE_DATABASE_P_H

#include "database.h"

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QStringView>

using namespace Core;

namespace Core {

struct TableInfo {
    QString table;
    QString field;
    QString select;
    QString orderBy;
};

struct ForeignKeyDesc {
    QString table;
    QString column;
    QString reference;
};

class DatabasePrivate {
public:
    DatabasePrivate(const QVariantMap &configuration,
                    const QString &connectionName);
    ~DatabasePrivate();

    bool init();

    QString autoIncrement(const QString &name) const;
    bool addForeignColumn(const QString &table, const QString &column,
                          const QString &reference);
    bool addForeignColumns(const QVector<ForeignKeyDesc> &columns);
    bool ensureConnected();
    bool setDbVersion(int version);
    int dbVersion() const;
    bool createTableUsers(const QString &tableName);
    bool createIndexesForUsers();
    bool createTableStatusChanges();
    bool createTableTags();
    bool createTableUserGroupTags();
    bool createTableLocationTags();
    bool createTableLessons();
    bool createTableLocations();
    bool createTableRelations();
    bool createTriggers();
    bool updateFrom(int version);
    bool createDb(int latestDbVersion);

    bool createOrUpdateDb();

    QSqlDatabase &db() { return m_db; }
    const QSqlDatabase &db() const { return m_db; }
    QSqlQuery prepareQuery(const QString &query) const;

    static QVariant idToDb(int id) {
        return id > 0 ? id : QVariant();
    }
    static int idFromDb(const QVariant &id) {
        return id.isNull() ? -1 : id.toInt();
    }

    static bool userFromQuery(const QSqlQuery &query, User *user);
    static bool statusChangeFromQuery(const QSqlQuery &query,
                                      StatusChange *statusChange);

    QVector<Relation> relations(const QString &field, int entityId,
                                Relation::Type type) const;
    static bool groupFromQuery(const QSqlQuery &query, Group *group);
    static bool lessonFromQuery(const QSqlQuery &query, Lesson *lesson);
    static bool lessonNoteFromQuery(const QSqlQuery &query, LessonNote *note);
    static bool locationFromQuery(const QSqlQuery &query, Location *location);

    bool findLastStatusChange(int userId, const QDateTime &before,
                              StatusChange *statusChange);
    bool updateUserStatus(int userId);

    static QStringView tableNameForGroupRole(User::Role role);
    bool addUsersToGroup(int groupId, const QVector<int> &studentIds,
                         QStringView table);
    bool removeUsersFromGroup(int groupId, const QVector<int> &studentIds,
                              QStringView table);
    bool groupsForUser(int studentId, Database::Groups *groups,
                       QStringView table) const;
    bool usersForGroup(int groupId, Database::Users *users,
                       QStringView table) const;

    bool addTagToIds(int tagId, const QVector<int> &entityIds,
                     const TableInfo &table);
    bool removeTagFromIds(int tagId, const QVector<int> &entityIds,
                          const TableInfo &table);
    bool tagsForId(int entityId, Database::Tags *tags,
                   const TableInfo &table) const;
    QSqlQuery entitiesForTag(int tagId, const TableInfo &table) const;
    bool tagExists(int tagId) const;
    bool tagHasChildren(int tagId) const;

private:
    friend class Database;
    friend class Database::Transaction;
    QVariantMap m_conf;
    QString m_connectionName;
    QSqlDatabase m_db;
};

} // namespace

extern const QString userSelect;
extern const QString groupSelect;
extern const QString lessonSelect;
extern const QString lessonNoteSelect;
extern const QString locationSelect;

#endif // STUDENTARIO_CORE_DATABASE_P_H
