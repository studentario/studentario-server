#ifndef STUDENTARIO_CORE_ERROR_H
#define STUDENTARIO_CORE_ERROR_H

#include <QString>

namespace Core {

class Error {
public:
    enum Code {
        NoError = 0,
        Unknown, // Solo pro uso temporanee
        DatabaseError,
        MissingRequiredField,
        PermissionDenied,
        LoginNotFound,
        InvalidPassword,
        NeedsAuthentication,
        InvalidParameters,
        UserNotFound,
        GroupNotFound,
        LoginAlreadyTaken,
        TagNotFound,
        LessonNotFound,
        LessonNoteNotFound,
        UserStatusAlreadySet,
        UserStatusNotFound,
        RelationNotFound,
        LocationNotFound,
    };

    Error(Code code = NoError, const QString &message = {}):
        m_code(code),
        m_message(message) {}
    virtual ~Error() = default;

    bool isError() const { return m_code != NoError; }
    operator bool() const { return isError(); }

    Code code() const { return m_code; }
    QString message() const { return m_message; }

private:
    Code m_code;
    QString m_message;
};

} // namespace

#endif // STUDENTARIO_CORE_ERROR_H
