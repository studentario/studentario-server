#include "database_p.h"

#include <QDebug>

using namespace Core;

static const QString relationSelect = QStringLiteral(
    "SELECT id,"
    " subjectId, relationType, objectId "
    "FROM `Relations` "
);

static bool relationFromQuery(const QSqlQuery &q, Relation *relation)
{
    int i = 0;
    relation->id = q.value(i++).toInt();
    relation->subjectId = q.value(i++).toInt();
    relation->type = static_cast<Relation::Type>(q.value(i++).toInt());
    relation->objectId = q.value(i++).toInt();
    return true;
}

bool DatabasePrivate::createTableRelations()
{
    QSqlDatabase &db = this->db();
    /* Usate pro:
     * - accompaniatores isParent studente
     */
    db.exec("CREATE TABLE `Relations` ("
            + autoIncrement("id") + ","
            " subjectId INTEGER NOT NULL,"
            " relationType INTEGER NOT NULL,"
            " objectId INTEGER NOT NULL,"
            " PRIMARY KEY(id),"
            " UNIQUE(subjectId, relationType, objectId)"
            ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    db.exec("CREATE INDEX RelationsIdx ON `Relations` (objectId, relationType)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    return true;
}

QVector<Relation> DatabasePrivate::relations(const QString &field,
                                             int entityId,
                                             Relation::Type type) const
{
    QVector<Relation> relations;

    QSqlQuery q(db());
    QString query = relationSelect +
        QStringLiteral("WHERE %1 = %2").arg(field).arg(entityId);
    if (type != Relation::Type::Invalid) {
        query += QStringLiteral(" AND relationType = %1").
            arg(static_cast<int>(type));
    } else {
        query += QStringLiteral(" ORDER BY relationType");
    }

    if (Q_UNLIKELY(!q.exec(query))) {
        qWarning() << "Failed to run query" << q.lastError();
        return relations;
    }

    while (q.next()) {
        relations.append(Relation());
        relationFromQuery(q, &relations.last());
    }
    return relations;
}

int Database::addRelation(const Relation &relation)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare("INSERT INTO `Relations` ("
              " subjectId, relationType, objectId) "
              "VALUES ("
              " ?, ?, ?)");
    q.addBindValue(relation.subjectId);
    q.addBindValue(static_cast<int>(relation.type));
    q.addBindValue(relation.objectId);

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "addRelation failed:" << q.lastError();
        return -1;
    }

    return q.lastInsertId().toInt();
}

Error Database::deleteRelation(int id)
{
    Q_D(Database);

    QSqlQuery q(d->db());
    q.prepare("DELETE FROM `Relations` WHERE id = :id");
    q.bindValue(":id", id);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "deleteRelation failed:" << q.lastError();
        return Error::DatabaseError;
    }

    return q.numRowsAffected() == 0 ? Error::RelationNotFound : Error::NoError;
}

QVector<Relation> Database::relationsBySubject(int subjectId,
                                               Relation::Type type) const
{
    Q_D(const Database);
    return d->relations("subjectId", subjectId, type);
}

QVector<Relation> Database::relationsByObject(int objectId,
                                              Relation::Type type) const
{
    Q_D(const Database);
    return d->relations("objectId", objectId, type);
}

bool Database::relationExists(int subjectId, Relation::Type type,
                              int objectId) const
{
    Q_D(const Database);
    QSqlQuery q(d->db());
    QString query = relationSelect +
        QStringLiteral("WHERE subjectId = %1"
                       " AND relationType = %2"
                       " AND objectId = %3").
        arg(subjectId).arg(static_cast<int>(type)).arg(objectId);

    if (Q_UNLIKELY(!q.exec(query))) {
        qWarning() << "Failed to run query" << q.lastError();
        return false;
    }

    return q.next();
}
