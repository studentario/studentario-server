#ifndef STUDENTARIO_CORE_LOCATION_H
#define STUDENTARIO_CORE_LOCATION_H

#include <QDateTime>
#include <QFlags>
#include <QString>

namespace Core {

struct Location {
    int id;
    QString name;
    QString description;
    int creatorId = -1;
    QDateTime created;
    QString color;
    QString icon;

    bool operator==(const Location &o) const {
        return id == o.id &&
            name == o.name &&
            description == o.description &&
            creatorId == o.creatorId &&
            created == o.created &&
            color == o.color &&
            icon == o.icon;
    }

    enum class Field {
        Name = 1 << 0,
        Description = 1 << 1,
        CreatorId = 1 << 2,
        Created = 1 << 3,
        Color = 1 << 4,
        Icon = 1 << 5,
    };
    Q_DECLARE_FLAGS(Fields, Field)
};

} // namespace

Q_DECLARE_OPERATORS_FOR_FLAGS(Core::Location::Fields)

#endif // STUDENTARIO_CORE_LOCATION_H
