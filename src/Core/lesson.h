#ifndef STUDENTARIO_CORE_LESSON_H
#define STUDENTARIO_CORE_LESSON_H

#include <QDateTime>
#include <QFlags>
#include <QString>

namespace Core {

struct Lesson {
    int id;
    int creatorId = -1;
    QDateTime created;
    QDateTime startTime;
    QDateTime endTime;
    int locationId = -1;

    bool operator==(const Lesson &o) const {
        return id == o.id &&
            creatorId == o.creatorId &&
            created == o.created &&
            startTime == o.startTime &&
            endTime == o.endTime &&
            locationId == o.locationId;
    }

    enum class Field {
        CreatorId = 1 << 2,
        Created = 1 << 3,
        StartTime = 1 << 4,
        EndTime = 1 << 5,
        LocationId = 1 << 6,
    };
    Q_DECLARE_FLAGS(Fields, Field)
};

struct LessonNote {
    int id;
    int creatorId;
    QDateTime modified;
    int typeId = -1;
    QString text;

    bool operator==(const LessonNote &o) const {
        return id == o.id &&
            creatorId == o.creatorId &&
            modified == o.modified &&
            typeId == o.typeId &&
            text == o.text;
    }
};

} // namespace

Q_DECLARE_OPERATORS_FOR_FLAGS(Core::Lesson::Fields)

#endif // STUDENTARIO_CORE_LESSON_H
