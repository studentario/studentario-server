#ifndef STUDENTARIO_CORE_GROUPS_H
#define STUDENTARIO_CORE_GROUPS_H

#include "error.h"
#include "group.h"
#include "user.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QScopedPointer>
#include <QString>

namespace Core {

class Database;

class GroupsPrivate;
class Groups {
public:
    Groups();
    virtual ~Groups();

    void setDatabase(Database *db);

    Error addGroup(const QJsonObject &group, int *groupId);
    Error updateGroup(const Group &group, const QJsonObject &changes);
    Error deleteGroup(int groupId);
    Error group(int id, Group *group) const;
    QVector<Group> groups(const QString &pattern = {}) const;

    Error groupsForUser(int userId, QVector<Group> *groups,
                        User::Role role, const QString &pattern = {}) const;
    Error addUsersToGroup(int groupId, QVector<int> &userIds, User::Role role);
    Error removeUsersFromGroup(int groupId, const QVector<int> &userIds,
                               User::Role role);
    Error usersForGroup(int groupId, QVector<User> *users,
                        User::Role role) const;

    static QJsonObject jsonFromDb(const Group &group);
    static QJsonArray jsonFromDb(const QVector<Group> &groups);

    struct Fields {
    static const QString GroupId;
    static const QString Name;
    static const QString Description;
    static const QString Created;
    static const QString LocationId;
    };

private:
    Q_DECLARE_PRIVATE(Groups)
    QScopedPointer<GroupsPrivate> d_ptr;
};

} // namespace

#endif // STUDENTARIO_CORE_GROUPS_H
