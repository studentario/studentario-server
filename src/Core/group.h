#ifndef STUDENTARIO_CORE_GROUP_H
#define STUDENTARIO_CORE_GROUP_H

#include <QDateTime>
#include <QFlags>
#include <QString>

namespace Core {

struct Group {
    int id;
    QString name;
    QString description;
    QDateTime created;
    int locationId = -1;

    bool operator==(const Group &o) const {
        return id == o.id &&
            name == o.name &&
            description == o.description &&
            created == o.created &&
            locationId == o.locationId;
    }

    enum class Field {
        Name = 1 << 0,
        Description = 1 << 1,
        LocationId = 1 << 2,
    };
    Q_DECLARE_FLAGS(Fields, Field)
};

} // namespace

Q_DECLARE_OPERATORS_FOR_FLAGS(Core::Group::Fields)

#endif // STUDENTARIO_CORE_GROUP_H
