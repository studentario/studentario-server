#ifndef STUDENTARIO_CORE_AUTHENTICATOR_H
#define STUDENTARIO_CORE_AUTHENTICATOR_H

#include "error.h"
#include "user.h"

#include <QJsonObject>
#include <QScopedPointer>

namespace Core {

class Database;

class AuthenticatorPrivate;
class Authenticator {
public:
    enum class Status {
        NotAuthenticated = 0,
        AuthenticatedWithPin,
        AuthenticatedWithPassword,
    };

    Authenticator();
    virtual ~Authenticator();

    void setDatabase(Database *db);

    static QByteArray generateSalt();
    static QByteArray passwordHash(const QByteArray &password,
                                   const QByteArray &salt);
    Error login(const QJsonObject &loginData);

    Status status() const;
    const User &authenticatedUser() const;

private:
    Q_DECLARE_PRIVATE(Authenticator)
    QScopedPointer<AuthenticatorPrivate> d_ptr;
};

} // namespace

#endif // STUDENTARIO_CORE_AUTHENTICATOR_H
