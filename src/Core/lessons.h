#ifndef STUDENTARIO_CORE_LESSONS_H
#define STUDENTARIO_CORE_LESSONS_H

#include "attendance.h"
#include "error.h"
#include "lesson.h"
#include "user.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QScopedPointer>
#include <QString>

namespace Core {

class Database;

class LessonsPrivate;
class Lessons {
public:
    Lessons();
    virtual ~Lessons();

    void setDatabase(Database *db);

    Error addLesson(const QJsonObject &lesson, int *lessonId);
    Error updateLesson(const Lesson &lesson, const QJsonObject &changes);
    Error deleteLesson(int lessonId);
    Error lesson(int id, Lesson *lesson) const;
    QVector<Lesson> lessons(const QJsonObject &filters) const;

    // Objecto con claves "teachers", "students" e "groups"
    Error inviteToLesson(int creatorId, int lessonId,
                         const QJsonObject &invitees);
    Error uninviteFromLesson(int lessonId, const QJsonObject &invitees);
    using Participants = QVector<Participant>;
    Error lessonsByParticipants(const Participants &participants,
                                QVector<Lesson> *lessons,
                                const QJsonObject &filters) const;
    // Objecto con claves "teachers" e "students"
    Error updateAttendance(int lessonId, const QJsonObject &participations);
    Error lessonAttendance(int lessonId,
                           QVector<DetailedParticipation> *participations,
                           const QJsonObject &filters) const;

    static QJsonObject jsonFromDb(const Lesson &lesson);
    static QJsonArray jsonFromDb(const QVector<Lesson> &lessons);

    static QJsonObject jsonFromDb(const Participation &participation);
    // Objecto con claves "teachers", "students" e "groups"
    static QJsonObject jsonFromDb(const QVector<DetailedParticipation> &participations,
                                  const QStringList &userFields = {});

    struct Fields {
    // Lessons
    static const QString LessonId;
    static const QString CreatorId;
    static const QString Created;
    static const QString StartTime;
    static const QString EndTime;
    static const QString LocationId;

    // Attendance
    static const QString Groups;
    static const QString Students;
    static const QString Teachers;
    static const QString Modified;
    static const QString Attended;
    static const QString Invited;
    static const QString Note;

    // Only for filters
    static const QString Since;
    static const QString To;
    };

private:
    Q_DECLARE_PRIVATE(Lessons)
    QScopedPointer<LessonsPrivate> d_ptr;
};

} // namespace

#endif // STUDENTARIO_CORE_LESSONS_H
