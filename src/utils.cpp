#include "utils.h"

#include <Cutelyst/Context>
#include <Cutelyst/Plugins/Session/Session>
#include <Cutelyst/Request>
#include <QDebug>
#include <QJsonArray>

using namespace Cutelyst;

Core::User::Roles Utils::authenticatedUserRoles(Context *c)
{
    int roles = Session::value(c, "userRoles").toInt();
    return Core::User::Roles(roles);
}

QJsonObject Utils::requestQueryAsJson(Context *c)
{
    const Cutelyst::ParamsMultiMap query = c->request()->queryParameters();
    QJsonObject ret;
    for (auto i = query.begin(); i != query.end(); i++) {
        bool reformatted = false;
        QJsonValue value;
        if (i.key() == "id" || i.key().endsWith("Id")) {
            const QString possibleNumber = i.value();
            bool ok = false;
            int number;
            number = possibleNumber.toInt(&ok);
            if (ok) {
                value = number;
                reformatted = true;
            }
        } else if (i.key() == "ids" || i.key().endsWith("Ids")) {
            const QStringList possibleNumbers = i.value().split(',');
            // Le clave pote apparer plure vices: adde al vector existente
            QJsonArray numbers = ret.value(i.key()).toArray();
            bool ok = true;
            for (const QString &possibleNumber: possibleNumbers) {
                int number;
                number = possibleNumber.toInt(&ok);
                if (!ok) break;

                numbers.append(number);
            }
            if (ok) {
                reformatted = true;
                value = numbers;
            }
        } else if (i.key().length() >= 3 &&
                   i.key().startsWith("is") && i.key()[2].isUpper()) {
            // booleano
            const QString possibleBool = i.value();
            if (possibleBool == "0" || possibleBool == "false") {
                value = false;
                reformatted = true;
            } else if (possibleBool == "1" || possibleBool == "true") {
                value = true;
                reformatted = true;
            }
        }

        if (!reformatted) {
            value = i.value();
        }
        ret.insert(i.key(), value);
    }
    return ret;
}

bool Utils::idsFromString(const QString &commaIds, QVector<int> *ids)
{
    const QStringList parts = commaIds.split(',');
    ids->reserve(parts.count());
    for (const QString &part: parts) {
        bool ok = false;
        int id = part.toInt(&ok);
        if (!ok) return false;
        ids->append(id);
    }
    return true;
}
