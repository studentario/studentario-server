#ifndef MASTER_H
#define MASTER_H

#include "database_controller.h"

using namespace Cutelyst;

class Master: public DatabaseController
{
    Q_OBJECT
public:
    explicit Master(QObject *parent = nullptr);
    ~Master();

    C_ATTR(create, :Path :ActionClass(REST))
    void create(Context *) {};

    C_ATTR(create_POST, :Private)
    void create_POST(Context *c);
};

#endif // MASTER_H
