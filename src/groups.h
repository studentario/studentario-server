#ifndef GROUPS_H
#define GROUPS_H

#include "database_controller.h"

#include "Core/groups.h"
#include "Core/locations.h"
#include "Core/tags.h"

using namespace Cutelyst;

class Groups: public DatabaseController
{
    Q_OBJECT
public:
    explicit Groups(QObject *parent = nullptr);
    ~Groups();

    C_ATTR(index, :Path :AutoArgs :ActionClass(REST))
    void index(Context *c);

    C_ATTR(index_GET, :Private)
    void index_GET(Context *c);

    C_ATTR(index_POST, :Private)
    void index_POST(Context *c);

    C_ATTR(group, :Path :CaptureArgs(1) :Object("group") :ActionClass(REST))
    void group(Context *c, const QString &groupId);

    C_ATTR(group_GET, :Private)
    void group_GET(Context *c, const QString &groupId);

    C_ATTR(group_PUT, :Private)
    void group_PUT(Context *c, const QString &groupId);

    C_ATTR(group_POST, :Private)
    void group_POST(Context *, const QString &) {}

    C_ATTR(group_DELETE, :Private)
    void group_DELETE(Context *c, const QString &groupId);

    /* Studentes */

    C_ATTR(students, :OnObject("group") :ActionClass(REST))
    void students(Context *, const QString &groupId,
                  const QString &method, const QString &studentId);

    C_ATTR(students_GET, :Private)
    void students_GET(Context *c);

    C_ATTR(students_POST, :Private)
    void students_POST(Context *c);

    C_ATTR(students_DELETE, :Private)
    void students_DELETE(Context *c);

    /* Inseniantes */

    C_ATTR(teachers, :OnObject("group") :ActionClass(REST))
    void teachers(Context *, const QString &groupId,
                  const QString &method, const QString &teacherId);

    C_ATTR(teachers_GET, :Private)
    void teachers_GET(Context *c);

    C_ATTR(teachers_POST, :Private)
    void teachers_POST(Context *c);

    C_ATTR(teachers_DELETE, :Private)
    void teachers_DELETE(Context *c);

protected:
    bool postFork(Cutelyst::Application *app) override;

private:
    int groupIdFromString(Context *c, const QString &groupId);

private:
    Core::Groups m_groups;
    Core::Locations m_locations;
    Core::Tags m_tags;
};

#endif // GROUPS_H
