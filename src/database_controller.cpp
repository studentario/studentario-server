#include "database_controller.h"

#include "Core/database.h"
#include "error.h"

#include <Cutelyst/Response>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

using namespace Cutelyst;

DatabaseController::DatabaseController(QObject *parent):
    Controller(parent),
    m_db(nullptr)
{
}

DatabaseController::~DatabaseController() = default;

void DatabaseController::setDatabase(Core::Database *db)
{
    m_db = db;
}

void DatabaseController::addDebugInfo(Cutelyst::Context *c,
                                      QJsonObject *response)
{
    response->insert(QStringLiteral("debug"),
        QJsonObject {
            { QStringLiteral("requestBody"), c->request()->bodyJsonObject() },
        });
}

void DatabaseController::setResponseValue(Cutelyst::Context *c,
                                          const QJsonValue &value)
{
    if (Q_UNLIKELY(c->response()->hasBody())) {
        qWarning() << "Response body already set";
        return;
    }
    c->response()->setJsonObjectBody(makeResponseValue(value));
}

void DatabaseController::setResponseObject(Cutelyst::Context *c,
                                           const QJsonObject &object)
{
    setResponseValue(c, object);
}

void DatabaseController::setResponseArray(Cutelyst::Context *c,
                                          const QJsonArray &array)
{
    setResponseValue(c, array);
}

void DatabaseController::setPreparedResponse(Cutelyst::Context *c,
                                             const QJsonObject &response)
{
    QJsonObject r(response);
    if (r.value(QStringLiteral("status")).toString() == "error") {
        Error::Code code = static_cast<Error::Code>(r.value("code").toInt());
        c->response()->setStatus(Error::httpStatusFromCode(code));
        addDebugInfo(c, &r);
    }
    c->response()->setJsonObjectBody(r);
}

void DatabaseController::setError(Cutelyst::Context *c,
                                  const Error &error)
{
    QJsonObject r = makeResponseError(error);
    addDebugInfo(c, &r);
    c->response()->setJsonObjectBody(r);
    c->response()->setStatus(error.httpStatus());
}

void DatabaseController::setResponseFromError(Cutelyst::Context *c,
                                              const Error &error)
{
    if (error) {
        setError(c, error);
    } else {
        setResponseObject(c, {});
    }
}

QJsonObject DatabaseController::makeResponseValue(const QJsonValue &value)
{
    return QJsonObject {
        { QStringLiteral("status"), QStringLiteral("ok") },
        { QStringLiteral("data"), value },
    };
}

QJsonObject DatabaseController::makeResponseError(const Error &error)
{
    return QJsonObject {
        { QStringLiteral("status"), QStringLiteral("error") },
        { QStringLiteral("message"), error.message() },
        { QStringLiteral("code"), error.code() },
    };
}
