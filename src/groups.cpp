#include "groups.h"

#include "Core/permissions.h"
#include "error.h"
#include "users.h"
#include "utils.h"

#include <Cutelyst/Plugins/Session/Session>
#include <QDebug>
#include <QJsonArray>

using namespace Cutelyst;

namespace {

QVector<int> parseIds(const QJsonValue &json)
{
    QVector<int> ids;
    const QJsonArray array = json.toArray();
    for (const QJsonValue &v: array) {
        ids.append(v.toInt());
    }
    return ids;
}

} // namespace

Groups::Groups(QObject *parent): DatabaseController(parent)
{
}

Groups::~Groups()
{
}

int Groups::groupIdFromString(Context *c, const QString &groupId)
{
    bool ok = false;
    int id = groupId.toInt(&ok);
    if (!ok) {
        setError(c, Error(Error::MissingRequiredField,
                          "The given group ID cannot be parsed"));
        return -1;
    }
    return id;
}

void Groups::index(Context *)
{
}

void Groups::index_GET(Context *c)
{
    Core::Permissions permissions;
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);

    const QVector<QPair<QString,Core::User::Role>> userQueries {
        { "studentId", Core::User::Role::Student },
        { "teacherId", Core::User::Role::Teacher },
        { "adminId", Core::User::Role::Admin },
    };
    Core::User::Role role = Core::User::Role::NoRole;
    int userId = -1;
    for (const auto &userQuery: userQueries) {
        QString idText = c->request()->queryParameter(userQuery.first);
        if (idText.isEmpty()) continue;

        if (role != Core::User::Role::NoRole) {
            setError(c, Error(Error::InvalidParameters,
                              "Can specify only one user selector"));
            return;
        }

        role = userQuery.second;
        userId = idText.toInt();
    }

    QString pattern = c->request()->queryParameter("searchText");
    QVector<Core::Group> groups;
    if (role != Core::User::Role::NoRole) {
        Error error = m_groups.groupsForUser(userId, &groups, role, pattern);
        if (error) {
            setError(c, error);
            return;
        }
    } else {
        groups = m_groups.groups(pattern);
    }

    QJsonArray response = Core::Groups::jsonFromDb(groups);
    if (permissions.can(roles, Core::Permissions::Action::ViewTag)) {
        Core::Error error = m_tags.loadTags(&response,
                                            Core::Tags::Entity::Groups);
        if (Q_UNLIKELY(error)) {
            setError(c, error);
            return;
        }
    }
    setResponseArray(c, response);
}

void Groups::index_POST(Context *c)
{
    QJsonObject group = c->request()->bodyJsonObject();

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::CreateGroup)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    int groupId = -1;
    Error error = m_groups.addGroup(group, &groupId);
    if (!error && groupId >= 0) {
        group.insert(Core::Groups::Fields::GroupId, groupId);
        setResponseObject(c, group);
    } else {
        setError(c, error);
    }
}

void Groups::group(Context *c, const QString &groupIdText)
{
    int groupId = groupIdFromString(c, groupIdText);
    if (groupId < 0) return; // Error jam inviate

    c->setStash("groupId", groupId);
}

void Groups::group_GET(Context *c, const QString &groupIdText)
{
    qDebug() << Q_FUNC_INFO << groupIdText;
    int groupId = groupIdFromString(c, groupIdText);
    if (groupId < 0) return; // Error jam inviate

    // TODO: verificar que le cliente ha le permission
    //setError(c, Error(Error::PermissionDenied));

    Core::Group group;
    Error error = m_groups.group(groupId, &group);
    if (error) {
        setError(c, error);
        return;
    }

    QJsonObject object = Core::Groups::jsonFromDb(group);
    QStringList memberFields = c->request()->queryParameters("members");

    using Role = Core::User::Role;
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;

    Core::Users::FieldSet fieldSet =
        Core::Users::fieldsFromNames(memberFields);

    if (fieldSet != Core::Users::FieldSet(Core::Users::FieldId::Empty)) {
        if (!permissions.can(roles, Core::Permissions::Action::ViewGroupMembers)) {
            setError(c, Error(Error::PermissionDenied));
            return;
        }

        QVector<Core::User> teachers;
        error = m_groups.usersForGroup(groupId, &teachers, Role::Teacher);
        if (error) {
            setResponseFromError(c, error);
            return;
        }
        object.insert("teachers", Core::Users::jsonFromDb(teachers, fieldSet));

        QVector<Core::User> students;
        error = m_groups.usersForGroup(groupId, &students, Role::Student);
        if (error) {
            setResponseFromError(c, error);
            return;
        }
        object.insert("students", Core::Users::jsonFromDb(students, fieldSet));
    }

    if (permissions.can(roles, Core::Permissions::Action::ViewTag)) {
        Core::Error error = m_tags.loadTags(&object,
                                            Core::Tags::Entity::Groups);
        if (Q_UNLIKELY(error)) {
            setError(c, error);
            return;
        }
    }

    if (group.locationId > 0) {
        Core::Location location;
        error = m_locations.location(group.locationId, &location);
        if (Q_UNLIKELY(error)) {
            setError(c, error);
            return;
        }

        object.insert(Core::Locations::Fields::Location,
                      Core::Locations::jsonFromDb(location));
    }

    setResponseObject(c, object);
}

void Groups::group_PUT(Context *c, const QString &groupIdText)
{
    qDebug() << Q_FUNC_INFO << groupIdText;
    int groupId = groupIdFromString(c, groupIdText);
    if (groupId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditGroup)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Core::Group group;
    if (Error error = m_groups.group(groupId, &group)) {
        setError(c, error);
        return;
    }

    const QJsonObject requestedChanges = c->request()->bodyJsonObject();
    Error error = m_groups.updateGroup(group, requestedChanges);
    setResponseFromError(c, error);
}

void Groups::group_DELETE(Context *c, const QString &groupIdText)
{
    int groupId = groupIdFromString(c, groupIdText);
    if (groupId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::DeleteGroup)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Error error = m_groups.deleteGroup(groupId);
    setResponseFromError(c, error);
}

void Groups::students(Context *c,
                      const QString &groupId,
                      const QString &method,
                      const QString &studentId)
{
    qDebug() << Q_FUNC_INFO << groupId << method << studentId;
    QVector<int> ids;
    if (Utils::idsFromString(studentId, &ids)) {
        c->setStash("studentIds", QVariant::fromValue(ids));
    }
}

void Groups::students_GET(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    int groupId = c->stash("groupId", -1).toInt();
    if (groupId < 0) return;

    QVector<Core::User> students;
    Error error = m_groups.usersForGroup(groupId, &students,
                                         Core::User::Role::Student);
    if (error) {
        setResponseFromError(c, error);
    } else {
        setResponseArray(c, Core::Users::jsonFromDb(students));
    }
}

void Groups::students_POST(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    int groupId = c->stash("groupId", -1).toInt();
    if (groupId < 0) return;

    QJsonObject requestData = c->request()->bodyJsonObject();

    QVector<int> studentIds = parseIds(requestData.value("userIds"));
    Error error = m_groups.addUsersToGroup(groupId, studentIds,
                                           Core::User::Role::Student);
    setResponseFromError(c, error);
}

void Groups::students_DELETE(Context *c)
{
    int groupId = c->stash("groupId", -1).toInt();
    if (groupId < 0) return;

    QVector<int> studentIds = c->stash("studentIds").value<QVector<int>>();
    if (studentIds.isEmpty()) return;

    qDebug() << Q_FUNC_INFO << groupId << studentIds;
    Error error = m_groups.removeUsersFromGroup(groupId, studentIds,
                                                Core::User::Role::Student);
    setResponseFromError(c, error);
}

void Groups::teachers(Context *c,
                      const QString &groupId,
                      const QString &method,
                      const QString &teacherId)
{
    qDebug() << Q_FUNC_INFO << groupId << method << teacherId;
    QVector<int> ids;
    if (Utils::idsFromString(teacherId, &ids)) {
        c->setStash("teacherIds", QVariant::fromValue(ids));
    }
}

void Groups::teachers_GET(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    int groupId = c->stash("groupId", -1).toInt();
    if (groupId < 0) return;

    QVector<Core::User> teachers;
    Error error = m_groups.usersForGroup(groupId, &teachers,
                                         Core::User::Role::Teacher);
    if (error) {
        setResponseFromError(c, error);
    } else {
        setResponseArray(c, Core::Users::jsonFromDb(teachers));
    }
}

void Groups::teachers_POST(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    int groupId = c->stash("groupId", -1).toInt();
    if (groupId < 0) return;

    QJsonObject requestData = c->request()->bodyJsonObject();

    QVector<int> teacherIds = parseIds(requestData.value("userIds"));
    Error error = m_groups.addUsersToGroup(groupId, teacherIds,
                                           Core::User::Role::Teacher);
    setResponseFromError(c, error);
}

void Groups::teachers_DELETE(Context *c)
{
    int groupId = c->stash("groupId", -1).toInt();
    if (groupId < 0) return;

    QVector<int> teacherIds = c->stash("teacherIds").value<QVector<int>>();
    if (teacherIds.isEmpty()) return;

    qDebug() << Q_FUNC_INFO << groupId << teacherIds;
    Error error = m_groups.removeUsersFromGroup(groupId, teacherIds,
                                                Core::User::Role::Teacher);
    setResponseFromError(c, error);
}

bool Groups::postFork(Cutelyst::Application *)
{
    m_groups.setDatabase(m_db);
    m_locations.setDatabase(m_db);
    m_tags.setDatabase(m_db);
    return true;
}
