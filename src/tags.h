#ifndef TAGS_H
#define TAGS_H

#include "database_controller.h"

#include "Core/tags.h"

#include <QVector>
#include <functional>

using namespace Cutelyst;

class Tags: public DatabaseController
{
    Q_OBJECT
public:
    explicit Tags(QObject *parent = nullptr);
    ~Tags();

    C_ATTR(index, :Path :AutoArgs :ActionClass(REST))
    void index(Context *c);

    C_ATTR(index_GET, :Private)
    void index_GET(Context *c);

    C_ATTR(index_POST, :Private)
    void index_POST(Context *c);

    C_ATTR(tag, :Path :CaptureArgs(1) :Object("tag") :ActionClass(REST))
    void tag(Context *c, const QString &tagId);

    C_ATTR(tag_GET, :Private)
    void tag_GET(Context *c, const QString &tagId);

    C_ATTR(tag_PUT, :Private)
    void tag_PUT(Context *c, const QString &tagId);

    C_ATTR(tag_POST, :Private)
    void tag_POST(Context *, const QString &) {}

    C_ATTR(tag_DELETE, :Private)
    void tag_DELETE(Context *c, const QString &tagId);

    C_ATTR(entities, :OnObject("tag") :ActionClass(REST))
    void entities(Context *) {}

    C_ATTR(entities_POST, :Private)
    void entities_POST(Context *c);

    C_ATTR(entities_DELETE, :Private)
    void entities_DELETE(Context *c);

protected:
    bool postFork(Cutelyst::Application *app) override;

private:
    int tagIdFromString(Context *c, const QString &tagId);
    QVector<int> tagIdsFromString(Context *c, const QString &tagIds);
    void addChildren(QJsonObject *object) const;
    void addChildren(QJsonArray *objects) const;

private:
    Core::Tags m_tags;
};

#endif // TAGS_H
