#ifndef LESSONS_H
#define LESSONS_H

#include "database_controller.h"

#include "Core/lessons.h"
#include "Core/locations.h"

using namespace Cutelyst;

class Lessons: public DatabaseController
{
    Q_OBJECT
public:
    explicit Lessons(QObject *parent = nullptr);
    ~Lessons();

    C_ATTR(index, :Path :AutoArgs :ActionClass(REST))
    void index(Context *c);

    C_ATTR(index_GET, :Private)
    void index_GET(Context *c);

    C_ATTR(index_POST, :Private)
    void index_POST(Context *c);

    C_ATTR(lesson, :Path :CaptureArgs(1) :Object("lesson") :ActionClass(REST))
    void lesson(Context *c, const QString &lessonId);

    C_ATTR(lesson_GET, :Private)
    void lesson_GET(Context *c, const QString &lessonId);

    C_ATTR(lesson_PUT, :Private)
    void lesson_PUT(Context *c, const QString &lessonId);

    C_ATTR(lesson_POST, :Private)
    void lesson_POST(Context *, const QString &) {}

    C_ATTR(lesson_DELETE, :Private)
    void lesson_DELETE(Context *c, const QString &lessonId);

    /* Invitationes */

    C_ATTR(invitees, :OnObject("lesson") :ActionClass(REST))
    void invitees(Context *) {}

    C_ATTR(invitees_POST, :Private)
    void invitees_POST(Context *c);

    C_ATTR(invitees_DELETE, :Private)
    void invitees_DELETE(Context *c);

    /* Participation */

    C_ATTR(attendance, :OnObject("lesson") :ActionClass(REST))
    void attendance(Context *) {}

    C_ATTR(attendance_GET, :Private)
    void attendance_GET(Context *c);

    C_ATTR(attendance_PUT, :Private)
    void attendance_PUT(Context *c);

protected:
    bool postFork(Cutelyst::Application *app) override;

private:
    int lessonIdFromString(Context *c, const QString &lessonId);
    void createManyLessons(Context *c, const QJsonObject &request);
    Core::Error readLesson(const Core::Lesson &lesson,
                           const QStringList &userFields,
                           QJsonObject &jsonOut) const;

private:
    Core::Lessons m_lessons;
    Core::Locations m_locations;
};

#endif // LESSONS_H
