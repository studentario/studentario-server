#include "unrestified_action.h"

#include <Cutelyst/Context>
#include <Cutelyst/Controller>
#include <QDebug>

using namespace Cutelyst;

UnrestifiedAction::UnrestifiedAction(Action *restAction, QObject *parent):
    Action(parent),
    m_restAction(restAction)
{
    setName(restAction->name());
    setAttributes(restAction->attributes());
    setController(restAction->controller());

    /* Le classe Action non ha un methodo pro obtener le
     * QMetaMethod, alora nos debe retrovar lo.
     */
    const QMetaObject *metaObject = controller()->metaObject();
    const QByteArray methodName = name().toUtf8();
    bool found = false;
    for (int i = 0; i < metaObject->methodCount(); i++) {
        const QMetaMethod method = metaObject->method(i);
        const QByteArray name = method.name();
        if (name == methodName) {
            setMethod(method);
            found = true;
            break;
        }
    }
    if (Q_UNLIKELY(!found)) {
        qWarning() << "Cannot find method" << name();
        return;
    }
}
