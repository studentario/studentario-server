#ifndef UTILS_H
#define UTILS_H

#include "Core/user.h"

#include <QJsonObject>

namespace Cutelyst {
class Context;
}

namespace Utils {

Core::User::Roles authenticatedUserRoles(Cutelyst::Context *c);
QJsonObject requestQueryAsJson(Cutelyst::Context *c);
bool idsFromString(const QString &commaIds, QVector<int> *ids);

} // namespace

#endif // UTILS_H
