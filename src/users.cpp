#include "users.h"

#include "Core/authenticator.h"
#include "Core/database.h"
#include "Core/permissions.h"
#include "error.h"
#include "utils.h"

#include <Cutelyst/Plugins/Session/Session>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>

using namespace Cutelyst;

static const QString s_roleStudent = QStringLiteral("student");
static const QString s_roleParent = QStringLiteral("parent");
static const QString s_roleTeacher = QStringLiteral("teacher");
static const QString s_roleAdmin = QStringLiteral("admin");
static const QString s_roleDirector = QStringLiteral("director");
static const QString s_roleMaster = QStringLiteral("master");

Users::Users(QObject *parent): DatabaseController(parent)
{
}

Users::~Users()
{
}

int Users::userIdFromString(Context *c, const QString &userId)
{
    if (userId == "me") {
        int id = Session::value(c, "userId").toInt();
        if (id <= 0) {
            setError(c, Error::NeedsAuthentication);
            return -1;
        }
        return id;
    } else {
        bool ok = false;
        int id = userId.toInt(&ok);
        if (!ok) {
            setError(c, Error(Error::MissingRequiredField,
                              "The given user ID cannot be parsed"));
            return -1;
        }
        return id;
    }
}

QJsonObject Users::addUser(QJsonObject &user)
{
    Error error;
    int userId = -1;

    Core::Database::Transaction transaction(m_db, &error);

    if (user.contains(Core::Users::Fields::Login)) {
        error = m_users.addUser(user, &userId);
    } else {
        error = m_users.addLoginlessUser(user, &userId);
    }

    if (!error && userId >= 0) {
        user.insert(Core::Users::Fields::UserId, userId);

        QJsonObject status = user["status"].toObject();
        if (!status.isEmpty()) {
            status.insert(Core::StatusChanges::Fields::UserId, userId);
            error = m_statusChanges.addUserStatusChange(status);
            if (error) {
                return makeResponseError(error);
            }
        }
        return makeResponseValue(user);
    } else {
        return makeResponseError(error);
    }
}

void Users::index(Context *)
{
}

void Users::index_GET(Context *c)
{
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::ViewUser)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    const QJsonObject query = Utils::requestQueryAsJson(c);

    Core::User::Filters filters;
    m_users.filtersFromJson(query, &filters);
    const Core::User::Sort sort = m_users.sortingFromJson(query);
    auto users = m_users.users(filters, sort);
    const QStringList fieldNames = c->request()->queryParameters("fields");
    Core::Users::FieldSet fieldSet = fieldNames.isEmpty() ?
        Core::Users::FieldId::PublicInfo :
        Core::Users::fieldsFromNames(fieldNames);
    QJsonArray response = Core::Users::jsonFromDb(users, fieldSet);
    bool canViewTags =
        permissions.can(roles, Core::Permissions::Action::ViewTag);
    if (fieldNames.contains("tags") && !canViewTags) {
        setError(c, Error(Error::PermissionDenied,
                          QStringLiteral("No permission to view tags")));
        return;
    }

    if (canViewTags &&
        (fieldNames.isEmpty() || fieldNames.contains("tags"))) {
        Core::Error error = m_tags.loadTags(&response,
                                            Core::Tags::Entity::Users);
        if (Q_UNLIKELY(error)) {
            setError(c, error);
            return;
        }
    }
    setResponseArray(c, response);
}

void Users::index_POST(Context *c)
{
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::CreateUser)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    QJsonDocument requestData = c->request()->bodyJsonDocument();
    if (requestData.isObject()) {
        QJsonObject user = requestData.object();
        const QJsonObject response = addUser(user);
        setPreparedResponse(c, response);
    } else {
        const QJsonArray users = requestData.array();
        QJsonArray responses;
        for (const QJsonValue &v: users) {
            QJsonObject user = v.toObject();
            const QJsonObject response = addUser(user);
            responses.append(response);
        }
        c->response()->setJsonArrayBody(responses);
    }
}

void Users::user(Context *c, const QString &userIdText)
{
    int userId = userIdFromString(c, userIdText);
    if (userId < 0) return; // Error jam inviate

    c->setStash("userId", userId);
}

void Users::user_GET(Context *c, const QString &userIdText)
{
    qDebug() << Q_FUNC_INFO << userIdText;
    int userId = userIdFromString(c, userIdText);
    if (userId < 0) return; // Error jam inviate

    Core::Permissions permissions;
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);

    int authenticatedUserId = Session::value(c, "userId").toInt();
    qDebug() << "authenticated as" << authenticatedUserId;
    qDebug() << "querying for" << userId;
    if (authenticatedUserId != userId) {
        if (!permissions.can(roles, Core::Permissions::Action::ViewUser)) {
            setError(c, Error(Error::PermissionDenied));
            return;
        }
    }

    Core::User user;
    Error error = m_users.user(userId, &user);
    if (error) {
        setError(c, error);
        return;
    }

    const QStringList fieldNames = c->request()->queryParameters("fields");
    Core::Users::FieldSet fieldSet = fieldNames.isEmpty() ?
        Core::Users::FieldId::PublicInfo :
        Core::Users::fieldsFromNames(fieldNames);
    QJsonObject object = Core::Users::jsonFromDb(user, fieldSet);

    bool canViewTags =
        permissions.can(roles, Core::Permissions::Action::ViewTag);
    if (fieldNames.contains("tags") && !canViewTags) {
        setError(c, Error(Error::PermissionDenied,
                          QStringLiteral("No permission to view tags")));
        return;
    }

    if (canViewTags &&
        (fieldNames.isEmpty() || fieldNames.contains("tags"))) {
        Core::Error error = m_tags.loadTags(&object,
                                            Core::Tags::Entity::Users);
        if (Q_UNLIKELY(error)) {
            setError(c, error);
            return;
        }
    }

    setResponseObject(c, object);
}

void Users::user_PUT(Context *c, const QString &userIdText)
{
    qDebug() << Q_FUNC_INFO << userIdText;
    int userId = userIdFromString(c, userIdText);
    if (userId < 0) return; // Error jam inviate

    Core::User user;
    Error loadError = m_users.user(userId, &user);

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    const QJsonObject requestedChanges = c->request()->bodyJsonObject();
    Core::User::Roles newRoles =
        m_users.rolesFromJson(requestedChanges, user.roles);
    Core::Permissions::Action action = Core::Permissions::Action::EditUser;
    if (!permissions.canManageUser(roles, action, user.roles) ||
        !permissions.canManageUser(roles, action, newRoles)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    if (loadError) {
        setError(c, loadError);
        return;
    }

    Error error = m_users.updateUser(user, requestedChanges);
    setResponseFromError(c, error);
}

void Users::user_DELETE(Context *c, const QString &userIdText)
{
    int userId = userIdFromString(c, userIdText);
    if (userId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::DeleteUser)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Error error = m_users.deleteUser(userId);
    setResponseFromError(c, error);
}

void Users::updatePin_POST(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    int userId = c->stash("userId", -1).toInt();
    if (userId < 0) return;

    QJsonObject requestData = c->request()->bodyJsonObject();

    Core::User user;
    m_users.user(userId, &user);
    requestData.insert(Core::Users::Fields::Login, user.login);

    Core::Authenticator auth;
    auth.setDatabase(m_db);
    Core::Error error = auth.login(requestData);
    if (error) {
        setError(c, error);
        return;
    }

    QByteArray newPin = requestData.value("newPin").toString().toUtf8();
    error = m_users.updatePin(userId, newPin);
    setResponseFromError(c, error);
}

void Users::status_POST(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    int userId = c->stash("userId", -1).toInt();
    if (userId < 0) return;

    QJsonObject requestData = c->request()->bodyJsonObject();

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditUser)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    int creatorId = Session::value(c, "userId").toInt();
    requestData.insert(Core::StatusChanges::Fields::CreatorId, creatorId);
    requestData.insert(Core::StatusChanges::Fields::UserId, userId);
    int changeId = 0;
    Core::Error error = m_statusChanges.addUserStatusChange(requestData,
                                                            &changeId);
    if (!error && changeId > 0) {
        requestData.insert(Core::StatusChanges::Fields::ChangeId, changeId);
        setResponseObject(c, requestData);
    } else {
        setResponseFromError(c, error);
    }
}

bool Users::postFork(Application *)
{
    m_users.setDatabase(m_db);
    m_statusChanges.setDatabase(m_db);
    m_tags.setDatabase(m_db);
    return true;
}
