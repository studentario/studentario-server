#ifndef LOCATIONS_H
#define LOCATIONS_H

#include "database_controller.h"

#include "Core/locations.h"
#include "Core/tags.h"

using namespace Cutelyst;

class Locations: public DatabaseController
{
    Q_OBJECT
public:
    explicit Locations(QObject *parent = nullptr);
    ~Locations();

    C_ATTR(index, :Path :AutoArgs :ActionClass(REST))
    void index(Context *c);

    C_ATTR(index_GET, :Private)
    void index_GET(Context *c);

    C_ATTR(index_POST, :Private)
    void index_POST(Context *c);

    C_ATTR(location, :Path :CaptureArgs(1) :Object("location") :ActionClass(REST))
    void location(Context *c, const QString &locationId);

    C_ATTR(location_GET, :Private)
    void location_GET(Context *c, const QString &locationId);

    C_ATTR(location_PUT, :Private)
    void location_PUT(Context *c, const QString &locationId);

    C_ATTR(location_POST, :Private)
    void location_POST(Context *, const QString &) {}

    C_ATTR(location_DELETE, :Private)
    void location_DELETE(Context *c, const QString &locationId);

protected:
    bool postFork(Cutelyst::Application *app) override;

private:
    int locationIdFromString(Context *c, const QString &locationId);

private:
    Core::Locations m_locations;
    Core::Tags m_tags;
};

#endif // LOCATIONS_H
