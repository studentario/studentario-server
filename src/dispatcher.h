#ifndef STUDENTARIO_DISPATCHER_H
#define STUDENTARIO_DISPATCHER_H

#include <Cutelyst/DispatchType>
#include <QHash>
#include <QString>
#include <QVector>

namespace Studentario {

class Dispatcher: public Cutelyst::DispatchType
{
    Q_OBJECT
public:
    explicit Dispatcher(QObject *parent = nullptr);
    ~Dispatcher();

    QByteArray list() const override;

    DispatchType::MatchType match(Cutelyst::Context *c,
                                  const QString &path,
                                  const QStringList &args) const override;

    QString uriForAction(Cutelyst::Action *action,
                         const QStringList &captures) const override;

    bool registerAction(Cutelyst::Action *action) override;

    bool inUse() override;

private:
    QHash<QString, Cutelyst::Action *> m_objects;
    QVector<Cutelyst::Action *> m_methods;
};

} // namespace

#endif // STUDENTARIO_DISPATCHER_H
