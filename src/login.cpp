#include "login.h"

#include "Core/users.h"
#include "error.h"

#include <Cutelyst/Plugins/Session/Session>
#include <QDebug>

using namespace Cutelyst;

Login::Login(QObject *parent): DatabaseController(parent)
{
}

Login::~Login()
{
}

void Login::index(Context *)
{
}

void Login::index_POST(Context *c)
{
    QJsonObject loginData = c->request()->bodyJsonObject();
    Core::Error error = m_authenticator.login(loginData);
    if (!error) {
        const auto &user = m_authenticator.authenticatedUser();
        auto authenticationStatus = m_authenticator.status();
        Session::setValue(c, "authenticationStatus", int(authenticationStatus));
        Session::setValue(c, "userId", user.id);
        Session::setValue(c, "userRoles", int(user.roles));
        using Users = Core::Users;
        QJsonObject reply = {
            { "user", Users::jsonFromDb(user, Users::FieldId::PublicInfo) },
            { "authenticationToken", Session::id(c) },
        };
        setResponseObject(c, reply);
    } else {
        setError(c, error);
    }
}

bool Login::postFork(Application *)
{
    m_authenticator.setDatabase(m_db);
    return true;
}
