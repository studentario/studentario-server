#include "lessons.h"

#include "Core/database.h"
#include "Core/permissions.h"
#include "error.h"
#include "users.h"
#include "utils.h"

#include <Cutelyst/Plugins/Session/Session>
#include <QDebug>
#include <QJsonArray>

using namespace Cutelyst;

Lessons::Lessons(QObject *parent): DatabaseController(parent)
{
}

Lessons::~Lessons()
{
}

int Lessons::lessonIdFromString(Context *c, const QString &lessonId)
{
    bool ok = false;
    int id = lessonId.toInt(&ok);
    if (!ok) {
        setError(c, Error(Error::MissingRequiredField,
                          "The given lesson ID cannot be parsed"));
        return -1;
    }
    return id;
}

void Lessons::createManyLessons(Context *c, const QJsonObject &request)
{
    int userId = Session::value(c, "userId").toInt();

    const QJsonObject invitees = request["invitees"].toObject();
    const QJsonArray lessons = request["lessons"].toArray();
    QJsonArray responses;
    for (const QJsonValue &lessonVal: lessons) {
        Error error;
        Core::Database::Transaction transaction(m_db, &error);

        QJsonObject lesson = lessonVal.toObject();
        lesson.insert(Core::Lessons::Fields::CreatorId, userId);
        int lessonId = -1;
        error = m_lessons.addLesson(lesson, &lessonId);
        if (!error && lessonId >= 0) {
            lesson.insert(Core::Lessons::Fields::LessonId, lessonId);
            error = m_lessons.inviteToLesson(userId, lessonId, invitees);
            responses.append(makeResponseValue(lesson));
        } else {
            responses.append(makeResponseError(error));
        }
    }
    c->response()->setJsonArrayBody(responses);
}

Core::Error Lessons::readLesson(const Core::Lesson &lesson,
                                const QStringList &userFields,
                                QJsonObject &jsonOut) const
{
    jsonOut = Core::Lessons::jsonFromDb(lesson);

    QVector<Core::DetailedParticipation> participations;
    QJsonObject filters; // vacue
    Error error =
        m_lessons.lessonAttendance(lesson.id, &participations, filters);
    if (error) return error;

    const QJsonObject attendance =
        m_lessons.jsonFromDb(participations, userFields);
    for (auto i = attendance.begin(); i != attendance.end(); i++) {
        jsonOut.insert(i.key(), i.value());
    }

    if (lesson.locationId > 0) {
        Core::Location location;
        error = m_locations.location(lesson.locationId, &location);
        if (error) return error;

        jsonOut.insert(Core::Locations::Fields::Location,
                       Core::Locations::jsonFromDb(location));
    }

    return Error::NoError;
}

void Lessons::index(Context *)
{
}

void Lessons::index_GET(Context *c)
{
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);

    const QJsonObject query = Utils::requestQueryAsJson(c);
    int studentId = query["studentId"].toInt();
    int teacherId = query["teacherId"].toInt();
    int groupId = query["groupId"].toInt();

    /* Si le usator es un studente, accompaniator o inseniante, ille potera
     * vider solmente su lectiones */
    int userId = Session::value(c, "userId").toInt();
    bool limitToSelf = false;
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::ViewLesson)) {
        limitToSelf = true;
    }

    int entityFilterCount = (studentId > 0) + (groupId > 0) + (teacherId > 0);
    if (entityFilterCount > 1) {
        setError(c, Error(Error::InvalidParameters,
                          "Can specify only one entity selector"));
        return;
    }

    QVector<Core::Lesson> lessons;
    if (entityFilterCount > 0) {
        Core::EntityType type;
        int entityId;
        if (studentId > 0) {
            type = Core::EntityType::Student;
            entityId = studentId;
        } else if (teacherId > 0) {
            type = Core::EntityType::Teacher;
            entityId = teacherId;
        } else {
            Q_ASSERT(groupId > 0);
            type = Core::EntityType::Group;
            entityId = groupId;
        }

        if (limitToSelf) {
            /* TODO (forsan): permitter le recerca per gruppo, si le usator es
             * un membro */
            if (type == Core::EntityType::Group) {
                setError(c, Error(Error::PermissionDenied,
                                  "Search by group not allowed"));
                return;
            }

            if (userId != entityId) {
                setError(c, Error(Error::PermissionDenied));
                return;
            }
        }

        Error error = m_lessons.lessonsByParticipants({{ entityId, type }},
                                                      &lessons, query);
        if (error) {
            setError(c, error);
            return;
        }
    } else if (limitToSelf) {
        const Core::Lessons::Participants participants {
            { userId, Core::EntityType::Student },
            { userId, Core::EntityType::Teacher },
        };
        Error error = m_lessons.lessonsByParticipants(participants,
                                                      &lessons, query);
        if (error) {
            setError(c, error);
            return;
        }
    } else {
        lessons = m_lessons.lessons(query);
    }

    QStringList userFields;
    if (permissions.can(roles, Core::Permissions::Action::ViewLessonMembers)) {
        userFields = c->request()->queryParameters("userFields");
    }

    QJsonArray fullLessons;
    for (const Core::Lesson &lesson: lessons) {
        QJsonObject fullLesson;
        Error error = readLesson(lesson, userFields, fullLesson);
        if (error) {
            setError(c, error);
            return;
        }
        fullLessons.append(fullLesson);
    }
    setResponseArray(c, fullLessons);
}

void Lessons::index_POST(Context *c)
{
    QJsonObject lesson = c->request()->bodyJsonObject();

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::CreateLesson)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    if (lesson.contains("lessons")) {
        createManyLessons(c, lesson);
        return;
    }

    int creatorId = Session::value(c, "userId").toInt();
    lesson.insert(Core::Lessons::Fields::CreatorId, creatorId);
    int lessonId = -1;
    Error error = m_lessons.addLesson(lesson, &lessonId);
    if (!error && lessonId >= 0) {
        lesson.insert(Core::Lessons::Fields::LessonId, lessonId);
        setResponseObject(c, lesson);
    } else {
        setError(c, error);
    }
}

void Lessons::lesson(Context *c, const QString &lessonIdText)
{
    int lessonId = lessonIdFromString(c, lessonIdText);
    if (lessonId < 0) return; // Error jam inviate

    c->setStash("lessonId", lessonId);
}

void Lessons::lesson_GET(Context *c, const QString &lessonIdText)
{
    qDebug() << Q_FUNC_INFO << lessonIdText;
    int lessonId = lessonIdFromString(c, lessonIdText);
    if (lessonId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::ViewLesson)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Core::Lesson lesson;
    Error error = m_lessons.lesson(lessonId, &lesson);
    if (error) {
        setError(c, error);
        return;
    }

    QStringList userFields;
    if (permissions.can(roles, Core::Permissions::Action::ViewLessonMembers)) {
        userFields = c->request()->queryParameters("userFields");
    }

    QJsonObject response;
    error = readLesson(lesson, userFields, response);
    if (error) {
        setError(c, error);
        return;
    }

    setResponseObject(c, response);
}

void Lessons::lesson_PUT(Context *c, const QString &lessonIdText)
{
    qDebug() << Q_FUNC_INFO << lessonIdText;
    int lessonId = lessonIdFromString(c, lessonIdText);
    if (lessonId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditLesson)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Core::Lesson lesson;
    if (Error error = m_lessons.lesson(lessonId, &lesson)) {
        setError(c, error);
        return;
    }

    const QJsonObject requestedChanges = c->request()->bodyJsonObject();
    Error error = m_lessons.updateLesson(lesson, requestedChanges);
    setResponseFromError(c, error);
}

void Lessons::lesson_DELETE(Context *c, const QString &lessonIdText)
{
    int lessonId = lessonIdFromString(c, lessonIdText);
    if (lessonId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::DeleteLesson)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Error error = m_lessons.deleteLesson(lessonId);
    setResponseFromError(c, error);
}

void Lessons::invitees_POST(Context *c)
{
    /* TODO: remove invitees_POST e invitees_DELETE e cambia le invitatos per
     * lesson_PUT.
     */
    qDebug() << Q_FUNC_INFO;
    int lessonId = c->stash("lessonId", -1).toInt();
    if (lessonId < 0) return;

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditLesson)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    int userId = Session::value(c, "userId").toInt();
    const QJsonObject invitees = c->request()->bodyJsonObject();
    Error error = m_lessons.inviteToLesson(userId, lessonId, invitees);
    setResponseFromError(c, error);
}

void Lessons::invitees_DELETE(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    int lessonId = c->stash("lessonId", -1).toInt();
    if (lessonId < 0) return;

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditLesson)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    const auto params = c->request()->queryParameters();
    QJsonObject inviteeData;
    for (auto i = params.begin(); i != params.end(); i++) {
        auto array = inviteeData[i.key()];
        QJsonArray ids = array.toArray();
        ids.append(i.value().toInt());
        array = ids;
    }

    Error error = m_lessons.uninviteFromLesson(lessonId, inviteeData);
    setResponseFromError(c, error);
}

void Lessons::attendance_GET(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    int lessonId = c->stash("lessonId", -1).toInt();
    if (lessonId < 0) return;

    QJsonObject filters; // vacue
    QVector<Core::DetailedParticipation> participations;
    Error error = m_lessons.lessonAttendance(lessonId, &participations,
                                             filters);
    if (error) {
        setResponseFromError(c, error);
        return;
    }

    QStringList userFields = c->request()->queryParameters("userFields");
    const QJsonObject attendance =
        m_lessons.jsonFromDb(participations, userFields);
    setResponseObject(c, attendance);
}

void Lessons::attendance_PUT(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    int lessonId = c->stash("lessonId", -1).toInt();
    if (lessonId < 0) return;

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditAttendance)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    QJsonObject requestData = c->request()->bodyJsonObject();

    Error error = m_lessons.updateAttendance(lessonId, requestData);
    setResponseFromError(c, error);
}

bool Lessons::postFork(Cutelyst::Application *)
{
    m_lessons.setDatabase(m_db);
    m_locations.setDatabase(m_db);
    return true;
}
