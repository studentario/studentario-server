#ifndef DATABASE_CONTROLLER_H
#define DATABASE_CONTROLLER_H

#include <Cutelyst/Controller>

class Error;

class QJsonArray;
class QJsonObject;
class QJsonValue;

namespace Core {
class Database;
}

class DatabaseController: public Cutelyst::Controller
{
    Q_OBJECT
public:
    explicit DatabaseController(QObject *parent = nullptr);
    ~DatabaseController();

    void setDatabase(Core::Database *db);

protected:
    Core::Database &db() { return *m_db; }

    void setResponseValue(Cutelyst::Context *c, const QJsonValue &value);
    void setResponseObject(Cutelyst::Context *c, const QJsonObject &object);
    void setResponseArray(Cutelyst::Context *c, const QJsonArray &array);
    void setError(Cutelyst::Context *c, const Error &error);
    void setResponseFromError(Cutelyst::Context *c, const Error &error);
    void setPreparedResponse(Cutelyst::Context *c,
                             const QJsonObject &response);

    static QJsonObject makeResponseValue(const QJsonValue &value);
    static QJsonObject makeResponseError(const Error &error);

private:
    void addDebugInfo(Cutelyst::Context *c, QJsonObject *response);

protected:
    Core::Database *m_db;
};

#endif // DATABASE_CONTROLLER_H
