#ifndef UNRESTIFIED_ACTION_H
#define UNRESTIFIED_ACTION_H

#include <Cutelyst/Action>

#include <QDebug>
#include <QObject>

/*
 * Takes a REST action, and always invoke the base method
 */
class UnrestifiedAction : public Cutelyst::Action
{
    Q_OBJECT

public:
    explicit UnrestifiedAction(Action *restAction, QObject *parent = nullptr);
    virtual ~UnrestifiedAction() override = default;

    qint8 numberOfCaptures() const override { return 1; }

private:
    Cutelyst::Action * m_restAction;
};

#endif // UNRESTIFIED_ACTION_H
