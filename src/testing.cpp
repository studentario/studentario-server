#include "testing.h"

#include "Core/authenticator.h"
#include "Core/database.h"
#include "Core/user.h"

#include <QCoreApplication>
#include <QDebug>
#include <QJsonObject>
#include <QSqlDatabase>
#include <QSqlQuery>

using namespace Cutelyst;

Testing::Testing(QObject *parent): DatabaseController(parent)
{
}

Testing::~Testing()
{
}

void Testing::wipeDb(Context *c)
{
    QSqlDatabase db = m_db->dbForTesting();
    db.transaction();
    const QStringList tables {
        "UserTags",
        "GroupTags",
        "LessonTags",
        "LessonNoteTags",
        "LocationTags",
        "Groups",
        "Students",
        "Teachers",
        "Admins",
        "Users",
        "StatusChanges",
        "Relations",
        "Tags",
        "Lessons",
        "Attendance",
        "LessonNotes",
        "LessonNotesGlue",
        "Locations",
    };
    for (const QString &table: tables) {
        db.exec("DELETE FROM `" + table + '`');
    }
    db.commit();
    m_db->createOrUpdateDb();

    c->response()->setBody(QByteArray("ok"));
    c->response()->setStatus(200);
}

void Testing::createMaster(Context *c)
{
    QByteArray password = "supreme";
    QByteArray salt = "47rey7rwh84e3drj43cf83w";
    QByteArray hash = Core::Authenticator::passwordHash(password, salt);
    Core::User master = {
        0, // id
        Core::User::Role::Master,
        "master",
        hash,
        salt,
        // Nulle PIN quando le usator es create
        QByteArray(), QDateTime(),
        "Supremo", // nomine
        QDate(), // birthdate
        "", // parolas clave
        {}, // info de contacto
        QDateTime::currentDateTime(),
    };
    int userId = m_db->addUser(master);
    c->response()->setJsonObjectBody({
        { "login", master.login },
        { "password", QString::fromUtf8(password) },
    });
    c->response()->setStatus(userId >= 0 ? 200 : 500);
}

void Testing::quit(Context *)
{
    qDebug() << "Telling application to quit";
    QCoreApplication::instance()->quit();
}
