#ifndef APIV1_H
#define APIV1_H

#include "groups.h"
#include "lessons.h"
#include "locations.h"
#include "login.h"
#include "status_changes.h"
#include "tags.h"
#include "users.h"

class ApiV1Users: public Users
{
    Q_OBJECT
public:
    using Users::Users;
};

class ApiV1Groups: public Groups
{
    Q_OBJECT
public:
    using Groups::Groups;
};

class ApiV1Lessons: public Lessons
{
    Q_OBJECT
public:
    using Lessons::Lessons;
};

class ApiV1Locations: public Locations
{
    Q_OBJECT
public:
    using Locations::Locations;
};

class ApiV1Login: public Login
{
    Q_OBJECT
public:
    using Login::Login;
};

class ApiV1Statuschanges: public StatusChanges
{
    Q_OBJECT
public:
    using StatusChanges::StatusChanges;
};

class ApiV1Tags: public Tags
{
    Q_OBJECT
public:
    using Tags::Tags;
};

#endif //APIV1_H
