#include "tags.h"

#include "Core/database.h"
#include "Core/permissions.h"
#include "error.h"
#include "users.h"
#include "utils.h"

#include <Cutelyst/Plugins/Session/Session>
#include <QDebug>
#include <QJsonArray>
#include <QStringList>

using namespace Cutelyst;

Tags::Tags(QObject *parent): DatabaseController(parent)
{
}

Tags::~Tags()
{
}

int Tags::tagIdFromString(Context *c, const QString &tagId)
{
    bool ok = false;
    int id = tagId.toInt(&ok);
    if (!ok) {
        setError(c, Error(Error::MissingRequiredField,
                          "The given tag ID cannot be parsed"));
        return -1;
    }
    return id;
}

QVector<int> Tags::tagIdsFromString(Context *c, const QString &tagIds)
{
    QVector<int> ret;
    if (!Utils::idsFromString(tagIds, &ret)) {
        setError(c, Error(Error::MissingRequiredField,
                          "The given tag IDs cannot be parsed"));
        ret.clear();
    }
    return ret;
}

void Tags::addChildren(QJsonObject *object) const
{
    int tagId = object->value(Core::Tags::Fields::TagId).toInt();
    QVector<Core::Tag> tags = m_tags.tags(tagId);
    object->insert(Core::Tags::Fields::Children, m_tags.jsonFromDb(tags));
}

void Tags::addChildren(QJsonArray *objects) const
{
    for (QJsonValueRef v: *objects) {
        QJsonObject o = v.toObject();
        addChildren(&o);
        v = o;
    }
}

void Tags::index(Context *)
{
}

void Tags::index_GET(Context *c)
{
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::ViewTag)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    const QJsonObject query = Utils::requestQueryAsJson(c);
    const QString pattern = query[Core::Tags::Fields::Name].toString();
    int parentTagId = 0;
    if (query.contains(Core::Tags::Fields::ParentTagId)) {
        parentTagId = query[Core::Tags::Fields::ParentTagId].toInt();
    }
    QVector<Core::Tag> tags = m_tags.tags(parentTagId, pattern);
    QJsonArray response = Core::Tags::jsonFromDb(tags);

    // Controla si le filios es requirite
    const QStringList fieldNames = c->request()->queryParameters("fields");
    if (fieldNames.contains(Core::Tags::Fields::Children)) {
        addChildren(&response);
    }

    setResponseArray(c, response);
}

void Tags::index_POST(Context *c)
{
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditTag)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    QJsonObject tag = c->request()->bodyJsonObject();

    int creatorId = Session::value(c, "userId").toInt();
    tag.insert(Core::Tags::Fields::CreatorId, creatorId);
    int tagId = -1;
    Error error = m_tags.addTag(tag, &tagId);
    if (!error && tagId >= 0) {
        tag.insert(Core::Tags::Fields::TagId, tagId);
        setResponseObject(c, tag);
    } else {
        setError(c, error);
    }
}

void Tags::tag(Context *c, const QString &tagIdText)
{
    QVector<int> tagIds = tagIdsFromString(c, tagIdText);
    if (tagIds.isEmpty()) return; // Error jam inviate

    c->setStash("tagIds", QVariant::fromValue(tagIds));
}

void Tags::tag_GET(Context *c, const QString &tagIdText)
{
    qDebug() << Q_FUNC_INFO << tagIdText;
    int tagId = tagIdFromString(c, tagIdText);
    if (tagId < 0) return; // Error jam inviate

    Core::Tag tag;
    Error error = m_tags.tag(tagId, &tag);
    if (error) {
        setError(c, error);
        return;
    }

    QJsonObject response = Core::Tags::jsonFromDb(tag);

    // Controla si le filios es requirite
    const QStringList fieldNames = c->request()->queryParameters("fields");
    if (fieldNames.contains(Core::Tags::Fields::Children)) {
        addChildren(&response);
    }

    setResponseObject(c, response);
}

void Tags::tag_PUT(Context *c, const QString &tagIdText)
{
    qDebug() << Q_FUNC_INFO << tagIdText;
    int tagId = tagIdFromString(c, tagIdText);
    if (tagId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditTag)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Core::Tag tag;
    if (Error error = m_tags.tag(tagId, &tag)) {
        setError(c, error);
        return;
    }

    const QJsonObject requestedChanges = c->request()->bodyJsonObject();
    Error error = m_tags.updateTag(tag, requestedChanges);
    setResponseFromError(c, error);
}

void Tags::tag_DELETE(Context *c, const QString &tagIdText)
{
    qDebug() << Q_FUNC_INFO;
    int tagId = tagIdFromString(c, tagIdText);
    if (tagId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditTag)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Error error = m_tags.deleteTag(tagId);
    setResponseFromError(c, error);
}

void Tags::entities_POST(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    QVector<int> tagIds = c->stash("tagIds").value<QVector<int>>();
    if (tagIds.isEmpty()) return;

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::AttachTag)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    const QJsonObject requestData = c->request()->bodyJsonObject();

    Error error;
    Core::Database::Transaction transaction(m_db, &error);
    for (int tagId: tagIds) {
        qDebug() << "Adding" << tagId << "to" << requestData;
        error = m_tags.assignTag(tagId, requestData);
        if (error) {
            setError(c, error);
            return;
        }
    }

    setResponseFromError(c, error);
}

void Tags::entities_DELETE(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    QVector<int> tagIds = c->stash("tagIds").value<QVector<int>>();
    if (tagIds.isEmpty()) return;

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::AttachTag)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    const QJsonObject requestData = Utils::requestQueryAsJson(c);
    Error error;
    Core::Database::Transaction transaction(m_db, &error);
    for (int tagId: tagIds) {
        error = m_tags.removeTag(tagId, requestData);
        if (error) {
            setError(c, error);
            return;
        }
    }

    setResponseFromError(c, error);
}

bool Tags::postFork(Cutelyst::Application *)
{
    m_tags.setDatabase(m_db);
    return true;
}
