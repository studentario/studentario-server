#include "dispatcher.h"

#include "actionchain.h"
#include "unrestified_action.h"

#include <Cutelyst/Action>
#include <Cutelyst/Controller>
#include <Cutelyst/utils.h>
#include <QDebug>

using namespace Studentario;

static const QString AttributeObject = QStringLiteral("Object");
static const QString AttributeOnObject = QStringLiteral("OnObject");

Dispatcher::Dispatcher(QObject *parent):
    Cutelyst::DispatchType(parent)
{
}

Dispatcher::~Dispatcher() = default;

QByteArray Dispatcher::list() const
{
    using namespace Cutelyst;
    if (m_methods.isEmpty()) {
        return QByteArray();
    }

    QVector<QStringList> rows;
    for (const Action *action: m_methods) {
        rows.append({ action->name(), action->attribute(AttributeOnObject) });
    }
    return Utils::buildTable(rows,
        { QStringLiteral("Action"), QStringLiteral("Object") },
        QStringLiteral("Loaded object actions:"));
}

Cutelyst::DispatchType::MatchType Dispatcher::match(Cutelyst::Context *c,
                                                    const QString &path,
                                                    const QStringList &args) const
{
    /* Nos necessita le ID del objecto, plus le nomine del methodo */
    if (args.count() < 2) return NoMatch;

    Cutelyst::Request *req = c->request();
    QString methodName = args[1];
    for (Cutelyst::Action *method: m_methods) {
        if (method->name() != methodName) continue;

        QString objectName = method->attribute(AttributeOnObject);
        Cutelyst::Action *object = m_objects.value(objectName, nullptr);
        if (!object) {
            qDebug() << "Object not found, skipping";
            continue;
        }

        /* Nos non vole directemente invocar le action del objecto, perque illo
         * es certemente del classe REST e va invocar le implementation del
         * methodo (POST, GET, DELETE), e non le action basic.
         *
         * Alora, nos crea un action pro exequer le methodo basic.
         */
        Cutelyst::Action *obj = new UnrestifiedAction(object, c);
        req->setArguments(args);
        req->setCaptures(args);
        req->setMatch(path);
        Cutelyst::ActionList chain = { obj, method };
        Cutelyst::ActionChain *action = new Cutelyst::ActionChain(chain, c);
        setupMatchedAction(c, action);
        return ExactMatch;
    }

    return NoMatch;
}

QString Dispatcher::uriForAction(Cutelyst::Action *action,
                                 const QStringList &captures) const
{
    qDebug() << Q_FUNC_INFO << action->name() << captures;
    return QString();
}

bool Dispatcher::registerAction(Cutelyst::Action *action)
{
    QString objectName = action->attribute(AttributeObject);
    if (!objectName.isEmpty()) {
        m_objects.insert(objectName, action);
        return false;
    }

    objectName = action->attribute(AttributeOnObject);
    if (objectName.isEmpty()) return false;

    m_methods.append(action);
    return true;
}

bool Dispatcher::inUse()
{
    return !m_methods.isEmpty();
}
