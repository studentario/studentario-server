#include "status_changes.h"

#include "Core/permissions.h"
#include "error.h"
#include "utils.h"

#include <Cutelyst/Plugins/Session/Session>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>

using namespace Cutelyst;

StatusChanges::StatusChanges(QObject *parent): DatabaseController(parent)
{
}

StatusChanges::~StatusChanges()
{
}

int StatusChanges::changeIdFromString(Context *c, const QString &changeIdText)
{
    bool ok = false;
    int changeId = changeIdText.toInt(&ok);
    if (!ok || changeId <= 0) {
        setError(c, Error(Error::MissingRequiredField,
                          "The given change ID cannot be parsed"));
        return -1;
    }
    return changeId;
}

void StatusChanges::index_GET(Context *c)
{
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    // Usa le permission EditUser, ma forsan nos deberea adder un specific?
    if (!permissions.can(roles, Core::Permissions::Action::EditUser)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    const QJsonObject query = Utils::requestQueryAsJson(c);
    const auto changes = m_statusChanges.userStatusChanges(query);

    QStringList fieldNames = c->request()->queryParameters("studentFields");
    auto studentFields = Core::Users::fieldsFromNames(fieldNames);
    fieldNames = c->request()->queryParameters("teacherFields");
    auto teacherFields = Core::Users::fieldsFromNames(fieldNames);
    setResponseArray(c, m_statusChanges.jsonFromDb(changes,
                                                   studentFields,
                                                   teacherFields));
}

void StatusChanges::statuschange(Context *c, const QString &changeIdText)
{
    int changeId = changeIdFromString(c, changeIdText);
    if (changeId <= 0) return; // Error jam inviate

    c->setStash("changeId", changeId);
}

void StatusChanges::statuschange_DELETE(Context *c, const QString &changeIdText)
{
    int changeId = changeIdFromString(c, changeIdText);
    if (changeId <= 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditUser)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Error error = m_statusChanges.deleteUserStatusChange(changeId);
    setResponseFromError(c, error);
}

bool StatusChanges::postFork(Cutelyst::Application *)
{
    m_statusChanges.setDatabase(m_db);
    return true;
}
