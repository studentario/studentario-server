#ifndef STUDENTARIO_ERROR_H
#define STUDENTARIO_ERROR_H

#include "Core/error.h"

#include <QJsonObject>

class Error: public Core::Error {
public:
    using Code = Core::Error::Code;

    using Core::Error::Error;
    Error(const Core::Error &error):
        Error(error.code(), error.message()) {}
    ~Error() = default;

    uint16_t httpStatus() const;

    static uint16_t httpStatusFromCode(Code code);
};

#endif // STUDENTARIO_ERROR_H
