#include "master.h"

#include "Core/authenticator.h"
#include "Core/database.h"
#include "Core/user.h"
#include "Core/users.h"
#include "error.h"

#include <QDebug>
#include <QJsonObject>

using namespace Cutelyst;

Master::Master(QObject *parent): DatabaseController(parent)
{
}

Master::~Master()
{
}

void Master::create_POST(Context *c)
{
    qDebug() << Q_FUNC_INFO;
    Core::User::Filters filters;
    filters.requestedRoles = Core::User::Role::Master;
    if (!m_db->users(filters).isEmpty()) {
        setError(c, Error(Error::PermissionDenied, "Master already created"));
        return;
    }

    using Field = Core::Users::Fields;
    QJsonObject params = c->request()->bodyJsonObject();
    QString login = params.value(Field::Login).toString();
    QByteArray password = params.value(Field::Password).toString().toUtf8();
    QByteArray salt = params.value(Field::Salt).toString().toUtf8();
    if (salt.isEmpty()) {
        salt = Core::Authenticator::generateSalt();
    }
    QString name = params.value(Field::Name).toString();

    QByteArray hash = Core::Authenticator::passwordHash(password, salt);
    Core::User master = {
        0, // id
        Core::User::Role::Master,
        login,
        hash,
        salt,
        // Nulle PIN quando le usator es create
        QByteArray(), QDateTime(),
        name, // nomine
        QDate(), // birthdate
        "", // parolas clave
        {}, // info de contacto
        QDateTime::currentDateTime(),
    };
    int userId = m_db->addUser(master);
    c->response()->setStatus(userId >= 0 ? 200 : 500);
    setResponseObject(c, {});
}
