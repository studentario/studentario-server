#ifndef STUDENTARIO_APPLICATION_H
#define STUDENTARIO_APPLICATION_H

#include "Core/database.h"

#include <Cutelyst/Application>

namespace Studentario {

class Application : public Cutelyst::Application
{
    Q_OBJECT
    CUTELYST_APPLICATION(IID "Studentario")
public:
    Q_INVOKABLE explicit Application(QObject *parent = nullptr);
    ~Application();

    bool init();

protected:
    bool postFork() override;

private:
    QScopedPointer<Core::Database> m_db;
};

} // namespace

#endif // STUDENTARIO_APPLICATION_H
