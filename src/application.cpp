#include "application.h"

#include "apiv1.h"
#include "dispatcher.h"
#include "master.h"
#include "root.h"
#ifdef ENABLE_TESTING
#include "testing.h"
#endif

#include <Cutelyst/Plugins/Session/Session>
#include <Cutelyst/Plugins/Utils/Sql>

#include <QNetworkCookie>

namespace Studentario {

Application::Application(QObject *parent):
    Cutelyst::Application(parent)
{
    QObject::connect(this, &Cutelyst::Application::beforeDispatch,
                     this, [this](Cutelyst::Context *c) {
        const QString auth = c->request()->header("Authorization");
        QString token = auth.mid(6);
        c->setStash(QStringLiteral("_c_session_id"), token);
    });

    QObject::connect(this, &Cutelyst::Application::afterDispatch,
                     this, [this](Cutelyst::Context *c) {
        /* Un REST API non usa cookies: remove los */
        const auto cookies = c->response()->cookies();
        for (const QNetworkCookie &cookie: cookies) {
            c->response()->removeCookies(cookie.name());
        }
    });
}

Application::~Application()
{
}

bool Application::init()
{
    new Root(this);
    new ApiV1Groups(this);
    new ApiV1Lessons(this);
    new ApiV1Locations(this);
    new ApiV1Login(this);
    new ApiV1Statuschanges(this);
    new ApiV1Tags(this);
    new ApiV1Users(this);
    new Master(this);
    new Dispatcher(this);
    new Cutelyst::Session(this);

#ifdef ENABLE_TESTING
    if (config(QStringLiteral("Testing")).toBool()) {
        new Testing(this);
    }
#endif

    QString connectionName =
        Cutelyst::Sql::databaseNameThread(QStringLiteral("studentario"));
    Core::Database db(config(), connectionName);
    return db.createOrUpdateDb();
}

bool Application::postFork()
{
    QString connectionName =
        Cutelyst::Sql::databaseNameThread(QStringLiteral("studentario"));
    m_db.reset(new Core::Database(config(), connectionName));
    if (Q_UNLIKELY(!m_db->init())) {
        return false;
    }

    for (Controller *c: controllers()) {
        DatabaseController *dbController =
            qobject_cast<DatabaseController*>(c);
        if (dbController) {
            dbController->setDatabase(m_db.get());
        }
    }
    return true;
}

} // namespace
