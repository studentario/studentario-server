#ifndef USERS_H
#define USERS_H

#include "database_controller.h"

#include "Core/status_changes.h"
#include "Core/tags.h"
#include "Core/users.h"

using namespace Cutelyst;

class Users: public DatabaseController
{
    Q_OBJECT
public:
    explicit Users(QObject *parent = nullptr);
    ~Users();

    C_ATTR(index, :Path :AutoArgs :ActionClass(REST))
    void index(Context *c);

    C_ATTR(index_GET, :Private)
    void index_GET(Context *c);

    C_ATTR(index_POST, :Private)
    void index_POST(Context *c);

    C_ATTR(user, :Path :CaptureArgs(1) :Object("user") :ActionClass(REST))
    void user(Context *c, const QString &userId);

    C_ATTR(user_GET, :Private)
    void user_GET(Context *c, const QString &userId);

    C_ATTR(user_POST, :Private)
    void user_POST(Context *, const QString &) {}

    C_ATTR(user_PUT, :Private)
    void user_PUT(Context *, const QString &);

    C_ATTR(user_DELETE, :Private)
    void user_DELETE(Context *c, const QString &userId);

    C_ATTR(updatePin, :OnObject("user") :ActionClass(REST))
    void updatePin(Context *) {}

    C_ATTR(updatePin_POST, :Private)
    void updatePin_POST(Context *c);

    C_ATTR(status, :OnObject("user") :ActionClass(REST))
    void status(Context *) {}

    C_ATTR(status_POST, :Private)
    void status_POST(Context *c);

protected:
    bool postFork(Application *app) override;

private:
    int userIdFromString(Context *c, const QString &userId);
    QJsonObject addUser(QJsonObject &user);

private:
    Core::Users m_users;
    Core::StatusChanges m_statusChanges;
    Core::Tags m_tags;
};

#endif //USERS_H
