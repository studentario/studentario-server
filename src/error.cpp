#include "error.h"

#include <Cutelyst/Response>
#include <QJsonValue>

uint16_t Error::httpStatus() const
{
    return httpStatusFromCode(code());
}

uint16_t Error::httpStatusFromCode(Code code)
{
    using Status = Cutelyst::Response::HttpStatus;
    switch (code) {
        case InvalidParameters:
        case MissingRequiredField:
            return Status::BadRequest;
        case InvalidPassword:
        case LoginNotFound:
        case NeedsAuthentication:
            return Status::Unauthorized;
        case PermissionDenied:
            return Status::Forbidden;
        case GroupNotFound:
        case LessonNotFound:
        case LocationNotFound:
        case UserNotFound:
        case TagNotFound:
            return Status::NotFound;
        default:
            return Status::InternalServerError;
    }
}
