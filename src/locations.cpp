#include "locations.h"

#include "Core/database.h"
#include "Core/permissions.h"
#include "error.h"
#include "users.h"
#include "utils.h"

#include <Cutelyst/Plugins/Session/Session>
#include <QDebug>
#include <QJsonArray>

using namespace Cutelyst;

Locations::Locations(QObject *parent): DatabaseController(parent)
{
}

Locations::~Locations()
{
}

int Locations::locationIdFromString(Context *c, const QString &locationId)
{
    bool ok = false;
    int id = locationId.toInt(&ok);
    if (!ok) {
        setError(c, Error(Error::MissingRequiredField,
                          "The given location ID cannot be parsed"));
        return -1;
    }
    return id;
}

void Locations::index(Context *)
{
}

void Locations::index_GET(Context *c)
{
    // Nulle controlo de permissiones, le information es public

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;

    QVector<Core::Location> locations = m_locations.locations();
    QJsonArray response = Core::Locations::jsonFromDb(locations);
    if (permissions.can(roles, Core::Permissions::Action::ViewTag)) {
        Core::Error error = m_tags.loadTags(&response,
                                            Core::Tags::Entity::Locations);
        if (Q_UNLIKELY(error)) {
            setError(c, error);
            return;
        }
    }
    setResponseArray(c, response);
}

void Locations::index_POST(Context *c)
{
    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditLocation)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    QJsonObject location = c->request()->bodyJsonObject();

    int creatorId = Session::value(c, "userId").toInt();
    location.insert(Core::Locations::Fields::CreatorId, creatorId);
    int locationId = -1;
    Error error = m_locations.addLocation(location, &locationId);
    if (!error && locationId >= 0) {
        location.insert(Core::Locations::Fields::LocationId, locationId);
        setResponseObject(c, location);
    } else {
        setError(c, error);
    }
}

void Locations::location(Context *c, const QString &locationIdText)
{
    int locationId = locationIdFromString(c, locationIdText);
    if (locationId < 0) return; // Error jam inviate

    c->setStash("locationId", locationId);
}

void Locations::location_GET(Context *c, const QString &locationIdText)
{
    qDebug() << Q_FUNC_INFO << locationIdText;
    int locationId = locationIdFromString(c, locationIdText);
    if (locationId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;

    Core::Location location;
    Error error = m_locations.location(locationId, &location);
    if (error) {
        setError(c, error);
    }

    QJsonObject response = Core::Locations::jsonFromDb(location);
    if (permissions.can(roles, Core::Permissions::Action::ViewTag)) {
        Core::Error error = m_tags.loadTags(&response,
                                            Core::Tags::Entity::Locations);
        if (Q_UNLIKELY(error)) {
            setError(c, error);
            return;
        }
    }
    setResponseObject(c, response);
}

void Locations::location_PUT(Context *c, const QString &locationIdText)
{
    qDebug() << Q_FUNC_INFO << locationIdText;
    int locationId = locationIdFromString(c, locationIdText);
    if (locationId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditLocation)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Core::Location location;
    if (Error error = m_locations.location(locationId, &location)) {
        setError(c, error);
        return;
    }

    const QJsonObject requestedChanges = c->request()->bodyJsonObject();
    Error error = m_locations.updateLocation(location, requestedChanges);
    setResponseFromError(c, error);
}

void Locations::location_DELETE(Context *c, const QString &locationIdText)
{
    int locationId = locationIdFromString(c, locationIdText);
    if (locationId < 0) return; // Error jam inviate

    Core::User::Roles roles = Utils::authenticatedUserRoles(c);
    Core::Permissions permissions;
    if (!permissions.can(roles, Core::Permissions::Action::EditLocation)) {
        setError(c, Error(Error::PermissionDenied));
        return;
    }

    Error error = m_locations.deleteLocation(locationId);
    setResponseFromError(c, error);
}

bool Locations::postFork(Cutelyst::Application *)
{
    m_locations.setDatabase(m_db);
    m_tags.setDatabase(m_db);
    return true;
}
