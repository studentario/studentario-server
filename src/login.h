#ifndef LOGIN_H
#define LOGIN_H

#include "database_controller.h"

#include "Core/authenticator.h"

using namespace Cutelyst;

class Login: public DatabaseController
{
    Q_OBJECT
public:
    explicit Login(QObject *parent = nullptr);
    ~Login();

    C_ATTR(index, :Path :AutoArgs :ActionClass(REST))
    void index(Context *c);

    C_ATTR(index_POST, :Private)
    void index_POST(Context *c);

protected:
    bool postFork(Application *app) override;

private:
    Core::Authenticator m_authenticator;
};

#endif // LOGIN_H
