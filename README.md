# Prerequisitos

## Qt

Le programma es disveloppate usante Qt 5.15. Le archivos fornite per le
QtProject functiona jam ben. Si on vole installar lo del sorgente (pro obtener
un installation minimal), on pote compilar lo assi:

    ./configure  -opensource -nomake examples -nomake tests \
        -no-dbus \
        -no-widgets \
        -no-gstreamer \
        -no-opengl \
        -system-sqlite \
        -prefix ~/Qt/5.15.2  # Usa le version real
    make -j8 module-qtbase
    # Nota: "make module-qtbase install" face un installation total!
    cd qtbase
    make install

Si on vole usar MySQL, on debe anque compilar le correspondente plugin. Si on
non ha compilate un proprie version de Qt, on pote discargar le sorgente de
qtbase e compilar le modulos SQL separatemente:

    cd qtbase/src/plugins/sqldrivers
    # Nota: usa le version real de Qt, e le percurso correcte pro MySQL
    ~/Qt/5.15.2/gcc_64/bin/qmake -- MYSQL_PREFIX="$HOME/.local/mysql/"
    make install

Si on vole usare SQLite, il es necessari usar al minus le version 3.35 qui
supporta le syntaxe "ALTER TABLE ... DROP COLUMN"; le binarios official de Qt
5.15.1 usa un version static del modulo, que es plus vetule. Alora, on debe
recompilar anque le modulo de sqlite.


## Cutelyst

Studentario es disveloppate con Cutelyst 2.13.0. On discarga le sorgente, e lo
compila usante le version de Qt que on ha previemente installate:

    PATH=~/Qt/5.15.2/bin/:$PATH cmake \
         -DCMAKE_INSTALL_PREFIX=~/.local \
         -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE \
         ..
    make -j8
    make install

On pote installar lo in un altere location, sin tanto que illo es addite al
`PATH`. Nota que `CMAKE_INSTALL_RPATH_USE_LINK_PATH` es necessari quando Qt non
es installate in un location que es mentionate in `LD_LIBRARY_PATH`.


# Compilation

## Pro developpatores

Pro disveloppar Studentario, il es consiliate compilar lo con le informationes
de copertura ("coverage") activate:

    mkdir build && cd build
    PATH=~/Qt/5.15.2/bin/:$PATH cmake \
         -DCMAKE_INSTALL_PREFIX=/tmp/opt \
         -DCMAKE_BUILD_TYPE=Coverage \
         ..
    make -j8

Il non es necessari installar lo.  Pro testar lo, il suffice exequer del
directorio de compilation (`build`) le commando:

    cutelyst2 -r --server --app-file src/libStudentario -- \
        --chdir .. \
        --ini ../devel.ini

Le base de datos es initialmente vacue. On debe crear le usator "maestro":

    curl -H "Content-Type: application/json" \
        -d '{"name":"Capo Supreme","login":"maestro","password":"parola"}' \
        http://localhost:3000/master/create

Si plus de controlo es necessari, on pote anque crear le maestro directemente
in le base de datos:

    NAME='Capo supreme'
    LOGIN=maestro
    PASSWORD=parola
    PIN="0000"
    SALT="$(openssl rand -hex 16)"
    sqlite3 /tmp/studentario.db \
        "INSERT INTO Users (isMaster, name, login, password, salt, pinCode, created)
         VALUES (1, '$NAME', '$LOGIN',
                 x'$(echo "$(echo -n $PASSWORD | xxd -p)$SALT" | xxd -p -r | openssl dgst -sha3-512 -hex | cut -d' ' -f2)',
                 x'$SALT', '$PIN', '$(date --rfc-3339=sec)');"

Postea, il essera possibile entrar in Studentario con le login "maestro" e le
contrasigno "parola".
