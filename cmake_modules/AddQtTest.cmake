macro(add_qt_test testName files)
    cmake_parse_arguments(
        ADDQTTEST
        "" #options
        "" # oneValueArgs
        "LIBRARIES;INCLUDE_DIRS;COMPILE_DEFS;ENVIRONMENT;WRAPPER" # multiValueArgs
        ${ARGN}
    )

    add_executable(${testName} ${files})

    target_link_libraries(${testName}
        PUBLIC "${ADDQTTEST_LIBRARIES}"
        PRIVATE Qt5::Core Qt5::Test
    )

    if(ADDQTTEST_INCLUDE_DIRS)
        target_include_directories(${testName} PRIVATE ${ADDQTTEST_INCLUDE_DIRS})
    endif()

    target_compile_definitions(${testName} PRIVATE
        BUILDING_TESTS=1
        ${ADDQTTEST_COMPILE_DEFS}
    )

    if(ADDQTTEST_ENVIRONMENT)
        set_tests_properties(${testName} PROPERTIES ENVIRONMENT "${ADDQTTEST_ENVIRONMENT}")
    endif()

    add_test(NAME ${testName} COMMAND ${ADDQTTEST_WRAPPER} ./${testName})
    set_property(TEST ${testName} PROPERTY TIMEOUT_AFTER_MATCH 5 "^FAIL")

endmacro()
