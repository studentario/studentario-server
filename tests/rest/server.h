#ifndef SERVER_H
#define SERVER_H

#include "request_utils.h"

#include <QProcess>
#include <QUrl>

class Server: public QProcess
{
    Q_OBJECT

public:
    Server(QObject *parent = nullptr);

    bool start();
    bool kill();

    QUrl serverUrl() const;

protected:
    void startProcess();

private:
    int m_port;
    RequestUtils m_rest;
};

#endif // SERVER_H
