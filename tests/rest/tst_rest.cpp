#include "base_test.h"
#include "test_utils.h"

#include <QCoreApplication>
#include <QDebug>
#include <QHash>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QNetworkReply>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <QUrl>
#include <QUrlQuery>
#include <memory>

class RestTest: public BaseTest
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    void testMaster();

    void testCreateUser_data();
    void testCreateUser();
    void testUpdateUser_data();
    void testUpdateUser();
    void testDeleteUser_data();
    void testDeleteUser();

    void testUserStatusChanges();

    void testCreateGroup_data();
    void testCreateGroup();
    void testUpdateGroup_data();
    void testUpdateGroup();
    void testDeleteGroup();
    void testGroupMembers();
    void testGroupTeachers();

    void testCreateLesson_data();
    void testCreateLesson();
    void testCreateLessonMany();
    void testUpdateLesson_data();
    void testUpdateLesson();
    void testDeleteLesson();
    void testLessonMembers();

    void testLocations();

    void testCreateTag_data();
    void testCreateTag();
    void testUpdateTag_data();
    void testUpdateTag();
    void testDeleteTag();
    void testTagAssignments();
};

void RestTest::initTestCase()
{
    BaseTest::initTestCase();
}

void RestTest::cleanupTestCase()
{
    BaseTest::cleanupTestCase();
}

void RestTest::init()
{
    m_fieldFilter.setTestStartTime(QDateTime::currentDateTime());
    QVERIFY(wipeDb());
    BaseTest::init();
}

void RestTest::cleanup()
{
    BaseTest::cleanup();
    m_logins.clear();
    m_groups.clear();
    m_fieldFilter.reset();
}

void RestTest::testMaster()
{
    QJsonObject input {
        { "name", "Capo supreme" },
        { "login", "grandecapo" },
        { "salt", "17resdfwh8hefdrj8fvcyd8" },
        { "password", "lassa me entrar" },
    };

    auto reply = postRequest("master/create", input);
    QVERIFY(wait(reply));
    QCOMPARE(replyStatus(reply), 200);
    delete reply;

    /* Verifica que ille pote entrar */

    QJsonObject loginData {
        { "login", "grandecapo" },
        { "password", "lassa me entrar" },
    };
    reply = postRequest("api/v1/login", loginData);
    QVERIFY(wait(reply));
    QCOMPARE(replyStatus(reply), 200);
    delete reply;

    /* Verifica que on non pote entrar con un contrasigno errate */

    loginData = {
        { "login", "grandecapo" },
        { "password", "admin" },
    };
    reply = postRequest("api/v1/login", loginData);
    QVERIFY(wait(reply));
    QCOMPARE(replyStatus(reply), 401);
    delete reply;

    /* Verifica que un altere capo supreme non pote esser create */

    input = {
        { "name", "Altere capo supreme" },
        { "login", "parvecapo" },
        { "salt", "17r84dfwh8hefdrj8fvcyd8" },
        { "password", "altere contrasigno" },
    };

    reply = postRequest("master/create", input);
    QVERIFY(wait(reply));
    QCOMPARE(replyStatus(reply), 403);
    delete reply;
}

void RestTest::testCreateUser_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QJsonObject>("user");
    QTest::addColumn<int>("expectedHttpStatus");

    const QJsonObject validUser {
        { "isStudent", true },
        { "name", "Un studente" },
        { "login", "tim" },
        { "password", "timtom" },
        { "pinCode", "6789" },
    };

    QJsonObject user = validUser;
    user.remove("login");
    QTest::newRow("no login") <<
        masterLogin << user << 200;

    QTest::newRow("ok") <<
        masterLogin << validUser << 200;

    user = validUser;
    user.insert("status", QJsonObject {
        { "status", "active" },
        { "time", "2021-02-18T07:51:00" },
    });
    QTest::newRow("con status") <<
        masterLogin << user << 200;
}

void RestTest::testCreateUser()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QJsonObject, user);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(populate());

    QVERIFY(login(loginInfo));

    auto reply = postRequest("api/v1/users", user);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void RestTest::testUpdateUser_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("userLogin");
    QTest::addColumn<QJsonObject>("changes");
    QTest::addColumn<int>("expectedHttpStatus");
    QTest::addColumn<QJsonObject>("expectedUser");

    const QJsonObject initialUserData {
        { "isStudent", true },
        { "isParent", false },
        { "isTeacher", false },
        { "isAdmin", false },
        { "isDirector", false },
        { "isMaster", false },
        { "name", "Le studente" },
        { "birthDate", "" },
        { "login", "studente" },
        { "keywords", "" },
        { "contactInfo", QJsonArray {} },
    };
    QJsonObject initialUserDataWithTags(initialUserData);
    initialUserDataWithTags.insert("tags", QJsonArray {});

    QTest::newRow("vacue") <<
        masterLogin << "studente" << QJsonObject {} << 200 <<
        initialUserDataWithTags;

    QTest::newRow("pinCode") <<
        masterLogin << "studente" << QJsonObject {
            {"pinCode", "0000"},
        } << 400 << QJsonObject{};

    QTest::newRow("usator non trovate") <<
        masterLogin << "inexistente" << QJsonObject {
            {"name", "AAA"},
        } << 404 << QJsonObject{};

    QTest::newRow("sin permissiones") <<
        studentLogin << "studente" << QJsonObject {
            {"name", "AAA"},
        } << 403 << QJsonObject{};

    QJsonObject user = initialUserDataWithTags;
    user["name"] = "Le ver studente";
    QTest::newRow("nomine") <<
        masterLogin << "studente" << QJsonObject {
            {"name", "Le ver studente"},
        } << 200 << user;

    user = initialUserDataWithTags;
    user["keywords"] = "uno, duo, tres";
    QTest::newRow("parolas clave") <<
        masterLogin << "studente" << QJsonObject {
            {"keywords", "uno, duo, tres" },
        } << 200 << user;

    user = initialUserDataWithTags;
    user["contactInfo"] = QJsonArray { "uno", "duo" };
    QTest::newRow("parolas clave") <<
        masterLogin << "studente" << QJsonObject {
            {"contactInfo", QJsonArray { "uno", "duo" } },
        } << 200 << user;

    user = initialUserDataWithTags;
    user["birthDate"] = "1998-10-20";
    QTest::newRow("data de nascentia") <<
        masterLogin << "studente" << QJsonObject {
            {"birthDate", "1998-10-20" },
        } << 200 << user;

    user = initialUserDataWithTags;
    user["isStudent"] = true;
    user["isParent"] = true;
    user["isTeacher"] = false;
    user["isAdmin"] = true;
    user["isDirector"] = true;
    QTest::newRow("cambia permissiones") <<
        masterLogin << "studente" << QJsonObject {
            {"isStudent", true },
            {"isParent", true },
            {"isTeacher", false },
            {"isAdmin", true },
            {"isDirector", true },
        } << 200 << user;

    QTest::newRow("cambia permissiones como administrator") <<
        adminLogin << "studente" << QJsonObject {
            {"isStudent", true },
            {"isAdmin", true },
        } << 403 << QJsonObject {};
}

void RestTest::testUpdateUser()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QString, userLogin);
    QFETCH(QJsonObject, changes);
    QFETCH(int, expectedHttpStatus);
    QFETCH(QJsonObject, expectedUser);

    QVERIFY(populate(BasicUsers | MoreUsers));

    QVERIFY(login(loginInfo));

    // si le login non existe, usa un numero clarmente inexistente
    int userId = m_logins.value(userLogin, 213423);

    QString path = "api/v1/users/" + QString::number(userId);
    auto reply = putRequest(path, changes);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;

    if (expectedHttpStatus == 200) {
        reply = getRequest(path);
        QVERIFY(wait(reply));

        QCOMPARE(replyStatus(reply), 200);
        QJsonObject json = replyJsonObject(reply)["data"].toObject();
        m_fieldFilter.setFieldsToRemove({
            "created", "creatorId", "icon", "color",
        });
        QJsonObject user = m_fieldFilter.filter(json);
        QCOMPARE(user["userId"].toInt(), userId);
        user.remove("userId");
        QCOMPARE(user, expectedUser);
    }
}

void RestTest::testDeleteUser_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<int>("userId");
    QTest::addColumn<int>("expectedHttpStatus");

    QTest::newRow("not found") <<
        masterLogin << 13245 << 404;

    QTest::newRow("no permissions") <<
        studentLogin << 1 << 403;

    QTest::newRow("ok") <<
        masterLogin << 2 << 200;
}

void RestTest::testDeleteUser()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(int, userId);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(populate());

    QVERIFY(login(loginInfo));

    auto reply = deleteRequest("api/v1/users/" + QString::number(userId));

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void RestTest::testUserStatusChanges()
{
    QVERIFY(populate(BasicUsers | GroupsWithMembers));
    QVERIFY(login(masterLogin));

    const QString userUrl = "api/v1/users/";
    const QString statusUrl = "api/v1/statuschanges/";

    auto addStatusChange = [this,&userUrl](int userId, const QJsonObject &data) {
        QString url = userUrl + QString::number(userId) + "/status";
        return std::unique_ptr<QNetworkReply>(postRequest(url, data));
    };

    // Initialmente, nulle cambio de stato
    {
        QScopedPointer<QNetworkReply> reply(getRequest(statusUrl));
        QCOMPARE(replyStatus(reply.get()), 200);
        const QJsonArray json = replyJsonObject(reply.data())["data"].toArray();
        QCOMPARE(json.count(), 0);
    }

    const int student1Id = m_logins.value("studente");
    const int student2Id = m_logins.value("studente2");
    const int masterId = 1;

    // Adde alicun cambios de stato
    {
        // Crea in ordine inverse, pro verificar le ordinamento
        auto reply = addStatusChange(student1Id, {
            { "status", "inactive" },
            { "time", "2023-03-20T07:01:00" },
            { "reason", "Abandonate" },
        });
        QCOMPARE(replyStatus(reply.get()), 200);
        QJsonObject data = replyJsonObject(reply.get())["data"].toObject();
        QVERIFY(data["changeId"].toInt() > 0);
        QCOMPARE(data["creatorId"].toInt(), masterId);
        reply = addStatusChange(student2Id, {
            { "status", "active" },
            { "time", "2023-03-18T07:01:00" },
            { "reason", "Prime studente2" },
        });
        QCOMPARE(replyStatus(reply.get()), 200);
        reply = addStatusChange(student1Id, {
            { "status", "active" },
            { "time", "2023-03-19T07:01:00" },
            { "reason", "Nove studente" },
        });
        QCOMPARE(replyStatus(reply.get()), 200);
    }

    // Verifica le addition
    {
        QScopedPointer<QNetworkReply> reply(getRequest(statusUrl));
        QCOMPARE(replyStatus(reply.get()), 200);
        const QJsonArray json = replyJsonObject(reply.data())["data"].toArray();
        m_fieldFilter.setFieldsToRemove({
            "locationId",
        });
        QJsonArray statusChanges = m_fieldFilter.filter(json);
        QVERIFY(!m_fieldFilter.hasError());
        QJsonArray expectedStatusChanges = QJsonDocument::fromJson(R"(
        [
          {
            "changeId": 2,
            "creatorId":1,"reason":"Prime studente2","status":"active",
            "groups":[
              {"description":"Prime gruppo","groupId":1,"name":"Gruppo 1"}
            ],
            "teachers":[{"userId":3}],
            "time":"2023-03-18T07:01:00",
            "user":{"userId":4},"userId":4
          }, {
            "changeId": 3,
            "creatorId":1,"reason":"Nove studente","status":"active",
            "groups":[
              {"description":"Prime gruppo","groupId":1,"name":"Gruppo 1"}
            ],
            "teachers":[{"userId":3}],
            "time":"2023-03-19T07:01:00",
            "user":{"userId":2},"userId":2
          }, {
            "changeId": 1,
            "creatorId":1,"reason":"Abandonate","status":"inactive",
            "groups":[
              {"description":"Prime gruppo","groupId":1,"name":"Gruppo 1"}
            ],
            "teachers":[{"userId":3}],
            "time":"2023-03-20T07:01:00",
            "user":{"userId":2},"userId":2
          }
        ])").array();
        QCOMPARE(statusChanges, expectedStatusChanges);
    }

    // Remove un record
    {
        QString url = statusUrl + "/2";
        QScopedPointer<QNetworkReply> reply(deleteRequest(url));
        QCOMPARE(replyStatus(reply.get()), 200);
    }

    // Verifica le remotion
    {
        QScopedPointer<QNetworkReply> reply(getRequest(statusUrl));
        QCOMPARE(replyStatus(reply.get()), 200);
        const QJsonArray json = replyJsonObject(reply.data())["data"].toArray();
        m_fieldFilter.setFieldsToRemove({
            "changeId",
            "creatorId", "status", "groups", "teachers", "time", "user",
        });
        QJsonArray statusChanges = m_fieldFilter.filter(json);
        QVERIFY(!m_fieldFilter.hasError());
        QJsonArray expectedStatusChanges = QJsonDocument::fromJson(R"(
        [
          { "reason":"Nove studente", "userId":2 },
          { "reason":"Abandonate", "userId":2 }
        ])").array();
        QCOMPARE(statusChanges, expectedStatusChanges);
    }
}

void RestTest::testCreateGroup_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QJsonObject>("group");
    QTest::addColumn<int>("expectedHttpStatus");

    const QJsonObject validGroup {
        { "name", "Un gruppo" },
        { "description", "Description del gruppo" },
    };

    QTest::newRow("vacue") <<
        masterLogin <<
        QJsonObject {} << 400;

    QJsonObject group = validGroup;
    group.remove("name");
    QTest::newRow("sin nomine") <<
        masterLogin <<
        group << 400;

    group = validGroup;
    group.remove("description");
    QTest::newRow("sin description") <<
        masterLogin <<
        group << 200;

    QTest::newRow("no permissions") <<
        studentLogin <<
        validGroup << 403;

    QTest::newRow("ok") <<
        masterLogin <<
        validGroup << 200;
}

void RestTest::testCreateGroup()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QJsonObject, group);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(populate());

    QVERIFY(login(loginInfo));

    auto reply = postRequest("api/v1/groups", group);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void RestTest::testUpdateGroup_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("groupId");
    QTest::addColumn<QJsonObject>("changes");
    QTest::addColumn<int>("expectedHttpStatus");
    QTest::addColumn<QJsonObject>("expectedGroup");

    /* Mantene isto in sync con populate() */
    const QJsonObject initialGroup1 = {
            { "groupId", 1 },
            { "name", "Gruppo 1" },
            { "description", "Prime gruppo" },
            { "locationId", 1 },
            { "location", QJsonObject {
                { "locationId", 1 },
                { "name", "Sala 1" },
                { "description", "Prime sala" },
            }},
    };
    QJsonObject initialGroup1WithTags(initialGroup1);
    initialGroup1WithTags.insert("tags", QJsonArray());

    QTest::newRow("non trovate") <<
        masterLogin << "13245" <<
        QJsonObject {{ "name", "Gruppo" }} << 404 << initialGroup1;

    QTest::newRow("ID del gruppo invalide") <<
        studentLogin << "1esf3" <<
        QJsonObject {} << 400 << initialGroup1;

    QTest::newRow("sin permissions") <<
        studentLogin << "1" <<
        QJsonObject {{ "name", "Gruppo" }} << 403 << initialGroup1;

    QJsonObject expectedGroup(initialGroup1WithTags);
    expectedGroup["name"] = "Nove";
    QTest::newRow("ok") <<
        masterLogin << "1" <<
        QJsonObject {
            { "name", "Nove" }
        } << 200 << expectedGroup;
}

void RestTest::testUpdateGroup()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QJsonObject, changes);
    QFETCH(QString, groupId);
    QFETCH(int, expectedHttpStatus);
    QFETCH(QJsonObject, expectedGroup);

    QVERIFY(populate(BasicUsers | EmptyGroups));

    QVERIFY(login(loginInfo));

    QString path = "api/v1/groups/" + groupId;
    auto reply = putRequest(path, changes);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;

    if (expectedHttpStatus == 200 ||
        expectedHttpStatus == 403) {
        // Verifica que le gruppo es lo que on expecta
        reply = getRequest(path);
        QVERIFY(wait(reply));
        QJsonObject json = replyJsonObject(reply)["data"].toObject();
        m_fieldFilter.setFieldsToRemove({
            "created", "creatorId", "icon", "color", "parentTagId", "tagId",
        });
        QJsonObject group = m_fieldFilter.filter(json);
        QCOMPARE(group, expectedGroup);
        delete reply;
    }
}

void RestTest::testDeleteGroup()
{
    // Casos de errores es testate in le tests a sol lectura
    QVERIFY(populate(BasicUsers | EmptyGroups));

    QVERIFY(login(masterLogin));

    auto reply = deleteRequest("api/v1/groups/2");

    QCOMPARE(replyStatus(reply), 200);
    delete reply;
}

void RestTest::testGroupMembers()
{
    QVERIFY(populate(BasicUsers | EmptyGroups));
    QVERIFY(login(masterLogin));

    int groupId = m_groups.value("Gruppo 1");
    QString baseUrl =
        "api/v1/groups/" + QString::number(groupId) + "/students";

    bool ok = createUser({
        { "isStudent", true },
        { "name", "Altere studente" },
        { "login", "studente2" },
        { "password", "stud3nt32" },
        { "pinCode", "0000" },
    });
    QVERIFY(ok);
    int student1Id = m_logins.value("studente");
    int student2Id = m_logins.value("studente2");

    auto studentNamesInGroup = [this](QNetworkReply *reply) {
        QStringList ret;
        const QJsonArray json = replyJsonObject(reply)["data"].toArray();
        for (const QJsonValue &v: json) {
            ret.append(v.toObject().value("name").toString());
        }
        return ret;
    };

    // Verifica que on pote adder studentes al gruppo
    {
        QJsonObject creationData {
            { "userIds", QJsonArray { student1Id, student2Id }},
        };
        auto reply = postRequest(baseUrl, creationData);
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    // Verifica que le studentes esseva addite
    {
        auto reply = getRequest(baseUrl);
        QCOMPARE(replyStatus(reply), 200);
        const QStringList expectedNames {"Altere studente", "Le studente"};
        QCOMPARE(studentNamesInGroup(reply), expectedNames);
        delete reply;
    }

    // Verifica que studentes pote esser removite del gruppo
    {
        auto reply = deleteRequest(baseUrl + "/" +
                                   QString::number(student1Id));
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    // Verifica que le studentes esseva removite
    {
        auto reply = getRequest(baseUrl);
        QCOMPARE(replyStatus(reply), 200);
        const QStringList expectedNames {"Altere studente"};
        QCOMPARE(studentNamesInGroup(reply), expectedNames);
        delete reply;
    }
}

void RestTest::testGroupTeachers()
{
    QVERIFY(populate(BasicUsers | EmptyGroups));
    QVERIFY(login(masterLogin));

    int groupId = m_groups.value("Gruppo 1");
    QString baseUrl =
        "api/v1/groups/" + QString::number(groupId) + "/teachers";

    bool ok = createUser({
        { "isTeacher", true },
        { "name", "Prime inseniante" },
        { "login", "teacher1" },
        { "password", "ins3ni4nt3" },
        { "pinCode", "0000" },
    });
    QVERIFY(ok);

    ok = createUser({
        { "isTeacher", true },
        { "name", "Secunde inseniante" },
        { "login", "teacher2" },
        { "password", "1ns3ni4nt3" },
        { "pinCode", "0000" },
    });
    QVERIFY(ok);
    int teacher1Id = m_logins.value("teacher1");
    int teacher2Id = m_logins.value("teacher2");

    auto teacherNamesInGroup = [this](QNetworkReply *reply) {
        QStringList ret;
        const QJsonArray json = replyJsonObject(reply)["data"].toArray();
        for (const QJsonValue &v: json) {
            ret.append(v.toObject().value("name").toString());
        }
        return ret;
    };

    // Verifica que on pote adder inseniantes al gruppo
    {
        QJsonObject creationData {
            { "userIds", QJsonArray { teacher1Id, teacher2Id }},
        };
        auto reply = postRequest(baseUrl, creationData);
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    // Verifica que le inseniantes esseva addite
    {
        auto reply = getRequest(baseUrl);
        QCOMPARE(replyStatus(reply), 200);
        const QStringList expectedNames {
            "Prime inseniante", "Secunde inseniante"
        };
        QCOMPARE(teacherNamesInGroup(reply), expectedNames);
        delete reply;
    }

    // Verifica que inseniantes pote esser removite del gruppo
    {
        auto reply = deleteRequest(baseUrl + "/" +
                                   QString::number(teacher2Id));
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    // Verifica que le inseniantes esseva removite
    {
        auto reply = getRequest(baseUrl);
        QCOMPARE(replyStatus(reply), 200);
        const QStringList expectedNames {"Prime inseniante"};
        QCOMPARE(teacherNamesInGroup(reply), expectedNames);
        delete reply;
    }
}

void RestTest::testCreateLesson_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QJsonObject>("lesson");
    QTest::addColumn<int>("expectedHttpStatus");

    const QJsonObject validLesson {
        { "startTime", "2023-03-19T16:00:00" },
        { "endTime", "2023-03-19T17:00:00" },
    };

    QTest::newRow("vacue") <<
        masterLogin <<
        QJsonObject {} << 400;

    QJsonObject lesson = validLesson;
    lesson.remove("endTime");
    QTest::newRow("sin tempore final") <<
        masterLogin <<
        lesson << 400;

    lesson = validLesson;
    lesson.remove("startTime");
    QTest::newRow("sin tempore initial") <<
        masterLogin <<
        lesson << 400;

    QTest::newRow("no permissions") <<
        studentLogin <<
        validLesson << 403;

    QTest::newRow("ok") <<
        masterLogin <<
        validLesson << 200;
}

void RestTest::testCreateLesson()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QJsonObject, lesson);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(populate());

    QVERIFY(login(loginInfo));

    auto reply = postRequest("api/v1/lessons", lesson);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void RestTest::testCreateLessonMany()
{
    QVERIFY(populate(BasicUsers | GroupsWithMembers));
    QVERIFY(login(masterLogin));

    const int student2Id = m_logins.value("studente2");
    const int teacherId = m_logins.value("inseniante1");
    const int groupId = m_groups.value("Gruppo 1");

    const QJsonObject request {
        { "lessons", QJsonArray {
            QJsonObject {
                { "startTime", "2023-03-19T16:00:00" },
                { "endTime", "2023-03-19T17:00:00" },
            },
            QJsonObject {
                { "startTime", "2023-04-19T16:00:00" },
                { "endTime", "2023-04-19T17:00:00" },
            },
            QJsonObject {
                { "startTime", "2023-05-31T16:00:00" },
                { "endTime", "2023-05-31T17:00:00" },
            },
        }},
        { "invitees", QJsonObject {
            { "groups", QJsonArray { groupId }},
            { "students", QJsonArray { student2Id }},
            { "teachers", QJsonArray { teacherId }},
        }},
    };
    QScopedPointer<QNetworkReply> reply(postRequest("api/v1/lessons",
                                                    request));
    QCOMPARE(replyStatus(reply.get()), 200);

    const QJsonArray json = replyJsonArray(reply.get());
    m_fieldFilter.setFieldsToRemove({ "lessonId" });
    QJsonArray response = m_fieldFilter.filter(json);
    QVERIFY(!m_fieldFilter.hasError());
    const QJsonArray expectedResponse = QJsonDocument::fromJson(R"(
    [
      {
        "data":{
          "creatorId": 1,
          "endTime":"2023-03-19T17:00:00", "startTime":"2023-03-19T16:00:00"
        },
        "status":"ok"
      },
      {
        "data":{
          "creatorId": 1,
          "endTime":"2023-04-19T17:00:00", "startTime":"2023-04-19T16:00:00"
        },
        "status":"ok"
      },
        {"data":{
          "creatorId": 1,
          "endTime":"2023-05-31T17:00:00", "startTime":"2023-05-31T16:00:00"
        },
        "status":"ok"
      }
    ])").array();
    QCOMPARE(response, expectedResponse);

    /* Check that the lessons and the invitations have been recorder */
    m_fieldFilter.setFieldsToRemove({
        "lessonId", "created", "modified", "locationId", "attended", "note",
        "creatorId", "userId", "groupId", "endTime", "description",
    });
    {
        QScopedPointer<QNetworkReply>
            reply(getRequest("api/v1/lessons", QUrlQuery("userFields=name")));
        QJsonArray response =
            m_fieldFilter.filter(replyJsonObject(reply.get())["data"].toArray());
        const QJsonArray expectedResponse = QJsonDocument::fromJson(R"(
        [
          {
            "startTime":"2023-03-19T16:00:00",
            "groups":[{"group":{"name":"Gruppo 1"},"invited":false}],
            "students":[
              {"invited":true,"user":{"name":"Altere studente"}},
              {"invited":false,"user":{"name":"Le studente"}}
            ],
            "teachers":[{"invited":false,"user":{"name":"Inseniante 123"}}]
          },
          {
            "startTime":"2023-04-19T16:00:00",
            "groups":[{"group":{"name":"Gruppo 1"},"invited":false}],
            "students":[
              {"invited":true,"user":{"name":"Altere studente"}},
              {"invited":false,"user":{"name":"Le studente"}}
            ],
            "teachers":[{"invited":false,"user":{"name":"Inseniante 123"}}]
          },
          {
            "startTime":"2023-05-31T16:00:00",
            "groups":[{"group":{"name":"Gruppo 1"},"invited":false}],
            "students":[
              {"invited":true,"user":{"name":"Altere studente"}},
              {"invited":false,"user":{"name":"Le studente"}}
            ],
            "teachers":[{"invited":false,"user":{"name":"Inseniante 123"}}]
          }
        ])").array();
        QCOMPARE(response, expectedResponse);
    }
}

void RestTest::testUpdateLesson_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("lessonId");
    QTest::addColumn<QJsonObject>("changes");
    QTest::addColumn<int>("expectedHttpStatus");
    QTest::addColumn<QJsonObject>("expectedLesson");

    /* Mantene isto in sync con populate() */
    const QJsonObject initialLesson1 = {
            { "lessonId", 1 },
            { "startTime", "2023-03-19T06:00:00" },
            { "endTime", "2023-03-19T07:00:00" },
            { "locationId", -1 },
            { "groups", QJsonArray{} },
            { "students", QJsonArray{} },
            { "teachers", QJsonArray{} },
    };

    QTest::newRow("non trovate") <<
        masterLogin << "13245" <<
        QJsonObject {{ "endTime", "2023-03-19T06:50:00" }} <<
        404 << initialLesson1;

    QTest::newRow("ID del lection invalide") <<
        studentLogin << "1esf3" <<
        QJsonObject {} << 400 << initialLesson1;

    QTest::newRow("sin permissiones") <<
        studentLogin << "1" <<
        QJsonObject {{ "endTime", "2023-03-19T06:50:00" }} <<
        403 << initialLesson1;

    QJsonObject expectedLesson(initialLesson1);
    expectedLesson["endTime"] = "2023-03-19T06:50:00";
    QTest::newRow("ok") <<
        masterLogin << "1" <<
        QJsonObject {
            { "endTime", "2023-03-19T06:50:00" }
        } << 200 << expectedLesson;

    expectedLesson = initialLesson1;
    expectedLesson["locationId"] = 1;
    expectedLesson["location"] = QJsonObject {
        { "locationId", 1 },
        { "name", "Sala 1" },
        { "description", "Prime sala" },
        { "color", "#ff8000" },
    };
    QTest::newRow("cambia location") <<
        masterLogin << "1" <<
        QJsonObject {
            { "locationId", 1 }
        } << 200 << expectedLesson;
}

void RestTest::testUpdateLesson()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QJsonObject, changes);
    QFETCH(QString, lessonId);
    QFETCH(int, expectedHttpStatus);
    QFETCH(QJsonObject, expectedLesson);

    QVERIFY(populate(BasicUsers | EmptyLessons | Locations));

    QVERIFY(login(loginInfo));

    QString path = "api/v1/lessons/" + lessonId;
    auto reply = putRequest(path, changes);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;

    if (expectedHttpStatus == 200 ||
        expectedHttpStatus == 403) {
        // Verifica que le lection es lo que on expecta
        QVERIFY(login(masterLogin));
        reply = getRequest(path);
        QVERIFY(wait(reply));
        QJsonObject json = replyJsonObject(reply)["data"].toObject();
        m_fieldFilter.setFieldsToRemove({
            "created", "creatorId", "icon",
        });
        QJsonObject lesson = m_fieldFilter.filter(json);
        QCOMPARE(lesson, expectedLesson);
        delete reply;
    }
}

void RestTest::testDeleteLesson()
{
    QVERIFY(populate(BasicUsers | EmptyLessons));

    QVERIFY(login(masterLogin));

    auto reply = deleteRequest("api/v1/lessons/2");

    QCOMPARE(replyStatus(reply), 200);
    delete reply;
}

void RestTest::testLessonMembers()
{
    QVERIFY(populate(BasicUsers | GroupsWithMembers | EmptyLessons));
    QVERIFY(login(masterLogin));

    const int lessonId = 1;
    QString baseUrl = "api/v1/lessons/" + QString::number(lessonId);

    const int student1Id = m_logins.value("studente");
    const int student2Id = m_logins.value("studente2");
    const int teacherId = m_logins.value("inseniante1");
    const int groupId = m_groups.value("Gruppo 1");

    auto userNamesInLesson = [this](const QJsonObject &json,
                                    const QString &userType) {
        QStringList ret;
        const QJsonArray users = json[userType].toArray();
        for (const QJsonValue &v: users) {
            ret.append(v.toObject()["user"].toObject()["name"].toString());
        }
        return ret;
    };

    auto participationByUserId = [this](const QJsonObject &json,
                                        const QString &userType,
                                        int userId) {
        const QJsonArray users = json[userType].toArray();
        for (const QJsonValue &v: users) {
            const QJsonObject user = v.toObject();
            if (user["user"].toObject()["userId"].toInt() == userId) {
                return user;
            }
        }
        return QJsonObject();
    };

    // Verifica que on pote invitar gruppos e studentes al lection
    {
        QJsonObject inviteData {
            { "groups", QJsonArray { groupId }},
            { "students", QJsonArray { student2Id }},
        };
        auto reply = postRequest(baseUrl + "/invitees", inviteData);
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    // Verifica que le gruppo es invitate
    {
        auto reply = getRequest(baseUrl);
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        const QJsonArray groups = json["groups"].toArray();
        QCOMPARE(groups.count(), 1);
        const QJsonObject group = groups[0].toObject()["group"].toObject();
        QCOMPARE(group["name"].toString(), "Gruppo 1");
        // Le lista contine le studentes del gruppo, plus le invitos individual
        QCOMPARE(json["students"].toArray().count(), 2);
        const QJsonObject expectedStudent1 {{"userId", student2Id }};
        QCOMPARE(json["students"].toArray()[0].toObject()["user"].toObject(),
                 expectedStudent1);
        const QJsonObject expectedStudent2 {{"userId", student1Id }};
        QCOMPARE(json["students"].toArray()[1].toObject()["user"].toObject(),
                 expectedStudent2);
        QCOMPARE(json["teachers"].toArray().count(), 1);
        delete reply;
    }

    // Verifica que on pote reciper le nomine del participantes
    {
        auto reply = getRequest(baseUrl, QUrlQuery("userFields=name"));
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QStringList expectedStudents { "Altere studente", "Le studente" };
        QCOMPARE(userNamesInLesson(json, "students"), expectedStudents);
        QStringList expectedTeachers { "Inseniante 123" };
        QCOMPARE(userNamesInLesson(json, "teachers"), expectedTeachers);
        delete reply;
    }

    // Verifica que le mesme information es presente in /attendance
    {
        auto reply = getRequest(baseUrl + "/attendance",
                                QUrlQuery("userFields=name"));
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QStringList expectedStudents { "Altere studente", "Le studente" };
        QCOMPARE(userNamesInLesson(json, "students"), expectedStudents);
        QStringList expectedTeachers { "Inseniante 123" };
        QCOMPARE(userNamesInLesson(json, "teachers"), expectedTeachers);
        delete reply;
    }

    // Registra le participation
    {
        const QJsonObject attendance {
            { "students", QJsonArray {
                QJsonObject {
                    { "userId", student1Id },
                    { "note", "Multo attente" },
                    { "attended", true },
                },
            }},
            { "teachers", QJsonArray {
                QJsonObject {
                    { "userId", teacherId },
                    { "attended", true },
                },
            }},
        };
        auto reply = putRequest(baseUrl + "/attendance", attendance);
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    // Verifica le participation
    {
        auto reply = getRequest(baseUrl + "/attendance",
                                QUrlQuery("userFields=name"));
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QStringList expectedStudents { "Altere studente", "Le studente" };
        /* Nota le sort(): al momento, le ordine retornate del servitor non es
         * alphabetic! TODO */
        QStringList studentNames = userNamesInLesson(json, "students");
        studentNames.sort();
        QCOMPARE(studentNames, expectedStudents);
        QStringList expectedTeachers { "Inseniante 123" };
        QCOMPARE(userNamesInLesson(json, "teachers"), expectedTeachers);

        const QJsonObject student1 =
            participationByUserId(json, "students", student1Id);
        QCOMPARE(student1["attended"].toBool(), true);
        QCOMPARE(student1["note"].toString(), "Multo attente");
        const QJsonObject student2 =
            participationByUserId(json, "students", student2Id);
        QCOMPARE(student2["attended"].toBool(), false);
        QVERIFY(student2["note"].toString().isEmpty());
        delete reply;
    }

    // Cambia le participation
    {
        const QJsonObject attendance {
            { "students", QJsonArray {
                QJsonObject {
                    { "userId", student1Id },
                    { "attended", false },
                },
            }},
        };
        auto reply = putRequest(baseUrl + "/attendance", attendance);
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    // Verifica le nove participation
    {
        auto reply = getRequest(baseUrl + "/attendance",
                                QUrlQuery("userFields=name"));
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QCOMPARE(json["students"].toArray().count(), 2);
        const QJsonObject student1 =
            participationByUserId(json, "students", student1Id);
        QCOMPARE(student1["attended"].toBool(), false);
        QCOMPARE(student1["note"].toString(), "Multo attente");
        delete reply;
    }

    // Remove le invitation
    {
        auto reply = deleteRequest(baseUrl + "/invitees",
                                   QUrlQuery("groups=1"));
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    // Verifica que le gruppo non plus es invitate
    {
        auto reply = getRequest(baseUrl);
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        const QJsonArray groups = json["groups"].toArray();
        QCOMPARE(groups.count(), 0);
        QCOMPARE(json["students"].toArray().count(), 2);
        QCOMPARE(json["teachers"].toArray().count(), 1);
        delete reply;
    }

    /* Remove le invitation al studentes: nos ha registrate le participation de
     * studente1, alora ille non pote esser disinvitate. */
    {
        QString query =
            QString("students=%1&students=%2").arg(student1Id).arg(student2Id);
        auto reply = deleteRequest(baseUrl + "/invitees", QUrlQuery(query));
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    /* Verifica que studente1 es presente, studente2 manca. */
    {
        auto reply = getRequest(baseUrl);
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QCOMPARE(json["students"].toArray().count(), 1);
        QCOMPARE(json["teachers"].toArray().count(), 1);
        const QJsonObject student1 =
            participationByUserId(json, "students", student1Id);
        QCOMPARE(student1["attended"].toBool(), false);
        QCOMPARE(student1["note"].toString(), "Multo attente");
        const QJsonObject student2 =
            participationByUserId(json, "students", student2Id);
        QVERIFY(student2.isEmpty());
        delete reply;
    }
}

void RestTest::testLocations()
{
    QVERIFY(populate(BasicUsers));
    QVERIFY(login(masterLogin));

    const int locationId = 1;
    QString baseUrl = "api/v1/locations/";

    // Creation
    {
        QJsonObject creationData {
            { "name", "Sala 1" },
            { "description", "Prime sala" },
            { "color", "#80ff00" },
            { "icon", "file:///icon.svg" },
        };
        auto reply = postRequest(baseUrl, creationData);
        QCOMPARE(replyStatus(reply), 200);
        delete reply;
    }

    m_fieldFilter.setFieldsToRemove({
        "created",
    });

    // Lectura
    {
        auto reply = getRequest(baseUrl + QString::number(locationId));
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QJsonObject location = m_fieldFilter.filter(json);
        QVERIFY(!m_fieldFilter.hasError());
        const QJsonObject expectedLocation {
            { "locationId", locationId },
            { "creatorId", 1 }, // master
            { "name", "Sala 1" },
            { "description", "Prime sala" },
            { "color", "#80ff00" },
            { "icon", "file:///icon.svg" },
            { "tags", QJsonArray {}},
        };
        QCOMPARE(location, expectedLocation);
        delete reply;
    }

    // Actualisation
    {
        const QJsonObject changes {
            { "name", "Sala 2" },
            { "description", "Secunde sala" },
            { "color", "#fFffff" },
            { "icon", "file:///icon2.png" },
        };
        auto reply = putRequest(baseUrl + QString::number(locationId),
                                changes);
        QCOMPARE(replyStatus(reply), 200);

        reply = getRequest(baseUrl + QString::number(locationId));
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QJsonObject location = m_fieldFilter.filter(json);
        QVERIFY(!m_fieldFilter.hasError());
        const QJsonObject expectedLocation {
            { "locationId", locationId },
            { "creatorId", 1 }, // master
            { "name", "Sala 2" },
            { "description", "Secunde sala" },
            { "color", "#ffffff" },
            { "icon", "file:///icon2.png" },
            { "tags", QJsonArray {}},
        };
        QCOMPARE(location, expectedLocation);
        delete reply;
    }

    // Deletion
    {
        auto reply = deleteRequest(baseUrl + QString::number(locationId));
        QCOMPARE(replyStatus(reply), 200);
        delete reply;

        reply = getRequest(baseUrl + QString::number(locationId));
        QCOMPARE(replyStatus(reply), 404);
    }
}

void RestTest::testCreateTag_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QJsonObject>("tag");
    QTest::addColumn<int>("expectedHttpStatus");

    QTest::newRow("vacue") <<
        masterLogin <<
        QJsonObject {} << 400;

    QTest::newRow("sin nomine") <<
        masterLogin <<
        QJsonObject {{ "description", "sin nomine" }} << 400;

    const QJsonObject validTag {
        { "name", "a valid tag" },
    };
    QJsonObject tag(validTag);
    tag.insert("parentTagId", 123);
    QTest::newRow("parente non valide") <<
        masterLogin <<
        tag << 400;

    QTest::newRow("no permissions") <<
        studentLogin <<
        validTag << 403;

    QTest::newRow("ok") <<
        masterLogin <<
        validTag << 200;
}

void RestTest::testCreateTag()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QJsonObject, tag);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(populate());

    QVERIFY(login(loginInfo));

    auto reply = postRequest("api/v1/tags", tag);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void RestTest::testUpdateTag_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("tagId");
    QTest::addColumn<QJsonObject>("changes");
    QTest::addColumn<int>("expectedHttpStatus");
    QTest::addColumn<QJsonObject>("expectedTag");

    /* Mantene isto in sync con populate() */
    const QJsonObject initialTag1 = {
            { "tagId", 1 },
            { "name", "ParentTag 1" },
            { "description", "Prime etiquetta" },
            { "creatorId", 1 },
            { "icon", "" },
            { "color", "" },
            { "parentTagId", -1 },
    };

    QTest::newRow("non trovate") <<
        masterLogin << "13245" <<
        QJsonObject {{ "name", "Mancante" }} <<
        404 << initialTag1;

    QTest::newRow("ID del etiquetta invalide") <<
        studentLogin << "1esf3" <<
        QJsonObject {} << 400 << initialTag1;

    QTest::newRow("sin permissiones") <<
        studentLogin << "1" <<
        QJsonObject {{ "name", "Non pote" }} <<
        403 << initialTag1;

    QJsonObject expectedTag(initialTag1);
    expectedTag["color"] = "#abc123";
    QTest::newRow("ok") <<
        masterLogin << "1" <<
        QJsonObject {
            { "color", "#ABC123" }
        } << 200 << expectedTag;
}

void RestTest::testUpdateTag()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QJsonObject, changes);
    QFETCH(QString, tagId);
    QFETCH(int, expectedHttpStatus);
    QFETCH(QJsonObject, expectedTag);

    QVERIFY(populate(Tags));

    QVERIFY(login(loginInfo));

    QString path = "api/v1/tags/" + tagId;
    auto reply = putRequest(path, changes);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;

    if (expectedHttpStatus == 200 ||
        expectedHttpStatus == 403) {
        // Verifica que le lection es lo que on expecta
        QVERIFY(login(masterLogin));
        reply = getRequest(path);
        QVERIFY(wait(reply));
        QJsonObject tag = replyJsonObject(reply)["data"].toObject();
        tag.remove("created");
        QCOMPARE(tag, expectedTag);
        delete reply;
    }
}

void RestTest::testDeleteTag()
{
    QVERIFY(populate(Tags));

    QVERIFY(login(masterLogin));

    // On non pote remover un etiquetta con filios
    {
        QScopedPointer<QNetworkReply> reply(deleteRequest("api/v1/tags/2"));
        QCOMPARE(replyStatus(reply.get()), 400);
    }

    {
        QScopedPointer<QNetworkReply> reply(deleteRequest("api/v1/tags/4"));
        QCOMPARE(replyStatus(reply.get()), 200);
    }
}

void RestTest::testTagAssignments()
{
    QVERIFY(populate(MoreUsers | EmptyGroups | Tags));
    QVERIFY(login(masterLogin));

    QString baseUrl = "api/v1/tags/";

    const int student1Id = m_logins.value("studente");
    const int student2Id = m_logins.value("studente3");
    const int group1Id = m_groups.value("Gruppo 1");
    const int location1Id = 1;

    auto tagNames = [this](const QJsonValue &json) {
        QStringList ret;
        const QJsonArray tags = json.toArray();
        for (const QJsonValue &v: tags) {
            ret.append(v.toObject()["name"].toString());
        }
        return ret;
    };

    // Verifica le etiquettas del gruppos
    {
        auto reply = getRequest("api/v1/groups/" + QString::number(group1Id),
                                QUrlQuery("fields=tags"));
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        const QStringList tags = tagNames(json["tags"]);
        const QStringList expectedTags { "ChildTag 1-1", "ChildTag 1-2" };
        QCOMPARE(tags, expectedTags);
        delete reply;
    }

    // Assigna un etiquetta a un gruppo
    {
        auto reply = postRequest(baseUrl + "2/entities",
            QJsonObject {{ "groupIds", QJsonArray { group1Id }}});
        QCOMPARE(replyStatus(reply), 200);
        delete reply;

        reply = getRequest("api/v1/groups/" + QString::number(group1Id),
                                QUrlQuery("fields=tags"));
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        const QStringList tags = tagNames(json["tags"]);
        const QStringList expectedTags {
            "ChildTag 1-1", "ChildTag 1-2", "ParentTag 2",
        };
        QCOMPARE(tags, expectedTags);
        delete reply;
    }

    // Disassigna un etiquetta a un gruppo
    {
        auto reply = deleteRequest(baseUrl + "2/entities",
                                   QUrlQuery("groupIds=" +
                                             QString::number(group1Id)));
        QCOMPARE(replyStatus(reply), 200);
        delete reply;

        reply = getRequest("api/v1/groups/" + QString::number(group1Id),
                                QUrlQuery("fields=tags"));
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        const QStringList tags = tagNames(json["tags"]);
        const QStringList expectedTags { "ChildTag 1-1", "ChildTag 1-2" };
        QCOMPARE(tags, expectedTags);
        delete reply;
    }

    // Verifica le etiquettas del usatores
    {
        auto reply = getRequest("api/v1/users/" + QString::number(student1Id),
                                QUrlQuery("fields=tags"));
        QCOMPARE(replyStatus(reply), 200);
        QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QStringList tags = tagNames(json["tags"]);
        QStringList expectedTags { "ChildTag 2-1", "ParentTag 1", };
        QCOMPARE(tags, expectedTags);
        delete reply;

        reply = getRequest("api/v1/users/" + QString::number(student2Id),
                                QUrlQuery("fields=tags"));
        QCOMPARE(replyStatus(reply), 200);
        json = replyJsonObject(reply)["data"].toObject();
        tags = tagNames(json["tags"]);
        expectedTags = QStringList {};
        QCOMPARE(tags, expectedTags);
        delete reply;
    }

    // Assigna alicun etiquetta a alicun usator
    {
        auto reply = postRequest(baseUrl + "2,4/entities",
            QJsonObject {{ "userIds", QJsonArray { student1Id, student2Id }}});
        QCOMPARE(replyStatus(reply), 200);
        delete reply;

        reply = getRequest("api/v1/users/" + QString::number(student1Id),
                           QUrlQuery("fields=tags"));
        QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QStringList tags = tagNames(json["tags"]);
        QStringList expectedTags {
            "ChildTag 1-2", "ChildTag 2-1", "ParentTag 1", "ParentTag 2",
        };
        QCOMPARE(tags, expectedTags);
        delete reply;

        reply = getRequest("api/v1/users/" + QString::number(student2Id),
                           QUrlQuery("fields=tags"));
        json = replyJsonObject(reply)["data"].toObject();
        tags = tagNames(json["tags"]);
        expectedTags = QStringList {
            "ChildTag 1-2", "ParentTag 2",
        };
        QCOMPARE(tags, expectedTags);
        delete reply;
    }

    // Disassigna un etiquetta al usatores
    {
        auto reply = deleteRequest(baseUrl + "2/entities",
            QUrlQuery("userIds=" + QString::number(student1Id) + ',' +
                      QString::number(student2Id)));
        QCOMPARE(replyStatus(reply), 200);
        delete reply;

        reply = getRequest("api/v1/users/" + QString::number(student1Id),
                                QUrlQuery("fields=tags"));
        QJsonObject json = replyJsonObject(reply)["data"].toObject();
        QStringList tags = tagNames(json["tags"]);
        QStringList expectedTags {
            "ChildTag 1-2", "ChildTag 2-1", "ParentTag 1",
        };
        QCOMPARE(tags, expectedTags);
        delete reply;

        reply = getRequest("api/v1/users/" + QString::number(student2Id),
                                QUrlQuery("fields=tags"));
        json = replyJsonObject(reply)["data"].toObject();
        tags = tagNames(json["tags"]);
        expectedTags = QStringList { "ChildTag 1-2" };
        QCOMPARE(tags, expectedTags);
        delete reply;
    }

    // Verifica le etiquettas del locationes
    {
        auto reply = getRequest("api/v1/locations/" +
                                QString::number(location1Id));
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        const QStringList tags = tagNames(json["tags"]);
        const QStringList expectedTags { "ParentTag 2" };
        QCOMPARE(tags, expectedTags);
        delete reply;
    }

    // Disassigna un etiquetta al location
    {
        auto reply = deleteRequest(baseUrl + "2/entities",
            QUrlQuery("locationIds=" + QString::number(location1Id)));
        QCOMPARE(replyStatus(reply), 200);
        delete reply;

        reply = getRequest("api/v1/locations/" +
                                QString::number(location1Id));
        QCOMPARE(replyStatus(reply), 200);
        const QJsonObject json = replyJsonObject(reply)["data"].toObject();
        const QStringList tags = tagNames(json["tags"]);
        const QStringList expectedTags {};
        QCOMPARE(tags, expectedTags);
        delete reply;
    }
}

QTEST_GUILESS_MAIN(RestTest)

#include "tst_rest.moc"
