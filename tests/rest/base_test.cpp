#include "base_test.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonValue>
#include <QNetworkReply>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <QUrl>
#include <QUrlQuery>
#include <memory>

const QJsonObject BaseTest::masterLogin {
    { "login", "master" }, { "password", "supreme" }
};

const QJsonObject BaseTest::adminLogin {
    { "login", "administrator1" }, { "password", "pwd" }
};

const QJsonObject BaseTest::studentLogin {
    { "login", "studente" }, { "password", "stud3nt3" }
};

const QJsonObject BaseTest::student2Login {
    { "login", "studente2" }, { "password", "stud3nt32" }
};

namespace {

QJsonArray idsToJson(const QVector<int> ids) {
    QJsonArray json;
    for (int id: ids) {
        json.append(id);
    }
    return json;
}

} // namespace

void BaseTest::initTestCase()
{
    startServer();
}

void BaseTest::cleanupTestCase()
{
    killServer();
}

void BaseTest::init()
{
}

void BaseTest::cleanup()
{
    RequestUtils::cleanup();
}

void BaseTest::startServer()
{
    QVERIFY(m_server.start());
    setServerUrl(m_server.serverUrl());
}

void BaseTest::killServer()
{
    QVERIFY(m_server.kill());
    QVERIFY(m_server.waitForFinished());
}

bool BaseTest::wipeDb()
{
    QScopedPointer<QNetworkReply> reply(getRequest("testing/wipeDb"));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Wiping DB failed";
        return false;
    }

    return true;
}

bool BaseTest::createMaster()
{
    QScopedPointer<QNetworkReply> reply(getRequest("testing/createMaster"));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Creating master account failed";
        return false;
    }

    m_masterCredentials = replyJsonObject(reply.get());
    return !m_masterCredentials.isEmpty();
}

bool BaseTest::createUser(const QJsonObject &userData)
{
    QJsonObject jsonData(userData);
    QScopedPointer<QNetworkReply> reply(postRequest("api/v1/users", jsonData));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Creating user account failed";
        return false;
    }

    const QJsonObject userReply =
        replyJsonObject(reply.get()).value("data").toObject();
    int userId = userReply.value("userId").toInt();
    m_logins.insert(userData.value("login").toString(), userId);
    return true;
}

bool BaseTest::createUsers(const QJsonArray &usersData)
{
    QScopedPointer<QNetworkReply> reply(postRequest("api/v1/users", usersData));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Creating user accounts failed";
        return false;
    }

    const QJsonArray replies = replyJsonArray(reply.get());
    if (replies.count() != usersData.count()) {
        qWarning() << "Didn't get a matching number of replies!";
        return false;
    }
    for (int i = 0; i < replies.count(); i++) {
        const QJsonObject reply = replies.at(i).toObject();
        if (reply.value("status") != "ok") {
            qWarning() << "Creation of element" << i << "failed:"
                << reply;
            return false;
        }
        const QJsonObject userReply = reply.value("data").toObject();
        int userId = userReply.value("userId").toInt();
        m_logins.insert(usersData.at(i).toObject().value("login").toString(),
                        userId);
    }
    return true;
}

bool BaseTest::addStatusChange(int userId, const QJsonObject &data)
{
    QString url = "api/v1/users/" + QString::number(userId) + "/status";
    QScopedPointer<QNetworkReply> reply(postRequest(url, data));
    return replyStatus(reply.get()) == 200;
}

bool BaseTest::createGroup(const QJsonObject &groupData)
{
    QJsonObject jsonData(groupData);
    QScopedPointer<QNetworkReply> reply(postRequest("api/v1/groups", jsonData));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Creating group account failed";
        return false;
    }

    const QJsonObject groupReply =
        replyJsonObject(reply.get()).value("data").toObject();
    int groupId = groupReply.value("groupId").toInt();
    m_groups.insert(groupData.value("name").toString(), groupId);
    return true;
}

bool BaseTest::createLesson(const QJsonObject &lessonData)
{
    QScopedPointer<QNetworkReply> reply(postRequest("api/v1/lessons",
                                                    lessonData));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Creating lesson failed";
        return false;
    }

    const QJsonObject o = replyJsonObject(reply.get());
    if (o.value("status") != "ok") {
        qWarning() << "Creation of lesson failed:" << o;
        return false;
    }
    return true;
}

bool BaseTest::createLessons(const QJsonArray &lessonsData)
{
    QJsonObject request {
        { "lessons", lessonsData },
    };
    QScopedPointer<QNetworkReply> reply(postRequest("api/v1/lessons",
                                                    request));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Creating lessons failed";
        return false;
    }
    return true;
}

bool BaseTest::createLocation(const QJsonObject &locationData)
{
    QScopedPointer<QNetworkReply> reply(postRequest("api/v1/locations",
                                                    locationData));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Creating location failed";
        return false;
    }

    const QJsonObject o = replyJsonObject(reply.get());
    if (o.value("status") != "ok") {
        qWarning() << "Creation of location failed:" << o;
        return false;
    }
    return true;
}

bool BaseTest::createTag(const QJsonObject &tagData)
{
    QScopedPointer<QNetworkReply> reply(postRequest("api/v1/tags", tagData));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Creating tag failed";
        return false;
    }

    const QJsonObject o = replyJsonObject(reply.get());
    if (o.value("status") != "ok") {
        qWarning() << "Creation of tag failed:" << o;
        return false;
    }
    return true;
}

bool BaseTest::addTeachersToGroup(int groupId, const QVector<int> &userIds)
{
    QJsonObject creationData {
        { "userIds", idsToJson(userIds) },
    };
    QString baseUrl =
        "api/v1/groups/" + QString::number(groupId) + "/teachers";
    QScopedPointer<QNetworkReply> reply(postRequest(baseUrl, creationData));
    return replyStatus(reply.get()) == 200;
}

bool BaseTest::addStudentsToGroup(int groupId, const QVector<int> &userIds)
{
    QJsonObject creationData {
        { "userIds", idsToJson(userIds) },
    };
    QString baseUrl =
        "api/v1/groups/" + QString::number(groupId) + "/students";
    QScopedPointer<QNetworkReply> reply(postRequest(baseUrl, creationData));
    return replyStatus(reply.get()) == 200;
}

bool BaseTest::inviteToLesson(int lessonId, const QJsonObject &invitees)
{
    QString baseUrl =
        "api/v1/lessons/" + QString::number(lessonId) + "/invitees";
    QScopedPointer<QNetworkReply> reply(postRequest(baseUrl, invitees));
    return replyStatus(reply.get()) == 200;
}

bool BaseTest::addAttendanceToLesson(int lessonId,
                                     const QJsonObject &attendance)
{
    QString baseUrl =
        "api/v1/lessons/" + QString::number(lessonId) + "/attendance";
    QScopedPointer<QNetworkReply> reply(putRequest(baseUrl, attendance));
    return replyStatus(reply.get()) == 200;
}

bool BaseTest::attachTags(const QVector<int> &tagIds,
                          const QJsonObject &entities)
{
    QString tags;
    for (int tagId: tagIds) {
        if (!tags.isEmpty()) tags += ',';
        tags += QString::number(tagId);
    }
    QString baseUrl =
        "api/v1/tags/" + tags + "/entities";
    QScopedPointer<QNetworkReply> reply(postRequest(baseUrl, entities));
    return replyStatus(reply.get()) == 200;
}

bool BaseTest::populate(PopulationItems items)
{
    bool ok = createMaster();
    if (!ok) return false;

    /* login como master pro crear le altere usatores */
    ok = login({{ "login", "master" }, { "password", "supreme" }});
    if (!ok) return false;

    ok = createUser({
        { "isStudent", true },
        { "name", "Le studente" },
        { "login", "studente" },
        { "password", "stud3nt3" },
        { "pinCode", "1234" },
    });
    if (!ok) return false;

    if (items & Tags) {
        ok = createMany(&BaseTest::createTag, {
            QJsonObject {
                { "name", "ParentTag 1" },
                { "description", "Prime etiquetta" },
            },
            QJsonObject {
                { "name", "ParentTag 2" },
            },
            QJsonObject {
                { "name", "ChildTag 1-1" },
                { "parentTagId", 1 },
            },
            QJsonObject {
                { "name", "ChildTag 1-2" },
                { "parentTagId", 1 },
            },
            QJsonObject {
                { "name", "ChildTag 2-1" },
                { "parentTagId", 2 },
            },
            QJsonObject {
                { "name", "ChildTag 2-2" },
                { "parentTagId", 2 },
            },
            QJsonObject {
                { "name", "ParentTag 3" },
            },
        });
        if (!ok) return false;

        ok = attachTags({ 1, 5 }, {
            { "userIds", QJsonArray { m_logins["studente"] }},
        });
        if (!ok) return false;
    }

    if (items & (Locations | EmptyGroups)) {
        ok = createLocation({
            { "name", "Sala 1" },
            { "description", "Prime sala" },
            { "color", "#ff8000" },
        });
        if (!ok) return false;

        if (items & Tags) {
            ok = attachTags({ 2 }, {
                { "locationIds", QJsonArray { 1 }},
            });
            if (!ok) return false;
        }
    }

    if (items & EmptyGroups) {
        ok = createMany(&BaseTest::createGroup, {
            QJsonObject {
                { "name", "Gruppo 1" },
                { "description", "Prime gruppo" },
                { "locationId", 1 },
            },
            QJsonObject {
                { "name", "Gruppo 2" },
                { "description", "Secunde gruppo" },
            },
        });
        if (!ok) return false;

        if (items & Tags) {
            ok = attachTags({ 3, 4 }, {
                { "groupIds", QJsonArray { 1, 2 }},
            });
            if (!ok) return false;
        }
    }

    if (items & GroupMembers) {
        int groupId = m_groups.value("Gruppo 1");
        ok = createUsers({
            QJsonObject {
                { "isTeacher", true },
                { "name", "Inseniante 123" },
                { "login", "inseniante1" },
                { "password", "ins1" },
                { "pinCode", "1234" },
            },
            QJsonObject {
                { "isStudent", true },
                { "name", "Altere studente" },
                { "login", "studente2" },
                { "password", "stud3nt32" },
                { "pinCode", "1234" },
            },
        });
        if (!ok) return false;

        ok = addTeachersToGroup(groupId, { m_logins["inseniante1"] });
        if (!ok) return false;
        ok = addStudentsToGroup(groupId, {
            m_logins["studente"], m_logins["studente2"] });
        if (!ok) return false;
    }

    if (items & MoreUsers) {
        ok = createUsers({
            QJsonObject {
                { "isStudent", true },
                { "name", "Tertie studente" },
                { "login", "studente3" },
                { "password", "pwd" },
            },
            QJsonObject {
                { "isParent", true },
                { "name", "Un patre" },
                { "login", "patre1" },
                { "password", "ins1" },
            },
            QJsonObject {
                { "isStudent", true },
                { "isParent", true },
                { "name", "Patre e studente" },
                { "login", "patre_studente1" },
                { "password", "pwd" },
            },
            QJsonObject {
                { "isTeacher", true },
                { "name", "Secunde inseniante" },
                { "login", "inseniante2" },
                { "password", "pwd" },
            },
            QJsonObject {
                { "isTeacher", true },
                { "name", "Tertie inseniante" },
                { "login", "inseniante3" },
                { "password", "pwd" },
            },
            QJsonObject {
                { "isAdmin", true },
                { "name", "Administrator" },
                { "login", "administrator1" },
                { "password", "pwd" },
            },
            QJsonObject {
                { "isDirector", true },
                { "name", "Director del schola" },
                { "login", "director1" },
                { "password", "pwd" },
            },
        });
        if (!ok) return false;

        if (items & Tags) {
            ok = attachTags({ 2, 4 }, {
                { "userIds", QJsonArray { m_logins["administrator1"] }},
            });
            if (!ok) return false;
        }
    }

    if (items & EmptyLessons) {
        ok = createLessons({
            QJsonObject {
                { "startTime", "2023-03-19T06:00:00" },
                { "endTime",   "2023-03-19T07:00:00" },
                { "creatorId", 1 },
            },
            QJsonObject {
                { "startTime", "2023-03-19T07:00:00" },
                { "endTime",   "2023-03-19T07:30:00" },
                { "creatorId", 2 },
            },
            QJsonObject {
                { "startTime", "2023-03-19T16:00:00" },
                { "endTime",   "2023-03-19T16:50:00" },
            },
            QJsonObject {
                { "startTime", "2023-03-19T15:00:00" },
                { "endTime",   "2023-03-19T15:45:00" },
            },
        });
        if (!ok) return false;
    }

    if (items & LessonAttendance) {
        ok = inviteToLesson(1, {
            { "teachers", QJsonArray { m_logins["inseniante1"], }},
            { "groups", QJsonArray { 1 }},
        });
        if (!ok) return false;

        ok = addAttendanceToLesson(1, {
            { "teachers", QJsonArray {
                QJsonObject {
                    { "userId", m_logins["inseniante1"] },
                    { "attended", true },
                },
            }},
            { "students", QJsonArray {
                QJsonObject {
                    { "userId", m_logins["studente"] },
                    { "attended", false },
                    { "note", "Malade" },
                },
                QJsonObject {
                    { "userId", m_logins["studente2"] },
                    { "attended", true },
                },
            }},
        });
        if (!ok) return false;

        ok = inviteToLesson(2, {
            { "teachers", QJsonArray { m_logins["inseniante1"], }},
            { "groups", QJsonArray { 1 }},
        });
        if (!ok) return false;

        ok = addAttendanceToLesson(2, {
            { "teachers", QJsonArray {
                QJsonObject {
                    { "userId", m_logins["inseniante1"] },
                    { "attended", true },
                },
            }},
            { "students", QJsonArray {
                QJsonObject {
                    { "userId", m_logins["studente"] },
                    { "attended", true },
                },
                QJsonObject {
                    { "userId", m_logins["studente2"] },
                    { "invited", true },
                },
            }},
        });
        if (!ok) return false;

        ok = inviteToLesson(3, {
            { "groups", QJsonArray { 1 }},
        });
        if (!ok) return false;

        ok = addAttendanceToLesson(4, {
            { "teachers", QJsonArray {
                QJsonObject {
                    { "userId", m_logins["inseniante1"] },
                    { "attended", true },
                    { "note", "Nota pro le inseniante" },
                },
            }},
            { "students", QJsonArray {
                QJsonObject {
                    { "userId", m_logins["studente"] },
                    { "attended", true },
                },
            }},
        });
        if (!ok) return false;
    }

    if (items & StatusChanges) {
        const int student1Id = m_logins.value("studente");
        ok = addStatusChange(student1Id, {
            { "status", "inactive" },
            { "time", "2023-01-20T07:01:00" },
            { "reason", "Abandona studente" },
        });
        if (!ok) return false;

        ok = addStatusChange(student1Id, {
            { "status", "active" },
            { "time", "2021-03-20T07:01:00" },
            { "reason", "Arriva studente" },
        });
        if (!ok) return false;

        ok = addStatusChange(student1Id, {
            { "status", "active" },
            { "time", "2023-03-20T07:01:00" },
            { "reason", "Retorna studente" },
        });
        if (!ok) return false;

        if (items & GroupMembers) {
            const int student2Id = m_logins.value("studente2");
            ok = addStatusChange(student2Id, {
                { "status", "active" },
                { "time", "2022-03-20T07:01:00" },
                { "reason", "Arriva studente2" },
            });
            if (!ok) return false;
        }

        if (items & MoreUsers) {
            const int student3Id = m_logins.value("studente3");
            ok = addStatusChange(student3Id, {
                { "status", "active" },
                { "time", "2020-11-20T07:01:00" },
                { "reason", "Arriva studente3" },
            });
            if (!ok) return false;

            ok = addStatusChange(student3Id, {
                { "status", "inactive" },
                { "time", "2022-12-20T07:01:00" },
                { "reason", "Abandona studente3" },
            });
            if (!ok) return false;
        }
    }

    m_authenticationToken.clear();
    return true;
}

bool BaseTest::login(const QJsonObject &loginData)
{
    QScopedPointer<QNetworkReply> reply(postRequest("api/v1/login", loginData));
    if (replyStatus(reply.get()) != 200) {
        qWarning() << "Login failed";
        return false;
    }

    const QString token =
        replyJsonObject(reply.get())["data"].toObject()
        ["authenticationToken"].toString();
    m_authenticationToken = token.toUtf8();
    return !m_authenticationToken.isEmpty();
}
