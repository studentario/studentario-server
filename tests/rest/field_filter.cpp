#include "field_filter.h"

QJsonObject FieldFilter::filter(const QJsonObject &o)
{
    QJsonObject filtered;
    for (auto i = o.begin(); i != o.end(); i++) {
        if (m_fieldsToRemove.contains(i.key())) {
            // face nihil
        } else if (i.key() == "created" || i.key() == "modified") {
            QDateTime time = QDateTime::fromString(i.value().toString(),
                                                   Qt::ISODate);
            if (time < m_testStartTime) {
                qWarning() << "creation time" << time <<
                    "is earlier than test time" << m_testStartTime;
                m_hasError = true;
            }
        } else {
            // copia le valor, filtrante recursivemente
            QJsonValue v;
            if (i.value().isObject()) {
                v = filter(i.value().toObject());
            } else if (i.value().isArray()) {
                v = filter(i.value().toArray());
            } else {
                v = i.value();
            }
            filtered.insert(i.key(), v);
        }
    }
    return filtered;
}

QJsonArray FieldFilter::filter(const QJsonArray &o)
{
    QJsonArray filtered;
    for (const QJsonValue &value: o) {
        // copia le valor, filtrante recursivemente
        QJsonValue v;
        if (value.isObject()) {
            v = filter(value.toObject());
        } else if (value.isArray()) {
            v = filter(value.toArray());
        } else {
            v = value;
        }
        filtered.append(v);
    }
    return filtered;
}
