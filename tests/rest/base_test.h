#ifndef BASE_TEST_H
#define BASE_TEST_H

#include "field_filter.h"
#include "request_utils.h"
#include "server.h"

#include <QHash>
#include <QJsonObject>
#include <QScopedPointer>
#include <functional>

class BaseTest: public QObject, protected RequestUtils
{
    Q_OBJECT

public:
    enum PopulationItem {
        BasicUsers = 0,
        EmptyGroups = 1 << 0,
        GroupMembers = 1 << 1,
        GroupsWithMembers = GroupMembers | EmptyGroups | BasicUsers,
        MoreUsers = 1 << 2, // Plure usatores con rolos differente
        EmptyLessons = 1 << 3,
        LessonAttendance = 1 << 4,
        LessonsWithAttendance = EmptyLessons | GroupsWithMembers |
            LessonAttendance,
        StatusChanges = 1 << 5,
        Tags = 1 << 6,
        Locations = 1 << 7,
    };
    Q_DECLARE_FLAGS(PopulationItems, PopulationItem)

    static const QJsonObject masterLogin;
    static const QJsonObject adminLogin;
    static const QJsonObject studentLogin;
    static const QJsonObject student2Login;

protected:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    void startServer();
    void killServer();

    bool wipeDb();
    bool createMaster();
    bool createUser(const QJsonObject &userData);
    bool createUsers(const QJsonArray &usersData);
    bool addStatusChange(int userId, const QJsonObject &data);
    bool createGroup(const QJsonObject &groupData);
    bool createLesson(const QJsonObject &lessonData);
    bool createLessons(const QJsonArray &lessonsData);
    bool createLocation(const QJsonObject &locationData);
    bool createTag(const QJsonObject &tagData);
    using CreateOne = std::function<bool(const QJsonObject &data)>;
    template<typename F> bool createMany(F createOne, const QJsonArray &elements) {
        for (const QJsonValue &v: elements) {
            bool ok = (this->*createOne)(v.toObject());
            if (!ok) return false;
        }
        return true;
    }
    bool addTeachersToGroup(int groupId, const QVector<int> &userIds);
    bool addStudentsToGroup(int groupId, const QVector<int> &userIds);
    bool inviteToLesson(int lessonId, const QJsonObject &invitees);
    bool addAttendanceToLesson(int lessonId, const QJsonObject &attendance);
    bool attachTags(const QVector<int> &tagIds, const QJsonObject &entities);
    bool populate(PopulationItems items = BasicUsers);
    bool login(const QJsonObject &loginData);

protected:
    Server m_server;
    QJsonObject m_masterCredentials;
    QHash<QString,int> m_logins;
    QHash<QString,int> m_groups;
    FieldFilter m_fieldFilter;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(BaseTest::PopulationItems)
Q_DECLARE_METATYPE(BaseTest::PopulationItems)

#endif // BASE_TEST_H
