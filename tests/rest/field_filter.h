#ifndef FIELD_FILTER_H
#define FIELD_FILTER_H

#include <QDateTime>
#include <QJsonArray>
#include <QJsonObject>
#include <QStringList>

class FieldFilter {
public:
    void setTestStartTime(const QDateTime &time) {
        // Remove le millisecundos, perque le DB non ha un tal resolution
        QTime t = time.time();
        m_testStartTime = time;
        m_testStartTime.setTime(t.addMSecs(-t.msec()));
    }

    void setFieldsToRemove(const QStringList &fields) {
        m_fieldsToRemove = fields;
    }

    void reset() {
        m_testStartTime = {};
        m_fieldsToRemove.clear();
        m_hasError = false;
    }

    QJsonObject filter(const QJsonObject &o);
    QJsonArray filter(const QJsonArray &o);
    // Si necessari, nos pote adder functiones specific a usatores, gruppos,
    // etc.

    bool hasError() const { return m_hasError; }

private:
    QDateTime m_testStartTime;
    QStringList m_fieldsToRemove;
    bool m_hasError = false;
};

#endif // FIELD_FILTER_H
