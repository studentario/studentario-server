#ifndef TESTUTILS_H
#define TESTUTILS_H

#include <QByteArray>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSet>
#include <QStringList>
#include <QTest>

namespace QTest {

template<>
char *toString(const QJsonObject &obj)
{
    QJsonDocument doc(obj);
    return QTest::toString(doc.toJson(QJsonDocument::Compact));
}

template<>
char *toString(const QJsonArray &array)
{
    QJsonDocument doc(array);
    return QTest::toString(doc.toJson(QJsonDocument::Compact));
}

template<>
char *toString(const QSet<QString> &strings)
{
    QStringList list(strings.begin(), strings.end());
    return QTest::toString(list.join(", "));
}

} // namespace

#endif // TESTUTILS_H
