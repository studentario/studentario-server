#include "base_test.h"
#include "test_utils.h"

#include <QCoreApplication>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QNetworkReply>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <QUrlQuery>

class ReadonlyTest: public BaseTest
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    void testLogin_data();
    void testLogin();
    void testSession_data();
    void testSession();
    void testPin_data();
    void testPin();

    void testCreateUserError_data();
    void testCreateUserError();
    void testUsers_data();
    void testUsers();
    void testUsersWithFields_data();
    void testUsersWithFields();
    void testUsersError_data();
    void testUsersError();
    void testGetUser_data();
    void testGetUser();
    void testGetUserError_data();
    void testGetUserError();

    void testUserStatusChangeFilters_data();
    void testUserStatusChangeFilters();
    void testUserStatusChangeFields_data();
    void testUserStatusChangeFields();

    void testListGroups_data();
    void testListGroups();
    void testGetGroup_data();
    void testGetGroup();
    void testDeleteGroup_data();
    void testDeleteGroup();

    void testListLessons_data();
    void testListLessons();
    void testGetLesson_data();
    void testGetLesson();
    void testDeleteLesson_data();
    void testDeleteLesson();
    void testLessonPermissions();

    void testListTags_data();
    void testListTags();
    void testGetTag_data();
    void testGetTag();
};

void ReadonlyTest::initTestCase()
{
    m_fieldFilter.setTestStartTime(QDateTime::currentDateTime());
    BaseTest::initTestCase();
    QVERIFY(wipeDb());
    QVERIFY(populate(MoreUsers |
                     LessonsWithAttendance |
                     StatusChanges |
                     Tags));
}

void ReadonlyTest::cleanupTestCase()
{
    BaseTest::cleanupTestCase();
}

void ReadonlyTest::init()
{
    BaseTest::init();
}

void ReadonlyTest::cleanup()
{
    BaseTest::cleanup();
}

void ReadonlyTest::testLogin_data()
{
    QTest::addColumn<QJsonObject>("input");
    QTest::addColumn<int>("expectedHttpStatus");

    QTest::newRow("empty") <<
        QJsonObject {} << 401;

    QTest::newRow("only login") <<
        QJsonObject {{ "login", "studente" }} << 400;

    QTest::newRow("wrong password") <<
        QJsonObject {
            { "login", "studente" },
            { "password", "badluck" },
        } << 401;

    QTest::newRow("wrong pin") <<
        QJsonObject {
            { "login", "studente" },
            { "pinCode", "9999" },
        } << 401;

    QTest::newRow("ok, password") <<
        QJsonObject {
            { "login", "studente" },
            { "password", "stud3nt3" },
        } << 200;

    QTest::newRow("ok, pin") <<
        QJsonObject {
            { "login", "studente" },
            { "pinCode", "1234" },
        } << 200;
}

void ReadonlyTest::testLogin()
{
    QFETCH(QJsonObject, input);
    QFETCH(int, expectedHttpStatus);

    auto reply = postRequest("api/v1/login", input);
    QVERIFY(wait(reply));

    int status = replyStatus(reply);
    QCOMPARE(status, expectedHttpStatus);

    if (status == 200) {
        const QJsonObject login = replyJsonObject(reply)["data"].toObject();
        const QJsonObject user = login["user"].toObject();
        qDebug() << "logins:" << m_logins;
        int expectedUserId = m_logins[input["login"].toString()];
        QCOMPARE(user["userId"].toInt(), expectedUserId);
        QVERIFY(!login["authenticationToken"].toString().isEmpty());
    }
    delete reply;
}

void ReadonlyTest::testSession_data()
{
    QTest::addColumn<QJsonObject>("loginInput");

    QTest::newRow("ok") <<
        QJsonObject {
            { "login", "studente" },
            { "password", "stud3nt3" },
        };
}

void ReadonlyTest::testSession()
{
    QFETCH(QJsonObject, loginInput);

    auto reply = postRequest("api/v1/login", loginInput);
    QVERIFY(wait(reply));

    QCOMPARE(replyStatus(reply), 200);

    const QString token =
        replyJsonObject(reply)["data"].toObject()
        ["authenticationToken"].toString();

    int expectedUserId = m_logins[loginInput["login"].toString()];
    delete reply;

    /* Usa le datos del session pro obtener le informationes del usator */
    m_authenticationToken = token.toUtf8();
    reply = getRequest("api/v1/users/me");
    QVERIFY(wait(reply));

    QCOMPARE(replyStatus(reply), 200);
    const QJsonObject info = replyJsonObject(reply)["data"].toObject();

    QCOMPARE(info["userId"].toInt(), expectedUserId);
    QCOMPARE(info["login"], loginInput["login"]);

    delete reply;
}

void ReadonlyTest::testPin_data()
{
    QTest::addColumn<QJsonObject>("request");
    QTest::addColumn<int>("expectedHttpStatus");

    QTest::newRow("empty") <<
        QJsonObject {
            { "pinCode", "1234" },
            { "newPin", "" },
        } << 400;

    QTest::newRow("pin authentication, ok") <<
        QJsonObject {
            { "pinCode", "1234" },
            { "newPin", "4321" },
        } << 200;

    QTest::newRow("pwd authentication, ok") <<
        QJsonObject {
            { "password", "stud3nt3" },
            { "newPin", "4321" },
        } << 200;
}

void ReadonlyTest::testPin()
{
    QFETCH(QJsonObject, request);
    QFETCH(int, expectedHttpStatus);

    bool ok = login({
        { "login", "studente" },
        { "password", "stud3nt3" },
    });
    QVERIFY(ok);

    // cambia le PIN
    auto reply = postRequest("api/v1/users/me/updatePin", request);
    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;

    if (expectedHttpStatus == 200) {
        m_authenticationToken.clear();
        const QJsonObject newLogin = {
            { "login", "studente" },
            { "pinCode", request["newPin"] },
        };
        QVERIFY(login(newLogin));
    }
}

void ReadonlyTest::testCreateUserError_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QJsonObject>("user");
    QTest::addColumn<int>("expectedHttpStatus");

    const QJsonObject validUser {
        { "isStudent", true },
        { "name", "Un studente" },
        { "login", "tim" },
        { "password", "timtom" },
        { "pinCode", "6789" },
    };

    QTest::newRow("empty") <<
        masterLogin << QJsonObject {} << 400;

    QJsonObject user = validUser;
    user.remove("password");
    QTest::newRow("no password") <<
        masterLogin << user << 400;

    user = validUser;
    user.remove("isStudent");
    QTest::newRow("no roles") <<
        masterLogin << user << 400;

    QTest::newRow("no permissions") <<
        studentLogin<< validUser << 403;

    user = validUser;
    user.insert("status", QJsonObject {
        { "status", "disastrose" },
        { "time", "2021-02-18T07:51:00" },
    });
    QTest::newRow("con status non valide") <<
        masterLogin << user << 400;
}

void ReadonlyTest::testCreateUserError()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QJsonObject, user);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(login(loginInfo));

    auto reply = postRequest("api/v1/users", user);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void ReadonlyTest::testUsers_data()
{
    QTest::addColumn<QString>("queryString");
    QTest::addColumn<bool>("requireOrder");
    QTest::addColumn<QStringList>("expectedLogins");

    QTest::newRow("nulle filtro") <<
        "" << false << QStringList {
            "administrator1", "director1",
            "inseniante1", "inseniante2", "inseniante3",
            "master", "patre1", "patre_studente1",
            "studente", "studente2", "studente3",
        };

    QTest::newRow("nulle filtro, ordinate per login") <<
        "sort=login" << true << QStringList {
            "administrator1", "director1",
            "inseniante1", "inseniante2", "inseniante3",
            "master", "patre1", "patre_studente1",
            "studente", "studente2", "studente3",
        };

    QTest::newRow("nulle filtro, ordinate per nomine") <<
        "sort=name" << true << QStringList {
            "administrator1", "studente2", "director1",
            "inseniante1", "studente",
            "patre_studente1", "inseniante2", "master",
            "inseniante3", "studente3", "patre1",
        };

    QTest::newRow("nulle filtro, ordinate per nomine desc") <<
        "sort=-name" << true << QStringList {
            "patre1", "studente3", "inseniante3",
            "master", "inseniante2", "patre_studente1",
            "studente", "inseniante1", "director1",
            "studente2", "administrator1",
        };

    QTest::newRow("nulle filtro, ordinate per id desc") <<
        "sort=-id" << true << QStringList {
            "director1", "administrator1", "inseniante3",
            "inseniante2", "patre_studente1", "patre1",
            "studente3", "studente2", "inseniante1",
            "studente", "master",
        };

    QTest::newRow("filtro per nomine") <<
        "name=tr" << false << QStringList {
            "administrator1", "patre1", "patre_studente1",
        };

    QTest::newRow("filtro per nomine e rolo") <<
        "name=tr&isParent=1" << false << QStringList {
            "patre1", "patre_studente1",
        };

    QTest::newRow("studentes") <<
        "isStudent=1" << false << QStringList {
            "patre_studente1", "studente", "studente2", "studente3",
        };

    QTest::newRow("inseniantes") <<
        "isTeacher=1" << false << QStringList {
            "inseniante1", "inseniante2", "inseniante3",
        };

    QTest::newRow("inseniantes") <<
        "isTeacher=1" << false << QStringList {
            "inseniante1", "inseniante2", "inseniante3",
        };

    QTest::newRow("parentes") <<
        "isParent=1" << false << QStringList {
            "patre1", "patre_studente1",
        };

    QTest::newRow("parentes, per nomine") <<
        "isParent=1" << false << QStringList {
            "patre_studente1", "patre1",
        };

    QTest::newRow("administratores") <<
        "isAdmin=1" << false << QStringList {
            "administrator1",
        };

    QTest::newRow("directores") <<
        "isDirector=1" << false << QStringList {
            "director1",
        };

    QTest::newRow("patrones") <<
        "isMaster=1" << false << QStringList {
            "master",
        };

    QTest::newRow("rolos combinate") <<
        "isStudent=1&isParent=1" << false << QStringList {
            "patre_studente1",
        };
}

void ReadonlyTest::testUsers()
{
    QFETCH(QString, queryString);
    QFETCH(bool, requireOrder);
    QFETCH(QStringList, expectedLogins);

    QVERIFY(login(masterLogin));

    auto reply = getRequest("api/v1/users", QUrlQuery(queryString));

    QCOMPARE(replyStatus(reply), 200);

    QStringList logins;
    const QJsonArray json = replyJsonObject(reply)["data"].toArray();
    for (const QJsonValue &v: json) {
        logins.append(v.toObject().value("login").toString());
    }

    if (requireOrder) {
        QCOMPARE(logins, expectedLogins);
    } else {
        QSet<QString> unsortedLogins(logins.begin(), logins.end());
        QSet<QString> expectedUnsortedLogins(expectedLogins.begin(),
                                             expectedLogins.end());
        QCOMPARE(unsortedLogins, expectedUnsortedLogins);
    }

    delete reply;
}

void ReadonlyTest::testUsersWithFields_data()
{
    QTest::addColumn<QString>("queryString");
    QTest::addColumn<QJsonArray>("expectedUsers");

    QTest::newRow("administratores, nulle campo") <<
        "isAdmin=1" << QJsonArray {
            QJsonObject {
                { "login", "administrator1" },
                { "name", "Administrator" },
                { "birthDate", "" },
                { "contactInfo", QJsonArray() },
                { "isAdmin", true },
                { "isDirector", false },
                { "isMaster", false },
                { "isTeacher", false },
                { "isStudent", false },
                { "isParent", false },
                { "keywords", "" },
                { "tags", QJsonArray {
                    QJsonObject {
                        { "tagId", 4 },
                        { "name", "ChildTag 1-2" },
                        { "description", "" },
                        { "parentTagId", 1 },
                    },
                    QJsonObject {
                        { "tagId", 2 },
                        { "name", "ParentTag 2" },
                        { "description", "" },
                        { "parentTagId", -1 },
                    },
                }},
            }
        };

    QTest::newRow("administratores, solo login") <<
        "isAdmin=1&fields=login" << QJsonArray {
            QJsonObject {
                { "login", "administrator1" },
            }
        };

    QTest::newRow("administratores, login e nomine") <<
        "isAdmin=1&fields=login&fields=name" << QJsonArray {
            QJsonObject {
                { "name", "Administrator" },
                { "login", "administrator1" },
            }
        };
}

void ReadonlyTest::testUsersWithFields()
{
    QFETCH(QString, queryString);
    QFETCH(QJsonArray, expectedUsers);

    QVERIFY(login(masterLogin));

    auto reply = getRequest("api/v1/users", QUrlQuery(queryString));

    QCOMPARE(replyStatus(reply), 200);

    QJsonArray json = replyJsonObject(reply)["data"].toArray();
    m_fieldFilter.setFieldsToRemove({
        "created", "creatorId", "icon", "color",
    });
    QJsonArray users = m_fieldFilter.filter(json);
    // Remove le userId, perque illo es dynamic
    for (int i = 0; i < users.count(); i++) {
        QJsonObject user = users.at(i).toObject();
        QVERIFY(user.contains("userId")); // sempre presente!
        user.remove("userId");
        users[i] = user;
    }
    QCOMPARE(users, expectedUsers);

    delete reply;
}

void ReadonlyTest::testUsersError_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("queryString");
    QTest::addColumn<int>("expectedHttpStatus");

    QTest::newRow("no permissions") <<
        studentLogin << "" << 403;
}

void ReadonlyTest::testUsersError()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QString, queryString);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(login(loginInfo));

    auto reply = getRequest("api/v1/users", QUrlQuery(queryString));

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void ReadonlyTest::testGetUser_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("userLogin");
    QTest::addColumn<QString>("queryString");
    QTest::addColumn<QJsonObject>("expectedUser");

    QTest::newRow("studente, nulle campo") <<
        masterLogin << "studente" << "" << QJsonObject {
            { "login", "studente" },
            { "name", "Le studente" },
            { "birthDate", "" },
            { "contactInfo", QJsonArray() },
            { "isTeacher", false },
            { "isStudent", true },
            { "keywords", "" },
            { "status", "active" },
            { "statusChangeTime", "2023-03-20T07:01:00" },
            { "tags", QJsonArray {
                QJsonObject {
                    { "tagId", 5 },
                    { "name", "ChildTag 2-1" },
                    { "description", "" },
                    { "parentTagId", 2 },
                },
                QJsonObject {
                    { "tagId", 1 },
                    { "name", "ParentTag 1" },
                    { "description", "Prime etiquetta" },
                    { "parentTagId", -1 },
                },
            }},
        };

    QTest::newRow("non active, status") <<
        masterLogin << "studente3" << "fields=status" << QJsonObject {
            { "status", "inactive" },
            { "statusChangeTime", "2022-12-20T07:01:00" },
        };

    QTest::newRow("studente, proprie datos") <<
        studentLogin << "studente" << "fields=name" << QJsonObject {
            { "name", "Le studente" },
        };
}

void ReadonlyTest::testGetUser()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QString, userLogin);
    QFETCH(QString, queryString);
    QFETCH(QJsonObject, expectedUser);

    QVERIFY(login(loginInfo));

    const int userId = m_logins.value(userLogin);
    QVERIFY(userId > 0);
    auto reply = getRequest("api/v1/users/" + QString::number(userId),
                            QUrlQuery(queryString));
    QCOMPARE(replyStatus(reply), 200);

    QJsonObject json = replyJsonObject(reply)["data"].toObject();
    QCOMPARE(json["userId"].toInt(), userId);
    m_fieldFilter.setFieldsToRemove({
        "userId", "isAdmin", "isDirector", "isMaster", "isParent",
        "created", "creatorId", "icon", "color",
    });
    QJsonObject user = m_fieldFilter.filter(json);

    QCOMPARE(user, expectedUser);

    delete reply;
}

void ReadonlyTest::testGetUserError_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("userLogin");
    QTest::addColumn<int>("expectedHttpStatus");

    QTest::newRow("no permissions") <<
        studentLogin << "studente3" << 403;
}

void ReadonlyTest::testGetUserError()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QString, userLogin);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(login(loginInfo));

    const int userId = m_logins.value(userLogin);
    QVERIFY(userId > 0);
    auto reply = getRequest("api/v1/users/" + QString::number(userId));

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void ReadonlyTest::testUserStatusChangeFilters_data()
{
    QTest::addColumn<QString>("queryString");
    /* Nos usa le rationes perque nos sape que illos es unic */
    QTest::addColumn<QStringList>("expectedReasons");

    QTest::newRow("nulle filtro") <<
        "" << QStringList {
            "Arriva studente3",
            "Arriva studente",
            "Arriva studente2",
            "Abandona studente3",
            "Abandona studente",
            "Retorna studente",
        };

    QTest::newRow("data initial") <<
        "since=2022-01-01T00:01:00" << QStringList {
            "Arriva studente2",
            "Abandona studente3",
            "Abandona studente",
            "Retorna studente",
        };

    QTest::newRow("data final") <<
        "to=2023-01-01T00:01:00" << QStringList {
            "Arriva studente3",
            "Arriva studente",
            "Arriva studente2",
            "Abandona studente3",
        };

    QTest::newRow("intervallo") <<
        "since=2022-03-20T07:01:01&to=2023-01-31T00:01:00" << QStringList {
            "Abandona studente3",
            "Abandona studente",
        };

    QTest::newRow("intervallo, non trovate") <<
        "since=2022-03-20T07:01:01&to=2022-04-30T00:01:00" << QStringList {};

    QTest::newRow("per studente") <<
        // 4 es studente2
        "userId=4" << QStringList {
            "Arriva studente2",
        };

    QTest::newRow("per studente, non trovate") <<
        // 7 es patre_studente1
        "userId=7" << QStringList {};

    QTest::newRow("per studente e datas") <<
        // 2 es studente1
        "userId=2&since=2022-01-20T07:01:01&to=2023-04-30" << QStringList {
            "Abandona studente",
            "Retorna studente",
        };
}

void ReadonlyTest::testUserStatusChangeFilters()
{
    QFETCH(QString, queryString);
    QFETCH(QStringList, expectedReasons);

    QVERIFY(login(masterLogin));

    m_fieldFilter.setFieldsToRemove({
        "creatorId", "groups", "teachers", "user", "userId", "status", "time",
    });

    QScopedPointer<QNetworkReply> reply(getRequest("api/v1/statuschanges",
                                                   QUrlQuery(queryString)));
    QCOMPARE(replyStatus(reply.get()), 200);

    QJsonArray json = replyJsonObject(reply.get())["data"].toArray();
    QJsonArray statusChanges = m_fieldFilter.filter(json);
    QVERIFY(!m_fieldFilter.hasError());

    QStringList reasons;
    for (const QJsonValue &v: statusChanges) {
        reasons.append(v.toObject()["reason"].toString());
    }
    QCOMPARE(reasons, expectedReasons);
}

void ReadonlyTest::testUserStatusChangeFields_data()
{
    QTest::addColumn<QString>("fields");
    QTest::addColumn<QString>("expectedJson");

    QTest::newRow("nulle campo extra") <<
        "" << R"({
          "changeId": 4,
          "groups":[{
            "description":"Prime gruppo",
            "groupId":1,
            "name":"Gruppo 1"
          }],
          "teachers":[{"userId":3}],
          "user":{"userId":4},
          "userId":4
        })";

    QTest::newRow("con nomine del studente") <<
        "studentFields=name" << R"({
          "changeId": 4,
          "groups":[{
            "description":"Prime gruppo",
            "groupId":1,
            "name":"Gruppo 1"
          }],
          "teachers":[{"userId":3}],
          "user":{"name":"Altere studente", "userId":4},
          "userId":4
        })";

    QTest::newRow("con nomine del inseniante") <<
        "teacherFields=name" << R"({
          "changeId": 4,
          "groups":[{
            "description":"Prime gruppo",
            "groupId":1,
            "name":"Gruppo 1"
          }],
          "teachers":[{"userId":3,"name":"Inseniante 123"}],
          "user":{"userId":4},
          "userId":4
        })";
}

void ReadonlyTest::testUserStatusChangeFields()
{
    QFETCH(QString, fields);
    QFETCH(QString, expectedJson);

    QVERIFY(login(masterLogin));

    const int student2Id = m_logins.value("studente2");
    QString queryString = QStringLiteral("userId=%1").arg(student2Id);
    if (!fields.isEmpty()) {
        queryString += '&';
        queryString += fields;
    }
    QScopedPointer<QNetworkReply> reply(getRequest("api/v1/statuschanges",
                                                   QUrlQuery(queryString)));
    QCOMPARE(replyStatus(reply.get()), 200);

    QJsonObject json = replyJsonObject(reply.get())["data"].toArray().
        first().toObject();
    m_fieldFilter.setFieldsToRemove({
        "creatorId", "locationId", "reason", "status", "time",
    });
    QJsonObject statusChanges = m_fieldFilter.filter(json);
    QVERIFY(!m_fieldFilter.hasError());

    QJsonObject expectedStatusChanges =
        QJsonDocument::fromJson(expectedJson.toUtf8()).object();
    QCOMPARE(statusChanges, expectedStatusChanges);
}

void ReadonlyTest::testListGroups_data()
{
    using QueryItems = QMap<QString,QString>;
    QTest::addColumn<QueryItems>("queryItems");
    QTest::addColumn<QStringList>("expectedGroupNames");

    QTest::newRow("pattern absente") <<
        QueryItems() << QStringList { "Gruppo 1", "Gruppo 2" };

    QTest::newRow("pattern vacue") <<
        QueryItems {
            { "searchText", "" },
        } << QStringList { "Gruppo 1", "Gruppo 2" };

    QTest::newRow("pattern Gruppo") <<
        QueryItems {
            { "searchText", "Gruppo" },
        } << QStringList { "Gruppo 1", "Gruppo 2" };

    QTest::newRow("pattern uppo 2") <<
        QueryItems {
            { "searchText", "uppo 2" },
        } << QStringList { "Gruppo 2" };

    QTest::newRow("per studente") <<
        QueryItems {
            { "studentId", "4" },
        } << QStringList { "Gruppo 1" };

    QTest::newRow("per studente, non trovate") <<
        QueryItems {
            { "studentId", "4000" },
        } << QStringList {};

    QTest::newRow("per studente, id del inseniante") <<
        QueryItems {
            { "studentId", "3" },
        } << QStringList {};

    QTest::newRow("per inseniante") <<
        QueryItems {
            { "teacherId", "3" },
        } << QStringList { "Gruppo 1" };

    QTest::newRow("per inseniante, id del studente") <<
        QueryItems {
            { "teacherId", "4" },
        } << QStringList {};

    QTest::newRow("per studente e pattern") <<
        QueryItems {
            { "studentId", "4" },
            { "searchText", "o 1" },
        } << QStringList { "Gruppo 1" };

    QTest::newRow("per studente e pattern, non trovate") <<
        QueryItems {
            { "studentId", "4" },
            { "searchText", "ppo 2" },
        } << QStringList {};
}

void ReadonlyTest::testListGroups()
{
    using QueryItems = QMap<QString,QString>;
    QFETCH(QueryItems, queryItems);
    QFETCH(QStringList, expectedGroupNames);

    QVERIFY(login(masterLogin));

    QUrlQuery query;
    for (auto i = queryItems.constBegin(); i != queryItems.constEnd(); i++) {
        query.addQueryItem(i.key(), i.value());
    }

    auto reply = getRequest("api/v1/groups", query);

    QCOMPARE(replyStatus(reply), 200);
    QJsonObject response = replyJsonObject(reply);
    QCOMPARE(response.value("status").toString(), QString("ok"));

    QStringList groupNames;
    const QJsonArray groups = response.value("data").toArray();
    for (const QJsonValue &v: groups) {
        const QJsonObject gruppo = v.toObject();
        QVERIFY(gruppo.value("groupId").toInt() > 0);
        groupNames.append(gruppo.value("name").toString());
    }
    QCOMPARE(groupNames, expectedGroupNames);
    delete reply;
}

void ReadonlyTest::testGetGroup_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("url");
    QTest::addColumn<int>("expectedHttpStatus");
    QTest::addColumn<QJsonObject>("expectedResponse");

    const QJsonObject basicResponse {
        { "groupId", 1 },
        { "name", "Gruppo 1" },
        { "description", "Prime gruppo" },
        { "locationId", 1 },
        { "location", QJsonObject {
            { "locationId", 1 },
            { "name", "Sala 1" },
            { "description", "Prime sala" },
        }},
    };
    const QJsonArray tags {
        QJsonObject {
            { "name", "ChildTag 1-1" },
            { "description", "" },
        },
        QJsonObject {
            { "name", "ChildTag 1-2" },
            { "description", "" },
        },
    };

    QTest::newRow("non trovate") <<
        masterLogin << "13245" << 404 << QJsonObject {};

    // manca permissiones pro etiquettas
    QTest::newRow("sin etiquettas") <<
        studentLogin << "1" << 200 << basicResponse;

    QJsonObject withTags(basicResponse);
    withTags.insert("tags", tags);
    QTest::newRow("ok") <<
        masterLogin << "1" << 200 << withTags;

    QTest::newRow("sin permissions") <<
        studentLogin << "1?members=name" << 403 << QJsonObject {};

    QJsonObject namesResponse(basicResponse);
    namesResponse.insert("teachers", QJsonArray({
        QJsonObject {
            { "userId", 3 },
            { "name", "Inseniante 123" },
        },
    }));
    namesResponse.insert("students", QJsonArray({
        QJsonObject {
            { "userId", 4 },
            { "name", "Altere studente" },
        },
        QJsonObject {
            { "userId", 2 },
            { "name", "Le studente" },
        },
    }));
    namesResponse.insert("tags", tags);
    QTest::newRow("con membros") <<
        masterLogin << "1?members=name" << 200 << namesResponse;

    QJsonObject fieldsResponse(basicResponse);
    fieldsResponse.insert("teachers", QJsonArray({
        QJsonObject {
            { "userId", 3 },
            { "name", "Inseniante 123" },
            { "login", "inseniante1" },
        },
    }));
    fieldsResponse.insert("students", QJsonArray({
        QJsonObject {
            { "userId", 4 },
            { "name", "Altere studente" },
            { "login", "studente2" },
        },
        QJsonObject {
            { "userId", 2 },
            { "name", "Le studente" },
            { "login", "studente" },
        },
    }));
    fieldsResponse.insert("tags", tags);
    QTest::newRow("con membros e login") <<
        masterLogin << "1?members=name&members=login" << 200 << fieldsResponse;
}

void ReadonlyTest::testGetGroup()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QString, url);
    QFETCH(int, expectedHttpStatus);
    QFETCH(QJsonObject, expectedResponse);

    QVERIFY(login(loginInfo));

    QString path;
    QString query;
    if (url.contains('?')) {
        QStringList parts = url.split('?');
        path = parts[0];
        query = parts[1];
    } else {
        path = url;
    }
    auto reply = getRequest("api/v1/groups/" + path, QUrlQuery(query));

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    QJsonObject json = replyJsonObject(reply)["data"].toObject();
    m_fieldFilter.setFieldsToRemove({
        "created", "creatorId", "icon", "color", "parentTagId", "tagId",
    });
    QJsonObject response = m_fieldFilter.filter(json);
    QCOMPARE(response, expectedResponse);
    delete reply;
}

void ReadonlyTest::testDeleteGroup_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("groupId");
    QTest::addColumn<int>("expectedHttpStatus");

    QTest::newRow("non trovate") <<
        masterLogin << "13245" << 404;

    QTest::newRow("ID del gruppo invalide") <<
        studentLogin << "1esf3" << 400;

    QTest::newRow("sin permissions") <<
        studentLogin << "1" << 403;
}

void ReadonlyTest::testDeleteGroup()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QString, groupId);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(login(loginInfo));

    auto reply = deleteRequest("api/v1/groups/" + groupId);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void ReadonlyTest::testListLessons_data()
{
    using QueryItems = QMap<QString,QString>;
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QueryItems>("queryItems");
    QTest::addColumn<QStringList>("expectedLessonTimes");

    QTest::newRow("sin filtros") <<
        masterLogin <<
        QueryItems() << QStringList {
            "2023-03-19T06:00:00",
            "2023-03-19T07:00:00",
            "2023-03-19T15:00:00",
            "2023-03-19T16:00:00",
        };

    QTest::newRow("de tempore") <<
        masterLogin <<
        QueryItems {
            { "since", "2023-03-19T07:01:00" },
        } << QStringList {
            "2023-03-19T07:00:00",
            "2023-03-19T15:00:00",
            "2023-03-19T16:00:00",
        };

    QTest::newRow("a tempore") <<
        masterLogin <<
        QueryItems {
            { "to", "2023-03-19T14:59:00" },
        } << QStringList { "2023-03-19T06:00:00", "2023-03-19T07:00:00" };

    QTest::newRow("intervallo") <<
        masterLogin <<
        QueryItems {
            { "since", "2023-03-19T07:01:00" },
            { "to", "2023-03-19T15:20:00" },
        } << QStringList { "2023-03-19T07:00:00", "2023-03-19T15:00:00" };

    QTest::newRow("de tempore, non trovate") <<
        masterLogin <<
        QueryItems {
            { "since", "2023-04-19T06:00:00" },
        } << QStringList {};

    QTest::newRow("per studente") <<
        masterLogin <<
        QueryItems {
            { "studentId", "2" },
        } << QStringList {
            "2023-03-19T06:00:00",
            "2023-03-19T07:00:00",
            "2023-03-19T15:00:00",
            "2023-03-19T16:00:00",
        };

    QTest::newRow("per studente, non trovate") <<
        masterLogin <<
        QueryItems {
            { "studentId", "4000" },
        } << QStringList {};

    QTest::newRow("per studente, id del inseniante") <<
        masterLogin <<
        QueryItems {
            { "studentId", "3" },
        } << QStringList {};

    QTest::newRow("per inseniante") <<
        masterLogin <<
        QueryItems {
            { "teacherId", "3" },
        } << QStringList {
            "2023-03-19T06:00:00",
            "2023-03-19T07:00:00",
            "2023-03-19T15:00:00",
            "2023-03-19T16:00:00",
        };

    QTest::newRow("per inseniante, id del studente") <<
        masterLogin <<
        QueryItems {
            { "teacherId", "4" },
        } << QStringList {};

    QTest::newRow("per studente e tempore initial") <<
        masterLogin <<
        QueryItems {
            { "studentId", "2" },
            { "since", "2023-03-19T14:00:00" },
        } << QStringList { "2023-03-19T15:00:00", "2023-03-19T16:00:00" };

    QTest::newRow("per studente e tempore final") <<
        masterLogin <<
        QueryItems {
            { "studentId", "2" },
            { "to", "2023-03-19T14:00:00" },
        } << QStringList { "2023-03-19T06:00:00", "2023-03-19T07:00:00" };

    QTest::newRow("per studente e tempore, non trovate") <<
        masterLogin <<
        QueryItems {
            { "studentId", "2" },
            { "to", "2023-02-19T10:00:00" },
        } << QStringList {};

    QTest::newRow("per gruppo") <<
        masterLogin <<
        QueryItems {
            { "groupId", "1" },
        } << QStringList {
            "2023-03-19T06:00:00",
            "2023-03-19T07:00:00",
            "2023-03-19T16:00:00",
        };

    /* Usatores non privilegiate debe vider solmente lor lectiones */

    QTest::newRow("studente2 sin filtros") <<
        student2Login <<
        QueryItems() << QStringList {
            "2023-03-19T06:00:00",
            "2023-03-19T07:00:00",
            "2023-03-19T16:00:00",
        };

    QTest::newRow("studente2 con filtro a se mesme") <<
        student2Login <<
        QueryItems{
            { "studentId", QString::number(m_logins["studente2"]) },
        } << QStringList {
            "2023-03-19T06:00:00",
            "2023-03-19T07:00:00",
            "2023-03-19T16:00:00",
        };
}

void ReadonlyTest::testListLessons()
{
    using QueryItems = QMap<QString,QString>;
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QueryItems, queryItems);
    QFETCH(QStringList, expectedLessonTimes);

    QVERIFY(login(loginInfo));

    QUrlQuery query;
    for (auto i = queryItems.constBegin(); i != queryItems.constEnd(); i++) {
        query.addQueryItem(i.key(), i.value());
    }

    auto reply = getRequest("api/v1/lessons", query);

    QCOMPARE(replyStatus(reply), 200);
    QJsonObject response = replyJsonObject(reply);
    QCOMPARE(response.value("status").toString(), QString("ok"));

    QStringList lessonTimes;
    const QJsonArray lessons = response.value("data").toArray();
    for (const QJsonValue &v: lessons) {
        const QJsonObject lesson = v.toObject();
        QVERIFY(lesson.value("lessonId").toInt() > 0);
        lessonTimes.append(lesson.value("startTime").toString());
    }
    QCOMPARE(lessonTimes, expectedLessonTimes);
    delete reply;
}

void ReadonlyTest::testGetLesson_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("url");
    QTest::addColumn<int>("expectedHttpStatus");
    QTest::addColumn<QJsonObject>("expectedResponse");

    const QJsonObject basicResponse {
        { "lessonId", 1 },
        { "startTime", "2023-03-19T06:00:00" },
        { "endTime", "2023-03-19T07:00:00" },
        { "creatorId", 1 },
        { "locationId", -1 },
        { "groups", QJsonArray {
            QJsonObject {
                { "attended", false },
                { "invited", false },
                { "creatorId", 1 },
                { "note", "" },
                { "group", QJsonObject {
                    { "description", "Prime gruppo" },
                    { "groupId", 1 },
                    { "locationId", 1 },
                    { "name", "Gruppo 1" },
                }},
            },
        }},
        { "students", QJsonArray {
            QJsonObject {
                { "attended", false },
                { "invited", false },
                { "creatorId", -1 },
                { "note", "Malade" },
                { "user", QJsonObject {
                    { "userId", 2 },
                }},
            },
            QJsonObject {
                { "attended", true },
                { "invited", false },
                { "creatorId", -1 },
                { "note", "" },
                { "user", QJsonObject {
                    { "userId", 4 },
                }},
            },
        }},
        { "teachers", QJsonArray {
            QJsonObject {
                { "attended", true },
                { "invited", false },
                { "creatorId", 1 },
                { "note", "" },
                { "user", QJsonObject {
                    { "userId", 3 },
                }},
            },
        }},
    };
    QTest::newRow("non trovate") <<
        masterLogin << "13245" << 404 << QJsonObject {};

    QTest::newRow("ok") <<
        masterLogin << "1" << 200 << basicResponse;

    QTest::newRow("sin permissions") <<
        studentLogin << "1" << 403 << QJsonObject {};

    QJsonObject namesResponse(basicResponse);
    {
        QJsonObject ta = namesResponse["teachers"].toArray()[0].toObject();
        QJsonObject teacher = ta["user"].toObject();
        teacher["name"] = "Inseniante 123";
        ta["user"] = teacher;
        namesResponse["teachers"] = QJsonArray { ta };
    }
    {
        QJsonObject sa0 = namesResponse["students"].toArray()[0].toObject();
        QJsonObject s0 = sa0["user"].toObject();
        s0["name"] = "Le studente";
        sa0["user"] = s0;
        QJsonObject sa1 = namesResponse["students"].toArray()[1].toObject();
        QJsonObject s1 = sa1["user"].toObject();
        s1["name"] = "Altere studente";
        sa1["user"] = s1;
        namesResponse["students"] = QJsonArray { sa0, sa1 };
    }
    QTest::newRow("con userFields") <<
        masterLogin << "1?userFields=name" << 200 << namesResponse;
}

void ReadonlyTest::testGetLesson()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QString, url);
    QFETCH(int, expectedHttpStatus);
    QFETCH(QJsonObject, expectedResponse);

    QVERIFY(login(loginInfo));

    QString path;
    QString query;
    if (url.contains('?')) {
        QStringList parts = url.split('?');
        path = parts[0];
        query = parts[1];
    } else {
        path = url;
    }
    auto reply = getRequest("api/v1/lessons/" + path, QUrlQuery(query));

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    QJsonObject response = replyJsonObject(reply)["data"].toObject();
    response.remove("created"); // nos non compara le data
    const QVector<QPair<QString,QString>> entities {
        { "groups", "group" },
        { "students", "user" },
        { "teachers", "user" },
    };
    for (const auto &entity: entities) {
        const QString arrayName = entity.first;
        const QString entityName = entity.second;
        const QJsonArray array = response.value(arrayName).toArray();
        QJsonArray newArray;
        for (const QJsonValue &gv: array) {
            QJsonObject att = gv.toObject();
            att.remove("modified");
            QJsonObject ent = att.value(entityName).toObject();
            ent.remove("created");
            att[entityName] = ent;
            newArray.append(att);
        }
        if (!newArray.isEmpty()) {
            response[arrayName] = newArray;
        }
    }
    QCOMPARE(response, expectedResponse);
    delete reply;
}

void ReadonlyTest::testDeleteLesson_data()
{
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QString>("lessonId");
    QTest::addColumn<int>("expectedHttpStatus");

    QTest::newRow("non trovate") <<
        masterLogin << "13245" << 404;

    QTest::newRow("ID del lection invalide") <<
        studentLogin << "1esf3" << 400;

    QTest::newRow("sin permissiones") <<
        studentLogin << "1" << 403;
}

void ReadonlyTest::testDeleteLesson()
{
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QString, lessonId);
    QFETCH(int, expectedHttpStatus);

    QVERIFY(login(loginInfo));

    auto reply = deleteRequest("api/v1/lessons/" + lessonId);

    QCOMPARE(replyStatus(reply), expectedHttpStatus);
    delete reply;
}

void ReadonlyTest::testLessonPermissions()
{
    const int lessonId = 1;
    const QString baseUrl = "api/v1/lessons/";
    const QString baseLessonUrl = baseUrl + QString::number(lessonId);

    const int student1Id = m_logins.value("studente");

    QVERIFY(login(studentLogin));

    auto createLessonStatus = [=]() {
        QJsonObject creationData {
            { "startTime", "2023-03-19T06:00:00" },
            { "endTime",   "2023-03-19T07:00:00" },
        };
        QScopedPointer<QNetworkReply> reply(
            postRequest(baseUrl, creationData));
        return replyStatus(reply.data());
    };

    auto listLessonStatus = [=](const QUrlQuery &query = {}) {
        QScopedPointer<QNetworkReply> reply(getRequest(baseUrl, query));
        return replyStatus(reply.data());
    };

    auto getLessonStatus = [=]() {
        QScopedPointer<QNetworkReply> reply(getRequest(baseLessonUrl));
        return replyStatus(reply.data());
    };

    auto deleteLessonStatus = [=]() {
        QScopedPointer<QNetworkReply> reply(deleteRequest(baseLessonUrl));
        return replyStatus(reply.data());
    };

    auto inviteStatus = [=]() {
        const QJsonObject invitees {
            { "students", QJsonArray { student1Id }},
        };
        QScopedPointer<QNetworkReply> reply(
            postRequest(baseLessonUrl + "/invitees", invitees));
        return replyStatus(reply.data());
    };

    auto uninviteStatus = [=]() {
        QScopedPointer<QNetworkReply> reply(deleteRequest(
            baseLessonUrl + "/invitees/" + QString::number(student1Id)));
        return replyStatus(reply.data());
    };

    auto updateAttendanceStatus = [=]() {
        const QJsonObject attendance {
            { "students", QJsonArray {
                QJsonObject {{ "userId", student1Id }},
            }},
        };
        QScopedPointer<QNetworkReply> reply(
            putRequest(baseLessonUrl + "/attendance", attendance));
        return replyStatus(reply.data());
    };

    QCOMPARE(createLessonStatus(), 403);

    QCOMPARE(listLessonStatus(QUrlQuery("studentId=4")), 403);
    // TODO: nos poterea permitter isto, si le studente es un membro:
    QCOMPARE(listLessonStatus(QUrlQuery("groupId=1")), 403);

    QCOMPARE(getLessonStatus(), 403);

    QCOMPARE(deleteLessonStatus(), 403);

    QCOMPARE(inviteStatus(), 403);
    QCOMPARE(uninviteStatus(), 403);

    QCOMPARE(updateAttendanceStatus(), 403);
}

void ReadonlyTest::testListTags_data()
{
    using QueryItems = QMap<QString,QString>;
    QTest::addColumn<QJsonObject>("loginInfo");
    QTest::addColumn<QueryItems>("queryItems");
    QTest::addColumn<QStringList>("expectedTagNames");

    QTest::newRow("sin filtros") <<
        masterLogin <<
        QueryItems() << QStringList {
            "ChildTag 1-1",
            "ChildTag 1-2",
            "ChildTag 2-1",
            "ChildTag 2-2",
            "ParentTag 1",
            "ParentTag 2",
            "ParentTag 3",
        };

    QTest::newRow("top-level") <<
        masterLogin <<
        QueryItems {
            { "parentTagId", "-1" },
        } << QStringList {
            "ParentTag 1",
            "ParentTag 2",
            "ParentTag 3",
        };

    QTest::newRow("per parente") <<
        masterLogin <<
        QueryItems {
            { "parentTagId", "1" },
        } << QStringList {
            "ChildTag 1-1",
            "ChildTag 1-2",
        };

    QTest::newRow("per nomine") <<
        masterLogin <<
        QueryItems {
            { "name", "Tag 1" },
        } << QStringList {
            "ChildTag 1-1",
            "ChildTag 1-2",
            "ParentTag 1",
        };
}

void ReadonlyTest::testListTags()
{
    using QueryItems = QMap<QString,QString>;
    QFETCH(QJsonObject, loginInfo);
    QFETCH(QueryItems, queryItems);
    QFETCH(QStringList, expectedTagNames);

    QVERIFY(login(loginInfo));

    QUrlQuery query;
    for (auto i = queryItems.constBegin(); i != queryItems.constEnd(); i++) {
        query.addQueryItem(i.key(), i.value());
    }

    auto reply = getRequest("api/v1/tags", query);

    QCOMPARE(replyStatus(reply), 200);
    QJsonObject response = replyJsonObject(reply);
    QCOMPARE(response.value("status").toString(), QString("ok"));

    QStringList tagNames;
    const QJsonArray tags = response.value("data").toArray();
    for (const QJsonValue &v: tags) {
        const QJsonObject tag = v.toObject();
        tagNames.append(tag.value("name").toString());
    }
    QCOMPARE(tagNames, expectedTagNames);
    delete reply;
}

void ReadonlyTest::testGetTag_data()
{
    QTest::addColumn<QString>("url");
    QTest::addColumn<int>("expectedHttpStatus");
    QTest::addColumn<QJsonObject>("expectedResponse");

    const QJsonObject basicResponse {
        { "parentTagId", -1 },
        { "name", "ParentTag 2" },
    };
    QTest::newRow("non trovate") <<
        "13245" << 404 << QJsonObject {};

    QTest::newRow("ok") <<
        "2" << 200 << basicResponse;

    QJsonObject childrenResponse(basicResponse);
    childrenResponse["children"] = QJsonArray {
        QJsonObject {
            { "name", "ChildTag 2-1" },
            { "parentTagId", 2 },
        },
        QJsonObject {
            { "name", "ChildTag 2-2" },
            { "parentTagId", 2 },
        },
    };
    QTest::newRow("con filios") <<
        "2?fields=children" << 200 << childrenResponse;
}

void ReadonlyTest::testGetTag()
{
    QFETCH(QString, url);
    QFETCH(int, expectedHttpStatus);
    QFETCH(QJsonObject, expectedResponse);

    QVERIFY(login(masterLogin));

    QString path;
    QString query;
    if (url.contains('?')) {
        QStringList parts = url.split('?');
        path = parts[0];
        query = parts[1];
    } else {
        path = url;
    }
    auto reply = QScopedPointer(getRequest("api/v1/tags/" + path,
                                           QUrlQuery(query)));

    QCOMPARE(replyStatus(reply.get()), expectedHttpStatus);
    QJsonObject json = replyJsonObject(reply.get())["data"].toObject();
    m_fieldFilter.setFieldsToRemove({
        "created", "creatorId", "tagId", "color", "icon", "description",
    });
    QJsonObject tag = m_fieldFilter.filter(json);
    QCOMPARE(tag, expectedResponse);
}

QTEST_GUILESS_MAIN(ReadonlyTest)

#include "tst_readonly.moc"
