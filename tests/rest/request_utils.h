#ifndef REQUEST_UTILS_H
#define REQUEST_UTILS_H

#include <QByteArray>
#include <QNetworkAccessManager>
#include <QString>
#include <QUrl>
#include <QUrlQuery>

class QNetworkReply;

class RequestUtils
{
public:
    RequestUtils();

    void setServerUrl(const QUrl &serverUrl);

    QNetworkRequest request(const QString &path,
                            const QUrlQuery &query = QUrlQuery());
    QNetworkReply *postRequest(const QString &path, const QByteArray &body,
                               const QString &contentType = QString());
    QNetworkReply *postRequest(const QString &path, const QJsonObject &data);
    QNetworkReply *postRequest(const QString &path, const QJsonArray &data);
    QNetworkReply *putRequest(const QString &path, const QJsonObject &data);
    QNetworkReply *getRequest(const QString &path,
                              const QUrlQuery &query = QUrlQuery());
    QNetworkReply *deleteRequest(const QString &path,
                                 const QUrlQuery &query = QUrlQuery());

    int replyStatus(QNetworkReply *reply);
    QJsonObject replyJsonObject(QNetworkReply *reply);
    QJsonArray replyJsonArray(QNetworkReply *reply);
    bool wait(QNetworkReply *reply);

    void cleanup();

protected:
    QUrl m_serverUrl;
    QNetworkAccessManager m_nam;
    QByteArray m_authenticationToken;
};

#endif // REQUEST_UTILS_H
