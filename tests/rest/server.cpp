#include "server.h"

#include <QElapsedTimer>
#include <QNetworkReply>

Server::Server(QObject *parent):
    QProcess(parent),
    m_port(3131)
{
    setProgram("cutelyst2");
    setProcessChannelMode(QProcess::ForwardedChannels);
}

bool Server::start()
{
    startProcess();
    QElapsedTimer timer;
    timer.start();
    while (true) {
        m_rest.setServerUrl(serverUrl());
        auto reply = m_rest.getRequest("ping");
        if (m_rest.replyStatus(reply) == 200) return true;
        if (state() == QProcess::NotRunning) {
            m_port++;
            startProcess();
        }
        if (timer.elapsed() > 10000) break;
    }
    return false;
}

bool Server::kill()
{
    QScopedPointer<QNetworkReply> reply(m_rest.getRequest("testing/quit"));
    return m_rest.wait(reply.get());
}

QUrl Server::serverUrl() const
{
    QUrl url = QUrl::fromUserInput("http://localhost/");
    url.setPort(m_port);
    return url;
}

void Server::startProcess()
{
    setArguments({
        "--server",
        "-p", QString::number(m_port),
        "--app-file", STUDENTARIO_APP,
        "--",
        "--ini", CUTELYST_CONFIG_FILE,
    });
    QProcess::start();
}
