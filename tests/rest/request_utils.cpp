#include "request_utils.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QSignalSpy>

RequestUtils::RequestUtils()
{
    m_nam.setCookieJar(nullptr);
}

void RequestUtils::setServerUrl(const QUrl &serverUrl)
{
    m_serverUrl = serverUrl;
}

void RequestUtils::cleanup()
{
    m_authenticationToken.clear();
}

QNetworkRequest RequestUtils::request(const QString &path,
                                      const QUrlQuery &query)
{
    QNetworkRequest req;
    QUrl url(m_serverUrl);
    url.setPath('/' + path);
    url.setQuery(query);
    req.setUrl(url);

    if (!m_authenticationToken.isEmpty()) {
        req.setRawHeader("Authorization", "Basic " + m_authenticationToken);
    }

    return req;
}

QNetworkReply *RequestUtils::postRequest(const QString &path,
                                         const QByteArray &body,
                                         const QString &contentType)
{
    QNetworkRequest req = request(path);
    if (!contentType.isEmpty()) {
        req.setHeader(QNetworkRequest::ContentTypeHeader, contentType);
    }

    return m_nam.post(req, body);
}

QNetworkReply *RequestUtils::postRequest(const QString &path,
                                         const QJsonObject &data)
{
    QByteArray body = QJsonDocument(data).toJson();
    return postRequest(path, body, "application/json");
}

QNetworkReply *RequestUtils::postRequest(const QString &path,
                                         const QJsonArray &data)
{
    QByteArray body = QJsonDocument(data).toJson();
    return postRequest(path, body, "application/json");
}

QNetworkReply *RequestUtils::putRequest(const QString &path,
                                        const QJsonObject &data)
{
    QByteArray body = QJsonDocument(data).toJson();
    QNetworkRequest req = request(path);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    return m_nam.put(req, body);
}

QNetworkReply *RequestUtils::getRequest(const QString &path,
                                        const QUrlQuery &query)
{
    QNetworkRequest req = request(path, query);
    return m_nam.get(req);
}

QNetworkReply *RequestUtils::deleteRequest(const QString &path,
                                           const QUrlQuery &query)
{
    QNetworkRequest req = request(path, query);
    return m_nam.deleteResource(req);
}

bool RequestUtils::wait(QNetworkReply *reply)
{
    if (!reply->isFinished()) {
        QSignalSpy finished(reply, &QNetworkReply::finished);
        bool emitted = finished.wait();
        if (!emitted) return false;
    }

    return true;
}

int RequestUtils::replyStatus(QNetworkReply *reply)
{
    if (!wait(reply)) return -1;
    return reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
}

QJsonObject RequestUtils::replyJsonObject(QNetworkReply *reply)
{
    if (!wait(reply)) return QJsonObject();
    return QJsonDocument::fromJson(reply->readAll()).object();
}

QJsonArray RequestUtils::replyJsonArray(QNetworkReply *reply)
{
    if (!wait(reply)) return QJsonArray();
    return QJsonDocument::fromJson(reply->readAll()).array();
}
