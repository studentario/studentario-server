#include "tst_database.h"

using LocationIds = QVector<int>;

namespace Core {

inline QByteArray toByteArray(const Location &l)
{
    return "Location("
        "id=" + QByteArray::number(l.id) +
        ", name=" + l.name.toUtf8() +
        ", desc=" + l.description.toUtf8() +
        ", creator=" + QByteArray::number(l.creatorId) +
        ", created=" + l.created.toString().toUtf8() +
        ", color=" + l.color.toUtf8() +
        ", icon=" + l.icon.toUtf8() +
        ")";
}

inline char *toString(const Location &l)
{
    return QTest::toString(toByteArray(l));
}

} // namespace

void DatabaseTest::testLocationCreationAndDeletion()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    // Testa le creation

    const int creatorId1 = 3;
    const int creatorId2 = 2;

    const Core::Location location1 = {
        0, "Classe 1", "Le prime classe",
        creatorId1, QDateTime::fromSecsSinceEpoch(12345),
        "#123abc",
        "file:///icon1.png",
    };
    int locationId1 = db.addLocation(location1);
    QVERIFY(locationId1 >= 0);

    const Core::Location location2 = {
        0, "Classe 1", "Le prime classe",
        creatorId2, QDateTime::fromSecsSinceEpoch(54321),
        "#abc123",
        "file:///icon2.png",
    };
    int locationId2 = db.addLocation(location2);
    QVERIFY(locationId2 >= 0);

    // Testa le lectura

    {
        Core::Location location;
        QVERIFY(db.location(locationId1, &location));
        QCOMPARE(location.id, locationId1);
        // nihilifica le ID, que in location1 es ancora 0
        location.id = 0;
        QCOMPARE(location, location1);
    }

    {
        Core::Location location;
        QVERIFY(db.location(locationId2, &location));
        QCOMPARE(location.id, locationId2);
        // nihilifica le ID, que in location2 es ancora 0
        location.id = 0;
        QCOMPARE(location, location2);
    }

    // Testa le modification

    {
        // nulle campo: expecta un error
        QTest::ignoreMessage(QtWarningMsg, "Nothing to update");
        Core::Location location;
        QVERIFY(db.location(locationId1, &location));
        Core::Location::Fields fields;
        Core::Error error = db.updateLocation(location, fields);
        QVERIFY(!error);
    }

    {
        // Cambia le campos singularmente
        Core::Location location, locationReloaded;
        QVERIFY(db.location(locationId1, &location));

        Core::Location::Fields fields = Core::Location::Field::CreatorId;
        location.creatorId = -1;
        Core::Error error = db.updateLocation(location, fields);
        QVERIFY(!error);
        QVERIFY(db.location(locationId1, &locationReloaded));
        QCOMPARE(locationReloaded, location);

        fields = Core::Location::Field::Name;
        location.name = "Classe 1A",
        error = db.updateLocation(location, fields);
        QVERIFY(!error);
        QVERIFY(db.location(locationId1, &locationReloaded));
        QCOMPARE(locationReloaded, location);

        fields = Core::Location::Field::Description;
        location.description = QString(),
        error = db.updateLocation(location, fields);
        QVERIFY(!error);
        QVERIFY(db.location(locationId1, &locationReloaded));
        QCOMPARE(locationReloaded, location);

        fields = Core::Location::Field::Color;
        location.color = "#321bca";
        error = db.updateLocation(location, fields);
        QVERIFY(!error);
        QVERIFY(db.location(locationId1, &locationReloaded));
        QCOMPARE(locationReloaded, location);
    }

    {
        // Cambia le campos insimul
        Core::Location location, locationReloaded;
        QVERIFY(db.location(locationId1, &location));

        Core::Location::Fields fields =
            Core::Location::Field::CreatorId |
            Core::Location::Field::Name |
            Core::Location::Field::Description |
            Core::Location::Field::Color |
            Core::Location::Field::Icon;
        location.creatorId = creatorId2;
        location.name = "Classe 1B";
        location.description = "Le studentes";
        location.color = "";
        location.icon = "http://example.com/icon.png";
        Core::Error error = db.updateLocation(location, fields);
        QVERIFY(!error);
        QVERIFY(db.location(locationId1, &locationReloaded));
        QCOMPARE(locationReloaded, location);
    }

    // Testa le deletion

    {
        Core::Location location;
        Core::Error error = db.deleteLocation(locationId2);
        QVERIFY(!error);
        QVERIFY(!db.location(locationId2, &location));
        QVERIFY(db.location(locationId1, &location));
    }

    {
        Core::Error error = db.deleteLocation(42134);
        QVERIFY(bool(error));
        QCOMPARE(error.code(), Core::Error::LocationNotFound);
    }
}

void DatabaseTest::testLocations_data()
{
    QTest::addColumn<LocationIds>("expectedLocationIds");

    QTest::newRow("tote locationes") <<
        LocationIds { 4, 2, 5, 3, 1 };
}

void DatabaseTest::testLocations()
{
    QFETCH(LocationIds, expectedLocationIds);

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    /* Adde le locationes */

    using Location = Core::Location;
    const QVector<Location> allLocations = {
        {
            0, "Sala 5",
        },
        {
            0, "Sala 2",
        },
        {
            0, "Sala 4",
        },
        {
            0, "Sala 1",
        },
        {
            0, "Sala 3",
        },
    };

    for (const Location &location: allLocations) {
        int locationId = db.addLocation(location);
        QVERIFY(locationId >= 0);
    }

    /* Lege del base de datos */

    const auto locations = db.locations();
    LocationIds locationIds;
    for (const Location &location: locations) {
        locationIds.append(location.id);
    }

    QCOMPARE(locationIds, expectedLocationIds);
    QVERIFY(m_capturedWarnings.isEmpty());
}
