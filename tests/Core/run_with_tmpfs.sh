#! /bin/sh

set -e

export TMPDIR="/run/user/$(id -u)"
exec $WRAPPER "$@"
