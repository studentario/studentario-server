#include "error.h"

#include <QScopedPointer>
#include <QTest>

class ErrorTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testEmpty();
    void testGetters();
    void testDestructor();
};

void ErrorTest::testEmpty()
{
    Core::Error error;
    QVERIFY(!error);
    QVERIFY(!error.isError());
    QCOMPARE(error.code(), Core::Error::NoError);
    QVERIFY(error.message().isEmpty());
}

void ErrorTest::testGetters()
{
    Core::Error error(Core::Error::MissingRequiredField, "Missing field");
    QVERIFY(error);
    QVERIFY(error.isError());
    QCOMPARE(error.code(), Core::Error::MissingRequiredField);
    QCOMPARE(error.message(), QString("Missing field"));
}

void ErrorTest::testDestructor()
{
    // Solmente pro invocar le destructor virtual
    QScopedPointer<Core::Error> error(
        new Core::Error(Core::Error::MissingRequiredField, "Bad luck"));
    QVERIFY(error);
}

QTEST_APPLESS_MAIN(ErrorTest)

#include "tst_error.moc"
