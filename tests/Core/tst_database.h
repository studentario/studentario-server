#ifndef STUDENTARIO_TST_DATABASE_H
#define STUDENTARIO_TST_DATABASE_H

#include "database.h"

#include <QScopedPointer>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QTemporaryFile>
#include <QTest>

Q_DECLARE_METATYPE(Core::User)
Q_DECLARE_METATYPE(Core::User::Fields)
Q_DECLARE_METATYPE(Core::User::Filters)
Q_DECLARE_METATYPE(Core::User::Roles)

namespace Core {

inline char *toString(const User &user)
{
    return QTest::toString(
        "User(" +
        QByteArray::number(user.id) + ", " +
        QByteArray::number(user.roles, 16) + ", " +
        user.login.toUtf8() + ", " +
        user.password + '-' + user.salt + '-' + user.pinCode + ", " +
        user.pinCodeCreation.toString().toUtf8() + ", " +
        user.name.toUtf8() + ", " +
        user.birthDate.toString().toUtf8() + ", " +
        user.keywords.toUtf8() + ", " +
        user.contactInfo.join(';') + ", " +
        user.created.toString().toUtf8());
}

} // namespace

namespace QTest {

template<>
inline char *toString(const QSet<int> &elements)
{
    QStringList list;
    for (int i: elements) {
        list.append(QString::number(i));
    }
    return QTest::toString(list.join(", "));
}

template<>
inline char *toString(const QVector<int> &elements)
{
    QStringList list;
    for (int i: elements) {
        list.append(QString::number(i));
    }
    return QTest::toString(list.join(", "));
}

} // namespace

class DatabaseTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void cleanup();

    void testConstruction();
    void testInit();
    void testCreateDb();

    void testUpgrade_data();
    void testUpgrade();

    void testReconnect();

    void testTransactionsCommit();
    void testTransactionsRollback();
    void testTransactionsWatchErrorCommit();
    void testTransactionsWatchErrorRollback();

    void testAddUser_data();
    void testAddUser();
    void testUpdateUser_data();
    void testUpdateUser();
    void testDeleteUser();
    void testUsers_data();
    void testUsers();
    void testLoginUniqueness();
    void testUserByLogin_data();
    void testUserByLogin();
    void testMaxUserId();
    void testUserStatusChanges();

    void testRelations();

    void testGroupCreationAndDeletion();
    void testGroups_data();
    void testGroups();

    void testUsersForGroup();

    void testLessonCreationAndDeletion();
    void testLessons_data();
    void testLessons();
    void testLessonParticipation();

    void testLessonNoteCreationAndDeletion();
    void testLessonNotes_data();
    void testLessonNotes();
    void testLessonNoteAttaching();

    void testLocationCreationAndDeletion();
    void testLocations_data();
    void testLocations();

    void testTagCreationAndDeletion();
    void testTags_data();
    void testTags();

    void testTagsForUsers();
    void testTagsForGroups();
    void testTagsForLessons();
    void testTagsForLessonNotes();
    void testTagsForLocations();

private:
    static void warningCollector(QtMsgType type,
                                 const QMessageLogContext &ctx,
                                 const QString &message);

    QVector<int> prepareGroups(Core::Database *db,
                               const QStringList &names);
    QVector<int> prepareStudents(Core::Database *db,
                                 const QStringList &names);
    QVector<int> prepareParents(Core::Database *db,
                                const QStringList &names);
    QVector<int> prepareTeachers(Core::Database *db,
                                 const QStringList &names);
    QVector<int> prepareAdmins(Core::Database *db,
                               const QStringList &names);
    QVector<int> prepareLessons(Core::Database *db,
                                const QVector<int64_t> &startTimes);
    QVector<int> prepareLessonNotes(Core::Database *db,
                                    const QStringList &texts);
    QVector<int> prepareLocations(Core::Database *db,
                                  const QStringList &names);
    QVector<int> prepareTags(Core::Database *db,
                             const QStringList &names);

    QString driverName() const;
    QString databaseName() const;
    QStringList mysqlConnectOptions() const;
    QString locateSqlFile(const QString &baseName) const;
    bool loadDumpFile(const QString &fileName);
    QString dumpAndCompare(const QString &fileNameWithExpectedDump);
    bool disconnectClients();

private:
    QVariantMap m_configuration;
    QScopedPointer<QTemporaryFile> m_dbFile;
    static QStringList m_capturedWarnings;
    static QtMessageHandler m_defaultMsgHandler;
};

#endif // STUDENTARIO_TST_DATABASE_H
