#include "tst_database.h"

#include <QCoreApplication>
#include <QProcess>
#include <QProcessEnvironment>
#include <QScopedPointer>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QTemporaryFile>
#include <QTest>

QStringList DatabaseTest::m_capturedWarnings;
QtMessageHandler DatabaseTest::m_defaultMsgHandler = nullptr;

QString DatabaseTest::driverName() const
{
    return m_configuration.value(QStringLiteral("DatabaseDriver"),
                                 QStringLiteral("QSQLITE")).toString();
}

QString DatabaseTest::databaseName() const
{
    return m_configuration.value(QStringLiteral("DatabaseName")).toString();
}

QStringList DatabaseTest::mysqlConnectOptions() const
{
    QString socket = m_configuration.value("DatabaseOptions").toString().
        split('=')[1];
    return {
        "-u", m_configuration.value("DatabaseUser").toString(),
        "-S", socket,
    };
}

QString DatabaseTest::locateSqlFile(const QString &baseName) const
{
    QString basePath = QStringLiteral(DATA_PATH "/") + baseName;
    const QStringList suffixes = {
        "_" + driverName().toLower(),
        QString(),
    };

    for (const QString &suffix: suffixes) {
        QString filePath = basePath + suffix + ".sql";
        if (QFile::exists(filePath)) return filePath;
    }
    return QString();
}

bool DatabaseTest::loadDumpFile(const QString &fileName)
{
    QString filePath = locateSqlFile(fileName);

    QProcess loader;
    if (driverName() == "QMYSQL") {
        loader.setProgram("mysql");
        loader.setArguments(mysqlConnectOptions() +
                            QStringList(databaseName()));
    } else {
        loader.setProgram("sqlite3");
        loader.setArguments({ databaseName() });
    }
    loader.setStandardInputFile(filePath);
    loader.start();
    bool ok = loader.waitForStarted() && loader.waitForFinished();
    QByteArray errorOutput = loader.readAllStandardError();
    if (!errorOutput.isEmpty()) {
        qWarning() << "Error output:" << QString::fromUtf8(errorOutput);
        return false;
    }
    return ok;
}

QString DatabaseTest::dumpAndCompare(const QString &fileNameWithExpectedDump)
{
    QTemporaryFile dumpFile;
    dumpFile.open();

    QProcess dumper;
    if (driverName() == "QMYSQL") {
        dumper.setProgram("mysqldump");
        dumper.setArguments(mysqlConnectOptions() + QStringList {
            "--compact",
            "--skip-add-drop-table",
            "--skip-add-locks",
            "--skip-comments",
            "--skip-disable-keys",
            "--skip-set-charset",
            "--skip-quote-names",
            "--triggers",
            databaseName(),
        });
    } else {
        dumper.setProgram("sqlite3");
        dumper.setArguments({ databaseName(), ".dump" });
    }
    dumper.setStandardOutputFile(dumpFile.fileName());
    dumper.start();

    if (!dumper.waitForStarted()) return "Could not start dumper process";
    if (!dumper.waitForFinished()) return "Dumper process failed";
    QByteArray errorOutput = dumper.readAllStandardError();
    if (!errorOutput.isEmpty()) {
        qWarning() << "Error output:" << QString::fromUtf8(errorOutput);
        return "Dumper process failed";
    }

    // Exclude alicun lineas non interessante

    QFile dumpedFile(dumpFile.fileName());
    QTemporaryFile filteredDumpedFile;
    if (!filteredDumpedFile.open()) return "Cannot create filtered dump file";
    if (!dumpedFile.open(QIODevice::ReadOnly)) return "Cannot open dump file";

    for (QByteArray line = dumpedFile.readLine(); !line.isEmpty();
         line = dumpedFile.readLine()) {
        if (line.startsWith("PRAGMA")) continue;
        if (line.startsWith("BEGIN TRANSACTION")) continue;
        if (line.startsWith("COMMIT")) continue;
        if (line.startsWith("/*")) continue;
        if (line.startsWith("DELIMITER ")) continue;

        // Elimina informationes non necessari
        if (line.startsWith(") ENGINE")) {
            line = ");\n";
        }

        filteredDumpedFile.write(line);
    }
    filteredDumpedFile.flush();

    // Exeque le commando `diff`

    QString filePath = locateSqlFile(fileNameWithExpectedDump);
    QProcess diff;
    diff.start("diff", { "-ub", filePath, filteredDumpedFile.fileName() });
    if (!diff.waitForStarted()) return "Could not start diff";
    if (!diff.waitForFinished()) return "Diff process failed";

    return QString::fromUtf8(diff.readAllStandardOutput());
}

bool DatabaseTest::disconnectClients()
{
    if (driverName() == "QMYSQL") {
        // From
        // https://dba.stackexchange.com/questions/7440/how-can-i-disconnect-clients-from-mysql
        QProcess pids;
        pids.setProgram("mysql");
        pids.setArguments(mysqlConnectOptions() + QStringList {
            "-NBe",
            QString("SELECT CONCAT('KILL ', id, ';')"
                    " FROM information_schema.processlist"
                    " WHERE db = '%1'").arg(databaseName()),
        });
        QProcess killer;
        killer.setProgram("mysql");
        killer.setArguments(mysqlConnectOptions() + QStringList { "-vv" });
        pids.setStandardOutputProcess(&killer);
        pids.start();
        killer.start();
        if (!pids.waitForStarted()) return false;
        if (!pids.waitForFinished()) return false;
        if (!killer.waitForFinished()) return false;
        if (killer.exitStatus() != QProcess::NormalExit ||
            killer.exitCode() != 0) {
            return false;
        }
        QByteArray output = killer.readAllStandardOutput();
        if (output.contains("KILL ")) {
            return true;
        } else {
            qDebug() << "Mysql output:" << output;
            qDebug() << "stderr:" << killer.readAllStandardError();
            return true;
        }
    } else {
        qWarning() << "disconnectClients() not implemented";
        return false;
    }
}

void DatabaseTest::initTestCase()
{
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    const QStringList keys = env.keys();
    for (const QString &key: keys) {
        if (key.startsWith("DBTEST_")) {
            m_configuration.insert(key.mid(7), env.value(key));
        }
    }

    if (databaseName().isEmpty()) {
        QString tmpFileTemplate =
            QDir::tempPath() + "/studentario-test-XXXXXX.db";
        m_dbFile.reset(new QTemporaryFile(tmpFileTemplate));
        m_dbFile->open();
        m_configuration.insert("DatabaseName", m_dbFile->fileName());
    }

    m_defaultMsgHandler = qInstallMessageHandler(warningCollector);
}

void DatabaseTest::cleanup()
{
    Core::Database db(m_configuration, "test");
    db.init();
    QSqlDatabase rawDb = db.dbForTesting();
    rawDb.transaction();
    const QStringList tables {
        "meta",
        "UserTags",
        "GroupTags",
        "LessonTags",
        "LessonNoteTags",
        "LocationTags",
        "Groups",
        "Students",
        "Teachers",
        "Admins",
        "Users",
        "StatusChanges",
        "Relations",
        "Tags",
        "Lessons",
        "Attendance",
        "LessonNotes",
        "LessonNotesGlue",
        "Locations",
    };
    for (const QString &table: tables) {
        rawDb.exec("DROP TABLE `" + table + '`');
    }
    rawDb.commit();

    m_capturedWarnings.clear();
}

void DatabaseTest::testConstruction()
{
    Core::Database db(m_configuration, "test");
}

void DatabaseTest::testInit()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.init());
}

void DatabaseTest::warningCollector(QtMsgType type,
                                    const QMessageLogContext &ctx,
                                    const QString &message)
{
     if (type == QtWarningMsg) {
         m_capturedWarnings.append(message);
     }
     m_defaultMsgHandler(type, ctx, message);
}

QVector<int> DatabaseTest::prepareGroups(Core::Database *db,
                                         const QStringList &names)
{
    QVector<int> ret;
    using Group = Core::Group;
    for (const QString &groupName: names) {
        Group group {
            0, groupName, "A description",
            QDateTime::fromSecsSinceEpoch(123456),
        };
        int groupId = db->addGroup(group);
        ret.append(groupId);
    }
    return ret;
}

QVector<int> DatabaseTest::prepareStudents(Core::Database *db,
                                           const QStringList &names)
{
    QVector<int> ret;
    using User = Core::User;
    for (const QString &userName: names) {
        User user {
            0, User::Role::Student, userName, "pwd0", "salt0", "pin",
            QDateTime::fromSecsSinceEpoch(123456), userName,
            QDate(2000, 12, 31), "kw", {},
            QDateTime::fromSecsSinceEpoch(123456),
        };
        int userId = db->addUser(user);
        ret.append(userId);
    }
    return ret;
}

QVector<int> DatabaseTest::prepareParents(Core::Database *db,
                                          const QStringList &names)
{
    QVector<int> ret;
    using User = Core::User;
    for (const QString &userName: names) {
        User user {
            0, User::Role::Parent, userName, "pwd0", "salt0", "pin",
            QDateTime::fromSecsSinceEpoch(123456), userName,
            QDate(2000, 12, 31), "kw", {},
            QDateTime::fromSecsSinceEpoch(123456),
        };
        int userId = db->addUser(user);
        ret.append(userId);
    }
    return ret;
}

QVector<int> DatabaseTest::prepareTeachers(Core::Database *db,
                                           const QStringList &names)
{
    QVector<int> ret;
    using User = Core::User;
    for (const QString &userName: names) {
        User user {
            0, User::Role::Teacher, userName, "pwd0", "salt0", "pin",
            QDateTime::fromSecsSinceEpoch(123456), userName,
            QDate(1990, 2, 28), "kw", {},
            QDateTime::fromSecsSinceEpoch(123456),
        };
        int userId = db->addUser(user);
        ret.append(userId);
    }
    return ret;
}

QVector<int> DatabaseTest::prepareAdmins(Core::Database *db,
                                         const QStringList &names)
{
    QVector<int> ret;
    using User = Core::User;
    for (const QString &userName: names) {
        User user {
            0, User::Role::Teacher, userName, "pwd0", "salt0", "pin",
            QDateTime::fromSecsSinceEpoch(123456), userName,
            QDate(1980, 5, 21), "kw", {},
            QDateTime::fromSecsSinceEpoch(123456),
        };
        int userId = db->addUser(user);
        ret.append(userId);
    }
    return ret;
}

QVector<int> DatabaseTest::prepareLessons(Core::Database *db,
                                          const QVector<int64_t> &startTimes)
{
    QVector<int> ret;
    using Lesson = Core::Lesson;
    for (int64_t timeSecs: startTimes) {
        QDateTime startTime = QDateTime::fromSecsSinceEpoch(timeSecs);
        QDateTime endTime = startTime.addSecs(900);
        Lesson lesson {
            0, -1, QDateTime::fromSecsSinceEpoch(123456),
            startTime, endTime, -1,
        };
        int lessonId = db->addLesson(lesson);
        ret.append(lessonId);
    }
    return ret;
}

QVector<int> DatabaseTest::prepareLessonNotes(Core::Database *db,
                                              const QStringList &texts)
{
    QVector<int> ret;
    using LessonNote = Core::LessonNote;
    for (const QString &text: texts) {
        LessonNote lessonNote {
            0, -1, QDateTime::fromSecsSinceEpoch(123456),
            -1, text,
        };
        int lessonNoteId = db->addLessonNote(lessonNote);
        ret.append(lessonNoteId);
    }
    return ret;
}

QVector<int> DatabaseTest::prepareLocations(Core::Database *db,
                                            const QStringList &names)
{
    QVector<int> ret;
    using Location = Core::Location;
    for (const QString &name: names) {
        Location location {
            0, name,
        };
        int locationId = db->addLocation(location);
        ret.append(locationId);
    }
    return ret;
}

QVector<int> DatabaseTest::prepareTags(Core::Database *db,
                                       const QStringList &names)
{
    QVector<int> ret;
    using Tag = Core::Tag;
    for (const QString &tagName: names) {
        Tag tag {
            0, -1, tagName, QString(), -1,
            QDateTime::fromSecsSinceEpoch(123456),
        };
        int tagId = -1;
        Core::Error error = db->addTag(tag, &tagId);
        if (error) {
            qWarning() << "Error creating tag" << tagName;
        }
        ret.append(tagId);
    }
    return ret;
}

void DatabaseTest::testCreateDb()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    QSqlDatabase rawDb = db.dbForTesting();
    QSqlQuery q =
        rawDb.exec("SELECT data FROM meta WHERE name = 'DB version'");
    QVERIFY(q.first());
}

void DatabaseTest::testUpgrade_data()
{
    QTest::addColumn<QString>("baseName");

    QTest::newRow("from version 1") <<
        "version1";

    QTest::newRow("from version 4") <<
        "version4";
}

void DatabaseTest::testUpgrade()
{
    QFETCH(QString, baseName);

    QString inputFile(baseName + "_input");
    if (locateSqlFile(inputFile).isEmpty()) {
        QSKIP("Input file missing, skipping test");
    }
    QString expectedOutput(baseName + "_output");

    QVERIFY(loadDumpFile(inputFile));

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    QString diff = dumpAndCompare(expectedOutput);
    if (diff.length() > 80) {
        QFile file(QStringLiteral("sql-") + QTest::currentDataTag() +
                   "-" + driverName() + ".diff");
        QVERIFY(file.open(QIODevice::WriteOnly));
        file.write(diff.toUtf8());
        qDebug() << "Diff written to" << file.fileName();
    }
    QCOMPARE(diff, QString());
}

void DatabaseTest::testReconnect()
{
    if (driverName() != "QMYSQL") {
        QSKIP("Reconnection test only implemented for MySQL");
    }

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const auto userIds = prepareStudents(&db, { "student" });

    QVERIFY(disconnectClients());

    Core::User readUser;
    QVERIFY(db.user(userIds.first(), &readUser));
}

QTEST_GUILESS_MAIN(DatabaseTest)
