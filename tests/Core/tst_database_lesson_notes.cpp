#include "tst_database.h"

using LessonIds = QVector<int>;
using LessonNotes = QVector<Core::LessonNote>;
using LessonNoteIds = QVector<int>;

void DatabaseTest::testLessonNoteCreationAndDeletion()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    // Testa le creation

    const int creatorId1 = 3;
    const int creatorId2 = 2;

    const Core::LessonNote lessonNote1 = {
        0, creatorId1, QDateTime::fromSecsSinceEpoch(12345),
        -1, "Prime nota",
    };
    int lessonNoteId1 = db.addLessonNote(lessonNote1);
    QVERIFY(lessonNoteId1 >= 0);

    const Core::LessonNote lessonNote2 = {
        0, creatorId2, QDateTime::fromSecsSinceEpoch(54321),
        -1, "Secunde nota",
    };
    int lessonNoteId2 = db.addLessonNote(lessonNote2);
    QVERIFY(lessonNoteId2 >= 0);

    // Testa le lectura

    {
        Core::LessonNote lessonNote;
        QVERIFY(db.lessonNote(lessonNoteId1, &lessonNote));
        QCOMPARE(lessonNote.id, lessonNoteId1);
        // nihilifica le ID, que in lessonNote1 es ancora 0
        lessonNote.id = 0;
        QCOMPARE(lessonNote, lessonNote1);
    }

    {
        Core::LessonNote lessonNote;
        QVERIFY(db.lessonNote(lessonNoteId2, &lessonNote));
        QCOMPARE(lessonNote.id, lessonNoteId2);
        // nihilifica le ID, que in lessonNote2 es ancora 0
        lessonNote.id = 0;
        QCOMPARE(lessonNote, lessonNote2);
    }

    // Testa le modification

    {
        Core::LessonNote lessonNote, lessonNoteReloaded;
        QVERIFY(db.lessonNote(lessonNoteId1, &lessonNote));

        lessonNote.text = "(modificate) prime nota";
        Core::Error error = db.updateLessonNote(lessonNote);
        QVERIFY(!error);
        QVERIFY(db.lessonNote(lessonNoteId1, &lessonNoteReloaded));
        QCOMPARE(lessonNoteReloaded, lessonNote);
    }

    // Testa le deletion

    {
        Core::LessonNote lessonNote;
        Core::Error error = db.deleteLessonNote(lessonNoteId2);
        QVERIFY(!error);
        QVERIFY(!db.lessonNote(lessonNoteId2, &lessonNote));
        QVERIFY(db.lessonNote(lessonNoteId1, &lessonNote));
    }

    {
        Core::Error error = db.deleteLessonNote(42134);
        QVERIFY(bool(error));
        QCOMPARE(error.code(), Core::Error::LessonNoteNotFound);
    }
}

void DatabaseTest::testLessonNotes_data()
{
    QTest::addColumn<int>("lessonId");
    QTest::addColumn<int>("creatorId");
    QTest::addColumn<LessonNoteIds>("expectedLessonNoteIds");

    QTest::newRow("tote notas") <<
        -1 << -1 <<
        LessonNoteIds { 4, 2, 3, 1, 5 };

    QTest::newRow("notas per lection") <<
        1 << -1 <<
        LessonNoteIds { 2, 3, 1 };

    QTest::newRow("notas per lection2") <<
        2 << -1 <<
        LessonNoteIds { 4, 5 };

    QTest::newRow("notas per creator") <<
        -1 << 4 <<
        LessonNoteIds { 3, 5 };

    QTest::newRow("notas per creator e lection") <<
        1 << 4 <<
        LessonNoteIds { 3 };

    QTest::newRow("nulle resultato") <<
        2 << 2 <<
        LessonNoteIds {};
}

void DatabaseTest::testLessonNotes()
{
    QFETCH(int, lessonId);
    QFETCH(int, creatorId);
    QFETCH(LessonNoteIds, expectedLessonNoteIds);

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    /* Adde le lectiones */
    const auto lessonIds = prepareLessons(&db, { 1000, 2000 });

    /* Adde le notas del lectiones */
    using LessonNote = Core::LessonNote;
    const QVector<LessonNote> allLessonNotes = {
        {
            0, 2, QDateTime::fromSecsSinceEpoch(12345),
            -1, "Prime nota",
        },
        {
            0, -1, QDateTime::fromSecsSinceEpoch(54321),
            -1, "Secunde nota",
        },
        {
            0, 4, QDateTime::fromSecsSinceEpoch(12346),
            -1, "Tewrtie nota",
        },
        {
            0, -1, QDateTime::fromSecsSinceEpoch(54322),
            -1, "Quarte nota",
        },
        {
            0, 4, QDateTime::fromSecsSinceEpoch(12341),
            -1, "Quinte nota",
        },
    };

    LessonNoteIds lessonNoteIds;
    for (const LessonNote &lessonNote: allLessonNotes) {
        int lessonNoteId = db.addLessonNote(lessonNote);
        QVERIFY(lessonNoteId >= 0);
        lessonNoteIds.append(lessonNoteId);
    }

    /* Adde le notas al lectiones */
    QVERIFY(db.attachLessonNote(lessonIds[0], lessonNoteIds[0]));
    QVERIFY(db.attachLessonNote(lessonIds[0], lessonNoteIds[1]));
    QVERIFY(db.attachLessonNote(lessonIds[0], lessonNoteIds[2]));
    QVERIFY(db.attachLessonNote(lessonIds[1], lessonNoteIds[3]));
    QVERIFY(db.attachLessonNote(lessonIds[1], lessonNoteIds[4]));

    /* Lege del base de datos */

    const auto lessonNotes = db.lessonNotes(lessonId, creatorId);
    LessonNoteIds noteIds;
    for (const LessonNote &lessonNote: lessonNotes) {
        noteIds.append(lessonNote.id);
    }

    QCOMPARE(noteIds, expectedLessonNoteIds);
    QVERIFY(m_capturedWarnings.isEmpty());
}

void DatabaseTest::testLessonNoteAttaching()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    /* Adde le lectiones */
    const auto lessonIds = prepareLessons(&db, { 1000, 2000 });

    /* Adde le notas del lectiones */
    using LessonNote = Core::LessonNote;
    const QVector<LessonNote> allLessonNotes = {
        {
            0, 2, QDateTime::fromSecsSinceEpoch(12345),
            -1, "Prime nota",
        },
        {
            0, -1, QDateTime::fromSecsSinceEpoch(54321),
            -1, "Secunde nota",
        },
        {
            0, 4, QDateTime::fromSecsSinceEpoch(12346),
            -1, "Tewrtie nota",
        },
        {
            0, -1, QDateTime::fromSecsSinceEpoch(54322),
            -1, "Quarte nota",
        },
        {
            0, 4, QDateTime::fromSecsSinceEpoch(12341),
            -1, "Quinte nota",
        },
    };

    LessonNoteIds lessonNoteIds;
    for (const LessonNote &lessonNote: allLessonNotes) {
        int lessonNoteId = db.addLessonNote(lessonNote);
        QVERIFY(lessonNoteId >= 0);
        lessonNoteIds.append(lessonNoteId);
    }

    /* Adde le notas al lectiones */
    bool ok = db.attachLessonNote(lessonIds[0], lessonNoteIds[0]);
    QVERIFY(ok);
    ok = db.attachLessonNote(lessonIds[0], lessonNoteIds[1]);
    QVERIFY(ok);
    ok = db.attachLessonNote(lessonIds[0], lessonNoteIds[2]);
    QVERIFY(ok);
    ok = db.attachLessonNote(lessonIds[1], lessonNoteIds[3]);
    QVERIFY(ok);
    ok = db.attachLessonNote(lessonIds[1], lessonNoteIds[4]);
    QVERIFY(ok);

    /* Lege del base de datos */

    auto extractIds = [](const LessonNotes &lessonNotes) {
        LessonNoteIds ids;
        for (const Core::LessonNote &l: lessonNotes) ids.append(l.id);
        return ids;
    };

    {
        LessonNotes lessonNotes = db.lessonNotes(lessonIds[0], -1);
        LessonNoteIds expectedIds = {
            lessonNoteIds[1], lessonNoteIds[2], lessonNoteIds[0],
        };
        QCOMPARE(extractIds(lessonNotes), expectedIds);
    }

    {
        // distacca un nota
        ok = db.detachLessonNote(lessonIds[0], lessonNoteIds[2]);
        QVERIFY(ok);
        LessonNotes lessonNotes = db.lessonNotes(lessonIds[0], -1);
        LessonNoteIds expectedIds = {
            lessonNoteIds[1], lessonNoteIds[0],
        };
        QCOMPARE(extractIds(lessonNotes), expectedIds);
    }

    {
        // Attacca un nota a duo lectiones
        ok = db.attachLessonNote(lessonIds[1], lessonNoteIds[0]);
        QVERIFY(ok);
        LessonNotes lessonNotes = db.lessonNotes(lessonIds[1], -1);
        QVERIFY(extractIds(lessonNotes).contains(lessonNoteIds[0]));
        // Verifica que illo es ancora attaccate anque al precedente
        lessonNotes = db.lessonNotes(lessonIds[0], -1);
        QVERIFY(extractIds(lessonNotes).contains(lessonNoteIds[0]));
    }

    {
        // Elimina un nota; le trigger debe remover lo del lectiones
        Core::Error error = db.deleteLessonNote(lessonNoteIds[0]);
        QVERIFY(!error);

        LessonNotes lessonNotes = db.lessonNotes(lessonIds[1], -1);;
        LessonNoteIds expectedIds = { lessonNoteIds[3], lessonNoteIds[4] };
        QCOMPARE(extractIds(lessonNotes), expectedIds);

        lessonNotes = db.lessonNotes(lessonIds[0], -1);;
        expectedIds = { lessonNoteIds[1] };
        QCOMPARE(extractIds(lessonNotes), expectedIds);
    }
}
