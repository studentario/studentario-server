#include "permissions.h"

#include <QCoreApplication>
#include <QTest>

Q_DECLARE_METATYPE(Core::Permissions::Action)
Q_DECLARE_METATYPE(Core::Permissions::Role)
Q_DECLARE_METATYPE(Core::Permissions::Roles)

class PermissionsTest: public QObject
{
    Q_OBJECT
    using Action = Core::Permissions::Action;
    using Role = Core::Permissions::Role;
    using Roles = Core::Permissions::Roles;

private Q_SLOTS:
    void testCan_data();
    void testCan();

    void testCanManageUser_data();
    void testCanManageUser();
};

void PermissionsTest::testCan_data()
{
    QTest::addColumn<Roles>("roles");
    QTest::addColumn<Action>("action");
    QTest::addColumn<bool>("shouldBeAllowed");

    QTest::newRow("CreateUser, student") <<
        Roles(Role::Student) << Action::CreateUser << false;
    QTest::newRow("CreateUser, parent") <<
        Roles(Role::Parent) << Action::CreateUser << false;
    QTest::newRow("CreateUser, teacher") <<
        Roles(Role::Teacher) << Action::CreateUser << false;
    QTest::newRow("CreateUser, admin") <<
        Roles(Role::Admin) << Action::CreateUser << true;
    QTest::newRow("CreateUser, director") <<
        Roles(Role::Director) << Action::CreateUser << true;
    QTest::newRow("CreateUser, master") <<
        Roles(Role::Master) << Action::CreateUser << true;
    QTest::newRow("CreateUser, mixed denied") <<
        Roles(Role::Student | Role::Teacher) << Action::CreateUser << false;
    QTest::newRow("CreateUser, mixed success") <<
        Roles(Role::Student | Role::Admin) << Action::CreateUser << true;

    QTest::newRow("EditUser, student") <<
        Roles(Role::Student) << Action::EditUser << false;
    QTest::newRow("EditUser, parent") <<
        Roles(Role::Parent) << Action::EditUser << false;
    QTest::newRow("EditUser, teacher") <<
        Roles(Role::Teacher) << Action::EditUser << false;
    QTest::newRow("EditUser, admin") <<
        Roles(Role::Admin) << Action::EditUser << true;
    QTest::newRow("EditUser, director") <<
        Roles(Role::Director) << Action::EditUser << true;
    QTest::newRow("EditUser, master") <<
        Roles(Role::Master) << Action::EditUser << true;
}

void PermissionsTest::testCan()
{
    QFETCH(Roles, roles);
    QFETCH(Action, action);
    QFETCH(bool, shouldBeAllowed);

    Core::Permissions permissions;
    bool allowed = permissions.can(roles, action);

    QCOMPARE(allowed, shouldBeAllowed);
}

void PermissionsTest::testCanManageUser_data()
{
    QTest::addColumn<Roles>("roles");
    QTest::addColumn<Action>("action");
    QTest::addColumn<Roles>("user");
    QTest::addColumn<bool>("shouldBeAllowed");

    QTest::newRow("EditUser, studente->studente") <<
        Roles(Role::Student) << Action::EditUser << Roles(Role::Student) << false;
    QTest::newRow("EditUser, parente->studente") <<
        Roles(Role::Parent) << Action::EditUser << Roles(Role::Student) << false;
    QTest::newRow("EditUser, inseniante->studente") <<
        Roles(Role::Teacher) << Action::EditUser << Roles(Role::Student) << false;
    QTest::newRow("EditUser, administrator->studente") <<
        Roles(Role::Admin) << Action::EditUser << Roles(Role::Student) << true;
    QTest::newRow("EditUser, administrator->parente") <<
        Roles(Role::Admin) << Action::EditUser << Roles(Role::Parent) << true;
    QTest::newRow("EditUser, administrator->inseniante") <<
        Roles(Role::Admin) << Action::EditUser << Roles(Role::Teacher) << true;

    QTest::newRow("EditUser, administrator->studente e director") <<
        Roles(Role::Admin) << Action::EditUser <<
        Roles(Role::Student|Role::Director) << false;
}

void PermissionsTest::testCanManageUser()
{
    QFETCH(Roles, roles);
    QFETCH(Action, action);
    QFETCH(Roles, user);
    QFETCH(bool, shouldBeAllowed);

    Core::Permissions permissions;
    bool allowed = permissions.canManageUser(roles, action, user);

    QCOMPARE(allowed, shouldBeAllowed);
}

QTEST_GUILESS_MAIN(PermissionsTest)

#include "tst_permissions.moc"
