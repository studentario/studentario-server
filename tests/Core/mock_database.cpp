#include "mock_database.h"

using namespace Core;

DatabasePrivate::DatabasePrivate(const QVariantMap &configuration,
                                 const QString &connectionName):
    m_configuration(configuration),
    m_connectionName(connectionName)
{
}

Database::Database(const QVariantMap &configuration,
                   const QString &connectionName):
    d_ptr(new DatabasePrivate(configuration, connectionName))
{
}

Database::~Database() = default;

bool Database::init()
{
    return false;
}

bool Database::ensureConnected()
{
    return false;
}

bool Database::createOrUpdateDb()
{
    return false;
}

int Database::addUser(const User &user)
{
    Q_D(Database);
    return d->m_addUserCb ? d->m_addUserCb(user) : -1;
}

bool Database::updateUser(const User &user, User::Fields fields)
{
    Q_D(Database);
    return d->m_updateUserCb ? d->m_updateUserCb(user, fields) : false;
}

Error Database::deleteUser(int userId)
{
    Q_D(Database);
    return d->m_deleteUserCb ?
        d->m_deleteUserCb(userId) : Error::DatabaseError;
}

bool Database::user(int id, User *user) const
{
    Q_D(const Database);
    return d->m_userCb ? d->m_userCb(id, user) : false;
}

QVector<User> Database::users(const User::Filters &filters,
                              User::Sort sort) const
{
    Q_D(const Database);
    return d->m_usersCb ?
        d->m_usersCb(filters, sort) : QVector<User>();
}

int Database::maxUserId() const
{
    Q_D(const Database);
    return d->m_maxUserIdCb ? d->m_maxUserIdCb() : -1;
}

int Database::addGroup(const Group &group)
{
    Q_D(Database);
    return d->m_addGroupCb ? d->m_addGroupCb(group) : -1;
}

Error Database::updateGroup(const Group &group, Group::Fields fields)
{
    Q_D(Database);
    return d->m_updateGroupCb ?
        d->m_updateGroupCb(group, fields) : Error::DatabaseError;
}

Error Database::deleteGroup(int groupId)
{
    Q_D(Database);
    return d->m_deleteGroupCb ?
        d->m_deleteGroupCb(groupId) : Error::DatabaseError;
}

bool Database::group(int id, Group *group) const
{
    Q_D(const Database);
    return d->m_groupCb ? d->m_groupCb(id, group) : false;
}

QVector<Group> Database::groups(const QString &pattern) const
{
    Q_D(const Database);
    return d->m_groupsCb ? d->m_groupsCb(pattern) : QVector<Group>();
}

bool Database::addUsersToGroup(int groupId, const QVector<int> &userIds,
                               User::Role role)
{
    Q_D(Database);
    return d->m_addUsersToGroupCb ?
        d->m_addUsersToGroupCb(groupId, userIds, role) : false;
}

bool Database::removeUsersFromGroup(int groupId, const QVector<int> &userIds,
                                    User::Role role)
{
    Q_D(Database);
    return d->m_removeUsersFromGroupCb ?
        d->m_removeUsersFromGroupCb(groupId, userIds, role) : false;
}

bool Database::groupsForUser(int userId, Groups *groups, User::Role role) const
{
    Q_D(const Database);
    return d->m_groupsForUserCb ?
        d->m_groupsForUserCb(userId, groups, role) : false;
}

bool Database::usersForGroup(int groupId, Users *users, User::Role role) const
{
    Q_D(const Database);
    return d->m_usersForGroupCb ?
        d->m_usersForGroupCb(groupId, users, role) : false;
}
