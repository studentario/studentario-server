CREATE TABLE Admins (
  userId int NOT NULL,
  groupId int NOT NULL,
  PRIMARY KEY (userId,groupId)
);
CREATE TABLE Attendance (
  id int NOT NULL AUTO_INCREMENT,
  creatorId int DEFAULT NULL,
  modified datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  lessonId int NOT NULL,
  userId int DEFAULT '-1',
  isTeacher tinyint NOT NULL DEFAULT '0',
  groupId int DEFAULT '-1',
  attended tinyint NOT NULL DEFAULT '-1',
  invited tinyint NOT NULL DEFAULT '0',
  note varchar(160) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY userId (userId,groupId,lessonId),
  UNIQUE KEY lessonId (lessonId,userId,groupId)
);
CREATE TABLE GroupTags (
  groupId int NOT NULL,
  tagId int NOT NULL,
  PRIMARY KEY (groupId,tagId),
  UNIQUE KEY tagId (tagId,groupId),
  CONSTRAINT GroupTags_ibfk_1 FOREIGN KEY (groupId) REFERENCES `Groups` (id),
  CONSTRAINT GroupTags_ibfk_2 FOREIGN KEY (tagId) REFERENCES Tags (id)
);
CREATE TABLE `Groups` (
  id int NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` varchar(2048) DEFAULT NULL,
  created datetime NOT NULL,
  locationId int DEFAULT NULL,
  PRIMARY KEY (id),
  KEY GroupNameIdx (`name`),
  KEY locationId (locationId),
  CONSTRAINT Groups_ibfk_1 FOREIGN KEY (locationId) REFERENCES Locations (id)
);
INSERT INTO Groups VALUES (1,'Gruppo 1','Prime gruppo','2021-03-20 22:32:28',NULL),(2,'Gruppo 2','Secunde gruppo','2021-03-20 22:32:28',NULL);
CREATE TABLE LessonNoteTags (
  noteId int NOT NULL,
  tagId int NOT NULL,
  PRIMARY KEY (noteId,tagId),
  UNIQUE KEY tagId (tagId,noteId),
  CONSTRAINT LessonNoteTags_ibfk_1 FOREIGN KEY (noteId) REFERENCES LessonNotes (id),
  CONSTRAINT LessonNoteTags_ibfk_2 FOREIGN KEY (tagId) REFERENCES Tags (id)
);
CREATE TABLE LessonNotes (
  id int NOT NULL AUTO_INCREMENT,
  creatorId int DEFAULT NULL,
  modified datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  typeId int DEFAULT NULL,
  `text` text,
  PRIMARY KEY (id),
  KEY LessonNotesIdx (modified)
);
CREATE TABLE LessonNotesGlue (
  lessonId int NOT NULL,
  priority int NOT NULL DEFAULT '0',
  noteId int NOT NULL,
  PRIMARY KEY (lessonId,priority,noteId)
);
CREATE TABLE LessonTags (
  lessonId int NOT NULL,
  tagId int NOT NULL,
  PRIMARY KEY (lessonId,tagId),
  UNIQUE KEY tagId (tagId,lessonId),
  CONSTRAINT LessonTags_ibfk_1 FOREIGN KEY (lessonId) REFERENCES Lessons (id),
  CONSTRAINT LessonTags_ibfk_2 FOREIGN KEY (tagId) REFERENCES Tags (id)
);
CREATE TABLE Lessons (
  id int NOT NULL AUTO_INCREMENT,
  creatorId int DEFAULT NULL,
  created datetime NOT NULL,
  startTime datetime NOT NULL,
  endTime datetime NOT NULL,
  locationId int DEFAULT NULL,
  PRIMARY KEY (id),
  KEY locationId (locationId),
  KEY LessonTimeIdx (startTime,endTime),
  KEY LessonTime2Idx (endTime,startTime),
  CONSTRAINT Lessons_ibfk_1 FOREIGN KEY (locationId) REFERENCES Locations (id)
);
CREATE TABLE LocationTags (
  locationId int NOT NULL,
  tagId int NOT NULL,
  PRIMARY KEY (locationId,tagId),
  UNIQUE KEY tagId (tagId,locationId),
  CONSTRAINT LocationTags_ibfk_1 FOREIGN KEY (locationId) REFERENCES Locations (id),
  CONSTRAINT LocationTags_ibfk_2 FOREIGN KEY (tagId) REFERENCES Tags (id)
);
CREATE TABLE Locations (
  id int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` varchar(2048) DEFAULT NULL,
  creatorId int DEFAULT NULL,
  created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  color int DEFAULT NULL,
  icon varchar(80) DEFAULT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE Relations (
  id int NOT NULL AUTO_INCREMENT,
  subjectId int NOT NULL,
  relationType int NOT NULL,
  objectId int NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY subjectId (subjectId,relationType,objectId),
  KEY RelationsIdx (objectId,relationType)
);
CREATE TABLE StatusChanges (
  id int NOT NULL AUTO_INCREMENT,
  creatorId int NOT NULL DEFAULT '-1',
  modified datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  userId int NOT NULL,
  `status` tinyint NOT NULL,
  `time` datetime NOT NULL,
  reason text,
  PRIMARY KEY (id),
  KEY StatusChangesIdx (userId,`time`),
  KEY StatusChanges2Idx (`time`)
);
CREATE TABLE Students (
  groupId int NOT NULL,
  userId int NOT NULL,
  PRIMARY KEY (groupId,userId)
);
INSERT INTO Students VALUES (1,2);
CREATE TABLE Tags (
  id int NOT NULL AUTO_INCREMENT,
  parentId int NOT NULL DEFAULT '-1',
  `name` varchar(128) DEFAULT NULL,
  `description` text,
  color int DEFAULT NULL,
  icon varchar(128) DEFAULT NULL,
  created datetime NOT NULL,
  creatorId int DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY parentId (parentId,`name`)
);
CREATE TABLE Teachers (
  userId int NOT NULL,
  groupId int NOT NULL,
  PRIMARY KEY (userId,groupId)
);
CREATE TABLE UserTags (
  userId int NOT NULL,
  tagId int NOT NULL,
  PRIMARY KEY (userId,tagId),
  UNIQUE KEY tagId (tagId,userId),
  CONSTRAINT UserTags_ibfk_1 FOREIGN KEY (userId) REFERENCES Users (id),
  CONSTRAINT UserTags_ibfk_2 FOREIGN KEY (tagId) REFERENCES Tags (id)
);
CREATE TABLE Users (
  id int NOT NULL AUTO_INCREMENT,
  isStudent tinyint(1) NOT NULL DEFAULT '0',
  isParent tinyint(1) NOT NULL DEFAULT '0',
  isTeacher tinyint(1) NOT NULL DEFAULT '0',
  isAdmin tinyint(1) NOT NULL DEFAULT '0',
  isDirector tinyint(1) NOT NULL DEFAULT '0',
  isMaster tinyint(1) NOT NULL DEFAULT '0',
  login varchar(64) NOT NULL,
  `password` varbinary(128) DEFAULT NULL,
  salt varbinary(16) DEFAULT NULL,
  pinCode char(8) DEFAULT NULL,
  pinCodeCreation datetime DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  birthDate datetime DEFAULT NULL,
  keywords varchar(128) DEFAULT NULL,
  contactInfo varchar(2048) DEFAULT NULL,
  created datetime NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  statusChangeTime datetime DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY login (login),
  KEY UserNameIdx (`name`),
  KEY LoginIdx (login)
);
INSERT INTO Users VALUES (1,1,0,0,0,0,1,'master','','','1234','2021-03-21 22:04:44','Supremo',NULL,'','Informationes de contacto','2021-03-20 22:32:28',0,NULL),(2,1,0,0,1,0,0,'studente','','','0000','2021-03-20 22:32:28','Le studente e administrator',NULL,'','{}','2021-03-20 22:32:28',0,NULL);
CREATE TABLE meta (
  id int NOT NULL,
  `name` varchar(32) NOT NULL,
  `data` varchar(256) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY `name` (`name`)
);
INSERT INTO meta VALUES (1,'DB version','5');
