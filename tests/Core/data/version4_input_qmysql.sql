CREATE TABLE `Admins` (
  userId int(11) NOT NULL,
  groupId int(11) NOT NULL,
  PRIMARY KEY (userId,groupId)
);
CREATE TABLE `Attendance` (
  id int(11) NOT NULL AUTO_INCREMENT,
  creatorId int(11) DEFAULT NULL,
  modified datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  lessonId int(11) NOT NULL,
  userId int(11) DEFAULT '-1',
  isTeacher tinyint(4) NOT NULL DEFAULT '0',
  groupId int(11) DEFAULT '-1',
  attended tinyint(4) NOT NULL DEFAULT '-1',
  invited tinyint(4) NOT NULL DEFAULT '0',
  note varchar(160) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY userId (userId,groupId,lessonId),
  UNIQUE KEY lessonId (lessonId,userId,groupId)
);
CREATE TABLE `GroupTags` (
  groupId int(11) NOT NULL,
  tagId int(11) NOT NULL,
  PRIMARY KEY (groupId,tagId),
  UNIQUE KEY tagId (tagId,groupId)
);
CREATE TABLE `Groups` (
  id int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  description varchar(2048) DEFAULT NULL,
  created datetime NOT NULL,
  PRIMARY KEY (id),
  KEY GroupNameIdx (`name`)
);
INSERT INTO `Groups` VALUES (1,'Gruppo 1','Prime gruppo','2021-03-20 22:32:28'),(2,'Gruppo 2','Secunde gruppo','2021-03-20 22:32:28');
CREATE TABLE `LessonNoteTags` (
  noteId int(11) NOT NULL,
  tagId int(11) NOT NULL,
  PRIMARY KEY (noteId,tagId),
  UNIQUE KEY tagId (tagId,noteId)
);
CREATE TABLE `LessonNotes` (
  id int(11) NOT NULL AUTO_INCREMENT,
  creatorId int(11) DEFAULT NULL,
  modified datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  typeId int(11) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (id),
  KEY LessonNotesIdx (modified)
);
CREATE TABLE `LessonNotesGlue` (
  lessonId int(11) NOT NULL,
  priority int(11) NOT NULL DEFAULT '0',
  noteId int(11) NOT NULL,
  PRIMARY KEY (lessonId,priority,noteId)
);
CREATE TABLE `LessonTags` (
  lessonId int(11) NOT NULL,
  tagId int(11) NOT NULL,
  PRIMARY KEY (lessonId,tagId),
  UNIQUE KEY tagId (tagId,lessonId)
);
CREATE TABLE `Lessons` (
  id int(11) NOT NULL AUTO_INCREMENT,
  creatorId int(11) DEFAULT NULL,
  created datetime NOT NULL,
  startTime datetime NOT NULL,
  endTime datetime NOT NULL,
  locationId int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY LessonTimeIdx (startTime,endTime),
  KEY LessonTime2Idx (endTime,startTime)
);
CREATE TABLE `Relations` (
  id int(11) NOT NULL AUTO_INCREMENT,
  subjectId int(11) NOT NULL,
  relationType int(11) NOT NULL,
  objectId int(11) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY subjectId (subjectId,relationType,objectId),
  KEY RelationsIdx (objectId,relationType)
);
CREATE TABLE `StatusChanges` (
  id int(11) NOT NULL AUTO_INCREMENT,
  creatorId int(11) NOT NULL DEFAULT '-1',
  modified datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  userId int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `time` datetime NOT NULL,
  reason text,
  PRIMARY KEY (id),
  KEY StatusChangesIdx (userId,`time`),
  KEY StatusChanges2Idx (`time`)
);
CREATE TABLE `Students` (
  groupId int(11) NOT NULL,
  userId int(11) NOT NULL,
  PRIMARY KEY (groupId,userId)
);
INSERT INTO `Students` VALUES (1,2);
CREATE TABLE `Tags` (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  description text,
  created datetime NOT NULL,
  creatorId int(11) DEFAULT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE `Teachers` (
  userId int(11) NOT NULL,
  groupId int(11) NOT NULL,
  PRIMARY KEY (userId,groupId)
);
CREATE TABLE `UserTags` (
  userId int(11) NOT NULL,
  tagId int(11) NOT NULL,
  PRIMARY KEY (userId,tagId),
  UNIQUE KEY tagId (tagId,userId)
);
CREATE TABLE `Users` (
  id int(11) NOT NULL AUTO_INCREMENT,
  isStudent tinyint(1) NOT NULL DEFAULT '0',
  isParent tinyint(1) NOT NULL DEFAULT '0',
  isTeacher tinyint(1) NOT NULL DEFAULT '0',
  isAdmin tinyint(1) NOT NULL DEFAULT '0',
  isDirector tinyint(1) NOT NULL DEFAULT '0',
  isMaster tinyint(1) NOT NULL DEFAULT '0',
  login varchar(64) NOT NULL,
  `password` varbinary(128) DEFAULT NULL,
  salt varbinary(16) DEFAULT NULL,
  pinCode char(8) DEFAULT NULL,
  pinCodeCreation datetime DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  birthDate datetime DEFAULT NULL,
  keywords varchar(128) DEFAULT NULL,
  contactInfo varchar(2048) DEFAULT NULL,
  created datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  statusChangeTime datetime DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY login (login),
  KEY UserNameIdx (`name`),
  KEY LoginIdx (login)
);
INSERT INTO `Users` VALUES (1,1,0,0,0,0,1,'master','','','1234','2021-03-21 22:04:44','Supremo',NULL,'','Informationes de contacto','2021-03-20 22:32:28',0,NULL),(2,1,0,0,1,0,0,'studente','','','0000','2021-03-20 22:32:28','Le studente e administrator',NULL,'','{}','2021-03-20 22:32:28',0,NULL);
CREATE TABLE `meta` (
  id int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `data` varchar(256) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY `name` (`name`)
);
INSERT INTO `meta` VALUES (1,'DB version','4');
