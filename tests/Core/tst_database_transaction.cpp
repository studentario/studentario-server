#include "tst_database.h"

using namespace Core;

const static Group testGroup {
    0, "nomine del gruppo", "desc", QDateTime::fromSecsSinceEpoch(12345),
};

void DatabaseTest::testTransactionsCommit()
{
    Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    int groupId = 0;

    {
        Database::Transaction transaction(&db);

        groupId = db.addGroup(testGroup);
        QVERIFY(groupId >= 0);

        QVERIFY(transaction.commit());
    }

    Group group;
    QVERIFY(db.group(groupId, &group));
    QCOMPARE(group.name, testGroup.name);
}

void DatabaseTest::testTransactionsRollback()
{
    Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    int groupId = 0;

    {
        Database::Transaction transaction(&db);

        groupId = db.addGroup(testGroup);
        QVERIFY(groupId >= 0);
    }

    Group group;
    QVERIFY(!db.group(groupId, &group));
}

void DatabaseTest::testTransactionsWatchErrorCommit()
{
    Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    Error error;
    int groupId = 0;

    {
        Database::Transaction transaction(&db, &error);

        groupId = db.addGroup(testGroup);
        QVERIFY(groupId >= 0);
    }

    Group group;
    QVERIFY(db.group(groupId, &group));
    QCOMPARE(group.name, testGroup.name);
}

void DatabaseTest::testTransactionsWatchErrorRollback()
{
    Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    Error error;
    int groupId = 0;

    {
        Database::Transaction transaction(&db, &error);

        groupId = db.addGroup(testGroup);
        QVERIFY(groupId >= 0);
        error = Error(Error::InvalidParameters);
    }

    Group group;
    QVERIFY(!db.group(groupId, &group));
}
