#include "tst_database.h"

#include <QPair>

namespace Core {

inline char *toString(User::Status status)
{
    return QTest::toString(int(status));
}

} // namespace

void DatabaseTest::testAddUser_data()
{
    using User = Core::User;
    using Roles = Core::User::Roles;

    QTest::addColumn<User>("user");
    QTest::addColumn<bool>("mustSucceed");

    QTest::newRow("empty") <<
        User {} << false;

    QTest::newRow("valid") <<
        User {
            0, Roles(),
            "login", "password", "salt", "pinCode",
            QDateTime::fromSecsSinceEpoch(654321),
            "name", QDate(1970, 11, 30), "keywords", { "contact", "info" },
            QDateTime::fromSecsSinceEpoch(123456),
        } << true;
}

void DatabaseTest::testAddUser()
{
    QFETCH(Core::User, user);
    QFETCH(bool, mustSucceed);

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    int userId = db.addUser(user);
    bool succeeded = userId >= 0;
    QCOMPARE(succeeded, mustSucceed);

    if (mustSucceed) {
        Core::User readUser;
        QVERIFY(db.user(userId, &readUser));

        user.id = userId;
        QCOMPARE(readUser, user);

        // Obtene lo via users()
        auto users = db.users();
        QCOMPARE(users.count(), 1);
        readUser = users.first();
        QCOMPARE(readUser, user);
    }
}

void DatabaseTest::testUpdateUser_data()
{
    using User = Core::User;
    using Role = Core::User::Role;
    using Roles = Core::User::Roles;

    QTest::addColumn<User>("user");
    QTest::addColumn<User::Fields>("fields");
    QTest::addColumn<bool>("mustSucceed");
    QTest::addColumn<User>("expectedUser");

    const User baseUser = {
        1, Roles(Role::Teacher),
        "secunde", "pwd1", "sal1", "",
        QDateTime(),
        "Nomine1", QDate(1980, 5, 28), "kw1",
        {},
        QDateTime(),
    };

    QTest::newRow("no changes") <<
        baseUser << User::Fields() << true << baseUser;

    User testUser = baseUser;
    User expectedUser = baseUser;
    testUser.roles = expectedUser.roles = Role::Master | Role::Admin;
    QTest::newRow("change roles") <<
        testUser << User::Fields(User::Field::Roles) <<
        true << expectedUser;

    testUser = baseUser;
    expectedUser = baseUser;
    testUser.login = expectedUser.login = "novelogin";
    QTest::newRow("change login") <<
        testUser << User::Fields(User::Field::Login) <<
        true << expectedUser;

    testUser = baseUser;
    expectedUser = baseUser;
    testUser.password = expectedUser.password = "pwd123";
    testUser.salt = expectedUser.salt = "sal123";
    QTest::newRow("change password") <<
        testUser << User::Fields(User::Field::Password) <<
        true << expectedUser;

    testUser = baseUser;
    expectedUser = baseUser;
    testUser.pinCode = expectedUser.pinCode = "1122";
    testUser.pinCodeCreation = expectedUser.pinCodeCreation =
        QDateTime::fromSecsSinceEpoch(67412);
    QTest::newRow("change PIN") <<
        testUser << User::Fields(User::Field::PinCode) <<
        true << expectedUser;

    testUser = baseUser;
    expectedUser = baseUser;
    testUser.name = expectedUser.name = "Nove nomine";
    QTest::newRow("change name") <<
        testUser << User::Fields(User::Field::Name) <<
        true << expectedUser;

    testUser = baseUser;
    expectedUser = baseUser;
    testUser.birthDate = expectedUser.birthDate = QDate(2001, 4, 22);
    QTest::newRow("change birthDate") <<
        testUser << User::Fields(User::Field::BirthDate) <<
        true << expectedUser;

    testUser = baseUser;
    expectedUser = baseUser;
    testUser.keywords = expectedUser.keywords = "Nove parolas clave";
    QTest::newRow("change keywords") <<
        testUser << User::Fields(User::Field::Keywords) <<
        true << expectedUser;

    testUser = baseUser;
    expectedUser = baseUser;
    testUser.contactInfo = expectedUser.contactInfo =
        QStringList { "mail:me@example.com", "tel:+213234" };
    QTest::newRow("change contactInfo") <<
        testUser << User::Fields(User::Field::ContactInfo) <<
        true << expectedUser;

    testUser = baseUser;
    expectedUser = baseUser;
    testUser.roles = Role::Student | Role::Parent;
    testUser.password = "pwd123";
    testUser.salt = "sal123";
    testUser.pinCode = expectedUser.pinCode = "8765";
    testUser.pinCodeCreation = expectedUser.pinCodeCreation =
        QDateTime::fromSecsSinceEpoch(7983475);
    testUser.keywords = "Non debe cambiar";
    testUser.contactInfo = QStringList { "uno", "duo" };
    QTest::newRow("extra fields 1") <<
        testUser << User::Fields(User::Field::PinCode) <<
        true << expectedUser;
}

void DatabaseTest::testUpdateUser()
{
    using Role = Core::User::Role;
    using User = Core::User;

    QFETCH(User, user);
    QFETCH(User::Fields, fields);
    QFETCH(bool, mustSucceed);
    QFETCH(User, expectedUser);

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    // prepara le base de datos

    QDateTime data = QDateTime::fromSecsSinceEpoch(12345);
    const QVector<User> users {
        {
            0, Role::Student, "prime", "pwd0", "sal0", "",
            QDateTime(), "Nomine0", QDate(2000, 6, 14), "kw0", {}, data,
        },
        {
            0, Role::Teacher, "secunde", "pwd1", "sal1", "",
            QDateTime(), "Nomine1", QDate(1980, 5, 28), "kw1", {}, data,
        },
    };
    int firstUserId = -1;
    for (const User &u: users) {
        int userId = db.addUser(u);
        QVERIFY(userId >= 0);
        if (firstUserId < 0) firstUserId = userId;
    }

    // initia le test
    user.id += firstUserId;
    bool succeeded = db.updateUser(user, fields);
    QCOMPARE(succeeded, mustSucceed);

    if (mustSucceed) {
        User readUser;
        QVERIFY(db.user(user.id, &readUser));

        expectedUser.id += firstUserId;
        readUser.created = QDateTime(); // non interessante
        QCOMPARE(readUser, expectedUser);
    }
}

void DatabaseTest::testDeleteUser()
{
    using Role = Core::User::Role;
    using User = Core::User;

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    // prepara le base de datos

    QDateTime data = QDateTime::fromSecsSinceEpoch(12345);
    const QVector<User> users {
        {
            0, Role::Student, "prime", "pwd0", "sal0", "",
            QDateTime(), "Nomine0", QDate(2000, 12, 31), "kw0", {}, data,
        },
        {
            0, Role::Teacher, "secunde", "pwd1", "sal1", "",
            QDateTime(), "Nomine1", QDate(1960, 6, 30), "kw1", {}, data,
        },
    };
    int firstUserId = -1;
    for (const User &u: users) {
        int userId = db.addUser(u);
        QVERIFY(userId >= 0);
        if (firstUserId < 0) firstUserId = userId;
    }

    // initia le test
    Core::Error error = db.deleteUser(firstUserId);
    QVERIFY(!error);

    User readUser;
    QVERIFY(!db.user(firstUserId, &readUser));
    QVERIFY(db.user(firstUserId + 1, &readUser));

    QCOMPARE(readUser.login, QStringLiteral("secunde"));

    error = db.deleteUser(firstUserId + 10);
    QCOMPARE(error.code(), Core::Error::UserNotFound);
}

void DatabaseTest::testUsers_data()
{
    using Role = Core::User::Role;
    using Filters = Core::User::Filters;
    using Records = QSet<int>;

    QTest::addColumn<Filters>("filters");
    QTest::addColumn<QSet<int>>("expectedUserIds");

    QTest::newRow("no filters") <<
        Filters() << Records { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    Filters filters;
    filters.requestedRoles = Role::NoRole;
    filters.pattern = "nt";
    QTest::newRow("pattern") <<
        filters << Records { 2, 4, 8, 9 };

    QTest::newRow("students") <<
        Filters { Role::Student } << Records { 2, 8 };

    QTest::newRow("parents") <<
        Filters { Role::Parent } << Records { 3, 8 };

    QTest::newRow("teachers") <<
        Filters { Role::Teacher } << Records { 4, 9 };

    QTest::newRow("admins") <<
        Filters { Role::Admin } << Records { 5 };

    QTest::newRow("directors") <<
        Filters { Role::Director } << Records { 6, 9 };

    QTest::newRow("masters") <<
        Filters { Role::Master } << Records { 7 };
}

void DatabaseTest::testUsers()
{
    QFETCH(Core::User::Filters, filters);
    QFETCH(QSet<int>, expectedUserIds);

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    /* Adde usatores con rolos differente */

    using Roles = Core::User::Roles;
    using Role = Core::User::Role;
    const QVector<QPair<Roles,QString>> rolesAndNames = {
        { Roles(), "Sin rolos" },
        { Role::Student, "Studente" },
        { Role::Parent, "Genitor" },
        { Role::Teacher, "Inseniante" },
        { Role::Admin, "Administrator" },
        { Role::Director, "Director galactic" },
        { Role::Master, "Capo supreme" },
        { Role::Student | Role::Parent, "Studente e accompaniator" },
        { Role::Teacher | Role::Director, "Inseniante e director" },
    };

    using User = Core::User;
    int i = 1;
    for (const auto &roleAndName: rolesAndNames) {
        User user {
            0, roleAndName.first,
            QString("login_%1").arg(i), "password", "salt", "pinCode",
            QDateTime::fromSecsSinceEpoch(46723),
            roleAndName.second, QDate(1990, 12, 31), "keywords",
            { "contact", "info" },
            QDateTime::fromSecsSinceEpoch(123456),
        };
        int userId = db.addUser(user);
        QVERIFY(userId >= 0);
        i++;
    }

    /* Lege del base de datos */

    const auto users = db.users(filters);
    QSet<int> userIds;
    for (const User &user: users) {
        userIds.insert(user.id);
    }

    QCOMPARE(userIds, expectedUserIds);
}

void DatabaseTest::testLoginUniqueness()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    Core::User user0 {
        0, Core::User::Role::Student,
        "mylogin", "password0", "salt0", "pinCode0",
        QDateTime::fromSecsSinceEpoch(234561),
        "name0", QDate(2000, 12, 31), "keywords0", { "contact0", "info0" },
        QDateTime::fromSecsSinceEpoch(123456),
    };
    QVERIFY(db.addUser(user0) > 0);

    /* Tote campos es differente, excepto le login */

    Core::User user1 {
        0, Core::User::Role::Teacher,
        "mylogin", "password1", "salt1", "pinCode1",
        QDateTime::fromSecsSinceEpoch(1234056),
        "name1", QDate(2001, 11, 30), "keywords1", { "contact1", "info1" },
        QDateTime::fromSecsSinceEpoch(1023456),
    };
    QVERIFY(db.addUser(user1) <= 0);
}

void DatabaseTest::testUserByLogin_data()
{
    QTest::addColumn<QString>("login");
    QTest::addColumn<QString>("expectedName");

    QTest::newRow("empty") << QString() << QString();
    QTest::newRow("not found") << QString("estranee") << QString();
    QTest::newRow("first") << QString("prime") << QString("Prime");
    QTest::newRow("second") << QString("secunde") << QString("Secunde");
    QTest::newRow("third") << QString("tertie") << QString("Tertie");
}

void DatabaseTest::testUserByLogin()
{
    QFETCH(QString, login);
    QFETCH(QString, expectedName);

    using Role = Core::User::Role;
    using User = Core::User;

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    // prepara le base de datos

    QDateTime data = QDateTime::fromSecsSinceEpoch(12345);
    const QVector<User> users {
        {
            0, Role::Student, "prime", "pwd0", "salt0", "pin",
            QDateTime(), "Prime", QDate(), "kw", {}, data,
        },
        {
            0, Role::Student, "secunde", "pwd0", "salt0", "pin",
            QDateTime(), "Secunde", QDate(), "kw", {}, data,
        },
        {
            0, Role::Student, "tertie", "pwd0", "salt0", "pin",
            QDateTime(), "Tertie", QDate(), "kw", {}, data,
        },
    };
    for (const User &u: users) {
        QVERIFY(db.addUser(u) >= 0);
    }

    // initia le test

    Core::User readUser;
    bool ok = db.user(login, &readUser);
    QCOMPARE(ok, !expectedName.isEmpty());

    QCOMPARE(readUser.name, expectedName);
}

void DatabaseTest::testMaxUserId()
{
    using User = Core::User;

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    QDateTime date = QDateTime::fromSecsSinceEpoch(12345);
    const User user {
        0, User::Role::Student, "prime", "pwd0", "salt0", "pin",
        QDateTime(), "Prime", QDate(), "kw", {}, date,
    };

    int initialMaxUserId = db.maxUserId();
    QVERIFY(db.addUser(user) >= 0);
    int finalMaxUserId = db.maxUserId();

    QCOMPARE(finalMaxUserId, initialMaxUserId + 1);
}

void DatabaseTest::testUserStatusChanges()
{
    using User = Core::User;
    using Error = Core::Error;
    using StatusChange = Core::StatusChange;

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const QDateTime testStartTime = QDateTime::currentDateTime();

    QDateTime date = QDateTime::fromSecsSinceEpoch(12345);
    const User user {
        0, User::Role::Student, "prime", "pwd0", "salt0", "pin",
        QDateTime(), "Prime", QDate(), "kw", {}, date,
    };
    const int userId = db.addUser(user);
    QVERIFY(userId >= 0);

    const int creatorId1 = 23;
    const int creatorId2 = 24;
    const QDateTime time1 = QDateTime::fromSecsSinceEpoch(100000);
    const QDateTime time2 = QDateTime::fromSecsSinceEpoch(200000);
    const QDateTime time3 = QDateTime::fromSecsSinceEpoch(300000);
    int statusId = -1;

    {
        // Status initial
        User u;
        QVERIFY(db.user(userId, &u));
        QCOMPARE(u.status, User::Status::Unknown);
        QVERIFY(u.statusChangeTime.isNull());
    }

    {
        // Adde un status
        StatusChange s {
            .creatorId = creatorId1,
            .userId = userId,
            .status = User::Status::Active,
            .time = time3,
            .reason = "Ration del activation",
        };
        Error e = db.addUserStatusChange(s, nullptr);
        QVERIFY(!e);
    }

    {
        // Verifica le status in le usator
        User u;
        QVERIFY(db.user(userId, &u));
        QCOMPARE(u.status, User::Status::Active);
        QCOMPARE(u.statusChangeTime, time3);
    }

    {
        // Adde un copula de statuses plus ancian
        int changeId1 = 0, changeId2 = 0;
        StatusChange s {
            .creatorId = creatorId2,
            .userId = userId,
            .status = User::Status::Active,
            .time = time1,
            .reason = "Prime activation",
        };
        Error e = db.addUserStatusChange(s, &changeId1);
        QVERIFY(!e);
        QVERIFY(changeId1 > 0);
        s.creatorId = -1;
        s.time = time2;
        s.status = User::Status::Inactive;
        s.reason = "Prime deactivation";
        e = db.addUserStatusChange(s, &changeId2);
        QVERIFY(!e);
        QVERIFY(changeId2 > changeId1);
        statusId = changeId1;
    }

    {
        // Le status in le usator non debe esser cambiate
        User u;
        QVERIFY(db.user(userId, &u));
        QCOMPARE(u.status, User::Status::Active);
        QCOMPARE(u.statusChangeTime, time3);
    }

    {
        // Crea un altere usator e adde su status
        const User user {
            0, User::Role::Student, "secunde", "pwd1", "salt1", "pin",
            QDateTime(), "Secunde", QDate(), "kw", {}, time1,
        };
        int userId2 = db.addUser(user);
        QVERIFY(userId2 >= 0);
        StatusChange s {
            .creatorId = creatorId2,
            .userId = userId2,
            .status = User::Status::Active,
            .time = time1.addDays(1),
            .reason = "Activation del secunde",
        };
        Error e = db.addUserStatusChange(s, nullptr);
        QVERIFY(!e);
    }

    {
        // Lista tote le cambios de status del prime usator
        auto changes = db.userStatusChanges(userId);
        QCOMPARE(changes.count(), 3);
        QCOMPARE(changes[0].reason, "Prime activation");
        QCOMPARE(changes[1].reason, "Prime deactivation");
        QCOMPARE(changes[2].reason, "Ration del activation");
        // Controla un cambio; nos assume que le alteres es correcte
        StatusChange expectedChange {
            0, creatorId2, QDateTime(), userId,
            User::Status::Active, time1, "Prime activation",
        };
        // nos non pote comparar le data de creation:
        QVERIFY(changes[0].modified.addSecs(1) >= testStartTime);
        QVERIFY(testStartTime.secsTo(changes[0].modified) < 20);
        changes[0].modified = QDateTime();
        QVERIFY(changes[0].id >= 1);
        changes[0].id = 0;
        QCOMPARE(changes[0], expectedChange);
    }

    {
        // Error si on scribe le mesme status
        StatusChange s {
            .creatorId = creatorId1,
            .userId = userId,
            .status = User::Status::Active,
            .time = time3.addSecs(90),
            .reason = "Duplicate",
        };
        Error e = db.addUserStatusChange(s, nullptr);
        QVERIFY(e);
        QCOMPARE(e.code(), Error::UserStatusAlreadySet);
        QCOMPARE(e.message(), "1");
    }

    {
        // List tote le cambios de status, de tote usatores
        auto changes = db.userStatusChanges(-1, time1, time2);
        QCOMPARE(changes.count(), 3);
        QCOMPARE(changes[0].reason, "Prime activation");
        QCOMPARE(changes[1].reason, "Activation del secunde");
        QCOMPARE(changes[2].reason, "Prime deactivation");
    }

    {
        Error e = db.deleteUserStatusChange(statusId);
        QVERIFY(!e);
        auto changes = db.userStatusChanges(-1, time1, time2);
        QCOMPARE(changes.count(), 2);
        QCOMPARE(changes[0].reason, "Activation del secunde");
        QCOMPARE(changes[1].reason, "Prime deactivation");
    }
}
