#include "tst_database.h"

void DatabaseTest::testRelations()
{
    using Type = Core::Relation::Type;

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const auto studentIds = prepareStudents(&db, {"s0", "s1", "s2", "s3", "s4"});
    const auto parentIds = prepareParents(&db, {"p0", "p1", "p2"});

    // Testa le creation

    Core::Relation relation1 {
        0, parentIds[0], Type::IsParent, studentIds[0]
    };
    int relationId1 = db.addRelation(relation1);
    QVERIFY(relationId1 >= 0);

    int relationId2 = db.addRelation({
        0, parentIds[0], Type::IsParent, studentIds[3] });
    QVERIFY(relationId2 >= 0);

    int relationId3 = db.addRelation({
        0, parentIds[1], Type::IsParent, studentIds[1] });
    QVERIFY(relationId3 >= 0);

    int relationId4 = db.addRelation({
        0, parentIds[2], Type::IsParent, studentIds[3] });
    QVERIFY(relationId4 >= 0);

    // Relationes duplicate non pote esser inserite
    int relationId1bis = db.addRelation({
        0, parentIds[0], Type::IsParent, studentIds[0] });
    QVERIFY(relationId1bis < 0);

    // Testa le lectura

    {
        auto relations = db.relationsBySubject(parentIds[0]);
        QCOMPARE(relations.count(), 2);
        QCOMPARE(relations[0].id, relationId1);
        QCOMPARE(relations[1].id, relationId2);
        // nihilifica le ID, que in relation1 es ancora 0
        relations[0].id = 0;
        QCOMPARE(relations[0], relation1);
    }

    {
        auto relations = db.relationsByObject(studentIds[1]);
        QCOMPARE(relations.count(), 1);
        QCOMPARE(relations[0].id, relationId3);
        QCOMPARE(relations[0].subjectId, parentIds[1]);
        QCOMPARE(relations[0].type, Type::IsParent);
        QCOMPARE(relations[0].objectId, studentIds[1]);
    }

    // Testa le existentia
    QVERIFY(db.relationExists(parentIds[0], Type::IsParent, studentIds[0]));
    QVERIFY(db.relationExists(parentIds[1], Type::IsParent, studentIds[1]));
    QVERIFY(db.relationExists(parentIds[0], Type::IsParent, studentIds[3]));
    QVERIFY(!db.relationExists(parentIds[0], Type::IsParent, studentIds[1]));

    // Testa le deletion

    {
        Core::Error error = db.deleteRelation(relationId2);
        QVERIFY(!error);
        const auto relations = db.relationsBySubject(parentIds[0]);
        QCOMPARE(relations.count(), 1);
        QVERIFY(!db.relationExists(parentIds[0], Type::IsParent,
                                   studentIds[3]));
    }

    {
        Core::Error error = db.deleteRelation(42134);
        QVERIFY(bool(error));
        QCOMPARE(error.code(), Core::Error::RelationNotFound);
    }

    // Verifica le trigger del deletion del usatores

    {
        Core::Error error = db.deleteUser(studentIds[1]);
        auto relations = db.relationsByObject(studentIds[1]);
        QCOMPARE(relations.count(), 0);

        relations = db.relationsBySubject(parentIds[0]);
        QCOMPARE(relations.count(), 1);
    }

    {
        Core::Error error = db.deleteUser(parentIds[2]);
        auto relations = db.relationsBySubject(parentIds[2]);
        QCOMPARE(relations.count(), 0);

        relations = db.relationsBySubject(parentIds[0]);
        QCOMPARE(relations.count(), 1);
    }
}
