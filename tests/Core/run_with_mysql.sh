#! /bin/sh

set -e

if [ -n "${SKIP_MYSQL_TEST}" ]; then
    echo "Skipping MySQL test on request."
    exit 0
fi

if ! command -v mysqld > /dev/null ; then
    echo "mysqld not found, skipping test."
    exit 0
fi

DATADIR=$(mktemp -d)
echo "Using $DATADIR as data dir"

VARDIR=$(mktemp -d)

MYSQL_SOCKET=/tmp/mysql.sock
mysqld -h "$DATADIR" --initialize-insecure
mysqld --no-defaults \
    --disable-log-bin \
    --skip-character-set-client-handshake \
    --skip-ssl \
    --secure-file-priv="$VARDIR" \
    --skip-grant-tables -h "$DATADIR" --socket="$MYSQL_SOCKET" \
    --general-log-file=/dev/stderr --log-error=/tmp/mysqlerr &
MYSQL_PID=$!

echo "mysqld started as PID $MYSQL_PID"

while [ ! -S $MYSQL_SOCKET ]
do
  sleep 0.2
  if ! ps -p "$MYSQL_PID" > /dev/null; then
      echo "mysqld failed to initialize"
      exit 1
  fi
done
echo "mysql is ready, starting test"

echo "CREATE DATABASE StudentarioTests;SET GLOBAL SQL_MODE=ANSI_QUOTES;" | mysql -S $MYSQL_SOCKET -u root

export DBTEST_DatabaseDriver="QMYSQL"
export DBTEST_DatabaseHost="localhost"
export DBTEST_DatabaseUser="root"
export DBTEST_DatabasePort="3306"
export DBTEST_DatabaseName="StudentarioTests"
export DBTEST_DatabaseOptions="UNIX_SOCKET=$MYSQL_SOCKET"

cleanUp() {
    echo "Cleaning up."
    mysqladmin -uroot --socket="$MYSQL_SOCKET" shutdown
    rm -r "$DATADIR"
    rm -rf "$VARDIR"
}

trap cleanUp EXIT INT TERM

$WRAPPER "$@"

trap - EXIT
cleanUp
