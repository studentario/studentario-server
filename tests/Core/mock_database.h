#ifndef STUDENTARIO_CORE_MOCK_DATABASE_H
#define STUDENTARIO_CORE_MOCK_DATABASE_H

#include "database.h"

#include <functional>

namespace Core {

class DatabasePrivate {
public:
    static DatabasePrivate *mockFor(Database *obj) {
        return obj->d_ptr.get();
    }

    using Roles = User::Roles;

    /* Usatores */

    typedef std::function<int(const User &)> OnAddUserCalled;
    void onAddUserCalled(OnAddUserCalled callback) { m_addUserCb = callback; }

    typedef std::function<bool(const User &, User::Fields)> OnUpdateUserCalled;
    void onUpdateUserCalled(OnUpdateUserCalled callback) { m_updateUserCb = callback; }

    typedef std::function<Error(int)> OnDeleteUserCalled;
    void onDeleteUserCalled(OnDeleteUserCalled callback) { m_deleteUserCb = callback; }

    typedef std::function<bool(int, User *)> OnUserCalled;
    void onUserCalled(OnUserCalled callback) { m_userCb = callback; }

    typedef std::function<QVector<User>(const User::Filters &filters,
                                        User::Sort)> OnUsersCalled;
    void onUsersCalled(OnUsersCalled callback) { m_usersCb = callback; }

    typedef std::function<int()> OnMaxUserIdCalled;
    void onMaxUserIdCalled(OnMaxUserIdCalled callback) { m_maxUserIdCb = callback; }

    /* Gruppos */

    typedef std::function<int(const Group &)> OnAddGroupCalled;
    void onAddGroupCalled(OnAddGroupCalled callback) { m_addGroupCb = callback; }

    typedef std::function<Error(const Group &, Group::Fields)>
        OnUpdateGroupCalled;
    void onUpdateGroupCalled(OnUpdateGroupCalled callback) { m_updateGroupCb = callback; }

    typedef std::function<Error(int)> OnDeleteGroupCalled;
    void onDeleteGroupCalled(OnDeleteGroupCalled callback) { m_deleteGroupCb = callback; }

    typedef std::function<bool(int, Group *)> OnGroupCalled;
    void onGroupCalled(OnGroupCalled callback) { m_groupCb = callback; }

    typedef std::function<QVector<Group>(const QString &)> OnGroupsCalled;
    void onGroupsCalled(OnGroupsCalled callback) { m_groupsCb = callback; }

    /* Gruppos <-> Usatores */

    typedef std::function<bool(int, const QVector<int>&, User::Role)>
        OnAddUsersToGroupCalled;
    void onAddUsersToGroupCalled(OnAddUsersToGroupCalled callback) {
        m_addUsersToGroupCb = callback;
    }

    typedef std::function<bool(int, const QVector<int>&, User::Role)>
        OnRemoveUsersFromGroupCalled;
    void onRemoveUsersFromGroupCalled(OnRemoveUsersFromGroupCalled callback) {
        m_removeUsersFromGroupCb = callback;
    }

    typedef std::function<bool(int, QVector<Group>*, User::Role)>
        OnGroupsForUserCalled;
    void onGroupsForUserCalled(OnGroupsForUserCalled callback) {
        m_groupsForUserCb = callback;
    }

    typedef std::function<bool(int, QVector<User>*, User::Role)>
        OnUsersForGroupCalled;
    void onUsersForGroupCalled(OnUsersForGroupCalled callback) {
        m_usersForGroupCb = callback;
    }

protected:
    friend class Database;
    DatabasePrivate(const QVariantMap &configuration,
                    const QString &connectionName);

protected:
    QVariantMap m_configuration;
    QString m_connectionName;

    OnAddUserCalled m_addUserCb;
    OnUpdateUserCalled m_updateUserCb;
    OnDeleteUserCalled m_deleteUserCb;
    OnUserCalled m_userCb;
    OnUsersCalled m_usersCb;
    OnMaxUserIdCalled m_maxUserIdCb;

    OnAddGroupCalled m_addGroupCb;
    OnUpdateGroupCalled m_updateGroupCb;
    OnDeleteGroupCalled m_deleteGroupCb;
    OnGroupCalled m_groupCb;
    OnGroupsCalled m_groupsCb;

    OnAddUsersToGroupCalled m_addUsersToGroupCb;
    OnRemoveUsersFromGroupCalled m_removeUsersFromGroupCb;
    OnGroupsForUserCalled m_groupsForUserCb;
    OnUsersForGroupCalled m_usersForGroupCb;
};

using DatabaseMock = DatabasePrivate;

} // namespace

#endif // STUDENTARIO_CORE_MOCK_DATABASE_H
