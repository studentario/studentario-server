#include "tst_database.h"

void DatabaseTest::testTagCreationAndDeletion()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    // Testa le creation

    const Core::Tag tag1 = {
        0, -1, "First tag", "A description", -1,
        QDateTime::fromSecsSinceEpoch(12345),
    };
    int tagId1 = -1;
    Core::Error error = db.addTag(tag1, &tagId1);
    QVERIFY(!error);
    QVERIFY(tagId1 >= 0);

    const Core::Tag tag2 = {
        0, tagId1, "Second tag", "Another description", 3,
        QDateTime::fromSecsSinceEpoch(54321),
    };
    int tagId2 = -1;
    error = db.addTag(tag2, &tagId2);
    QVERIFY(!error);
    QVERIFY(tagId2 >= 0);

    // Testa le lectura

    {
        Core::Tag tag;
        QVERIFY(db.tag(tagId1, &tag));
        QCOMPARE(tag.id, tagId1);
        // nihilifica le ID, que in tag1 es ancora 0
        tag.id = 0;
        QCOMPARE(tag, tag1);
    }

    {
        Core::Tag tag;
        QVERIFY(db.tag(tagId2, &tag));
        QCOMPARE(tag.id, tagId2);
        // nihilifica le ID, que in tag2 es ancora 0
        tag.id = 0;
        QCOMPARE(tag, tag2);
    }

    // Testa le modification

    {
        // nulle campo: expecta un error
        QTest::ignoreMessage(QtWarningMsg, "Nothing to update");
        Core::Tag tag;
        QVERIFY(db.tag(tagId1, &tag));
        Core::Tag::Fields fields;
        Core::Error error = db.updateTag(tag, fields);
        QVERIFY(!error);
    }

    {
        // Cambia le campos singularmente
        Core::Tag tag, tagReloaded;
        QVERIFY(db.tag(tagId1, &tag));

        Core::Tag::Fields fields = Core::Tag::Field::ParentId;
        tag.parentId = 15;
        Core::Error error = db.updateTag(tag, fields);
        QCOMPARE(error.code(), Core::Error::InvalidParameters);
        tag.parentId = 2;
        error = db.updateTag(tag, fields);
        QVERIFY(!error);

        QVERIFY(db.tag(tagId1, &tagReloaded));
        QCOMPARE(tagReloaded, tag);

        fields = Core::Tag::Field::Name;
        tag.name = "Prime etiquetta";
        error = db.updateTag(tag, fields);
        QVERIFY(!error);
        QVERIFY(db.tag(tagId1, &tagReloaded));
        QCOMPARE(tagReloaded, tag);

        fields = Core::Tag::Field::Description;
        tag.description = "Un texto plus longe\nMa non troppo";
        error = db.updateTag(tag, fields);
        QVERIFY(!error);
        QVERIFY(db.tag(tagId1, &tagReloaded));
        QCOMPARE(tagReloaded, tag);

        fields = Core::Tag::Field::CreatorId;
        tag.creatorId = 42;
        error = db.updateTag(tag, fields);
        QVERIFY(!error);
        QVERIFY(db.tag(tagId1, &tagReloaded));
        QCOMPARE(tagReloaded, tag);

        fields = Core::Tag::Field::Color;
        tag.color = "#abc123";
        error = db.updateTag(tag, fields);
        QVERIFY(!error);
        QVERIFY(db.tag(tagId1, &tagReloaded));
        QCOMPARE(tagReloaded, tag);

        fields = Core::Tag::Field::Icon;
        tag.icon = "file:///icon.png";
        error = db.updateTag(tag, fields);
        QVERIFY(!error);
        QVERIFY(db.tag(tagId1, &tagReloaded));
        QCOMPARE(tagReloaded, tag);
    }

    {
        // Cambia le campos insimul
        Core::Tag tag, tagReloaded;
        QVERIFY(db.tag(tagId1, &tag));

        Core::Tag::Fields fields =
            Core::Tag::Field::ParentId | Core::Tag::Field::Name |
            Core::Tag::Field::Description | Core::Tag::Field::Color;
        tag.parentId = -1;
        tag.name = "Prime";
        tag.description = "";
        tag.color = "";
        Core::Error error = db.updateTag(tag, fields);
        QVERIFY(!error);
        QVERIFY(db.tag(tagId1, &tagReloaded));
        QCOMPARE(tagReloaded, tag);
    }

    // Testa le deletion

    {
        Core::Tag tag;
        Core::Error error = db.deleteTag(tagId1);
        QCOMPARE(error.code(), Core::Error::InvalidParameters);

        // remove tag2, que non ha filios
        error = db.deleteTag(tagId2);
        QVERIFY(!error);
        QVERIFY(!db.tag(tagId2, &tag));
        QVERIFY(db.tag(tagId1, &tag));
    }

    {
        Core::Error error = db.deleteTag(42134);
        QVERIFY(bool(error));
        QCOMPARE(error.code(), Core::Error::TagNotFound);
    }
}

void DatabaseTest::testTags_data()
{
    QTest::addColumn<int>("parentId");
    QTest::addColumn<QString>("pattern");
    QTest::addColumn<QStringList>("expectedTagNames");

    QTest::newRow("all tags") <<
        0 << "" <<
        QStringList {
            "afirst","etiquetta1", "etiquetta2",
            "filio1", "filio2", "last", "nepote",
        };

    QTest::newRow("short pattern, any parent") <<
        0 << "i" <<
        QStringList {
            "afirst", "etiquetta1", "etiquetta2", "filio1", "filio2"
        };

    QTest::newRow("short pattern, with parent") <<
        -1 << "1" << QStringList { "etiquetta1" };

    QTest::newRow("short pattern, with other parent") <<
        2 << "1" << QStringList { "filio1" };

    QTest::newRow("long pattern") <<
        -1 << "etiquetta" << QStringList { "etiquetta1", "etiquetta2" };

    QTest::newRow("not found") <<
        -1 << "awa" << QStringList {};

    QTest::newRow("with parent") <<
        2 << "" << QStringList { "filio1", "filio2" };

    QTest::newRow("all toplevel") <<
        -1 << "" <<
        QStringList { "afirst","etiquetta1", "etiquetta2", "last" };

    QTest::newRow("injection1") <<
        -1 << "'; DROP TABLE Tags;" << QStringList {};
}

void DatabaseTest::testTags()
{
    QFETCH(int, parentId);
    QFETCH(QString, pattern);
    QFETCH(QStringList, expectedTagNames);

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    /* Adde le etiquettas */

    const QVector<QPair<QString,int>> allTagNamesAndParent = {
        { "afirst", -1 },
        { "etiquetta1", -1 },
        { "etiquetta2", -1 },
        { "last", -1 },
        { "filio1", 2 },
        { "filio2", 2 },
        { "nepote", 5 },
    };

    using Tag = Core::Tag;
    for (const auto &tagData: allTagNamesAndParent) {
        Tag tag {
            0, tagData.second, tagData.first, "A description", -1,
            QDateTime::fromSecsSinceEpoch(123456),
        };
        Core::Error error = db.addTag(tag, nullptr);
        QVERIFY(!error);
    }

    /* Lege del base de datos */

    const auto tags = db.tags(parentId, pattern);
    QStringList tagNames;
    for (const Tag &tag: tags) {
        tagNames.append(tag.name);
    }

    QCOMPARE(tagNames, expectedTagNames);
    QVERIFY(m_capturedWarnings.isEmpty());
}

void DatabaseTest::testTagsForUsers()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const auto studentIds = prepareStudents(&db, {"s0", "s1", "s2", "s3", "s4"});
    const auto tagIds = prepareTags(&db, {"t0", "t1", "t2", "t3", "t4"});

    // Testa le addition

    QVERIFY(db.addTagToUsers(tagIds[1], { studentIds[0], studentIds[4] }));
    QVERIFY(db.addTagToUsers(tagIds[2], {
        studentIds[1], studentIds[2], studentIds[4],
    }));

    // Testa le lectura: etiquettas per usator

    using Tags = Core::Database::Tags;

    {
        Tags tags;
        QVERIFY(db.tagsForUser(studentIds[3], &tags));
        QVERIFY(tags.isEmpty());
    }

    {
        Tags tags;
        QVERIFY(db.tagsForUser(studentIds[0], &tags));
        QCOMPARE(tags.count(), 1);
        QCOMPARE(tags[0].id, tagIds[1]);
    }

    {
        Tags tags;
        QVERIFY(db.tagsForUser(studentIds[4], &tags));
        QCOMPARE(tags.count(), 2);
        QCOMPARE(tags[0].id, tagIds[1]);
        QCOMPARE(tags[1].id, tagIds[2]);
    }

    // Testa le lectura: studentes con etiquetta

    using Users = Core::Database::Users;

    {
        Users users;
        QVERIFY(db.usersForTag(tagIds[0], &users));
        QVERIFY(users.isEmpty());
    }

    {
        Users users;
        QVERIFY(db.usersForTag(tagIds[1], &users));
        QCOMPARE(users.count(), 2);
        QCOMPARE(users[0].id, studentIds[0]);
        QCOMPARE(users[1].id, studentIds[4]);
    }

    {
        Users users;
        QVERIFY(db.usersForTag(tagIds[2], &users));
        QCOMPARE(users.count(), 3);
        QCOMPARE(users[0].id, studentIds[1]);
        QCOMPARE(users[1].id, studentIds[2]);
        QCOMPARE(users[2].id, studentIds[4]);
    }

    // Remove alicun ligamines

    QVERIFY(db.removeTagFromUsers(tagIds[2], {
        studentIds[1], studentIds[4],
    }));

    {
        Users users;
        QVERIFY(db.usersForTag(tagIds[2], &users));
        QCOMPARE(users.count(), 1);
        QCOMPARE(users[0].id, studentIds[2]);
    }

    // Remove un etiquetta, verifica que le trigger functiona

    Core::Error error = db.deleteTag(tagIds[2]);
    QVERIFY(!error);

    {
        Users users;
        QVERIFY(db.usersForTag(tagIds[2], &users));
        QCOMPARE(users.count(), 0);
    }

    // Remove un usator, verifica que le trigger functiona

    {
        QVERIFY(!db.deleteUser(studentIds[4]));

        Tags tags;
        QVERIFY(db.tagsForUser(studentIds[4], &tags));
        QCOMPARE(tags.count(), 0);

        Users users;
        QVERIFY(db.usersForTag(tagIds[1], &users));
        QCOMPARE(users.count(), 1);
        QCOMPARE(users[0].id, studentIds[0]);
    }
}

void DatabaseTest::testTagsForGroups()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const auto groupIds = prepareGroups(&db, {"g0", "g1", "g2", "g3", "g4"});
    const auto tagIds = prepareTags(&db, {"t0", "t1", "t2", "t3", "t4"});

    // Testa le addition

    QVERIFY(db.addTagToGroups(tagIds[1], { groupIds[0], groupIds[4] }));
    QVERIFY(db.addTagToGroups(tagIds[2], {
        groupIds[1], groupIds[2], groupIds[4],
    }));

    // Testa le lectura: etiquettas per gruppo

    using Tags = Core::Database::Tags;

    {
        Tags tags;
        QVERIFY(db.tagsForGroup(groupIds[3], &tags));
        QVERIFY(tags.isEmpty());
    }

    {
        Tags tags;
        QVERIFY(db.tagsForGroup(groupIds[0], &tags));
        QCOMPARE(tags.count(), 1);
        QCOMPARE(tags[0].id, tagIds[1]);
    }

    {
        Tags tags;
        QVERIFY(db.tagsForGroup(groupIds[4], &tags));
        QCOMPARE(tags.count(), 2);
        QCOMPARE(tags[0].id, tagIds[1]);
        QCOMPARE(tags[1].id, tagIds[2]);
    }

    // Testa le lectura: gruppos con etiquetta

    using Groups = Core::Database::Groups;

    {
        Groups groups;
        QVERIFY(db.groupsForTag(tagIds[0], &groups));
        QVERIFY(groups.isEmpty());
    }

    {
        Groups groups;
        QVERIFY(db.groupsForTag(tagIds[1], &groups));
        QCOMPARE(groups.count(), 2);
        QCOMPARE(groups[0].id, groupIds[0]);
        QCOMPARE(groups[1].id, groupIds[4]);
    }

    {
        Groups groups;
        QVERIFY(db.groupsForTag(tagIds[2], &groups));
        QCOMPARE(groups.count(), 3);
        QCOMPARE(groups[0].id, groupIds[1]);
        QCOMPARE(groups[1].id, groupIds[2]);
        QCOMPARE(groups[2].id, groupIds[4]);
    }

    // Remove alicun ligamines

    QVERIFY(db.removeTagFromGroups(tagIds[2], {
        groupIds[1], groupIds[4],
    }));

    {
        Groups groups;
        QVERIFY(db.groupsForTag(tagIds[2], &groups));
        QCOMPARE(groups.count(), 1);
        QCOMPARE(groups[0].id, groupIds[2]);
    }

    // Remove un etiquetta, verifica que le trigger functiona

    Core::Error error = db.deleteTag(tagIds[2]);
    QVERIFY(!error);

    {
        Groups groups;
        QVERIFY(db.groupsForTag(tagIds[2], &groups));
        QCOMPARE(groups.count(), 0);
    }

    // Remove un gruppo, verifica que le trigger functiona

    {
        QVERIFY(!db.deleteGroup(groupIds[4]));

        Tags tags;
        QVERIFY(db.tagsForGroup(groupIds[4], &tags));
        QCOMPARE(tags.count(), 0);

        Groups groups;
        QVERIFY(db.groupsForTag(tagIds[1], &groups));
        QCOMPARE(groups.count(), 1);
        QCOMPARE(groups[0].id, groupIds[0]);
    }
}

void DatabaseTest::testTagsForLessons()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const auto lessonIds =
        prepareLessons(&db, { 1000, 2000, 3000, 4000, 5000 });
    const auto tagIds = prepareTags(&db, {"t0", "t1", "t2", "t3", "t4"});

    // Testa le addition

    QVERIFY(db.addTagToLessons(tagIds[1], { lessonIds[0], lessonIds[4] }));
    QVERIFY(db.addTagToLessons(tagIds[2], {
        lessonIds[1], lessonIds[2], lessonIds[4],
    }));

    // Testa le lectura: etiquettas per lection

    using Tags = Core::Database::Tags;

    {
        Tags tags;
        QVERIFY(db.tagsForLesson(lessonIds[3], &tags));
        QVERIFY(tags.isEmpty());
    }

    {
        Tags tags;
        QVERIFY(db.tagsForLesson(lessonIds[0], &tags));
        QCOMPARE(tags.count(), 1);
        QCOMPARE(tags[0].id, tagIds[1]);
    }

    {
        Tags tags;
        QVERIFY(db.tagsForLesson(lessonIds[4], &tags));
        QCOMPARE(tags.count(), 2);
        QCOMPARE(tags[0].id, tagIds[1]);
        QCOMPARE(tags[1].id, tagIds[2]);
    }

    // Testa le lectura: lectiones con etiquetta

    using Lessons = Core::Database::Lessons;

    {
        Lessons lessons;
        QVERIFY(db.lessonsForTag(tagIds[0], &lessons));
        QVERIFY(lessons.isEmpty());
    }

    {
        Lessons lessons;
        QVERIFY(db.lessonsForTag(tagIds[1], &lessons));
        QCOMPARE(lessons.count(), 2);
        QCOMPARE(lessons[0].id, lessonIds[0]);
        QCOMPARE(lessons[1].id, lessonIds[4]);
    }

    {
        Lessons lessons;
        QVERIFY(db.lessonsForTag(tagIds[2], &lessons));
        QCOMPARE(lessons.count(), 3);
        QCOMPARE(lessons[0].id, lessonIds[1]);
        QCOMPARE(lessons[1].id, lessonIds[2]);
        QCOMPARE(lessons[2].id, lessonIds[4]);
    }

    // Remove alicun ligamines

    QVERIFY(db.removeTagFromLessons(tagIds[2], {
        lessonIds[1], lessonIds[4],
    }));

    {
        Lessons lessons;
        QVERIFY(db.lessonsForTag(tagIds[2], &lessons));
        QCOMPARE(lessons.count(), 1);
        QCOMPARE(lessons[0].id, lessonIds[2]);
    }

    // Remove un etiquetta, verifica que le trigger functiona

    Core::Error error = db.deleteTag(tagIds[2]);
    QVERIFY(!error);

    {
        Lessons lessons;
        QVERIFY(db.lessonsForTag(tagIds[2], &lessons));
        QCOMPARE(lessons.count(), 0);
    }

    // Remove un lection, verifica que le trigger functiona

    {
        QVERIFY(!db.deleteLesson(lessonIds[4]));

        Tags tags;
        QVERIFY(db.tagsForLesson(lessonIds[4], &tags));
        QCOMPARE(tags.count(), 0);

        Lessons lessons;
        QVERIFY(db.lessonsForTag(tagIds[1], &lessons));
        QCOMPARE(lessons.count(), 1);
        QCOMPARE(lessons[0].id, lessonIds[0]);
    }
}

void DatabaseTest::testTagsForLessonNotes()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const auto lessonNoteIds =
        prepareLessonNotes(&db, { "Uno", "Duo", "Tres", "Quatro", "Cinque" });
    const auto tagIds = prepareTags(&db, {"t0", "t1", "t2", "t3", "t4"});

    // Testa le addition

    QVERIFY(db.addTagToLessonNotes(tagIds[1], { lessonNoteIds[0], lessonNoteIds[4] }));
    QVERIFY(db.addTagToLessonNotes(tagIds[2], {
        lessonNoteIds[1], lessonNoteIds[2], lessonNoteIds[4],
    }));

    // Testa le lectura: etiquettas per lection

    using Tags = Core::Database::Tags;

    {
        Tags tags;
        QVERIFY(db.tagsForLessonNote(lessonNoteIds[3], &tags));
        QVERIFY(tags.isEmpty());
    }

    {
        Tags tags;
        QVERIFY(db.tagsForLessonNote(lessonNoteIds[0], &tags));
        QCOMPARE(tags.count(), 1);
        QCOMPARE(tags[0].id, tagIds[1]);
    }

    {
        Tags tags;
        QVERIFY(db.tagsForLessonNote(lessonNoteIds[4], &tags));
        QCOMPARE(tags.count(), 2);
        QCOMPARE(tags[0].id, tagIds[1]);
        QCOMPARE(tags[1].id, tagIds[2]);
    }

    // Testa le lectura: lectiones con etiquetta

    using LessonNotes = Core::Database::LessonNotes;

    {
        LessonNotes lessonNotes;
        QVERIFY(db.lessonNotesForTag(tagIds[0], &lessonNotes));
        QVERIFY(lessonNotes.isEmpty());
    }

    {
        LessonNotes lessonNotes;
        QVERIFY(db.lessonNotesForTag(tagIds[1], &lessonNotes));
        QCOMPARE(lessonNotes.count(), 2);
        QCOMPARE(lessonNotes[0].id, lessonNoteIds[0]);
        QCOMPARE(lessonNotes[1].id, lessonNoteIds[4]);
    }

    {
        LessonNotes lessonNotes;
        QVERIFY(db.lessonNotesForTag(tagIds[2], &lessonNotes));
        QCOMPARE(lessonNotes.count(), 3);
        QCOMPARE(lessonNotes[0].id, lessonNoteIds[1]);
        QCOMPARE(lessonNotes[1].id, lessonNoteIds[2]);
        QCOMPARE(lessonNotes[2].id, lessonNoteIds[4]);
    }

    // Remove alicun ligamines

    QVERIFY(db.removeTagFromLessonNotes(tagIds[2], {
        lessonNoteIds[1], lessonNoteIds[4],
    }));

    {
        LessonNotes lessonNotes;
        QVERIFY(db.lessonNotesForTag(tagIds[2], &lessonNotes));
        QCOMPARE(lessonNotes.count(), 1);
        QCOMPARE(lessonNotes[0].id, lessonNoteIds[2]);
    }

    // Remove un etiquetta, verifica que le trigger functiona

    Core::Error error = db.deleteTag(tagIds[2]);
    QVERIFY(!error);

    {
        LessonNotes lessonNotes;
        QVERIFY(db.lessonNotesForTag(tagIds[2], &lessonNotes));
        QCOMPARE(lessonNotes.count(), 0);
    }

    // Remove un lection, verifica que le trigger functiona

    {
        QVERIFY(!db.deleteLessonNote(lessonNoteIds[4]));

        Tags tags;
        QVERIFY(db.tagsForLessonNote(lessonNoteIds[4], &tags));
        QCOMPARE(tags.count(), 0);

        LessonNotes lessonNotes;
        QVERIFY(db.lessonNotesForTag(tagIds[1], &lessonNotes));
        QCOMPARE(lessonNotes.count(), 1);
        QCOMPARE(lessonNotes[0].id, lessonNoteIds[0]);
    }
}

void DatabaseTest::testTagsForLocations()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const auto locationIds =
        prepareLocations(&db, { "a", "b", "c", "d", "e" });
    const auto tagIds = prepareTags(&db, {"t0", "t1", "t2", "t3", "t4"});

    // Testa le addition

    QVERIFY(db.addTagToLocations(tagIds[1], { locationIds[0], locationIds[4] }));
    QVERIFY(db.addTagToLocations(tagIds[2], {
        locationIds[1], locationIds[2], locationIds[4],
    }));

    // Testa le lectura: etiquettas per location

    using Tags = Core::Database::Tags;

    {
        Tags tags;
        QVERIFY(db.tagsForLocation(locationIds[3], &tags));
        QVERIFY(tags.isEmpty());
    }

    {
        Tags tags;
        QVERIFY(db.tagsForLocation(locationIds[0], &tags));
        QCOMPARE(tags.count(), 1);
        QCOMPARE(tags[0].id, tagIds[1]);
    }

    {
        Tags tags;
        QVERIFY(db.tagsForLocation(locationIds[4], &tags));
        QCOMPARE(tags.count(), 2);
        QCOMPARE(tags[0].id, tagIds[1]);
        QCOMPARE(tags[1].id, tagIds[2]);
    }

    // Testa le lectura: locationes con etiquetta

    using Locations = Core::Database::Locations;

    {
        Locations locations;
        QVERIFY(db.locationsForTag(tagIds[0], &locations));
        QVERIFY(locations.isEmpty());
    }

    {
        Locations locations;
        QVERIFY(db.locationsForTag(tagIds[1], &locations));
        QCOMPARE(locations.count(), 2);
        QCOMPARE(locations[0].id, locationIds[0]);
        QCOMPARE(locations[1].id, locationIds[4]);
    }

    {
        Locations locations;
        QVERIFY(db.locationsForTag(tagIds[2], &locations));
        QCOMPARE(locations.count(), 3);
        QCOMPARE(locations[0].id, locationIds[1]);
        QCOMPARE(locations[1].id, locationIds[2]);
        QCOMPARE(locations[2].id, locationIds[4]);
    }

    // Remove alicun ligamines

    QVERIFY(db.removeTagFromLocations(tagIds[2], {
        locationIds[1], locationIds[4],
    }));

    {
        Locations locations;
        QVERIFY(db.locationsForTag(tagIds[2], &locations));
        QCOMPARE(locations.count(), 1);
        QCOMPARE(locations[0].id, locationIds[2]);
    }

    // Remove un etiquetta, verifica que le trigger functiona

    Core::Error error = db.deleteTag(tagIds[2]);
    QVERIFY(!error);

    {
        Locations locations;
        QVERIFY(db.locationsForTag(tagIds[2], &locations));
        QCOMPARE(locations.count(), 0);
    }

    // Remove un location, verifica que le trigger functiona

    {
        QVERIFY(!db.deleteLocation(locationIds[4]));

        Tags tags;
        QVERIFY(db.tagsForLocation(locationIds[4], &tags));
        QCOMPARE(tags.count(), 0);

        Locations locations;
        QVERIFY(db.locationsForTag(tagIds[1], &locations));
        QCOMPARE(locations.count(), 1);
        QCOMPARE(locations[0].id, locationIds[0]);
    }
}
