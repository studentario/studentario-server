#include "utils.h"

#include <QScopedPointer>
#include <QTest>

class UtilsTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testStringToColor_data();
    void testStringToColor();
    void testStringFromColor_data();
    void testStringFromColor();
};

void UtilsTest::testStringToColor_data()
{
    QTest::addColumn<QString>("string");
    QTest::addColumn<QVariant>("expectedColor");

    QTest::newRow("empty") << QString() << QVariant();
    QTest::newRow("valid") << "#51AB9f" << QVariant(0x51ab9f);
}

void UtilsTest::testStringToColor()
{
    QFETCH(QString, string);
    QFETCH(QVariant, expectedColor);

    QVariant color = Core::Utils::stringToColor(string);
    QCOMPARE(color, expectedColor);
}

void UtilsTest::testStringFromColor_data()
{
    QTest::addColumn<QVariant>("color");
    QTest::addColumn<QString>("expectedString");

    QTest::newRow("empty") << QVariant() << QString();
    QTest::newRow("valid") << QVariant(0x51ab9f) << "#51ab9f";
}

void UtilsTest::testStringFromColor()
{
    QFETCH(QVariant, color);
    QFETCH(QString, expectedString);

    QString string = Core::Utils::stringFromColor(color);
    QCOMPARE(string, expectedString);
}

QTEST_APPLESS_MAIN(UtilsTest)

#include "tst_utils.moc"
