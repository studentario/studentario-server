#include "tst_database.h"

using LessonIds = QVector<int>;
using Participants = QVector<Core::Participant>;

namespace Core {

inline QByteArray toByteArray(const Participant &p)
{
    QByteArray type;
    switch (p.type) {
    case EntityType::Student: type = "Student"; break;
    case EntityType::Teacher: type = "Teacher"; break;
    case EntityType::Group: type = "Group"; break;
    default: type = "Invalid"; break;
    }
    return type + '(' + QByteArray::number(p.id) + ')';
}

inline char *toString(const Participant &p)
{
    return QTest::toString(toByteArray(p));
}

inline char *toString(const Participants &ps)
{
    QByteArray text;
    for (const auto &p: ps) {
        if (!text.isEmpty()) text += ", ";
        text += toByteArray(p);
    }
    return QTest::toString(text);
}

} // namespace

void DatabaseTest::testLessonCreationAndDeletion()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    // Testa le creation

    const int creatorId1 = 3;
    const int creatorId2 = 2;
    const int locationId2 = -1;

    const int locationId1 = db.addLocation({
        0, "Classroom",
    });
    const int locationId3 = db.addLocation({
        0, "Classroom2",
    });
    QVERIFY(locationId1 >= 0);
    const Core::Lesson lesson1 = {
        0, creatorId1, QDateTime::fromSecsSinceEpoch(12345),
        QDateTime::fromSecsSinceEpoch(300000),
        QDateTime::fromSecsSinceEpoch(301000),
        locationId1,
    };
    int lessonId1 = db.addLesson(lesson1);
    QVERIFY(lessonId1 >= 0);

    const Core::Lesson lesson2 = {
        0, creatorId2, QDateTime::fromSecsSinceEpoch(54321),
        QDateTime::fromSecsSinceEpoch(200000),
        QDateTime::fromSecsSinceEpoch(201000),
        locationId2,
    };
    int lessonId2 = db.addLesson(lesson2);
    QVERIFY(lessonId2 >= 0);

    // Testa le lectura

    {
        Core::Lesson lesson;
        QVERIFY(db.lesson(lessonId1, &lesson));
        QCOMPARE(lesson.id, lessonId1);
        // nihilifica le ID, que in lesson1 es ancora 0
        lesson.id = 0;
        QCOMPARE(lesson, lesson1);
    }

    {
        Core::Lesson lesson;
        QVERIFY(db.lesson(lessonId2, &lesson));
        QCOMPARE(lesson.id, lessonId2);
        // nihilifica le ID, que in lesson2 es ancora 0
        lesson.id = 0;
        QCOMPARE(lesson, lesson2);
    }

    // Testa le modification

    {
        // nulle campo: expecta un error
        QTest::ignoreMessage(QtWarningMsg, "Nothing to update");
        Core::Lesson lesson;
        QVERIFY(db.lesson(lessonId1, &lesson));
        Core::Lesson::Fields fields;
        Core::Error error = db.updateLesson(lesson, fields);
        QVERIFY(!error);
    }

    {
        // Cambia le campos singularmente
        Core::Lesson lesson, lessonReloaded;
        QVERIFY(db.lesson(lessonId1, &lesson));

        Core::Lesson::Fields fields = Core::Lesson::Field::CreatorId;
        lesson.creatorId = -1;
        Core::Error error = db.updateLesson(lesson, fields);
        QVERIFY(!error);
        QVERIFY(db.lesson(lessonId1, &lessonReloaded));
        QCOMPARE(lessonReloaded, lesson);

        fields = Core::Lesson::Field::StartTime;
        lesson.startTime = QDateTime::fromSecsSinceEpoch(300100),
        error = db.updateLesson(lesson, fields);
        QVERIFY(!error);
        QVERIFY(db.lesson(lessonId1, &lessonReloaded));
        QCOMPARE(lessonReloaded, lesson);

        fields = Core::Lesson::Field::EndTime;
        lesson.endTime = QDateTime::fromSecsSinceEpoch(302100),
        error = db.updateLesson(lesson, fields);
        QVERIFY(!error);
        QVERIFY(db.lesson(lessonId1, &lessonReloaded));
        QCOMPARE(lessonReloaded, lesson);

        fields = Core::Lesson::Field::LocationId;
        lesson.locationId = locationId3;
        error = db.updateLesson(lesson, fields);
        QVERIFY(!error);
        QVERIFY(db.lesson(lessonId1, &lessonReloaded));
        QCOMPARE(lessonReloaded, lesson);
    }

    {
        // Cambia le campos insimul
        Core::Lesson lesson, lessonReloaded;
        QVERIFY(db.lesson(lessonId1, &lesson));

        Core::Lesson::Fields fields =
            Core::Lesson::Field::CreatorId |
            Core::Lesson::Field::StartTime |
            Core::Lesson::Field::EndTime |
            Core::Lesson::Field::LocationId;
        lesson.creatorId = creatorId2;
        lesson.startTime = QDateTime::fromSecsSinceEpoch(300000);
        lesson.endTime = QDateTime::fromSecsSinceEpoch(302000);
        lesson.locationId = locationId2;
        Core::Error error = db.updateLesson(lesson, fields);
        QVERIFY(!error);
        QVERIFY(db.lesson(lessonId1, &lessonReloaded));
        QCOMPARE(lessonReloaded, lesson);
    }

    // Testa le deletion

    {
        Core::Lesson lesson;
        Core::Error error = db.deleteLesson(lessonId2);
        QVERIFY(!error);
        QVERIFY(!db.lesson(lessonId2, &lesson));
        QVERIFY(db.lesson(lessonId1, &lesson));
    }

    {
        Core::Error error = db.deleteLesson(42134);
        QVERIFY(bool(error));
        QCOMPARE(error.code(), Core::Error::LessonNotFound);
    }
}

void DatabaseTest::testLessons_data()
{
    QTest::addColumn<QDateTime>("since");
    QTest::addColumn<QDateTime>("to");
    QTest::addColumn<int>("locationId");
    QTest::addColumn<LessonIds>("expectedLessonIds");

    QTest::newRow("tote lectiones") <<
        QDateTime() << QDateTime() << -1 <<
        LessonIds { 4, 2, 5, 3, 1 };

    QTest::newRow("per location") <<
        QDateTime() << QDateTime() << 1 <<
        LessonIds { 2, 5 };

    QTest::newRow("per tempore initial") <<
        QDateTime::fromSecsSinceEpoch(3100) << QDateTime() << -1 <<
        LessonIds { 5, 3, 1 };

    QTest::newRow("per tempore final") <<
        QDateTime() << QDateTime::fromSecsSinceEpoch(3100) << -1 <<
        LessonIds { 4, 2, 5 };

    QTest::newRow("combination filtros 1") <<
        QDateTime::fromSecsSinceEpoch(2100) << QDateTime::fromSecsSinceEpoch(4200) << -1 <<
        LessonIds { 2, 5, 3 };

    QTest::newRow("combination filtros 2") <<
        QDateTime::fromSecsSinceEpoch(2100) << QDateTime::fromSecsSinceEpoch(4200) << 1 <<
        LessonIds { 2, 5 };
}

void DatabaseTest::testLessons()
{
    QFETCH(QDateTime, since);
    QFETCH(QDateTime, to);
    QFETCH(int, locationId);
    QFETCH(LessonIds, expectedLessonIds);

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    /* Adde le lectiones */

    const int locationId1 = db.addLocation({
        0, "Classroom",
    });
    QCOMPARE(locationId1, 1);

    using Lesson = Core::Lesson;
    const QVector<Lesson> allLessons = {
        {
            0, 2, QDateTime::fromSecsSinceEpoch(12345),
            QDateTime::fromSecsSinceEpoch(5000),
            QDateTime::fromSecsSinceEpoch(5500),
            -1,
        },
        {
            0, -1, QDateTime::fromSecsSinceEpoch(54321),
            QDateTime::fromSecsSinceEpoch(2000),
            QDateTime::fromSecsSinceEpoch(2200),
            1,
        },
        {
            0, 5, QDateTime::fromSecsSinceEpoch(12345),
            QDateTime::fromSecsSinceEpoch(4000),
            QDateTime::fromSecsSinceEpoch(4400),
            -1,
        },
        {
            0, -1, QDateTime::fromSecsSinceEpoch(54322),
            QDateTime::fromSecsSinceEpoch(1000),
            QDateTime::fromSecsSinceEpoch(1100),
            -1,
        },
        {
            0, 4, QDateTime::fromSecsSinceEpoch(12341),
            QDateTime::fromSecsSinceEpoch(3000),
            QDateTime::fromSecsSinceEpoch(3300),
            1,
        },
    };

    for (const Lesson &lesson: allLessons) {
        int lessonId = db.addLesson(lesson);
        QVERIFY(lessonId >= 0);
    }

    /* Lege del base de datos */

    const auto lessons = db.lessons(since, to, locationId);
    LessonIds lessonIds;
    for (const Lesson &lesson: lessons) {
        lessonIds.append(lesson.id);
    }

    QCOMPARE(lessonIds, expectedLessonIds);
    QVERIFY(m_capturedWarnings.isEmpty());
}

void DatabaseTest::testLessonParticipation()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const auto lessonIds = prepareLessons(&db, {5000, 2000, 4000, 1000, 3000});
    const auto studentIds = prepareStudents(&db, {"s0", "s1", "s2", "s3", "s4"});
    const auto teacherIds = prepareTeachers(&db, {"t0", "t1", "t2"});
    const auto groupIds = prepareGroups(&db, {"Gruppo 0", "Gruppo 1", "Gruppo 2"});
    const int creatorId = 1;

    using Role = Core::User::Role;
    using Lessons = Core::Database::Lessons;
    using EntityType = Core::EntityType;
    using Participant = Core::Participant;
    using Participations = Core::Database::Participations;

    // Testa le addition

    QVERIFY(db.addUsersToGroup(groupIds[0], {
        studentIds[0], studentIds[4]
    }, Role::Student));
    QVERIFY(db.addUsersToGroup(groupIds[1], {
        studentIds[1], studentIds[2], studentIds[4],
    }, Role::Student));
    QVERIFY(db.addUsersToGroup(groupIds[2], studentIds, Role::Student));

    QVERIFY(db.addUsersToGroup(groupIds[0], { teacherIds[0] }, Role::Teacher));
    QVERIFY(db.addUsersToGroup(groupIds[1], { teacherIds[1] }, Role::Teacher));

    // Invita usatores e gruppos al lectiones

    QVERIFY(db.inviteToLesson(creatorId, lessonIds[0],
                              { groupIds[0] }, EntityType::Group));
    QVERIFY(db.inviteToLesson(creatorId, lessonIds[3],
                              { groupIds[0] }, EntityType::Group));
    QVERIFY(db.inviteToLesson(creatorId, lessonIds[4],
                              { groupIds[0] }, EntityType::Group));
    QVERIFY(db.inviteToLesson(creatorId, lessonIds[4],
                              { studentIds[2] }, EntityType::Student));

    QVERIFY(db.inviteToLesson(creatorId, lessonIds[1],
                              { groupIds[1] }, EntityType::Group));
    QVERIFY(db.inviteToLesson(creatorId, lessonIds[2],
                              { groupIds[2] }, EntityType::Group));
    QVERIFY(db.inviteToLesson(creatorId, lessonIds[2],
                              { studentIds[1] }, EntityType::Student));
    QVERIFY(db.inviteToLesson(creatorId, lessonIds[2],
                              { teacherIds[2] }, EntityType::Teacher));

    auto extractIds = [](const Lessons &lessons) {
        LessonIds ids;
        for (const Core::Lesson &l: lessons) ids.append(l.id);
        return ids;
    };

    auto extractParticipants = [](const Participations &participations) {
        Participants participants;
        for (const auto &p: participations) {
            participants.append(p.participant);
        }
        return participants;
    };

    {
        // Studente parte de gruppos
        Lessons lessons;
        const Participant participant { studentIds[0], EntityType::Student };
        QVERIFY(db.lessonsByParticipants({ participant }, &lessons));
        LessonIds expectedIds = {
            lessonIds[3], lessonIds[4], lessonIds[2], lessonIds[0],
        };
        QCOMPARE(extractIds(lessons), expectedIds);
    }

    {
        // Studente parte de gruppos e invitate
        Lessons lessons;
        const Participant participant { studentIds[2], EntityType::Student };
        QVERIFY(db.lessonsByParticipants({ participant }, &lessons));
        LessonIds expectedIds = { lessonIds[1], lessonIds[4], lessonIds[2] };
        QCOMPARE(extractIds(lessons), expectedIds);
    }

    {
        // Ambe le participantes precedente
        Lessons lessons;
        Participants participants {
            { studentIds[0], EntityType::Student },
            { studentIds[2], EntityType::Student },
        };
        QVERIFY(db.lessonsByParticipants(participants, &lessons));
        LessonIds expectedIds = {
            lessonIds[3], lessonIds[1], lessonIds[4], lessonIds[2], lessonIds[0],
        };
        QCOMPARE(extractIds(lessons), expectedIds);
    }

    {
        // Inseniante parte de gruppos
        Lessons lessons;
        const Participant participant { teacherIds[0], EntityType::Teacher };
        QVERIFY(db.lessonsByParticipants({ participant }, &lessons));
        LessonIds expectedIds = { lessonIds[3], lessonIds[4], lessonIds[0] };
        QCOMPARE(extractIds(lessons), expectedIds);
    }

    {
        // Inseniante invitate
        Lessons lessons;
        const Participant participant { teacherIds[2], EntityType::Teacher };
        QVERIFY(db.lessonsByParticipants({ participant }, &lessons));
        LessonIds expectedIds = { lessonIds[2] };
        QCOMPARE(extractIds(lessons), expectedIds);
    }

    {
        // Ambe le participantes precedente
        Lessons lessons;
        Participants participants {
            { teacherIds[0], EntityType::Teacher },
            { teacherIds[2], EntityType::Teacher },
        };
        QVERIFY(db.lessonsByParticipants(participants, &lessons));
        LessonIds expectedIds = {
            lessonIds[3], lessonIds[4], lessonIds[2], lessonIds[0],
        };
        QCOMPARE(extractIds(lessons), expectedIds);
    }

    {
        // Gruppo
        Lessons lessons;
        const Participant participant { groupIds[0], EntityType::Group };
        QVERIFY(db.lessonsByParticipants({ participant }, &lessons));
        LessonIds expectedIds = {
            lessonIds[3], lessonIds[4], lessonIds[0],
        };
        QCOMPARE(extractIds(lessons), expectedIds);
    }

    {
        // Participation per invitation
        Participations participations;
        QVERIFY(db.lessonAttendance(lessonIds[4], &participations));
        Participants expectedParticipants = {
            { groupIds[0], EntityType::Group },
            { studentIds[2], EntityType::Student },
        };
        QCOMPARE(extractParticipants(participations), expectedParticipants);
        QCOMPARE(participations[0].invited, false);
        QCOMPARE(participations[0].attended, false);
        // Le studente es invitate
        QCOMPARE(participations[1].invited, true);
        QCOMPARE(participations[1].attended, false);
        // Controla le creator
        QCOMPARE(participations[0].creatorId, creatorId);
        QCOMPARE(participations[1].creatorId, creatorId);
    }

    {
        // Actualisa le participation
        const Participations participations {
            {
                0, creatorId, QDateTime(), lessonIds[4],
                { studentIds[2], EntityType::Student },
                true, true, "Multo attente",
            },
            {
                0, creatorId, QDateTime(), lessonIds[4],
                { teacherIds[0], EntityType::Teacher },
                false, true, "Inseniante",
            },
            {
                0, creatorId, QDateTime(), lessonIds[4],
                { studentIds[0], EntityType::Student },
                false, true, "",
            },
        };
        QVERIFY(db.updateAttendance(lessonIds[4], participations));
    }

    {
        // Verifica le participation actualisate
        Participations participations;
        QVERIFY(db.lessonAttendance(lessonIds[4], &participations));
        Participants expectedParticipants = {
            { groupIds[0], EntityType::Group },
            { studentIds[0], EntityType::Student },
            { studentIds[2], EntityType::Student },
            { teacherIds[0], EntityType::Teacher },
        };
        QCOMPARE(extractParticipants(participations), expectedParticipants);
        QVERIFY(participations[1].note.isEmpty());
        QVERIFY(participations[1].attended);
        QVERIFY(!participations[1].invited);
        QCOMPARE(participations[2].note, "Multo attente");
        QVERIFY(participations[2].attended);
        QVERIFY(participations[2].invited);
        QCOMPARE(participations[3].note, "Inseniante");
    }

    {
        // Disinvita le gruppo
        QVERIFY(db.uninviteFromLesson(lessonIds[4],
                                      { groupIds[0] }, EntityType::Group));
        // Disinvita un studente qui participava: isto non va functionar
        QVERIFY(db.uninviteFromLesson(lessonIds[4],
                                      { studentIds[0] }, EntityType::Student));
        Participations participations;
        QVERIFY(db.lessonAttendance(lessonIds[4], &participations));
        Participants expectedParticipants = {
            { studentIds[0], EntityType::Student },
            { studentIds[2], EntityType::Student },
            { teacherIds[0], EntityType::Teacher },
        };
        QCOMPARE(extractParticipants(participations), expectedParticipants);
    }

    {
        // Altere lection; verifica que disinvitar non participantes functiona
        Participations participations;
        QVERIFY(db.lessonAttendance(lessonIds[2], &participations));
        Participants expectedParticipants = {
            { groupIds[2], EntityType::Group },
            { studentIds[1], EntityType::Student },
            { teacherIds[2], EntityType::Teacher },
        };
        QCOMPARE(extractParticipants(participations), expectedParticipants);
        QVERIFY(db.uninviteFromLesson(lessonIds[2],
                                      { studentIds[1] }, EntityType::Student));
        participations.clear();
        QVERIFY(db.lessonAttendance(lessonIds[2], &participations));
        expectedParticipants = {
            { groupIds[2], EntityType::Group },
            { teacherIds[2], EntityType::Teacher },
        };
        QCOMPARE(extractParticipants(participations), expectedParticipants);
        QVERIFY(db.uninviteFromLesson(lessonIds[2],
                                      { teacherIds[2] }, EntityType::Teacher));
        participations.clear();
        QVERIFY(db.lessonAttendance(lessonIds[2], &participations));
        expectedParticipants = {
            { groupIds[2], EntityType::Group },
        };
        QCOMPARE(extractParticipants(participations), expectedParticipants);
    }

    {
        // Remove le gruppo, verifica que le trigger functiona
        Core::Error error = db.deleteGroup(groupIds[2]);
        QVERIFY(!error);
        Participations participations;
        QVERIFY(db.lessonAttendance(lessonIds[2], &participations));
        Participants expectedParticipants = {};
        QCOMPARE(extractParticipants(participations), expectedParticipants);
    }
}
