#include "tst_database.h"

void DatabaseTest::testGroupCreationAndDeletion()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    // Testa le creation

    const int locationId1 = db.addLocation({
        0, "Classroom",
    });
    QVERIFY(locationId1 >= 0);

    const Core::Group group1 = {
        0, "First group", "A description",
        QDateTime::fromSecsSinceEpoch(12345),
    };
    int groupId1 = db.addGroup(group1);
    QVERIFY(groupId1 >= 0);

    const Core::Group group2 = {
        0, "Second group", "Another description",
        QDateTime::fromSecsSinceEpoch(54321),
        locationId1,
    };
    int groupId2 = db.addGroup(group2);
    QVERIFY(groupId2 >= 0);

    // Gruppos con location inexistente non pote esser create
    int groupId3 = db.addGroup({
        0, "Third group", "Failure",
        QDateTime::fromSecsSinceEpoch(54321),
        42,
    });
    qDebug() << "groupd id is" << groupId3;
    QVERIFY(groupId3 < 0);

    // Testa le lectura

    {
        Core::Group group;
        QVERIFY(db.group(groupId1, &group));
        QCOMPARE(group.id, groupId1);
        // nihilifica le ID, que in group1 es ancora 0
        group.id = 0;
        QCOMPARE(group, group1);
    }

    {
        Core::Group group;
        QVERIFY(db.group(groupId2, &group));
        QCOMPARE(group.id, groupId2);
        // nihilifica le ID, que in group2 es ancora 0
        group.id = 0;
        QCOMPARE(group, group2);
    }

    // Testa le modification

    {
        // nulle campo: expecta un error
        QTest::ignoreMessage(QtWarningMsg, "Nothing to update");
        Core::Group group;
        QVERIFY(db.group(groupId1, &group));
        Core::Group::Fields fields;
        Core::Error error = db.updateGroup(group, fields);
        QVERIFY(!error);
    }

    {
        // Cambia le campos singularmente
        Core::Group group, groupReloaded;
        QVERIFY(db.group(groupId1, &group));

        Core::Group::Fields fields = Core::Group::Field::Name;
        group.name = "Prime gruppo";
        Core::Error error = db.updateGroup(group, fields);
        QVERIFY(!error);
        QVERIFY(db.group(groupId1, &groupReloaded));
        QCOMPARE(groupReloaded, group);

        fields = Core::Group::Field::Description;
        group.description = "Un texto plus longe\nMa non troppo";
        error = db.updateGroup(group, fields);
        QVERIFY(!error);
        QVERIFY(db.group(groupId1, &groupReloaded));
        QCOMPARE(groupReloaded, group);

        fields = Core::Group::Field::LocationId;
        group.locationId = locationId1;
        error = db.updateGroup(group, fields);
        QVERIFY(!error);
        QVERIFY(db.group(groupId1, &groupReloaded));
        QCOMPARE(groupReloaded, group);
    }

    {
        // Cambia le campos insimul
        Core::Group group, groupReloaded;
        QVERIFY(db.group(groupId1, &group));

        Core::Group::Fields fields =
            Core::Group::Field::Name | Core::Group::Field::Description;
        group.name = "Prime";
        group.description = "";
        Core::Error error = db.updateGroup(group, fields);
        QVERIFY(!error);
        QVERIFY(db.group(groupId1, &groupReloaded));
        QCOMPARE(groupReloaded, group);
    }

    // Testa le deletion

    {
        Core::Group group;
        Core::Error error = db.deleteGroup(groupId2);
        QVERIFY(!error);
        QVERIFY(!db.group(groupId2, &group));
        QVERIFY(db.group(groupId1, &group));
    }

    {
        Core::Error error = db.deleteGroup(42134);
        QVERIFY(bool(error));
        QCOMPARE(error.code(), Core::Error::GroupNotFound);
    }
}

void DatabaseTest::testGroups_data()
{
    QTest::addColumn<QString>("pattern");
    QTest::addColumn<QStringList>("expectedGroupNames");

    QTest::newRow("all groups") <<
        "" << QStringList { "first", "group1", "group2", "last" };

    QTest::newRow("short pattern") <<
        "r" << QStringList { "first", "group1", "group2" };

    QTest::newRow("long pattern") <<
        "group" << QStringList { "group1", "group2" };

    QTest::newRow("not found") <<
        "awa" << QStringList {};

    QTest::newRow("injection1") <<
        "'; DROP TABLE Groups;" << QStringList {};
}

void DatabaseTest::testGroups()
{
    QFETCH(QString, pattern);
    QFETCH(QStringList, expectedGroupNames);

    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    /* Adde le gruppos */

    const QStringList allGroupNames = {
        "first", "group1", "group2", "last"
    };

    using Group = Core::Group;
    for (const QString &groupName: allGroupNames) {
        Group group {
            0, groupName, "A description",
            QDateTime::fromSecsSinceEpoch(123456),
        };
        int groupId = db.addGroup(group);
        QVERIFY(groupId >= 0);
    }

    /* Lege del base de datos */

    const auto groups = db.groups(pattern);
    QStringList groupNames;
    for (const Group &group: groups) {
        groupNames.append(group.name);
    }

    QCOMPARE(groupNames, expectedGroupNames);
    QVERIFY(m_capturedWarnings.isEmpty());
}

void DatabaseTest::testUsersForGroup()
{
    Core::Database db(m_configuration, "test");
    QVERIFY(db.createOrUpdateDb());

    const auto groupIds = prepareGroups(&db, {"g0", "g1", "g2", "g3"});
    const auto studentIds = prepareStudents(&db, {"s0", "s1", "s2", "s3", "s4"});
    const auto teacherIds = prepareTeachers(&db, {"t0", "t1", "t2", "t3", "t4"});
    const auto adminIds = prepareAdmins(&db, {"a0", "a1", "a2", "a3", "a4"});

    using Role = Core::User::Role;

    // Testa le addition

    QVERIFY(db.addUsersToGroup(groupIds[1], { studentIds[0], studentIds[4] },
                               Role::Student));
    QVERIFY(db.addUsersToGroup(groupIds[2], {
        studentIds[1], studentIds[2], studentIds[4],
    }, Role::Student));

    QVERIFY(db.addUsersToGroup(groupIds[0], { teacherIds[0], teacherIds[4] },
                               Role::Teacher));
    QVERIFY(db.addUsersToGroup(groupIds[2], {
        teacherIds[1], teacherIds[2], teacherIds[4],
    }, Role::Teacher));

    QVERIFY(db.addUsersToGroup(groupIds[1], { adminIds[0], adminIds[4] },
                               Role::Admin));
    QVERIFY(db.addUsersToGroup(groupIds[2], {
        adminIds[0], adminIds[2], adminIds[4],
    }, Role::Admin));

    // Testa le lectura: gruppos per usator

    using Groups = Core::Database::Groups;

    {
        Groups groups;
        QVERIFY(db.groupsForUser(studentIds[3], &groups, Role::Student));
        QVERIFY(groups.isEmpty());
    }

    {
        Groups groups;
        QVERIFY(db.groupsForUser(studentIds[0], &groups, Role::Student));
        QCOMPARE(groups.count(), 1);
        QCOMPARE(groups[0].id, groupIds[1]);
    }

    {
        Groups groups;
        QVERIFY(db.groupsForUser(studentIds[4], &groups, Role::Student));
        QCOMPARE(groups.count(), 2);
        QCOMPARE(groups[0].id, groupIds[1]);
        QCOMPARE(groups[1].id, groupIds[2]);
    }

    {
        Groups groups;
        QVERIFY(db.groupsForUser(teacherIds[4], &groups, Role::Teacher));
        QCOMPARE(groups.count(), 2);
        QCOMPARE(groups[0].id, groupIds[0]);
        QCOMPARE(groups[1].id, groupIds[2]);
    }

    {
        Groups groups;
        QVERIFY(db.groupsForUser(adminIds[4], &groups, Role::Admin));
        QCOMPARE(groups.count(), 2);
        QCOMPARE(groups[0].id, groupIds[1]);
        QCOMPARE(groups[1].id, groupIds[2]);
    }

    // Testa le lectura: studentes in gruppo

    using Users = Core::Database::Users;

    {
        Users users;
        QVERIFY(db.usersForGroup(groupIds[0], &users, Role::Student));
        QVERIFY(users.isEmpty());
    }

    {
        Users users;
        QVERIFY(db.usersForGroup(groupIds[1], &users, Role::Student));
        QCOMPARE(users.count(), 2);
        QCOMPARE(users[0].id, studentIds[0]);
        QCOMPARE(users[1].id, studentIds[4]);
    }

    {
        Users users;
        QVERIFY(db.usersForGroup(groupIds[2], &users, Role::Student));
        QCOMPARE(users.count(), 3);
        QCOMPARE(users[0].id, studentIds[1]);
        QCOMPARE(users[1].id, studentIds[2]);
        QCOMPARE(users[2].id, studentIds[4]);
    }

    // Remove alicun ligamines

    QVERIFY(db.removeUsersFromGroup(groupIds[2], {
        studentIds[1], studentIds[4],
    }, Role::Student));

    {
        Users users;
        QVERIFY(db.usersForGroup(groupIds[2], &users, Role::Student));
        QCOMPARE(users.count(), 1);
        QCOMPARE(users[0].id, studentIds[2]);
    }

    QVERIFY(db.removeUsersFromGroup(groupIds[2], {
        teacherIds[4], teacherIds[2],
    }, Role::Teacher));

    {
        Users users;
        QVERIFY(db.usersForGroup(groupIds[2], &users, Role::Teacher));
        QCOMPARE(users.count(), 1);
        QCOMPARE(users[0].id, teacherIds[1]);
    }

    QVERIFY(db.removeUsersFromGroup(groupIds[2], {
        adminIds[2], adminIds[0],
    }, Role::Admin));

    {
        Users users;
        QVERIFY(db.usersForGroup(groupIds[2], &users, Role::Admin));
        QCOMPARE(users.count(), 1);
        QCOMPARE(users[0].id, adminIds[4]);
    }

    // Remove un gruppo, verifica que le trigger functiona

    Core::Error error = db.deleteGroup(groupIds[2]);
    QVERIFY(!error);

    {
        Users users;
        QVERIFY(db.usersForGroup(groupIds[2], &users, Role::Student));
        QCOMPARE(users.count(), 0);
    }

    {
        Users users;
        QVERIFY(db.usersForGroup(groupIds[2], &users, Role::Teacher));
        QCOMPARE(users.count(), 0);
    }

    {
        Users users;
        QVERIFY(db.usersForGroup(groupIds[2], &users, Role::Admin));
        QCOMPARE(users.count(), 0);
    }

    // Remove un usator, verifica que le trigger functiona

    {
        QVERIFY(!db.deleteUser(studentIds[4]));

        Groups groups;
        QVERIFY(db.groupsForUser(studentIds[4], &groups, Role::Student));
        QCOMPARE(groups.count(), 0);

        Users users;
        QVERIFY(db.usersForGroup(groupIds[1], &users, Role::Student));
        QCOMPARE(users.count(), 1);
        QCOMPARE(users[0].id, studentIds[0]);
    }

    {
        QVERIFY(!db.deleteUser(teacherIds[4]));

        Groups groups;
        QVERIFY(db.groupsForUser(teacherIds[4], &groups, Role::Teacher));
        QCOMPARE(groups.count(), 0);

        Users users;
        QVERIFY(db.usersForGroup(groupIds[0], &users, Role::Teacher));
        QCOMPARE(users.count(), 1);
        QCOMPARE(users[0].id, teacherIds[0]);
    }

    {
        QVERIFY(!db.deleteUser(adminIds[4]));

        Groups groups;
        QVERIFY(db.groupsForUser(adminIds[4], &groups, Role::Admin));
        QCOMPARE(groups.count(), 0);

        Users users;
        QVERIFY(db.usersForGroup(groupIds[1], &users, Role::Admin));
        QCOMPARE(users.count(), 1);
        QCOMPARE(users[0].id, adminIds[0]);
    }
}
