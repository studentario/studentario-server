#include "mock_authenticator.h"

using namespace Core;

QByteArray Authenticator::generateSalt()
{
    return QByteArray("sal");
}

QByteArray Authenticator::passwordHash(const QByteArray &password,
                                       const QByteArray &salt)
{
    return "hash:" + password + salt;
}
