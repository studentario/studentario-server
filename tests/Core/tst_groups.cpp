#include "mock_database.h"
#include "groups.h"

#include <QCoreApplication>
#include <QJsonDocument>
#include <QScopedPointer>
#include <QTest>

Q_DECLARE_METATYPE(Core::Group)
Q_DECLARE_METATYPE(Core::Group::Fields)
Q_DECLARE_METATYPE(Core::User)
Q_DECLARE_METATYPE(Core::Error::Code)

namespace Core {

char *toString(const Group &group)
{
    return QTest::toString(
        "Group(" +
        QByteArray::number(group.id) + ", " +
        group.name.toUtf8() + ", " +
        group.description.toUtf8() + ", " +
        group.created.toString().toUtf8() + ", " +
        QByteArray::number(group.locationId) +
        ')');
}

} // namespace

namespace QTest {
template<>
char *toString(const QJsonObject &obj)
{
    QJsonDocument doc(obj);
    return QTest::toString(doc.toJson(QJsonDocument::Compact));
}

template<>
char *toString(const QJsonArray &a)
{
    QJsonDocument doc(a);
    return QTest::toString(doc.toJson(QJsonDocument::Compact));
}
} // namespace

class GroupsTest: public QObject
{
    Q_OBJECT
    using Group = Core::Group;
    using User = Core::User;

private Q_SLOTS:
    void testAddGroup_data();
    void testAddGroup();

    void testUpdateGroup_data();
    void testUpdateGroup();

    void testDeleteGroup();

    void testGroup_data();
    void testGroup();

    void testGroups();
    void testAddStudentsToGroup_data();
    void testAddStudentsToGroup();
    void testRemoveStudentsFromGroup_data();
    void testRemoveStudentsFromGroup();
    void testStudentsForGroup_data();
    void testStudentsForGroup();
    void testGroupsForStudent_data();
    void testGroupsForStudent();

    void testAddTeachersToGroup_data();
    void testAddTeachersToGroup();
    void testRemoveTeachersFromGroup_data();
    void testRemoveTeachersFromGroup();
    void testTeachersForGroup_data();
    void testTeachersForGroup();

    void testJsonFromDb_data();
    void testJsonFromDb();
};

void GroupsTest::testAddGroup_data()
{
    QTest::addColumn<QJsonObject>("jsonGroup");
    QTest::addColumn<bool>("mustSucceed");
    QTest::addColumn<Group>("expectedGroup");

    QTest::newRow("empty") <<
        QJsonObject {} << false << Group {};

    QTest::newRow("complete") <<
        QJsonObject {
            { "name", "Prime classe" },
            { "description", "Description del classe 1" },
            { "locationId", 12 },
        } <<
        true <<
        Group {
            0,
            "Prime classe",
            "Description del classe 1",
            QDateTime(),
            12,
        };
}

void GroupsTest::testAddGroup()
{
    QFETCH(QJsonObject, jsonGroup);
    QFETCH(bool, mustSucceed);
    QFETCH(Core::Group, expectedGroup);

    QDateTime startTestTime = QDateTime::currentDateTime();

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    Group storedGroup;
    dbMock->onAddGroupCalled([&](const Group &group) {
        storedGroup = group;
        return mustSucceed ? 1 : -1;
    });

    int groupId = -1;
    Core::Error error = groups.addGroup(jsonGroup, &groupId);
    bool succeeded = !error;
    QCOMPARE(succeeded, mustSucceed);

    if (mustSucceed) {
        QVERIFY(groupId >= 0);

        QDateTime endTestTime = QDateTime::currentDateTime();
        QVERIFY(storedGroup.created >= startTestTime);
        QVERIFY(storedGroup.created <= endTestTime);

        /* Nullifica le data, perque nos non vole comparar lo */
        storedGroup.created = QDateTime();
        QCOMPARE(storedGroup, expectedGroup);
    }
}

void GroupsTest::testUpdateGroup_data()
{
    using EC = Core::Error::Code;
    QTest::addColumn<QJsonObject>("changes");
    QTest::addColumn<EC>("dbResult");
    QTest::addColumn<Group>("expectedGroup");
    QTest::addColumn<Group::Fields>("expectedFields");
    QTest::addColumn<EC>("expectedResult");

    // This must be kept on sync with testUpdateGroup()
    const Group startingGroup {
        1234, "Name", "Description",
        QDateTime::fromSecsSinceEpoch(1234567),
    };

    QTest::newRow("empty") <<
        QJsonObject {} << EC::NoError <<
        startingGroup << Group::Fields() << EC::NoError;

    QTest::newRow("empty name") <<
        QJsonObject {
            { "name", "" },
            { "description", "Description del classe 1" },
        } << EC::NoError <<
        Group {} <<
        Group::Fields() <<
        EC::InvalidParameters;

    QTest::newRow("invalid param") <<
        QJsonObject {
            { "name", "Name" },
            { "missing", "This does not exist" },
        } << EC::NoError <<
        Group {} <<
        Group::Fields() <<
        EC::InvalidParameters;

    Group group = startingGroup;
    group.name = "Prime classe";
    group.description = "Description del classe 1";
    QTest::newRow("complete") <<
        QJsonObject {
            { "name", "Prime classe" },
            { "description", "Description del classe 1" },
        } <<
        EC::NoError <<
        group <<
        Group::Fields(Group::Field::Name |
                      Group::Field::Description) <<
        EC::NoError;

    group = startingGroup;
    group.description = "Description del classe 1";
    QTest::newRow("DB error") <<
        QJsonObject {
            { "description", "Description del classe 1" },
        } <<
        EC::GroupNotFound <<
        group <<
        Group::Fields(Group::Field::Description) <<
        EC::GroupNotFound;
}

void GroupsTest::testUpdateGroup()
{
    QFETCH(QJsonObject, changes);
    QFETCH(Core::Error::Code, dbResult);
    QFETCH(Group, expectedGroup);
    QFETCH(Group::Fields, expectedFields);
    QFETCH(Core::Error::Code, expectedResult);

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    Group updatedGroup = Group {};
    Group::Fields updatedFields;
    dbMock->onUpdateGroupCalled([&](const Group &group, Group::Fields fields) {
        updatedGroup = group;
        updatedFields = fields;
        return dbResult;
    });

    const Group group {
        1234, "Name", "Description",
        QDateTime::fromSecsSinceEpoch(1234567),
    };
    Core::Error error = groups.updateGroup(group, changes);
    QCOMPARE(error.code(), expectedResult);
    QCOMPARE(updatedGroup, expectedGroup);
    QCOMPARE(updatedFields, expectedFields);
}

void GroupsTest::testDeleteGroup()
{
    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    int deletedId = -1;
    Core::Error returnValue(Core::Error::DatabaseError);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    dbMock->onDeleteGroupCalled([&](int groupId) {
        deletedId = groupId;
        return returnValue;
    });

    const int groupId = 56;
    Core::Error error = groups.deleteGroup(groupId);
    QCOMPARE(deletedId, groupId);
    QCOMPARE(error, returnValue);
}

void GroupsTest::testGroup_data()
{
    QTest::addColumn<Group>("dbGroup");
    QTest::addColumn<bool>("dbResult");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    const Group baseGroup = {
        23, "Classe 2", "Description 2",
    };

    QTest::newRow("not found") <<
        Group {} << false << Core::Error::GroupNotFound;

    QTest::newRow("complete") <<
        baseGroup << true << Core::Error::NoError;
}

void GroupsTest::testGroup()
{
    QFETCH(Group, dbGroup);
    QFETCH(bool, dbResult);
    QFETCH(Core::Error::Code, expectedResult);

    const int groupId = 53;

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    int loadedGroupId = -1;
    dbMock->onGroupCalled([&](int id, Group *group) {
        loadedGroupId = id;
        *group = dbGroup;
        return dbResult;
    });

    Core::Group group;
    Core::Error error = groups.group(groupId, &group);
    QCOMPARE(loadedGroupId, groupId);

    QCOMPARE(group, dbGroup);
    QCOMPARE(error.code(), expectedResult);
}

void GroupsTest::testGroups()
{
    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    const QVector<Group> dbResult = {
        Group {
            1, "g1", "G1", QDateTime(),
        },
        Group {
            2, "g2", "G2", QDateTime(),
        },
    };
    QString pattern;
    dbMock->onGroupsCalled([&](const QString &requestedPattern) {
        pattern = requestedPattern;
        return dbResult;
    });

    const QString expectedPattern = "g";
    const QVector<Group> loadedGroups = groups.groups(expectedPattern);
    QCOMPARE(pattern, expectedPattern);

    /* Compara solmente le ID, perque nos ha altere tests pro verificar le
     * conversion */
    const QVector<int> expectedGroupIds = { 1, 2 };
    QVector<int> groupIds;
    for (const auto &u: loadedGroups) {
        groupIds.append(u.id);
    }
    QCOMPARE(groupIds, expectedGroupIds);
}

void GroupsTest::testAddStudentsToGroup_data()
{
    QTest::addColumn<int>("groupId");
    QTest::addColumn<QVector<int>>("studentIds");
    QTest::addColumn<bool>("dbResult");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    QTest::newRow("DB error") <<
        28 << QVector<int> {2, 4, 5} <<
        false << Core::Error::DatabaseError;

    QTest::newRow("OK") <<
        12 << QVector<int> {1, 7, 5} <<
        true << Core::Error::NoError;
}

void GroupsTest::testAddStudentsToGroup()
{
    QFETCH(int, groupId);
    QFETCH(QVector<int>, studentIds);
    QFETCH(bool, dbResult);
    QFETCH(Core::Error::Code, expectedResult);

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    int requestedGroupId = -1;
    using UserIds = QVector<int>;
    UserIds requestedStudentIds;
    User::Role requestedRole = User::Role::NoRole;
    dbMock->onAddUsersToGroupCalled([&](int id, const UserIds &studentIds,
                                        User::Role role) {
        requestedGroupId = id;
        requestedStudentIds = studentIds;
        requestedRole = role;
        return dbResult;
    });

    Core::Error error =
        groups.addUsersToGroup(groupId, studentIds, User::Role::Student);
    QCOMPARE(requestedGroupId, groupId);

    QCOMPARE(error.code(), expectedResult);
    QCOMPARE(requestedStudentIds, studentIds);
    QCOMPARE(requestedRole, User::Role::Student);
}

void GroupsTest::testRemoveStudentsFromGroup_data()
{
    QTest::addColumn<int>("groupId");
    QTest::addColumn<QVector<int>>("studentIds");
    QTest::addColumn<bool>("dbResult");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    QTest::newRow("DB error") <<
        28 << QVector<int> {2, 4, 5} <<
        false << Core::Error::DatabaseError;

    QTest::newRow("OK") <<
        12 << QVector<int> {1, 7, 5} <<
        true << Core::Error::NoError;
}

void GroupsTest::testRemoveStudentsFromGroup()
{
    QFETCH(int, groupId);
    QFETCH(QVector<int>, studentIds);
    QFETCH(bool, dbResult);
    QFETCH(Core::Error::Code, expectedResult);

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    int requestedGroupId = -1;
    using UserIds = QVector<int>;
    UserIds requestedStudentIds;
    User::Role requestedRole = User::Role::NoRole;
    dbMock->onRemoveUsersFromGroupCalled(
            [&](int id, const UserIds &studentIds, User::Role role) {
        requestedGroupId = id;
        requestedStudentIds = studentIds;
        requestedRole = role;
        return dbResult;
    });

    Core::Error error = groups.removeUsersFromGroup(groupId, studentIds,
                                                    User::Role::Student);
    QCOMPARE(requestedGroupId, groupId);

    QCOMPARE(error.code(), expectedResult);
    QCOMPARE(requestedStudentIds, studentIds);
    QCOMPARE(requestedRole, User::Role::Student);
}

void GroupsTest::testStudentsForGroup_data()
{
    QTest::addColumn<int>("groupId");
    QTest::addColumn<bool>("dbResult");
    QTest::addColumn<QVector<User>>("dbStudents");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    QTest::newRow("DB error") <<
        28 << false << QVector<User> {} << Core::Error::DatabaseError;

    Core::User::Roles roles = User::Role::Student;
    QTest::newRow("OK") <<
        83 << true <<
        QVector<User> {
            User { 3, roles, "Studente1", },
            User { 4, roles, "Studente2", },
        } << Core::Error::NoError;
}

void GroupsTest::testStudentsForGroup()
{
    QFETCH(int, groupId);
    QFETCH(bool, dbResult);
    QFETCH(QVector<User>, dbStudents);
    QFETCH(Core::Error::Code, expectedResult);

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    int requestedGroupId = -1;
    User::Role requestedRole = User::Role::NoRole;
    using Users = QVector<User>;
    Users returnedStudents;
    dbMock->onUsersForGroupCalled([&](int id, Users *students,
                                      User::Role role) {
        requestedGroupId = id;
        requestedRole = role;
        *students = dbStudents;
        return dbResult;
    });

    Core::Error error =
        groups.usersForGroup(groupId, &returnedStudents, User::Role::Student);
    QCOMPARE(requestedGroupId, groupId);
    QCOMPARE(requestedRole, User::Role::Student);

    QCOMPARE(error.code(), expectedResult);
    QCOMPARE(returnedStudents, dbStudents);
}

void GroupsTest::testGroupsForStudent_data()
{
    QTest::addColumn<int>("userId");
    QTest::addColumn<QString>("pattern");
    QTest::addColumn<QVector<Group>>("dbGroups");
    QTest::addColumn<bool>("dbResult");
    QTest::addColumn<QVector<int>>("expectedGroupIndexes");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    QTest::newRow("error") <<
        43 << QString() << QVector<Group> {} <<
        false << QVector<int> {} << Core::Error::DatabaseError;

    QTest::newRow("ok") <<
        23 << QString() <<
        QVector<Group> {
            { 1, "Group1", },
            { 3, "Group3", },
        } <<
        true << QVector<int> { 0, 1 } << Core::Error::NoError;

    QTest::newRow("with pattern") <<
        13 << QString("UN") <<
        QVector<Group> {
            { 1, "Gruppo Uno", },
            { 4, "Gruppo unic", },
            { 5, "Gruppo 1", },
            { 6, "un gruppo", },
        } <<
        true << QVector<int> { 0, 1, 3 } << Core::Error::NoError;
}

void GroupsTest::testGroupsForStudent()
{
    QFETCH(int, userId);
    QFETCH(QString, pattern);
    QFETCH(QVector<Group>, dbGroups);
    QFETCH(bool, dbResult);
    QFETCH(QVector<int>, expectedGroupIndexes);
    QFETCH(Core::Error::Code, expectedResult);

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    int queriedUserId = -1;
    User::Role queriedRole = User::Role::NoRole;
    dbMock->onGroupsForUserCalled([&](int id, QVector<Group> *groups,
                                      User::Role role) {
        queriedUserId = id;
        queriedRole = role;
        *groups = dbGroups;
        return dbResult;
    });

    QVector<Group> groupList;
    Core::Error error =
        groups.groupsForUser(userId, &groupList, User::Role::Student,
                             pattern);
    QCOMPARE(queriedUserId, userId);
    QCOMPARE(queriedRole, User::Role::Student);

    QVector<Group> expectedGroups;
    for (int i: expectedGroupIndexes) {
        expectedGroups.append(dbGroups[i]);
    }
    QCOMPARE(groupList, expectedGroups);
    QCOMPARE(error.code(), expectedResult);
}

void GroupsTest::testAddTeachersToGroup_data()
{
    QTest::addColumn<int>("groupId");
    QTest::addColumn<QVector<int>>("teacherIds");
    QTest::addColumn<bool>("dbResult");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    QTest::newRow("DB error") <<
        28 << QVector<int> {2, 4, 5} <<
        false << Core::Error::DatabaseError;

    QTest::newRow("OK") <<
        12 << QVector<int> {1, 7, 5} <<
        true << Core::Error::NoError;
}

void GroupsTest::testAddTeachersToGroup()
{
    QFETCH(int, groupId);
    QFETCH(QVector<int>, teacherIds);
    QFETCH(bool, dbResult);
    QFETCH(Core::Error::Code, expectedResult);

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    int requestedGroupId = -1;
    User::Role requestedRole = User::Role::NoRole;
    using UserIds = QVector<int>;
    UserIds requestedTeacherIds;
    dbMock->onAddUsersToGroupCalled([&](int id, const UserIds &teacherIds,
                                        User::Role role) {
        requestedGroupId = id;
        requestedTeacherIds = teacherIds;
        requestedRole = role;
        return dbResult;
    });

    Core::Error error =
        groups.addUsersToGroup(groupId, teacherIds, User::Role::Teacher);
    QCOMPARE(requestedGroupId, groupId);

    QCOMPARE(error.code(), expectedResult);
    QCOMPARE(requestedTeacherIds, teacherIds);
    QCOMPARE(requestedRole, User::Role::Teacher);
}

void GroupsTest::testRemoveTeachersFromGroup_data()
{
    QTest::addColumn<int>("groupId");
    QTest::addColumn<QVector<int>>("teacherIds");
    QTest::addColumn<bool>("dbResult");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    QTest::newRow("DB error") <<
        28 << QVector<int> {2, 4, 5} <<
        false << Core::Error::DatabaseError;

    QTest::newRow("OK") <<
        12 << QVector<int> {1, 7, 5} <<
        true << Core::Error::NoError;
}

void GroupsTest::testRemoveTeachersFromGroup()
{
    QFETCH(int, groupId);
    QFETCH(QVector<int>, teacherIds);
    QFETCH(bool, dbResult);
    QFETCH(Core::Error::Code, expectedResult);

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    int requestedGroupId = -1;
    using UserIds = QVector<int>;
    UserIds requestedTeacherIds;
    User::Role requestedRole = User::Role::NoRole;
    dbMock->onRemoveUsersFromGroupCalled(
            [&](int id, const UserIds &teacherIds, User::Role role) {
        requestedGroupId = id;
        requestedTeacherIds = teacherIds;
        requestedRole = role;
        return dbResult;
    });

    Core::Error error = groups.removeUsersFromGroup(groupId, teacherIds,
                                                    User::Role::Teacher);
    QCOMPARE(requestedGroupId, groupId);

    QCOMPARE(error.code(), expectedResult);
    QCOMPARE(requestedTeacherIds, teacherIds);
    QCOMPARE(requestedRole, User::Role::Teacher);
}

void GroupsTest::testTeachersForGroup_data()
{
    QTest::addColumn<int>("groupId");
    QTest::addColumn<bool>("dbResult");
    QTest::addColumn<QVector<User>>("dbTeachers");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    QTest::newRow("DB error") <<
        28 << false << QVector<User> {} << Core::Error::DatabaseError;

    Core::User::Roles roles = User::Role::Teacher;
    QTest::newRow("OK") <<
        83 << true <<
        QVector<User> {
            User { 3, roles, "Inseniante1", },
            User { 4, roles, "Inseniante2", },
        } << Core::Error::NoError;
}

void GroupsTest::testTeachersForGroup()
{
    QFETCH(int, groupId);
    QFETCH(bool, dbResult);
    QFETCH(QVector<User>, dbTeachers);
    QFETCH(Core::Error::Code, expectedResult);

    Core::Groups groups;
    Core::Database db(QVariantMap(), "");
    groups.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    int requestedGroupId = -1;
    User::Role requestedRole = User::Role::NoRole;
    using Users = QVector<User>;
    Users returnedTeachers;
    dbMock->onUsersForGroupCalled([&](int id, Users *teachers,
                                      User::Role role) {
        requestedGroupId = id;
        requestedRole = role;
        *teachers = dbTeachers;
        return dbResult;
    });

    Core::Error error = groups.usersForGroup(groupId, &returnedTeachers,
                                             User::Role::Teacher);
    QCOMPARE(requestedGroupId, groupId);
    QCOMPARE(requestedRole, User::Role::Teacher);

    QCOMPARE(error.code(), expectedResult);
    QCOMPARE(returnedTeachers, dbTeachers);
}

void GroupsTest::testJsonFromDb_data()
{
    QTest::addColumn<QVector<Group>>("groups");
    QTest::addColumn<QJsonArray>("expectedJson");

    QTest::newRow("empty") << QVector<Group> {} << QJsonArray {};

    QTest::newRow("two groups") <<
        QVector<Group> {
            { 4, "First", "Group 1", QDateTime::fromSecsSinceEpoch(12345), 6 },
            { 6, "Second", "Group 2", QDateTime::fromSecsSinceEpoch(54321) },
        } <<
        QJsonArray {
            QJsonObject {
                { "groupId", 4 },
                { "name", "First" },
                { "description", "Group 1" },
                { "created", "1970-01-01T06:25:45" },
                { "locationId", 6 },
            },
            QJsonObject {
                { "groupId", 6 },
                { "name", "Second" },
                { "description", "Group 2" },
                { "created", "1970-01-01T18:05:21" },
                { "locationId", -1 },
            }
        };

    QTest::newRow("no date") <<
        QVector<Group> {
            { 1, "Quando", "sin data", QDateTime(), 8 },
        } <<
        QJsonArray {
            QJsonObject {
                { "groupId", 1 },
                { "name", "Quando" },
                { "description", "sin data" },
                { "locationId", 8 },
            }
        };
}

void GroupsTest::testJsonFromDb()
{
    QFETCH(QVector<Group>, groups);
    QFETCH(QJsonArray, expectedJson);

    QJsonArray json = Core::Groups::jsonFromDb(groups);
    QCOMPARE(json, expectedJson);
}

QTEST_GUILESS_MAIN(GroupsTest)

#include "tst_groups.moc"
