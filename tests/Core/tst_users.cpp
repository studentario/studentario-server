#include "mock_database.h"
#include "users.h"

#include <QCoreApplication>
#include <QJsonDocument>
#include <QScopedPointer>
#include <QTest>

Q_DECLARE_METATYPE(Core::Group)
Q_DECLARE_METATYPE(Core::User)
Q_DECLARE_METATYPE(Core::User::Fields)
Q_DECLARE_METATYPE(Core::Error::Code)

namespace Core {

char *toString(const User &user)
{
    return QTest::toString(
        "User(" +
        QByteArray::number(user.id) + ", " +
        QByteArray::number(user.roles, 16) + ", " +
        user.login.toUtf8() + ", " +
        user.password + '-' + user.salt + '-' + user.pinCode + ", " +
        user.pinCodeCreation.toString().toUtf8() + ", " +
        user.birthDate.toString().toUtf8() + ", " +
        user.keywords.toUtf8() + ", " +
        user.contactInfo.join(';') + ", " +
        user.created.toString().toUtf8());
}

} // namespace

namespace QTest {
template<>
char *toString(const QJsonObject &obj)
{
    QJsonDocument doc(obj);
    return QTest::toString(doc.toJson(QJsonDocument::Compact));
}
} // namespace

class UsersTest: public QObject
{
    Q_OBJECT
    using Group = Core::Group;
    using User = Core::User;

private Q_SLOTS:
    void testAddUser_data();
    void testAddUser();
    void testAddLoginlessUser_data();
    void testAddLoginlessUser();

    void testUpdatePin_data();
    void testUpdatePin();

    void testUpdateUser_data();
    void testUpdateUser();

    void testDeleteUser();

    void testUser_data();
    void testUser();

    void testUsers();
};

void UsersTest::testAddUser_data()
{
    using Role = Core::Users::Role;
    using Roles = Core::Users::Roles;

    QTest::addColumn<QJsonObject>("jsonUser");
    QTest::addColumn<bool>("mustSucceed");
    QTest::addColumn<User>("expectedUser");

    QTest::newRow("empty") <<
        QJsonObject {} << false << User {};

    QTest::newRow("student, complete") <<
        QJsonObject {
            { "isStudent", true },
            { "login", "usator" },
            { "password", "pwd" },
            { "name", "Arthuro" },
            { "birthDate", "1999-12-13" },
            { "keywords", "nulle parola clave" },
            { "contactInfo", QJsonArray {"uno", "duo"} },
        } <<
        true <<
        User {
            0, Roles(Role::Student),
            /* le contrasigno ha essite substituite per su hash (que in le mock
             * del Authenticator es le concatenation de password e salt) */
            "usator", "hash:pwdsal", "sal", "",
            QDateTime(),
            "Arthuro", QDate(1999, 12, 13), "nulle parola clave",
            { "uno", "duo" },
            QDateTime(),
        };

    QTest::newRow("mixed roles") <<
        QJsonObject {
            { "isStudent", false },
            { "isTeacher", true },
            { "isAdmin", true },
            { "login", "admin_e_maestro" },
            { "password", "secreto" },
        } <<
        true <<
        User {
            0, Role::Teacher | Role::Admin,
            "admin_e_maestro", "hash:secretosal", "sal", "",
            QDateTime(),
            "", QDate(), "", {},
            QDateTime(),
        };
}

void UsersTest::testAddUser()
{
    QFETCH(QJsonObject, jsonUser);
    QFETCH(bool, mustSucceed);
    QFETCH(Core::User, expectedUser);

    QDateTime startTestTime = QDateTime::currentDateTime();

    Core::Users users;
    Core::Database db(QVariantMap(), "");
    users.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    User storedUser;
    dbMock->onAddUserCalled([&](const User &user) {
        storedUser = user;
        return mustSucceed ? 1 : -1;
    });

    int userId = -1;
    Core::Error error = users.addUser(jsonUser, &userId);
    bool succeeded = !error;
    QCOMPARE(succeeded, mustSucceed);

    if (mustSucceed) {
        QVERIFY(userId >= 0);

        QDateTime endTestTime = QDateTime::currentDateTime();
        QVERIFY(storedUser.created >= startTestTime);
        QVERIFY(storedUser.created <= endTestTime);

        /* Nullifica le data, perque nos non vole comparar lo */
        storedUser.created = QDateTime();
        QCOMPARE(storedUser, expectedUser);
    }
}

void UsersTest::testAddLoginlessUser_data()
{
    QTest::addColumn<int>("addUserFailures");
    QTest::addColumn<int>("expectedCallCount");
    QTest::addColumn<QByteArray>("expectedLogin");

    QTest::newRow("no failures") <<
        0 << 1 << QByteArray("auto_user_4"); // 4 = 1 * 3 + 1

    QTest::newRow("some failures") <<
        5 << 6 << QByteArray("auto_user_19"); // 19 = 5 * 3 + 1

    QTest::newRow("too many failures") <<
        10 << 10 << QByteArray();
}

void UsersTest::testAddLoginlessUser()
{
    QFETCH(int, addUserFailures);
    QFETCH(int, expectedCallCount);
    QFETCH(QByteArray, expectedLogin);

    QJsonObject jsonUser {
        { "isStudent", true },
        { "name", "Arthuro" },
        { "keywords", "nulle parola clave" },
        { "contactInfo", QJsonArray {"uno", "duo"} },
    };

    Core::Users users;
    Core::Database db(QVariantMap(), "");
    users.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    User storedUser;
    int addUserCalledCount = 0;
    dbMock->onAddUserCalled([&](const User &user) {
        storedUser = user;
        addUserCalledCount++;
        return addUserCalledCount > addUserFailures ? 1 : -1;
    });

    /* Defini le maxUserId como le numero de vices que iste methodo es
     * invocate, multiplicate per tres. */
    int maxUserCalledCount = 0;
    dbMock->onMaxUserIdCalled([&]() {
        maxUserCalledCount++;
        return maxUserCalledCount * 3;
    });

    int userId = -1;
    Core::Error error = users.addLoginlessUser(jsonUser, &userId);
    bool succeeded = !error;
    QCOMPARE(succeeded, !expectedLogin.isEmpty());

    QCOMPARE(addUserCalledCount, expectedCallCount);
    QCOMPARE(maxUserCalledCount, expectedCallCount);
    if (succeeded) {
        QCOMPARE(storedUser.login, expectedLogin);
    }
}

void UsersTest::testUpdatePin_data()
{
    QTest::addColumn<int>("userId");
    QTest::addColumn<QByteArray>("pin");
    QTest::addColumn<bool>("dbSuccess");
    QTest::addColumn<bool>("expectedSuccess");

    QTest::newRow("empty") << 23 << QByteArray("") << false << false;
    QTest::newRow("short") << 54 << QByteArray("12") << true << false;
    QTest::newRow("ok") << 12 << QByteArray("1234") << true << true;
}

void UsersTest::testUpdatePin()
{
    QFETCH(int, userId);
    QFETCH(QByteArray, pin);
    QFETCH(bool, dbSuccess);
    QFETCH(bool, expectedSuccess);

    QDateTime startTestTime = QDateTime::currentDateTime();

    Core::Users users;
    Core::Database db(QVariantMap(), "");
    users.setDatabase(&db);

    User updatedUser;
    User::Fields updatedFields;
    auto dbMock = Core::DatabaseMock::mockFor(&db);
    dbMock->onUpdateUserCalled([&](const User &user,
                                   const User::Fields &fields) {
        updatedUser = user;
        updatedFields = fields;
        return dbSuccess;
    });

    Core::Error error = users.updatePin(userId, pin);
    bool succeeded = !error;
    QDateTime endTestTime = QDateTime::currentDateTime();

    QCOMPARE(succeeded, expectedSuccess);

    if (succeeded) {
        QVERIFY(updatedUser.pinCodeCreation >= startTestTime);
        QVERIFY(updatedUser.pinCodeCreation <= endTestTime);

        QCOMPARE(updatedUser.pinCode, pin);
    }
}

void UsersTest::testUpdateUser_data()
{
    QTest::addColumn<Core::User>("initialUser");
    QTest::addColumn<QJsonObject>("changes");
    QTest::addColumn<bool>("dbSuccess");
    QTest::addColumn<Core::User>("expectedUser");
    QTest::addColumn<Core::User::Fields>("expectedFields");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    using User = Core::User;
    using Error = Core::Error;

    const User baseUser = {
        14, User::Roles(User::Role::Student),
        "usator", "hash:pwdsal", "sal", "1234",
        QDateTime(),
        "Arthuro", QDate(2000, 12, 31), "nulle parola clave",
        { "uno", "duo" },
        QDateTime(),
    };

    User initialUser = baseUser;
    User expectedUser = baseUser;

    expectedUser.name = "Dominico";
    QTest::newRow("name") << initialUser <<
        QJsonObject {
            { "name", "Dominico" },
        } <<
        true <<
        expectedUser <<
        User::Fields(User::Field::Name) <<
        Error::NoError;

    expectedUser = User {};
    QTest::newRow("empty login") << initialUser <<
        QJsonObject {
            { "login", "" },
        } <<
        true <<
        expectedUser <<
        User::Fields() <<
        Error::InvalidParameters;

    expectedUser = baseUser;
    expectedUser.login = "arthuro1";
    QTest::newRow("valid login") << initialUser <<
        QJsonObject {
            { "login", "arthuro1" },
        } <<
        true <<
        expectedUser <<
        User::Fields(User::Field::Login) <<
        Error::NoError;

    expectedUser = User {};
    QTest::newRow("empty password") << initialUser <<
        QJsonObject {
            { "password", "" },
        } <<
        true <<
        expectedUser <<
        User::Fields() <<
        Error::InvalidParameters;

    expectedUser = baseUser;
    expectedUser.password = "hash:passsal";
    QTest::newRow("valid password") << initialUser <<
        QJsonObject {
            { "password", "pass" },
        } <<
        true <<
        expectedUser <<
        User::Fields(User::Field::Password) <<
        Error::NoError;

    expectedUser = baseUser;
    expectedUser.birthDate = QDate(1999, 11, 30);
    QTest::newRow("new birthdate") << initialUser <<
        QJsonObject {
            { "birthDate", "1999-11-30" },
        } <<
        true <<
        expectedUser <<
        User::Fields(User::Field::BirthDate) <<
        Error::NoError;

    expectedUser = baseUser;
    expectedUser.keywords = "alicun cosa importante";
    QTest::newRow("DB error") << initialUser <<
        QJsonObject {
            { "keywords", "alicun cosa importante" },
        } <<
        false <<
        expectedUser <<
        User::Fields(User::Field::Keywords) <<
        Error::DatabaseError;
}

void UsersTest::testUpdateUser()
{
    QFETCH(Core::User, initialUser);
    QFETCH(QJsonObject, changes);
    QFETCH(bool, dbSuccess);
    QFETCH(Core::User, expectedUser);
    QFETCH(Core::User::Fields, expectedFields);
    QFETCH(Core::Error::Code, expectedResult);

    using User = Core::User;

    Core::Users users;
    Core::Database db(QVariantMap(), "");
    users.setDatabase(&db);

    User updatedUser = {};
    User::Fields updatedFields;
    auto dbMock = Core::DatabaseMock::mockFor(&db);
    dbMock->onUpdateUserCalled([&](const User &user,
                                   const User::Fields &fields) {
        updatedUser = user;
        updatedFields = fields;
        return dbSuccess;
    });

    Core::Error error = users.updateUser(initialUser, changes);
    QCOMPARE(error.code(), expectedResult);
    QCOMPARE(updatedUser, expectedUser);
    QCOMPARE(updatedFields, expectedFields);
}

void UsersTest::testDeleteUser()
{
    Core::Users users;
    Core::Database db(QVariantMap(), "");
    users.setDatabase(&db);

    int deletedId = -1;
    Core::Error returnValue(Core::Error::DatabaseError);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    dbMock->onDeleteUserCalled([&](int userId) {
        deletedId = userId;
        return returnValue;
    });

    const int userId = 56;
    Core::Error error = users.deleteUser(userId);
    QCOMPARE(deletedId, userId);
    QCOMPARE(error, returnValue);
}

void UsersTest::testUser_data()
{
    using Roles = Core::Users::Roles;

    QTest::addColumn<User>("dbUser");
    QTest::addColumn<bool>("dbResult");
    QTest::addColumn<Core::Error::Code>("expectedResult");

    const User baseUser = {
        23, Roles(),
    };

    QTest::newRow("not found") <<
        User {} << false << Core::Error::UserNotFound;

    QTest::newRow("student, complete") <<
        baseUser << true << Core::Error::NoError;
}

void UsersTest::testUser()
{
    QFETCH(User, dbUser);
    QFETCH(bool, dbResult);
    QFETCH(Core::Error::Code, expectedResult);

    const int userId = 53;

    Core::Users users;
    Core::Database db(QVariantMap(), "");
    users.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    int loadedUserId = -1;
    dbMock->onUserCalled([&](int id, User *user) {
        loadedUserId = id;
        *user = dbUser;
        return dbResult;
    });

    Core::User user;
    Core::Error error = users.user(userId, &user);
    QCOMPARE(loadedUserId, userId);

    QCOMPARE(user, dbUser);
    QCOMPARE(error.code(), expectedResult);
}

void UsersTest::testUsers()
{
    using Role = Core::Users::Role;

    Core::Users users;
    Core::Database db(QVariantMap(), "");
    users.setDatabase(&db);

    auto dbMock = Core::DatabaseMock::mockFor(&db);
    Core::User::Roles roles;
    const QVector<User> dbResult = {
        User {
            1, Role::Director | Role::Teacher,
            "u1", "", "", "", QDateTime(), "", QDate(), "", {}, QDateTime(),
        },
        User {
            2, Role::Director | Role::Teacher,
            "u2", "", "", "", QDateTime(), "", QDate(), "", {}, QDateTime(),
        },
    };
    dbMock->onUsersCalled([&](const User::Filters &filters,
                              User::Sort sort) {
        roles = filters.requestedRoles;
        return dbResult;
    });

    const Core::User::Roles expectedRoles = Role::Director;
    Core::User::Filters filters;
    filters.requestedRoles = expectedRoles;
    const QVector<User> loadedUsers = users.users(filters);
    QCOMPARE(roles, expectedRoles);

    /* Compara solmente le ID, perque nos ha altere tests pro verificar le
     * conversion */
    const QVector<int> expectedUserIds = { 1, 2 };
    QVector<int> userIds;
    for (const auto &u: loadedUsers) {
        userIds.append(u.id);
    }
    QCOMPARE(userIds, expectedUserIds);
}

QTEST_GUILESS_MAIN(UsersTest)

#include "tst_users.moc"
